
#=======================================
#  Storable Class /config/ebs_taxcode
#=======================================

STORABLE CLASS /config/ebs_taxcode {

	READ_ACCESS = "Self";
	WRITE_ACCESS = "Self";
	DESCR = "eBS Cloud friendly singleton to manage configurable taxcodes for any tax engine or user defined";
	IS_PARTITIONED = "0";

	#===================
	#  Fields 
	#===================

	ARRAY PIN_FLD_TAXES {

		DESCR = "hold the custom tax data";
		ORDER = 0;
		AUDITABLE = 0;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;

		#===================
		#  Fields 
		#===================

		STRING PIN_FLD_DESCR {

			DESCR = "Name or description of the tax. Example, ""VAT""";
			ORDER = 0;
			LENGTH = 100;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		TIMESTAMP PIN_FLD_END_T {

			DESCR = "End of validity date for the tax.\n     Format is ""mm/dd/yy"".  Example, ""02/27/03""";
			ORDER = 0;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		STRING PIN_FLD_NAME {

			DESCR = "List of valid names for the jurisdiction, separated by \n    ! semicolon ("";""). This would be the ""nexus"" for the TYPE.\n    ! Example:\n    !   1. If TYPE is ""Federal"", then list could be:\n    !        ""US; CA; GB; FR""\n    !   2. If TYPE is ""State"", then list could be:\n    !        ""CA; NY; TX""\n    ! Use ""*"" to denote all values in the domain";
			ORDER = 0;
			LENGTH = 60;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		DECIMAL PIN_FLD_PERCENT {

			DESCR = "The tax rate for the product (in percentage, e.g. 0.50 = 50%)";
			ORDER = 0;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		TIMESTAMP PIN_FLD_START_T {

			DESCR = "Start of validity date for the tax.\n    Format is ""mm/dd/yy"".  Example, ""02/27/03""";
			ORDER = 0;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		ENUM PIN_FLD_TAXPKG_TYPE {

			DESCR = "Tax Package: \n                   T    Taxware (Sales/Use and WorldTax)\n                   Q    Vertex Quantum (sales and use tax)\n                   C    Vertex CommTax21 (telco tax)\n                   U    Custom (user defined) tax tables\n\n";
			ORDER = 0;
			CREATE = Required;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		STRING PIN_FLD_TAX_CODE {

			DESCR = "identifies product to be taxed";
			ORDER = 0;
			LENGTH = 60;
			CREATE = Required;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		ENUM PIN_FLD_TAX_WHEN {

			DESCR = "Tax rule:\n  ""Std""- Standard Tax Taxes are computed based on the taxable amount, and added to the total\n   ""Tax""- Tax on Tax Taxes are computed based on previous taxable amounts and taxes, and added to the total \n   ""Fee""- Tax amount Use specified amount (in the rate column) as the tax to be added to the total Similar to a adding a fee or surcharge regardless of the amount to be taxed\n   ""Inc""- Inclusive tax The amount already has the tax included. The taxable amount and tax amount are broken up from the gross amount, and the tax is added to the total \n   ""NCS""- Non Cumulative Taxes are computed base on the taxable amount, but are not added to the total\n   ""NCT""- Non Cumulative Tax on Tax Taxes are computed based on previous taxable amounts and taxes, but are not added to the total \n   ""NCF""- Non Cumulative Fee  The tax amount will not be added to the total\n   ""NCI""- Non Cumulative Inclusive The taxable amount and tax amount are broken up from the gross amount, but the tax will not be added to the total";
			ORDER = 0;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		ENUM PIN_FLD_TYPE {

			DESCR = "Type of jurisdiction (e.g. Fed, State, County, etc.)\n    ! Valid values are:\n    !    ""Federal""   --> PIN_RATE_TAX_JUR_FED\n    !    ""State""     --> PIN_RATE_TAX_JUR_STATE\n    !    ""County""    --> PIN_RATE_TAX_JUR_COUNTY \n *    !    ""City""      --> PIN_RATE_TAX_JUR_CITY";
			ORDER = 0;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

	}

}


#=======================================
#  Storable Class /config/ebs_taxcode
#=======================================

STORABLE CLASS /config/ebs_taxcode IMPLEMENTATION ORACLE7 {


	#===================
	#  Fields 
	#===================

	ARRAY PIN_FLD_TAXES {

		SQL_TABLE = "ebs_config_taxcode_t";

		#===================
		#  Fields 
		#===================

		STRING PIN_FLD_DESCR {

			SQL_COLUMN = "descr";
		}

		TIMESTAMP PIN_FLD_END_T {

			SQL_COLUMN = "end_t";
		}

		STRING PIN_FLD_NAME {

			SQL_COLUMN = "name";
		}

		DECIMAL PIN_FLD_PERCENT {

			SQL_COLUMN = "percent";
		}

		TIMESTAMP PIN_FLD_START_T {

			SQL_COLUMN = "start_t";
		}

		ENUM PIN_FLD_TAXPKG_TYPE {

			SQL_COLUMN = "pkg_type";
		}

		STRING PIN_FLD_TAX_CODE {

			SQL_COLUMN = "tax_code";
		}

		ENUM PIN_FLD_TAX_WHEN {

			SQL_COLUMN = "tax_when";
		}

		ENUM PIN_FLD_TYPE {

			SQL_COLUMN = "type";
		}

	}

}

