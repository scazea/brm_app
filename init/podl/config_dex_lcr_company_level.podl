
#=======================================
#  Field PIN_FLD_RATES
#=======================================

ARRAY PIN_FLD_RATES {

	ID = 230;
}


#=======================================
#  Field DEX_FLD_ACCOUNT_SUBTYPE
#=======================================

STRING DEX_FLD_ACCOUNT_SUBTYPE {

	ID = 10005;
	DESCR = "DexYP Account Subtype";
}


#=======================================
#  Field DEX_FLD_ACCOUNT_TYPE
#=======================================

STRING DEX_FLD_ACCOUNT_TYPE {

	ID = 10004;
	DESCR = "DexYP Account Type = Local | National | Internal";
}


#=======================================
#  Field DEX_FLD_FINANCE_ID
#=======================================

STRING DEX_FLD_FINANCE_ID {

	ID = 10000;
	DESCR = "DexYP L1 Finance ID";
}


#=======================================
#  Field PIN_FLD_ACTION_TYPE
#=======================================

STRING PIN_FLD_ACTION_TYPE {

	ID = 7704;
}


#=======================================
#  Field PIN_FLD_BRAND_NAME
#=======================================

STRING PIN_FLD_BRAND_NAME {

	ID = 1832;
}


#=======================================
#  Field PIN_FLD_BUSINESS_PARTNER
#=======================================

STRING PIN_FLD_BUSINESS_PARTNER {

	ID = 9391;
}


#=======================================
#  Field PIN_FLD_FIXED_AMOUNT
#=======================================

DECIMAL PIN_FLD_FIXED_AMOUNT {

	ID = 2220;
}


#=======================================
#  Field PIN_FLD_PARAM_NAME
#=======================================

STRING PIN_FLD_PARAM_NAME {

	ID = 5776;
}


#=======================================
#  Field PIN_FLD_PARAM_VALUE
#=======================================

STRING PIN_FLD_PARAM_VALUE {

	ID = 5778;
}


#=======================================
#  Field PIN_FLD_PERCENT
#=======================================

DECIMAL PIN_FLD_PERCENT {

	ID = 531;
}


#=======================================
#  Field PIN_FLD_VALID_FROM
#=======================================

TIMESTAMP PIN_FLD_VALID_FROM {

	ID = 6429;
}


#=======================================
#  Field PIN_FLD_VALID_TO
#=======================================

TIMESTAMP PIN_FLD_VALID_TO {

	ID = 6430;
}


#=======================================
#  Storable Class /config/dex_lcr_company_level
#=======================================

STORABLE CLASS /config/dex_lcr_company_level {

	READ_ACCESS = "Self";
	WRITE_ACCESS = "Self";
	DESCR = "Config table to hold company level information to calculate late charge rates.";
	IS_PARTITIONED = "0";

	#===================
	#  Fields 
	#===================

	ARRAY PIN_FLD_RATES {

		DESCR = "Array of company parameters";
		ORDER = 0;
		AUDITABLE = 0;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;

		#===================
		#  Fields 
		#===================

		STRING DEX_FLD_ACCOUNT_SUBTYPE {

			DESCR = "DexYP Account Subtype";
			ORDER = 0;
			LENGTH = 60;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		STRING DEX_FLD_ACCOUNT_TYPE {

			DESCR = "DexYP Account Type = Local | National | Internal";
			ORDER = 0;
			LENGTH = 60;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		STRING DEX_FLD_FINANCE_ID {

			DESCR = "DexYP AR region";
			ORDER = 0;
			LENGTH = 60;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		STRING PIN_FLD_ACTION_TYPE {

			DESCR = "Parameter Type";
			ORDER = 0;
			LENGTH = 60;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		STRING PIN_FLD_BRAND_NAME {

			DESCR = "Brand Area";
			ORDER = 0;
			LENGTH = 60;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		STRING PIN_FLD_BUSINESS_PARTNER {

			DESCR = "Business Entity";
			ORDER = 0;
			LENGTH = 60;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		DECIMAL PIN_FLD_FIXED_AMOUNT {

			DESCR = "Parameter value fixed amount";
			ORDER = 0;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		STRING PIN_FLD_PARAM_NAME {

			DESCR = "Parameter Name";
			ORDER = 0;
			LENGTH = 60;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		STRING PIN_FLD_PARAM_VALUE {

			DESCR = "Parameter value in characters/string";
			ORDER = 0;
			LENGTH = 60;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		DECIMAL PIN_FLD_PERCENT {

			DESCR = "Parameter value percentage";
			ORDER = 0;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		TIMESTAMP PIN_FLD_VALID_FROM {

			DESCR = "Effective from date";
			ORDER = 0;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

		TIMESTAMP PIN_FLD_VALID_TO {

			DESCR = "Effective to date";
			ORDER = 0;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}

	}

}


#=======================================
#  Storable Class /config/dex_lcr_company_level
#=======================================

STORABLE CLASS /config/dex_lcr_company_level IMPLEMENTATION ORACLE7 {


	#===================
	#  Fields 
	#===================

	ARRAY PIN_FLD_RATES {

		SQL_TABLE = "cfg_dex_lcr_company_level_t";

		#===================
		#  Fields 
		#===================

		STRING DEX_FLD_ACCOUNT_SUBTYPE {

			SQL_COLUMN = "account_subtype";
		}

		STRING DEX_FLD_ACCOUNT_TYPE {

			SQL_COLUMN = "account_type";
		}

		STRING DEX_FLD_FINANCE_ID {

			SQL_COLUMN = "ar_region";
		}

		STRING PIN_FLD_ACTION_TYPE {

			SQL_COLUMN = "param_type";
		}

		STRING PIN_FLD_BRAND_NAME {

			SQL_COLUMN = "brand_area";
		}

		STRING PIN_FLD_BUSINESS_PARTNER {

			SQL_COLUMN = "business_entity";
		}

		DECIMAL PIN_FLD_FIXED_AMOUNT {

			SQL_COLUMN = "param_value_amt";
		}

		STRING PIN_FLD_PARAM_NAME {

			SQL_COLUMN = "param_name";
		}

		STRING PIN_FLD_PARAM_VALUE {

			SQL_COLUMN = "param_value_char";
		}

		DECIMAL PIN_FLD_PERCENT {

			SQL_COLUMN = "param_value_pct";
		}

		TIMESTAMP PIN_FLD_VALID_FROM {

			SQL_COLUMN = "effective_from";
		}

		TIMESTAMP PIN_FLD_VALID_TO {

			SQL_COLUMN = "effective_to";
		}

	}

}

