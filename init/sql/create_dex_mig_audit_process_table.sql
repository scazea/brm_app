DECLARE
   FCNT INTEGER;
BEGIN
   SELECT COUNT(*) INTO FCNT FROM USER_TABLES WHERE TABLE_NAME = 'DEX_MIG_AUDIT_PROCESS_T';

IF(FCNT = 1) THEN
   DBMS_OUTPUT.PUT_LINE('TABLE DEX_MIG_AUDIT_PROCESS_T EXISTS. SKIPPING CREATE FOR THIS TABLE');
ELSE
   EXECUTE IMMEDIATE '
     create table DEX_MIG_AUDIT_PROCESS_T
        (program_type VARCHAR2(60),
        program_name VARCHAR2(60),
        data_file_directory VARCHAR2(255),
        source_file_name VARCHAR2(60),
        table_name VARCHAR2(60),
        total_records NUMBER,
        success_records NUMBER,
        failed_records NUMBER,
        log_directory VARCHAR2(255),
        log_filename VARCHAR2(60),
        error_directory VARCHAR2(255),
        error_file_name VARCHAR2(60),
        host VARCHAR2(60),
        user_name VARCHAR2(60),
        start_date DATE,
        end_date DATE,
        processing_time VARCHAR2(60),
        process_status VARCHAR2(60),
        batch_num NUMBER
        )';
   END IF;
END;
/
