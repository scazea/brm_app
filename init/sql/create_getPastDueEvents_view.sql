Create or Replace view getPastDueEvents (billing_account_id, bill_id, bill_no, bill_start, bill_end, ar_item_code, datetime, event_type, event_id, event_no, description, sys_desc, gross_amount,disc_amount,net_amount,tax_amount,total_amount,client_id) as
	select acct.account_no, billinfo.bill_info_id, bill.bill_no , unix2date(bill.start_t,'GMT'), unix2date(bill.end_t,'GMT'), item.item_no, unix2date(event.end_t,'GMT'),'charge',event.poid_id0, event.event_no ,event.descr, event.sys_descr, 
	(select SUM(eb.amount) from event_bal_impacts_t eb where eb.obj_id0 = event.poid_id0 and eb.resource_id < 999 and eb.impact_type != 4 group by eb.obj_id0),
	(select SUM(eb.discount) from event_bal_impacts_t eb where eb.obj_id0 = event.poid_id0 and eb.resource_id < 999 and eb.impact_type != 4 group by eb.obj_id0),
	(select SUM(eb.amount) from event_bal_impacts_t eb where eb.obj_id0 = event.poid_id0 and eb.resource_id < 999 and eb.impact_type != 4 group by eb.obj_id0) - (select SUM(eb.discount) from event_bal_impacts_t eb where eb.obj_id0 = event.poid_id0 and eb.resource_id < 999 and eb.impact_type != 4 group by eb.obj_id0), 
	(select SUM(eb.amount) from event_bal_impacts_t eb where eb.obj_id0 = event.poid_id0 and eb.resource_id < 999 and eb.impact_type = 4 group by eb.obj_id0),
	(select SUM(eb.amount) from event_bal_impacts_t eb where eb.obj_id0 = event.poid_id0 and eb.resource_id < 999 and eb.impact_type != 4 group by eb.obj_id0) - (select SUM(eb.discount) from event_bal_impacts_t eb where eb.obj_id0 = event.poid_id0 and eb.resource_id < 999 and eb.impact_type != 4 group by eb.obj_id0) + (select SUM(eb.amount) from event_bal_impacts_t eb where eb.obj_id0 = event.poid_id0 and eb.resource_id < 999 and eb.impact_type = 4 group by eb.obj_id0),
	acct.account_no
	from account_t acct, bill_t bill, billinfo_t billinfo, event_bal_impacts_t eventbal , event_t event, item_t item
	where acct.poid_id0 = bill.account_obj_id0 
	and bill.billinfo_obj_id0 = billinfo.poid_id0
	and billinfo.bal_grp_obj_id0 = eventbal.bal_grp_obj_id0
	and event.poid_id0 = eventbal.obj_id0
	and eventbal.item_obj_id0 = item.poid_id0
	and item.due > 0 
	--and item.due_t < ((SYSDATE - TO_DATE('01-01-1970 00:00:00', 'DD-MM-YYYY HH24:MI:SS')) * 24 * 60 * 60) and item.due_t != 0
	order by bill.start_t

commit;
quit;
/

