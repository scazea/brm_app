WHENEVER SQLERROR EXIT SQL.SQLCODE

create or replace FUNCTION unix2date(unixtimestamp_in IN NUMBER, 
        tz_in IN VARCHAR2  DEFAULT 'EST')
    RETURN DATE IS
    l_date DATE;
BEGIN

    SELECT CAST(from_tz(CAST(to_date('19700101000000'
                                    ,'YYYYMMDDHH24MISS') +
                             numtodsinterval(unixtimestamp_in
                                            ,'SECOND') AS TIMESTAMP)
                       ,'GMT') at TIME ZONE (tz_in) AS DATE)
      INTO l_date
      FROM dual;

    RETURN l_date;
END unix2date;
/
show errors;

