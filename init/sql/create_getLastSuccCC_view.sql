Create or Replace view getLastSuccCC (billing_account_id,transaction_id, masked_number, payment_method) as
	select a.account_no, eb.trans_id, e.descr, substr(e.poid_type,24,length(e.poid_type))
	from account_t a, event_t e, event_billing_payment_t eb
	where e.poid_id0 = eb.obj_id0
	and a.poid_id0 = e.account_obj_id0

commit;
quit;
/
