--------------------------------------------------------
--  File created - Tuesday-January-24-2017   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package BBOX_DASH
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "BBOX_DASH" AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
  PROCEDURE GET_REVMIX(
    I_DATE IN VARCHAR2,
    I_MAX_LENGTH IN INT,
    O_RESOURCE_ID_LIST OUT VARCHAR2,
    RESULTS OUT SYS_REFCURSOR);

  PROCEDURE GET_AR_AGING(
    I_MAX_LENGTH IN INT,
    O_RESOURCE_ID_LIST OUT VARCHAR2,
    RESULTS OUT SYS_REFCURSOR);
    
  PROCEDURE GET_ACTIVE_CUSTOMERS(
      I_DATE IN VARCHAR2,
    RESULTS OUT SYS_REFCURSOR);
    
  PROCEDURE GET_INACTIVE_CUSTOMERS(
      I_DATE IN VARCHAR2,
    RESULTS OUT SYS_REFCURSOR);
    
  PROCEDURE GET_PAYMENTS(
    I_DATE IN VARCHAR2,
    RESULTS OUT SYS_REFCURSOR);
    
END BBOX_DASH;

/
--------------------------------------------------------
--  DDL for Package Body BBOX_DASH
--------------------------------------------------------
create or replace PACKAGE BODY "BBOX_DASH" AS

-- This one gets the Revenue Mix of top $$$ products, with cutoff date from I_DATE
--    and summary top "I_MAX_LENGTH" products lookup JSON record
PROCEDURE GET_REVMIX(
    I_DATE IN VARCHAR2,
    I_MAX_LENGTH IN INT,
    O_RESOURCE_ID_LIST OUT VARCHAR2,
    RESULTS OUT SYS_REFCURSOR) AS
BEGIN
-- Get the summary JSON lookup reference. Json is embedded into O_RESOURCE_ID_LIST
  SELECT '{"PRODUCT": [' || (LISTAGG('{"UNIQUE_ID": ' || PID || ', "PRODUCT_NAME": "' || p.NAME || '", "TOTAL": ' || SUMA || '}', ',') WITHIN GROUP (ORDER BY SUMA DESC)) || ']}' as RESOURCE_ID_LIST
  INTO O_RESOURCE_ID_LIST
  FROM
    (SELECT PID, SUMA
    FROM
      (SELECT ep.PRODUCT_OBJ_ID0 PID, ROUND(SUM(eb.AMOUNT), 2) SUMA
      FROM EVENT_T e
      JOIN EVENT_BAL_IMPACTS_T eb ON e.POID_ID0 = eb.OBJ_ID0
      JOIN EVENT_BILLING_PRODUCT_T ep ON e.POID_ID0 = ep.OBJ_ID0
      WHERE eb.RESOURCE_ID  < 999
      AND e.end_t           > date2unix(I_DATE)
      AND eb.RATE_TAG <> 'Tax'
      GROUP BY ep.PRODUCT_OBJ_ID0
      ORDER BY SUM(eb.amount) DESC
      ) A
    WHERE rownum <= I_MAX_LENGTH
    ) toper
    JOIN PRODUCT_T p ON toper.PID = p.POID_ID0;

-- Return the result of detail records. NOTE: This is summariezed by GROUP BY.
--    Needs to be fast because Dashboard cannot wait
  OPEN RESULTS FOR
    select ROUND(SUM(eb.AMOUNT), 2) AS VALUE, 
      an.COUNTRY, an.STATE, 
      DATE2UNIX(TRUNC( unix2date(e.end_t, '0:00'), 'DAY') + 6, '0:00')*1000 AS VALID_TO_STR, 
      ep.PRODUCT_OBJ_ID0 AS UNIQUE_ID
    FROM account_nameinfo_t an
    JOIN event_t e ON an.OBJ_ID0 = e.ACCOUNT_OBJ_ID0
    JOIN EVENT_BAL_IMPACTS_T eb ON e.POID_ID0 = eb.OBJ_ID0
    JOIN EVENT_BILLING_PRODUCT_T ep ON e.POID_ID0 = ep.OBJ_ID0
    WHERE an.REC_ID = 1
    AND eb.RESOURCE_ID < 999
    AND e.end_t           > date2unix(I_DATE, '0:00')
    AND eb.RATE_TAG <> 'Tax'
    GROUP BY an.STATE, an.COUNTRY, an.STATE, ep.PRODUCT_OBJ_ID0, TRUNC( unix2date(e.end_t, '0:00'), 'DAY') + 6
    ORDER BY TRUNC( unix2date(e.end_t, '0:00'), 'DAY') + 6 asc, an.COUNTRY, an.STATE;
END GET_REVMIX;


-- This one gets the AR Aging with top $$$ products, summary top "I_MAX_LENGTH"
--    products lookup JSON record
PROCEDURE GET_AR_AGING(
    I_MAX_LENGTH IN INT,
    O_RESOURCE_ID_LIST OUT VARCHAR2,
    RESULTS OUT SYS_REFCURSOR) AS
BEGIN
-- Get the summary JSON lookup reference. Json is embedded into O_RESOURCE_ID_LIST
  SELECT '{"PRODUCT": [' || (LISTAGG('{"UNIQUE_ID": ' || PID || ', "PRODUCT_NAME": "' || p.NAME || '"}', ',') WITHIN GROUP (ORDER BY SUMA DESC)) || ']}' as RESOURCE_ID_LIST
  INTO O_RESOURCE_ID_LIST
  FROM
    (SELECT PID, SUMA
    FROM
      (SELECT ep.PRODUCT_OBJ_ID0 PID, ROUND(SUM(eb.AMOUNT), 2) SUMA
      FROM EVENT_BAL_IMPACTS_T eb
      JOIN EVENT_BILLING_PRODUCT_T ep ON eb.OBJ_ID0 = ep.OBJ_ID0
      JOIN ITEM_T i ON eb.ITEM_OBJ_ID0 = i.POID_ID0 and i.STATUS = 2
      WHERE eb.RESOURCE_ID  < 999
      GROUP BY ep.PRODUCT_OBJ_ID0
      ORDER BY SUM(eb.amount) DESC
      ) A
    WHERE rownum <= I_MAX_LENGTH
    ) toper
    JOIN PRODUCT_T p ON toper.PID = p.POID_ID0;

-- Return the result of detail records. NOTE: This is summariezed by GROUP BY.
--    Needs to be fast because Dashboard cannot wait
  OPEN RESULTS FOR
    SELECT ROUND(SUM(dues.due * evts.TOTAL/dues.ITEM_TOTAL)) AS VALUE,
      an.COUNTRY, an.STATE,
      dues.BUCKET AS CATEGORY,
      evts.PRODUCT_OBJ_ID0 AS UNIQUE_ID
    FROM ACCOUNT_NAMEINFO_T an
    JOIN
      (SELECT b.account_obj_id0,i.POID_ID0, i.DUE, i.item_total, unix2date(i.DUE_T) due_on,
        CASE
          WHEN (SYSDATE-unix2date(i.DUE_T)) < 30
          THEN '0-30'
          WHEN (SYSDATE-unix2date(i.DUE_T)) < 60
          THEN '30-60'
          WHEN (SYSDATE-unix2date(i.DUE_T)) < 90
          THEN '60-90'
          ELSE '90+'
        END AS BUCKET
      FROM item_t i
      JOIN bill_t b ON i.ar_bill_obj_id0 = b.poid_id0
      WHERE i.STATUS = 2
      ) dues ON an.obj_id0 = dues.account_obj_id0
    JOIN
      (SELECT eb.item_obj_id0, eb.PRODUCT_OBJ_ID0, SUM(eb.amount) TOTAL
      FROM EVENT_BAL_IMPACTS_T eb
      JOIN ITEM_T i ON eb.item_obj_id0   = i.poid_id0 AND i.status = 2
      WHERE eb.RESOURCE_ID < 999
      GROUP BY eb.item_obj_id0, eb.PRODUCT_OBJ_ID0) evts ON dues.poid_id0 = evts.item_obj_id0
    WHERE an.REC_ID = 1
    GROUP BY an.COUNTRY, an.STATE, evts.PRODUCT_OBJ_ID0, dues.BUCKET
    ORDER BY dues.BUCKET;
END GET_AR_AGING;


-- This one gets the breakdown of payments, with cutoff date from I_DATE
PROCEDURE GET_PAYMENTS(
    I_DATE IN VARCHAR2,
    RESULTS OUT SYS_REFCURSOR) AS
BEGIN
  OPEN RESULTS FOR
  SELECT ROUND(SUM(ep.AMOUNT), 2) AS VALUE,
    an.COUNTRY,
    UPPER(SUBSTR(e.POID_TYPE, 24)) AS CATEGORY,
    an.STATE,
    DATE2UNIX(TRUNC( unix2date(e.END_T, '0:00'), 'DAY') + 6, '0:00')*1000 AS VALID_TO_STR
  FROM event_t e
  JOIN event_billing_payment_t ep ON e.POID_ID0   = ep.OBJ_ID0
  JOIN ACCOUNT_NAMEINFO_T an ON e.ACCOUNT_OBJ_ID0 = an.OBJ_ID0
  WHERE ep.STATUS = 0
  AND e.END_T >= date2unix(I_DATE, '0:00')
  GROUP BY an.COUNTRY, an.STATE,
        UPPER(SUBSTR(e.POID_TYPE, 24)),
        TRUNC( unix2date(e.END_T, '0:00'), 'DAY') + 6
  ORDER BY TRUNC( unix2date(e.END_T, '0:00'), 'DAY') + 6;
END GET_PAYMENTS;

-- This one gets the total count of activated cusotmers, with cutoff date from I_DATE
PROCEDURE GET_ACTIVE_CUSTOMERS(
    I_DATE IN VARCHAR2,
    RESULTS OUT SYS_REFCURSOR) AS
BEGIN
  OPEN RESULTS FOR
    SELECT an.COUNTRY, an.STATE,
      COUNT(A.POID_ID0) AS COUNTER,
      DATE2UNIX(TRUNC( unix2date(a.LAST_STATUS_T, '0:00'), 'DAY') + 6, '0:00')*1000 AS VALID_FROM_STR
    FROM account_t a
    JOIN ACCOUNT_NAMEINFO_T an ON a.POID_ID0   = an.OBJ_ID0 AND an.COUNTRY IS NOT NULL
    WHERE an.REC_ID = 1
      AND A.STATUS  = 10100
      AND A.LAST_STATUS_T >= date2unix(I_DATE, '0:00')
    GROUP BY an.COUNTRY, an.STATE,
      TRUNC( unix2date(a.LAST_STATUS_T, '0:00'), 'DAY')        + 6
    ORDER BY TRUNC( unix2date(a.LAST_STATUS_T, '0:00'), 'DAY') + 6;
END GET_ACTIVE_CUSTOMERS;


-- This one gets the total count of inactivated cusotmers, with cutoff date from I_DATE
PROCEDURE GET_INACTIVE_CUSTOMERS(
    I_DATE IN VARCHAR2,
    RESULTS OUT SYS_REFCURSOR) AS
BEGIN
  OPEN RESULTS FOR
    SELECT an.COUNTRY, an.STATE,
      COUNT(A.POID_ID0) AS COUNTER,
      DATE2UNIX(TRUNC( unix2date(a.LAST_STATUS_T, '0:00'), 'DAY') + 6, '0:00')*1000 AS VALID_FROM_STR
    FROM account_t a
    JOIN ACCOUNT_NAMEINFO_T an ON a.POID_ID0   = an.OBJ_ID0 AND an.COUNTRY IS NOT NULL
    WHERE an.REC_ID = 1
      AND A.STATUS  > 10100
      AND A.LAST_STATUS_T >= date2unix(I_DATE, '0:00')
    GROUP BY an.COUNTRY, an.STATE,
      TRUNC( unix2date(a.LAST_STATUS_T, '0:00'), 'DAY')        + 6
    ORDER BY TRUNC( unix2date(a.LAST_STATUS_T, '0:00'), 'DAY') + 6;
END GET_INACTIVE_CUSTOMERS;

END BBOX_DASH;

