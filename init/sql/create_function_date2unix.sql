WHENEVER SQLERROR EXIT SQL.SQLCODE

create or replace FUNCTION date2unix(date_in IN DATE,
          tz_in IN VARCHAR2  DEFAULT 'EST') RETURN NUMBER IS
    --
    l_unixtimestamp NUMBER;
    ex EXCEPTION;
    PRAGMA EXCEPTION_INIT(ex,-01878);
BEGIN
    l_unixtimestamp := 0;

    SELECT (CAST(from_tz(CAST(date_in AS TIMESTAMP)
                        , tz_in) at TIME ZONE 'GMT' AS DATE) -
           to_date('1970-01-01','YYYY-MM-DD')) * 86400
      INTO l_unixtimestamp
      FROM dual;

    RETURN round(l_unixtimestamp);

EXCEPTION
    WHEN ex THEN
        SELECT (CAST(from_tz(CAST(current_date AS TIMESTAMP), tz_in) at TIME ZONE 'GMT' AS DATE) - to_date('1970-01-01','YYYY-MM-DD')) * 86400
          INTO l_unixtimestamp
          FROM dual;

        RETURN round(l_unixtimestamp);
END date2unix;
/
show errors;

