Create or Replace view getStorecECPCC (order_id, account_no, pay_type, name, address, city, state, zip, country, debit_num, debit_exp, routing_no) as
    select pi.name, a.account_no, 'CC',pcc.name, pcc.address, pcc.city, pcc.state, pcc.zip, pcc.country, pcc.debit_num, pcc.debit_exp, ''
    from account_t a, payinfo_t pi, payinfo_cc_t pcc
    where  a.poid_id0 = pi.account_obj_id0 
    and pi.poid_id0 = pcc.obj_id0
    union
    select pi.name, a.account_no, 'DD' , pdd.name, pdd.address, pdd.city, pdd.state, pdd.zip, pdd.country, pdd.debit_num, '' ,pdd.bank_no
    from account_t a, payinfo_t pi, payinfo_dd_t pdd
    where  a.poid_id0 = pi.account_obj_id0 
    and pi.poid_id0 = pdd.obj_id0;
commit;
quit;
/

