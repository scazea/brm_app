<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID>0.0.0.1 /ledger_report -1</POID>
	<NAME><xsl:value-of select="$PROGRAM_NAME"/></NAME>
	<START_T><xsl:value-of select="action/params/@start_t"/></START_T>
	<END_T><xsl:value-of select="action/params/@end_t"/></END_T>
	<TYPE><xsl:value-of select="action/params/@type"/></TYPE>
	<GL_REPORT_TYPE><xsl:value-of select="action/params/@gl_report_type"/></GL_REPORT_TYPE>
	<CURRENCY_ONLY><xsl:value-of select="action/params/@currency_only"/></CURRENCY_ONLY>

</flist>
</xsl:template>

</xsl:stylesheet>
