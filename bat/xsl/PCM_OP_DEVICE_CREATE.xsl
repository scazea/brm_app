<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID>0.0.0.1 /device 1</POID>
	<PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
	<DEVICE_ID><xsl:value-of select="action/params/@device_id"/></DEVICE_ID>
	<STATE_ID><xsl:value-of select="action/params/@state_id"/></STATE_ID>

</flist>
</xsl:template>

</xsl:stylesheet>
