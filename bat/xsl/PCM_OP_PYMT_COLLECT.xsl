<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
	<xsl:choose>
		<xsl:when test="action/params/@descr">
			<DESCR><xsl:value-of select="action/params/@descr"/></DESCR>
		</xsl:when>
		<xsl:otherwise>
			<DESCR>"Payment Batch:: Number of Payments in Batch:1"</DESCR>
		</xsl:otherwise>
	</xsl:choose>

	<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
	<PROGRAM_NAME>${PROGRAM_NAME}</PROGRAM_NAME>

	<xsl:choose>
		<xsl:when test="action/params/@pay_type = 'cc'">
			<BATCH_INFO>
				<CURRENCY>840</CURRENCY>
				<BATCH_TOTAL><xsl:value-of select="action/params/@amount"/></BATCH_TOTAL>
				<BATCH_ID/>
				<SUBMITTER_ID>root.0.0.0.1</SUBMITTER_ID>
			</BATCH_INFO>
			<CHARGES>
				<CURRENCY>840</CURRENCY>
				<BILLINFO_OBJ><xsl:value-of select="action/params/@billinfo_obj"/></BILLINFO_OBJ>
				<ACTG_TYPE>2</ACTG_TYPE>
				<ACCOUNT_OBJ><xsl:value-of select="$ACCOUNT_OBJ"/></ACCOUNT_OBJ>
				<SELECT_RESULT>0</SELECT_RESULT>
				<SELECT_STATUS>1</SELECT_STATUS>
				<COMMAND>0</COMMAND>
				<PAY_TYPE><xsl:value-of select="action/params/@pay_type"/></PAY_TYPE>
				<PAYMENT>
					<DESCR/>
					<TOPUP_RESOURCE_INFO>
						<BAL_IMPACTS>
							<RESOURCE_ID>840</RESOURCE_ID>
							<AMOUNT><xsl:value-of select="action/params/@amount"/></AMOUNT>
							<BAL_GRP_OBJ><xsl:value-of select="action/params/@bal_grp_obj"/></BAL_GRP_OBJ>
						</BAL_IMPACTS>
					</TOPUP_RESOURCE_INFO>
				</PAYMENT>
			</CHARGES>
		</xsl:when>
		<xsl:when test="action/params/@pay_type = 'check'">
			<CHARGES>
				<AMOUNT><xsl:value-of select="action/params/@amount"/></AMOUNT>
				<ACCOUNT_OBJ><xsl:value-of select="$ACCOUNT_OBJ"/></ACCOUNT_OBJ>
				<COMMAND>0</COMMAND>
				<PAY_TYPE>${PAY_TYPE_CHECK}</PAY_TYPE>
				<CURRENCY>${SYSTEM_CURRENCY}</CURRENCY>
				<PAYMENT>
					<DESCR>${PROGRAM_NAME}</DESCR>
					<INHERITED_INFO>
						<CHECK_INFO>
							<CHECK_NO/>
							<BANK_CODE/>
							<BANK_ACCOUNT_NO/>
						</CHECK_INFO>
					</INHERITED_INFO>
				</PAYMENT>
			</CHARGES>
		</xsl:when>
	</xsl:choose>

</flist>
</xsl:template>

</xsl:stylesheet>
