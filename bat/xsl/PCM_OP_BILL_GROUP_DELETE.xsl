<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<xsl:choose>
		<xsl:when test="action/params/@account_obj_billing_group">
			<POID><xsl:value-of select="action/params/@account_obj_billing_group"/></POID>
		</xsl:when>
		<xsl:otherwise>
			<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
		</xsl:otherwise>
	</xsl:choose>
	<GROUP_OBJ><xsl:value-of select="action/params/@group_poid"/></GROUP_OBJ>
</flist>
</xsl:template>

</xsl:stylesheet>
