<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<xsl:choose>
		<xsl:when test="action/params/@account_obj">
			<POID><xsl:value-of select="action/params/@account_obj"/></POID>
		</xsl:when>
		<xsl:otherwise>
			<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
		</xsl:otherwise>
	</xsl:choose>
	<BILLINFO_OBJ><xsl:value-of select="action/params/@billinfo_obj"/></BILLINFO_OBJ>
	<ACTG_LAST_T><xsl:value-of select="action/params/@actg_last_t"/></ACTG_LAST_T>
	<END_T><xsl:value-of select="action/params/@end_date"/></END_T>
</flist>
</xsl:template>

</xsl:stylesheet>
