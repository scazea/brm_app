<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="common.xsl"/>
<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
	<SERVICE_OBJ><xsl:value-of select="action/params/@service_obj"/></SERVICE_OBJ>
	<PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
	<DEAL_INFO>
		<NAME><xsl:value-of select="action/params/@name"/></NAME>
		<POID>
			<bat_function name="findPoidByName">
				<arg>/deal</arg>
				<arg><xsl:value-of select="action/params/@name"/></arg>
			</bat_function>
		</POID>
		<START_T>0</START_T>
		<END_T>0</END_T>
		<FLAGS>0</FLAGS>
		<xsl:for-each select="action/params/product">
			<PRODUCTS>
				<PRODUCT_OBJ>
					<bat_function name="findPoidByName">
						<arg>/product</arg>
						<arg><xsl:value-of select="@name"/></arg>
					</bat_function>
				</PRODUCT_OBJ>
				<QUANTITY>1</QUANTITY>
				<STATUS>1</STATUS>
			</PRODUCTS>
		</xsl:for-each>
	</DEAL_INFO>
</flist>
</xsl:template>

</xsl:stylesheet>
