<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<xsl:choose>
		<xsl:when test="action/params/@account_obj">
			<POID><xsl:value-of select="action/params/@account_obj"/></POID>
		</xsl:when>
		<xsl:otherwise>
			<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
		</xsl:otherwise>
	</xsl:choose>
	<BILLINFO_OBJ><xsl:value-of select="action/params/@billinfo_obj"/></BILLINFO_OBJ>
	<xsl:choose>
		<xsl:when test="action/params/@program_name">
			<PROGRAM_NAME><xsl:value-of select="action/params/@program_name"/></PROGRAM_NAME>
		</xsl:when>
		<xsl:otherwise>
			<PROGRAM_NAME>${PROGRAM_NAME}</PROGRAM_NAME>
		</xsl:otherwise>
	</xsl:choose>
	<AMOUNT><xsl:value-of select="action/params/@amount"/></AMOUNT>
</flist>
</xsl:template>

</xsl:stylesheet>
