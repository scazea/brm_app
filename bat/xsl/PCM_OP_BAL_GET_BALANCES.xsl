<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID><xsl:value-of select="action/params/@account_obj"/></POID>
	<BALANCES><CURRENT_BAL>0</CURRENT_BAL></BALANCES>
</flist>
</xsl:template>

</xsl:stylesheet>
