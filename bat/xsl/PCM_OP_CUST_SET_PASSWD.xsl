<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
<xsl:include href="utils/variables.xsl"/>
<xsl:include href="common.xsl"/>
<xsl:template match="/">

<flist>
	<POID><xsl:value-of select="action/params/@account_obj"/></POID>
	<PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
	<PIN_FLD_PASSWORDS>
		<PASSWD_CLEAR><xsl:value-of select="action/params/@passwd"/></PASSWD_CLEAR>
	</PIN_FLD_PASSWORDS>
	<SERVICE_OBJ><xsl:value-of select="action/params/@service_obj"/></SERVICE_OBJ>
</flist>

</xsl:template>
</xsl:stylesheet>
