<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="common.xsl"/>
<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID>0.0.0.1 /account 1</POID>
	<REGULATED_FLAG>0</REGULATED_FLAG>
	<TAXES>
		<SHIP_FROM><xsl:value-of select="action/params/@ship_from"/></SHIP_FROM>
		<SHIP_TO><xsl:value-of select="action/params/@ship_to"/></SHIP_TO>
		<ORDER_ACCEPT><xsl:value-of select="action/params/@order_accept"/></ORDER_ACCEPT>
		<ORDER_ORIGIN><xsl:value-of select="action/params/@order_origin"/></ORDER_ORIGIN>
		<AMOUNT_TAXED><xsl:value-of select="action/params/@amount_taxed"/></AMOUNT_TAXED>
		<LOCATION_MODE><xsl:value-of select="action/params/@location_mode"/></LOCATION_MODE>
		<TAX_CODE><xsl:value-of select="action/params/@tax_code"/></TAX_CODE>
	</TAXES>
</flist>
</xsl:template>

</xsl:stylesheet>
