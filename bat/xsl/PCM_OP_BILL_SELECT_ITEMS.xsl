<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
	<CURRENCY>840</CURRENCY>
	<BILLINFO>
		<BILLINFO_OBJ><xsl:value-of select="action/params/@billinfo_obj"/></BILLINFO_OBJ>
			<BILLS>
				<AMOUNT><xsl:value-of select="action/params/@amount"/></AMOUNT>
				<BILL_OBJ><xsl:value-of select="action/params/@bill_obj"/></BILL_OBJ>
				<CURRENCY>840</CURRENCY>
			</BILLS>
		</BILLINFO>
	<AMOUNT><xsl:value-of select="action/params/@amount"/></AMOUNT>
	<INCLUDE_CHILDREN>1</INCLUDE_CHILDREN>
</flist>
</xsl:template>

</xsl:stylesheet>
