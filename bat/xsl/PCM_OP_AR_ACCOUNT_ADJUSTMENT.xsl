<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
	<AMOUNT><xsl:value-of select="action/params/@amount"/></AMOUNT>
	<PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
	<DESCR>Bunit Test</DESCR>
	<CURRENCY>840</CURRENCY>
	<xsl:if test="action/params/@version">
		<STR_VERSION><xsl:value-of select="action/params/@version"/></STR_VERSION>
	</xsl:if>
	<xsl:if test="action/params/@reason">
		<STRING_ID><xsl:value-of select="action/params/@reason"/></STRING_ID>
	</xsl:if>
</flist>
</xsl:template>

</xsl:stylesheet>
