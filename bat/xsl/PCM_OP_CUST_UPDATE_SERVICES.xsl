<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="common.xsl"/>
<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
	<DESCR><xsl:value-of select="action/params/@descr"/></DESCR>
	<PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
	<SERVICES>
		<POID><xsl:value-of select="action/params/@service_obj"/></POID>
		<STATUS_FLAGS><xsl:value-of select="action/params/@status_flags"/></STATUS_FLAGS>
		<STATUS><xsl:value-of select="action/params/@status"/></STATUS>
	</SERVICES>
</flist>
</xsl:template>

</xsl:stylesheet>
