<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID>0.0.0.1 /account 1</POID>
	<PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
	<xsl:choose>
		<xsl:when test="action/params/@descr">
			<DESCR><xsl:value-of select="action/params/@descr"/></DESCR>
		</xsl:when>
		<xsl:otherwise>
			<DESCR>"Payment Batch:: Number of Payments in Batch:1"</DESCR>
		</xsl:otherwise>
	</xsl:choose>
	<BATCH_INFO>
		<CURRENCY>840</CURRENCY>
		<BATCH_TOTAL><xsl:value-of select="action/params/@amount"/></BATCH_TOTAL>
		<BATCH_ID/>
		<SUBMITTER_ID>root.0.0.0.1</SUBMITTER_ID>
	</BATCH_INFO>
	<CHARGES>
		<CURRENCY>840</CURRENCY>
		<BILLINFO_OBJ><xsl:value-of select="action/params/@billinfo_obj"/></BILLINFO_OBJ>
		<ACTG_TYPE>2</ACTG_TYPE>
		<ACCOUNT_OBJ><xsl:value-of select="$ACCOUNT_OBJ"/></ACCOUNT_OBJ>
		<BILLS>
			<BILL_NO><xsl:value-of select="action/params/@bill_no"/></BILL_NO>
		</BILLS>
		<AMOUNT><xsl:value-of select="action/params/@amount"/></AMOUNT>
		<SELECT_RESULT>0</SELECT_RESULT>
		<SELECT_STATUS>1</SELECT_STATUS>
		<COMMAND>0</COMMAND>
		<ACCOUNT_NO><xsl:value-of select="action/params/@account_no"/></ACCOUNT_NO>
		<PAY_TYPE><xsl:value-of select="action/params/@pay_type"/></PAY_TYPE>
		<PAYMENT>
			<DESCR/>
			<INHERITED_INFO>
        			<CASH_INFO>
					<EFFECTIVE_T><xsl:value-of select="action/params/@effective_t"/></EFFECTIVE_T>
            				<RECEIPT_NO/>
        			</CASH_INFO>
			</INHERITED_INFO>
		</PAYMENT>

	</CHARGES>

</flist>
</xsl:template>

</xsl:stylesheet>
