<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:template match="/">
<flist>
	<POID>0.0.0.1 /search -1</POID>
	<FLAGS>256</FLAGS>
	<!--TEMPLATE/-->
	<xsl:if test="not(//*/flist/RESULTS/@elem)">
		<RESULTS elem="0"/>
	</xsl:if>
</flist>
</xsl:template>

</xsl:stylesheet>
