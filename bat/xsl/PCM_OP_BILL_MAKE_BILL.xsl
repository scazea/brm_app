<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="common.xsl"/>
<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID>
	<xsl:choose>
	<xsl:when test="action/params/@account_obj"><xsl:value-of select="action/params/@account_obj"/></xsl:when>
	<xsl:otherwise><xsl:value-of select="$ACCOUNT_OBJ"/></xsl:otherwise>
	</xsl:choose>
	</POID>

	<BILLINFO_OBJ><xsl:value-of select="action/params/@billinfo_obj"/></BILLINFO_OBJ>

	<PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
</flist>
</xsl:template>

</xsl:stylesheet>
