<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
	<PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
	<INHERITED_INFO>
		<TOPUP_INFO>
			<BILLINFO_OBJ>${BILLINFO_OBJ}</BILLINFO_OBJ>
			<BAL_GRP_OBJ>${BAL_GRP_OBJ}</BAL_GRP_OBJ>
			<TOPUP_AMT><xsl:value-of select="action/params/@amount"/></TOPUP_AMT>
			<PAYINFO>
				<ACCOUNT_OBJ><xsl:value-of select="$ACCOUNT_OBJ"/></ACCOUNT_OBJ>
				<POID>0.0.0.1 /payinfo/cc -1</POID>
				<PAY_TYPE>10003</PAY_TYPE>
				<CC_INFO>
					<SECURITY_ID/>
					<NAME>test</NAME>
					<DEBIT_EXP>0120</DEBIT_EXP>
					<DEBIT_NUM>4111111111111111</DEBIT_NUM>
					<ADDRESS>Test</ADDRESS>
					<CITY>Test</CITY>
					<STATE>CA</STATE>
					<ZIP>12345</ZIP>
					<COUNTRY>USA</COUNTRY>
				</CC_INFO>
			</PAYINFO>
		</TOPUP_INFO>
	</INHERITED_INFO>
</flist>
</xsl:template>

</xsl:stylesheet>
