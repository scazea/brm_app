<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<xsl:choose>
		<xsl:when test="action/params/@account_obj">
			<POID><xsl:value-of select="action/params/@account_obj"/></POID>
		</xsl:when>
		<xsl:otherwise>
			<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:choose>
		<xsl:when test="action/params/@parent">
			<PARENT><xsl:value-of select="action/params/@parent"/></PARENT>
		</xsl:when>
		<xsl:otherwise>
			<PARENT><xsl:value-of select="$ACCOUNT_OBJ"/></PARENT>
		</xsl:otherwise>
	</xsl:choose>
	<NAME><xsl:value-of select="action/params/@group_name"/></NAME>
</flist>
</xsl:template>

</xsl:stylesheet>
