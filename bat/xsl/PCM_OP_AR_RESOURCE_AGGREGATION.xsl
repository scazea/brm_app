<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
	<EVENTS>
		<POID><xsl:value-of select="action/params/@event_cycle_forward"/></POID>
	</EVENTS>

</flist>
</xsl:template>

</xsl:stylesheet>
