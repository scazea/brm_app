<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:variable name="ACCOUNT_OBJ">
	<xsl:choose>
		<xsl:when test="/params/@account_obj"><xsl:value-of select="/params/@account_obj"/></xsl:when>
		<xsl:otherwise>${ACCOUNT_OBJ}</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="PROGRAM_NAME" select="'${PROGRAM_NAME}'"/>
<xsl:variable name="PROCESSED_FROM" select="'${PROCESSED_FROM}'"/>
<xsl:variable name="PROFILE_OBJ" select="'${PROFILE_OBJ}'"/>

</xsl:stylesheet>
