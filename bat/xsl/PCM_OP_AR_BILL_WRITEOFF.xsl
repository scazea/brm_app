<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
			<POID><xsl:value-of select="action/params/@bill_obj"/></POID>
	<xsl:choose>
		<xsl:when test="action/params/@program_name">
			<PROGRAM_NAME><xsl:value-of select="action/params/@program_name"/></PROGRAM_NAME>
		</xsl:when>
		<xsl:otherwise>
			<PROGRAM_NAME>${PROGRAM_NAME}</PROGRAM_NAME>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:choose>
		<xsl:when test="action/params/@flags">
			<FLAGS><xsl:value-of select="action/params/@flags"/></FLAGS>
		</xsl:when>
		<xsl:otherwise>
			<FLAGS>1</FLAGS>
		</xsl:otherwise>
	</xsl:choose>
	<DESCR><xsl:value-of select="action/params/@descr"/></DESCR>
</flist>
</xsl:template>

</xsl:stylesheet>
