<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID>0.0.0.1 0 -1</POID>
	<NAMEINFO elem="2">
		<FIRST_NAME>Customer</FIRST_NAME>
		<LAST_NAME>Subscriber</LAST_NAME>
		<CONTACT_TYPE>SERVICE</CONTACT_TYPE>
		<ADDRESS>900 SW 1st Ave</ADDRESS>
		<CITY>Miami</CITY>
		<STATE>FL</STATE>
		<ZIP>33130</ZIP>
		<COUNTRY>USA</COUNTRY>
		<EMAIL_ADDR>subscriber@somewhere.com</EMAIL_ADDR>
		<TITLE>Mr</TITLE>
		<COMPANY>EBillSoft</COMPANY>
		<PHONES>
			<PHONE>2013620123</PHONE>
			<TYPE>1</TYPE>
		</PHONES>
	</NAMEINFO>
</flist>
</xsl:template>

</xsl:stylesheet>
