<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
	<xsl:choose>
		<xsl:when test="action/params/@include_children">
			<INCLUDE_CHILDREN><xsl:value-of select="action/params/@include_children"/></INCLUDE_CHILDREN>
		</xsl:when>
		<xsl:otherwise>
			<INCLUDE_CHILDREN>0</INCLUDE_CHILDREN>
		</xsl:otherwise>
	</xsl:choose>
</flist>
</xsl:template>

</xsl:stylesheet>
