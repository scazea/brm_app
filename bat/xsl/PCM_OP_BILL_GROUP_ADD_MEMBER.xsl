<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID><xsl:value-of select="action/params/@group_poid"/></POID>
	<MEMBERS elem="1">
		<OBJECT><xsl:value-of select="action/params/@member_1_account_obj"/></OBJECT>
	</MEMBERS>
	<MEMBERS elem="2">
		<OBJECT><xsl:value-of select="action/params/@member_2_account_obj"/></OBJECT>
	</MEMBERS>
</flist>
</xsl:template>
</xsl:stylesheet>
