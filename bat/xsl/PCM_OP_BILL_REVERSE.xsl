<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID>0.0.0.1 /account 1 0</POID>
	<PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
	<DESCR>Credit Card  Reversal Batch Payment Reversal Batch</DESCR>
	<REVERSALS elem="1">
		<PAYMENT_TRANS_ID><xsl:value-of select="action/params/@trans_id"/></PAYMENT_TRANS_ID>
		<DESCR/>
		<PAY_TYPE>10003</PAY_TYPE>
		<INHERITED_INFO>
			<CC_INFO elem="0">
				<REASON_CODE/>
				<EFFECTIVE_T><xsl:value-of select="action/params/@effective_t"/></EFFECTIVE_T>
			</CC_INFO>
		</INHERITED_INFO>
	</REVERSALS>
	<BATCH_INFO elem="0">
		<BATCH_TOTAL><xsl:value-of select="action/params/@amount"/></BATCH_TOTAL>
		<SUBMITTER_ID>root.0.0.0.1</SUBMITTER_ID>
	</BATCH_INFO>
</flist>
</xsl:template>

</xsl:stylesheet>
