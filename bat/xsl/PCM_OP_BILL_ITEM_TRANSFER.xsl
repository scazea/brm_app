<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
	<ITEM_OBJ><xsl:value-of select="action/params/@adj_item_obj"/></ITEM_OBJ>
	<PROGRAM_NAME>BUNIT_PCM_OP_BILL_ITEM_TRANSFER</PROGRAM_NAME>
	<ITEMS>
		<POID><xsl:value-of select="action/params/@item_cycle_forward"/></POID>
		<AMOUNT><xsl:value-of select="action/params/@amount"/></AMOUNT>
		<BILL_OBJ><xsl:value-of select="action/params/@bill_obj"/></BILL_OBJ>
		<AR_BILL_OBJ><xsl:value-of select="action/params/@ar_bill_obj"/></AR_BILL_OBJ>
		<CURRENCY>840</CURRENCY>
	</ITEMS>
</flist>
</xsl:template>
</xsl:stylesheet>
