<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="common.xsl"/>
<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
	<SERVICE_OBJ><xsl:value-of select="action/params/@service_obj"/></SERVICE_OBJ>
	<PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
	<TO_DEAL_INFO>
		<DEAL_OBJ><xsl:value-of select="action/params/@new_deal_obj"/></DEAL_OBJ>
	</TO_DEAL_INFO>
	<FROM_DEAL_INFO>
		<PACKAGE_ID><xsl:value-of select="action/params/@package_id"/></PACKAGE_ID>
		<DEAL_OBJ><xsl:value-of select="action/params/@old_deal_obj"/></DEAL_OBJ>
	</FROM_DEAL_INFO>
	<TRANSITION_TYPE>1</TRANSITION_TYPE>
</flist>
</xsl:template>

</xsl:stylesheet>
