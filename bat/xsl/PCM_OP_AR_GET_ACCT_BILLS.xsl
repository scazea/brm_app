<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
	<INCLUDE_CHILDREN>
		<xsl:choose>
	     		<xsl:when test="@include_children"><xsl:value-of select="@include_children"/></xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</INCLUDE_CHILDREN>

</flist>
</xsl:template>

</xsl:stylesheet>
