<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID><xsl:value-of select="action/params/@from_account_obj"/></POID>
	<TRANSFER_TARGET><xsl:value-of select="action/params/@to_account_obj"/></TRANSFER_TARGET>
	<AMOUNT><xsl:value-of select="action/params/@amount"/></AMOUNT>
	<PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
	<CURRENCY>840</CURRENCY>
</flist>
</xsl:template>

</xsl:stylesheet>
