Rem
Rem $Header: bus_platform_vob/bus_platform/data/sql/tt_load_procedures.sql /cgbubrm_7.5.0.portalbase/1 2015/11/27 05:46:44 nishahan Exp $
Rem
Rem tt_load_procedures.sql
Rem
Rem Copyright (c) 2011, Oracle and/or its affiliates. All rights reserved. 
Rem
Rem    NAME
Rem      tt_load_procedures.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    abgeorg     06/03/11 - Created
Rem

SET ECHO ON
SET FEEDBACK 1
SET NUMWIDTH 10
SET LINESIZE 80
SET TRIMSPOOL ON
SET TAB OFF
SET PAGESIZE 100

run /u02/brm/ebs/brm_oob/brm_app/7.5/sys/dm_tt/data/create_tt_wrappers.plb
run /u02/brm/ebs/brm_oob/brm_app/7.5/sys/dm_tt/data/tt_create_pkg_pin_sequence.plb
run /u02/brm/ebs/brm_oob/brm_app/7.5/sys/dm_tt/data/pin_rel_tt_updater_sp.plb
run /u02/brm/ebs/brm_oob/brm_app/7.5/sys/dm_tt/data/tt_create_procedures.plb
run /u02/brm/ebs/brm_oob/brm_app/7.5/sys/dm_tt/data/tt_create_pkg_auditrevenue_pack.plb
run /u02/brm/ebs/brm_oob/brm_app/7.5/sys/dm_tt/data/tt_get_latest_poids_procedure.plb
