Rem
Rem $Header: bus_platform_vob/bus_platform/data/sql/update_dd_residency.sql /cgbubrm_7.5.0.portalbase/1 2015/11/27 05:46:53 nishahan Exp $
Rem
Rem update_dd_residency.sql
Rem
Rem Copyright (c) 2010, 2011, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      update_dd_residency.sql - Update the residency types for global objects for Timesten
Rem
Rem    DESCRIPTION
Rem      This file updates the residency types of certain storable class.
Rem      This is done as part of the TimesTen integration project.
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    abgeorg     01/28/10 - Created
Rem    zhhan       02/12/10 - Updated
Rem    zhhan       04/05/10 - Updated
Rem    zhhan       04/29/10 - Updated
Rem    abgeorg     05/07/10 - Updated
Rem    abgeorg     05/14/10 - Updated
Rem    abgeorg     05/18/10 - Updated
Rem    zhhan       05/18/10 - adding event residency_type to support compact and expanded format
Rem    rkonjady    02/21/11 - Updated for TTV2
Rem    rkonjady	   03/24/11 - enabling audit
update dd_objects_t set residency_type=101 where name like '/uniqueness%';
update dd_objects_t set residency_type=102 where name like '/config%';
update dd_objects_t set residency_type=102 where name like '/deal%';
update dd_objects_t set residency_type=102 where name like '/plan%';
update dd_objects_t set residency_type=102 where name like '/product%';
update dd_objects_t set residency_type=102 where name like '/rate%';
update dd_objects_t set residency_type=102 where name like '/discount%';
update dd_objects_t set residency_type=102 where name like '/zone%';
update dd_objects_t set residency_type=102 where name like '/rollover%';
update dd_objects_t set residency_type=102 where name like '/sponsorship%';
update dd_objects_t set residency_type=102 where name like '/transition%';
update dd_objects_t set residency_type=102 where name like '/dependency%';
update dd_objects_t set residency_type=102 where name like '/fold%';
update dd_objects_t set residency_type=102 where name like '/strings%';
update dd_objects_t set residency_type=102 where name like '/channel%';


update dd_objects_t set residency_type=1 where name like '/item%' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name like '/journal' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name like '/profile%';
update dd_objects_t set residency_type=1 where name like '/service%' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name like '/service/pcm_client' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name like '/service/admin_client' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=6 where name like '/reservation%';
update dd_objects_t set residency_type=0 where name like '/device%';
update dd_objects_t set residency_type=0 where name like '/order%';
update dd_objects_t set residency_type=0 where name like '/block%';
update dd_objects_t set residency_type=0 where name like '/batch%';
update dd_objects_t set residency_type=0 where name like '/invoice%';
update dd_objects_t set residency_type=0 where name like '/channel_event%';
update dd_objects_t set residency_type=0 where name like '/group%'; 
update dd_objects_t set residency_type=304 where name like '/event%';

update dd_objects_t set residency_type=301 where name = '/event/activity/content';
update dd_objects_t set residency_type=301 where name = '/event/activity/telco';
update dd_objects_t set residency_type=301 where name = '/event/session/dialup';
update dd_objects_t set residency_type=301 where name = '/event/session/telco';
update dd_objects_t set residency_type=301 where name = '/event/session/telco/gsm';
update dd_objects_t set residency_type=301 where name = '/event/session/telco/gprs';
update dd_objects_t set residency_type=301 where name = '/event/session/telco/gprs/master';
update dd_objects_t set residency_type=301 where name = '/event/session/telco/gprs/subsession';
update dd_objects_t set residency_type=301 where name = '/event/session/telco/imt';
update dd_objects_t set residency_type=301 where name = '/event/session/telco/pdc';

update dd_objects_t set residency_type=1 where name = '/event';

update dd_objects_t set residency_type=1 where name = '/account' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name = '/balance_group' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name = '/bill' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name = '/history_bills' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name = '/billinfo' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name = '/ordered_balgrp' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name = '/payinfo' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name = '/payinfo/cc' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name = '/payinfo/dd' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name = '/payinfo/invoice' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name = '/purchased_discount' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name = '/purchased_product' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name = '/schedule' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name = '/schedule/device' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name = '/tmp_unprocessed_events' and residency_type != 5 and residency_type != 7;
update dd_objects_t set residency_type=1 where name = '/user_activity' and residency_type != 6;
update dd_objects_t set residency_type = 0 where name like '/au_%';
update dd_objects_t set residency_type = 0 where name like '/data%';
update dd_objects_t set residency_type = 102 where name like '/search%';


update dd_objects_t set residency_type=0 where name = '/admin_action';
update dd_objects_t set residency_type=0 where name = '/audit_account_products';
update dd_objects_t set residency_type=0 where name = '/cmt_reference';
update dd_objects_t set residency_type=0 where name = '/edr_field_mapping';
update dd_objects_t set residency_type=0 where name = '/filter_set';
update dd_objects_t set residency_type=0 where name = '/invoice_detail';
update dd_objects_t set residency_type=0 where name = '/job';
update dd_objects_t set residency_type=0 where name = '/job_batch';
update dd_objects_t set residency_type=0 where name = '/lock_program';
update dd_objects_t set residency_type=0 where name = '/message';
update dd_objects_t set residency_type=0 where name = '/monitor_queue';
update dd_objects_t set residency_type=0 where name = '/note';
update dd_objects_t set residency_type=0 where name = '/opcode';
update dd_objects_t set residency_type=0 where name = '/pop';
update dd_objects_t set residency_type=0 where name = '/portal_sap_masterdata';
update dd_objects_t set residency_type=0 where name = '/remittance_info';
update dd_objects_t set residency_type=0 where name = '/sap';
update dd_objects_t set residency_type=0 where name = '/topup';
update dd_objects_t set residency_type=0 where name = '/topuprules';
update dd_objects_t set residency_type=1 where name = '/associated_bus_profile';
update dd_objects_t set residency_type=1 where name = '/best_pricing';
update dd_objects_t set residency_type=1 where name = '/bulkacct';
update dd_objects_t set residency_type=0 where name = '/collections_action';
update dd_objects_t set residency_type=0 where name = '/collections_scenario';
update dd_objects_t set residency_type=0 where name = '/notification_specs';
update dd_objects_t set residency_type=0 where name = '/ppv_purchase';
update dd_objects_t set residency_type=0 where name = '/ppv_schedule';
update dd_objects_t set residency_type=0 where name = '/process_audit';
update dd_objects_t set residency_type=1 where name = '/purchased_bundle';
update dd_objects_t set residency_type=0 where name = '/ra_threshold_status';
update dd_objects_t set residency_type=0 where name = '/recycle_suspended_usage';
update dd_objects_t set residency_type=1 where name = '/rel_event_extract_sync';
update dd_objects_t set residency_type=0 where name = '/remittance_calculation';
update dd_objects_t set residency_type=0 where name = '/sms_settle_report';
update dd_objects_t set residency_type=0 where name = '/smt_acct_mig';
update dd_objects_t set residency_type=0 where name = '/suspended_batch';
update dd_objects_t set residency_type=0 where name = '/suspended_usage';
update dd_objects_t set residency_type=0 where name = '/suspended_usage_edits';
update dd_objects_t set residency_type=0 where name = '/tmp_suspended_usage';
update dd_objects_t set residency_type=0 where name = '/transaction_id';
update dd_objects_t set residency_type=0 where name = '/unique_account_no';
update dd_objects_t set residency_type=1 where name = '/tmp_unprocessed_events';
update dd_objects_t set residency_type=1 where name = '/rerate_session';



alter table event_t add (compact_sub_event CLOB);

commit;
quit;
