Rem
Rem $Header: bus_platform_vob/bus_platform/data/sql/create_delete_cascade.sql /cgbubrm_7.5.0.portalbase/1 2015/11/27 05:35:42 nishahan Exp $
Rem
Rem create_delete_cascade.sql
Rem
Rem Copyright (c) 2010, Oracle and/or its affiliates. All rights reserved. 
Rem
Rem    NAME
Rem      create_delete_cascade.sql - createion of cascade delete property for journal and item class.
Rem
Rem    DESCRIPTION
Rem      This is for the purge tool for purging unwated bill, item and journal objects from TimesTen memory. For having cascade delete in TimesTen the corresponding table in Oracle also needs to have the cascade delete property.
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    abgeorg     01/28/10 - Created
Rem
ALTER TABLE ITEM_T ADD CONSTRAINT pk_item PRIMARY KEY(POID_ID0);
ALTER TABLE journal_t add CONSTRAINT fk_item FOREIGN KEY (item_obj_id0) REFERENCES item_t(poid_id0) ON DELETE CASCADE;
quit;
