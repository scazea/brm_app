-- @(#)$Id: bus_param_use_billed_time_for_gl.source /cgbubrm_7.5.0.rwsmod/1 2014/08/30 17:50:41 mnarasim Exp $
--
-- Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved.
-- This material is the confidential property of Oracle Corporation or its  
-- licensors and may be used, reproduced, stored or transmitted only in
-- accordance with a valid Oracle license or sublicense agreement.

DECLARE
	v_param_name	config_business_params_t.param_name%type;
	v_count		NUMBER;
	v_rec_id	config_business_params_t.rec_id%type;
	v_err_code CONSTANT NUMBER := -20004;
BEGIN
	-- Add 'billing' business param 'use_actual_billed_time_for_gl_report'
	BEGIN
		SELECT param_name INTO v_param_name
		FROM config_business_params_t
		WHERE param_name = 'use_actual_billed_time_for_gl_report';
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			SELECT COUNT(*) INTO v_count FROM config_t
			WHERE name = 'billing';

		IF v_count=1 THEN

			SELECT MAX(NVL(rec_id, 0)) + 1 INTO v_rec_id
			FROM config_business_params_t
			WHERE obj_id0 = (SELECT poid_id0 FROM config_t
				WHERE poid_type = '/config/business_params'
				AND name = 'billing');

			INSERT
			INTO config_business_params_t
			(obj_id0
			,rec_id
			,param_name
			,param_type
			,param_value
			,param_description)
			SELECT poid_id0
			,v_rec_id
			,'use_actual_billed_time_for_gl_report'
			,1
			,'0'
			,'This parameter enables ledger reporting to consider revenue as billed based on the actual time of billing. Valid values: 0 - (Default) Revenue is considered as billed at the bill period end time, 1 - Revenue is considered as billed at the actual time of billing.'
			FROM config_t
			WHERE poid_type = '/config/business_params'
			AND name = 'billing';

			COMMIT;

		END IF;

		WHEN OTHERS THEN
			RAISE_APPLICATION_ERROR(v_err_code,
			'Error occurred while inserting business parameter ' ||
			'use_actual_billed_time_for_gl_report ' ||
			sqlerrm, TRUE);
	END;

EXCEPTION
	WHEN OTHERS THEN
		RAISE_APPLICATION_ERROR(v_err_code,
		'Error happened due to ' || sqlerrm, TRUE);
		ROLLBACK;
END;
/
