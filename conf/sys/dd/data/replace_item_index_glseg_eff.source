-- Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved.
--
-- This material is the confidential property of Oracle Corporation or its
-- licensors and may be used, reproduced, stored or transmitted only in
-- accordance with a valid Oracle license or sublicense agreement.
--
--
-- The index i_item_glseg_eff__id on table item_t is designed to be used when
-- item's effective_t is accessed by the ledger reporting stored procedures. In
-- order to accommodate an enhancement to the ledger reporting functionality,
-- the definition of this index needs to be changed.
--
-- IMPORTANT: Before running this script, edit this file to replace
-- $PIN_CONF_TBLSPACEX1 and $PIN_CONF_STORAGE_MED_INS with desired tablespace
-- and storage parameters based on guidelines in file
-- BRM_Home/setup/scripts/pin_tables.values.
--
-- NOTES:
--  - If the BRM ledger reporting feature is not used, there is no need to
--    run this upgrade script
--  - If this index does not currently exist, this script does nothing
--  - After the index is created by this script, it needs to be rebuilt

DECLARE
	v_index_name VARCHAR2(100);
	ddl_stmt VARCHAR2(4000);
	v_errmsg VARCHAR2(2000);
	V_INDEX_CREATION_ERROR CONSTANT number := -20001;
BEGIN
	v_index_name := 'i_item_glseg_eff__id';

	-- If index exists, replace it with new definition, else do nothing
	IF (PIN_UPG_COMMON.INDEX_EXISTS(v_index_name, v_errmsg)) THEN

		ddl_stmt := 'DROP INDEX ' || v_index_name;
		EXECUTE IMMEDIATE ddl_stmt;

		ddl_stmt := 'CREATE INDEX ' || v_index_name ||
		' ON item_t
			(gl_segment, gl.get_gl_billed_t(effective_t, opened_t))
			$PIN_CONF_TBLSPACEX1
			$PIN_CONF_STORAGE_MED_INS
			NOLOGGING PARALLEL';
		EXECUTE IMMEDIATE ddl_stmt;

		ddl_stmt := 'ALTER INDEX ' || v_index_name ||
			' NOPARALLEL LOGGING';
		EXECUTE IMMEDIATE ddl_stmt;

	ELSE
		DBMS_OUTPUT.PUT_LINE('Index ' || v_index_name ||
			' not present on table item_t, ' ||
			'so no need to replace it');
	END IF;

EXCEPTION
	WHEN OTHERS THEN
		RAISE_APPLICATION_ERROR(V_INDEX_CREATION_ERROR,
			'INDEX CREATION FAILED FOR ' || v_index_name ||
			' on table item_t ' || sqlerrm, TRUE);
END;
/
