-- @(#)$Id: bus_param_activity_use_exact_time_for_deferred_action.source /cgbubrm_7.5.0.rwsmod/1 2013/03/26 10:35:28 amandavi Exp $
--
-- Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved. 
-- This material is the confidential property of Oracle Corporation or its
-- licensors and may be used, reproduced, stored or transmitted only in
-- accordance with a valid Oracle license or sublicense agreement.

DECLARE

	v_param_name config_business_params_t.param_name%type;
	v_count        NUMBER;
	v_rec_id       NUMBER(38);
	v_err_code CONSTANT NUMBER := -20004;

BEGIN
	
-- Update of config business params for "activity"

        BEGIN
                SELECT param_name INTO v_param_name FROM config_business_params_t WHERE param_name = 'use_exact_time_for_deferred_action';

        EXCEPTION

                WHEN NO_DATA_FOUND THEN
                SELECT COUNT(*) INTO v_count FROM config_t WHERE name = 'activity';
                IF v_count=1 THEN

                SELECT MAX(NVL(rec_id, 0)) + 1 INTO v_rec_id FROM config_business_params_t
                        WHERE obj_id0 = (SELECT poid_id0 FROM config_t
                        WHERE poid_type = '/config/business_params'
                        AND name = 'activity');

                INSERT
                        INTO config_business_params_t
                                (obj_id0
                                ,rec_id
                                ,param_name
                                ,param_type
                                ,param_value
                                ,param_description)
                        SELECT poid_id0
                        ,v_rec_id
                        ,'use_exact_time_for_deferred_action'
                        ,1
                        ,'0'
                        ,'This parameter determines use of the exact scheduled time for a deferred action. Valid values are 0 (Default) - Disabled: When creating a deferred action, round off scheduled time to midnight and 1 - Enabled: Use exact time specified in input without rounding to midnight.'
                        FROM config_t
                        WHERE poid_type = '/config/business_params'
                        AND name = 'activity';
                COMMIT;

                END IF;

                WHEN OTHERS THEN
                RAISE_APPLICATION_ERROR(v_err_code,'Error occurred while inserting business parameter use_exact_time_for_deferred_action '||sqlerrm, TRUE);

        END;

EXCEPTION
	WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(v_err_code,'Error happenning due to '||sqlerrm, TRUE);  
        ROLLBACK;
END;
/
