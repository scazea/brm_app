
OS=linux
VERSION=7.5

INCDIR=$(PIN_HOME)/include
INCDIR_CUST=$(BRM_SRC)/include
LIBDIR_BRM=$(BRM_BINS)/lib

##########

CC_linux = gcc
CC = $(CC_$(OS))

C++_linux = gcc
C++ = $(C++_$(OS))

APP_CC_comp_linux = gcc
APP_CC_link_linux= gcc
APP_CC_comp = $(APP_CC_comp_$(OS))
APP_CC_link = $(APP_CC_link_$(OS))

###########

CFLAGS_linux= -m32 -fPIC -g -Wall -Wno-unused-variable -Wno-parentheses # -Werror
CFLAGS= $(CFLAGS_$(OS)) -DFLIST_HEAP -DFOR_CM

C++FLAGS_linux = -x c++ -m32 -DPIN_NOT_USING_OSTREAM
C++FLAGS= $(C++FLAGS_$(OS)) -DPCMCPP_CONST_SAFE

CPPFLAGS = -I$(INCDIR) -I$(INCDIR_CUST)

LDFLAGS_linux= -m elf_i386 -shared -L/usr/lib
LDFLAGS = $(LDFLAGS_$(OS)) -L$(LIBDIR_BRM)

SL_EXT_linux= so
SL_EXT= $(SL_EXT_$(OS))

OS_LIBS_linux= -lm
OS_LIBS= $(OS_LIBS_$(OS))

APP_CFLAGS_linux= -m32 -fPIC -g -pthread
APP_CFLAGS= $(APP_CFLAGS_$(OS))

APP_LDFLAGS_linux= -m32 -Wl,--export-dynamic -L/usr/lib
APP_LDFLAGS = -L$(LIBDIR_BRM) $(APP_LDFLAGS_$(OS))

APP_OS_LIBS_linux= -lpthread

APP_LDLIBS= -lportal -lmta -lpinsys $(APP_OS_LIBS_$(OS))

MAKE_SUBS_THREADED=for i in $(SUBDIRS) ; \
	do \
	if [ -d $$i ] ; then \
	cd $$i ; \
	echo "* Entering `pwd`" ; \
	if [ -f Makefile ] ; then \
	$(MAKE) $@ || exit $$? ; \
	fi ; \
	cd .. ; \
	fi ; \
	done

# Escape quotes so they are handled correctly
# # this will replace quotes with \"
 SED_FIX_QUOTES='s/\"/\\\"/g'

# # Escape backspaces so they are handled correctly
# # this will replace backspaces with \\
 SED_FIX_BACKSPACES='s/\\/\\\\\\/g'

# # Cause missing environment variables to error
# # this will replace ${VAR} with ${VAR:?}
 SED_MISSING_VAR='s/$${\(.*\)}/$${\1:?}/g'

 SED_COMMAND=sed -e $(SED_FIX_BACKSPACES) -e $(SED_FIX_QUOTES) -e $(SED_MISSING_VAR)

# # Enclose all lines in quotes
 AWK_PART=awk '{ print "echo \"" $$0 "\"" }'

 PROCESS_TOKENS=$(SED_COMMAND) | ${AWK_PART} | ksh


# default target
build:

stop_cm:
	@arm.pl stop cm

config:

clean:

clobber:
