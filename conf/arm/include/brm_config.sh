export BRM_START='dm_oracle dm_prov_telco dm_vertex eai_js dm_ach dm_ebstax cm dm_eai formatter batch_controller'

if [ "$HOSTNAME" != "brmdev-01" ]; then
	export BRM_START="${BRM_START} dm_oracle_log dm_oracle_web cm_log cm_web"
fi

if [ ! -f $CUST_BINS/bin/cm_log ]; then
	echo "Creando link $CUST_BINS/bin/cm_log"
	ln -vs $BRM_BINS/bin/cm $CUST_BINS/bin/cm_log
fi

if [ ! -f $CUST_BINS/bin/cm_web ]; then
	echo "Creando link $CUST_BINS/bin/cm_web"
	ln -vs $BRM_BINS/bin/cm $CUST_BINS/bin/cm_web
fi

if [ ! -f $CUST_BINS/bin/dm_oracle_log ]; then
	echo "Creando link $CUST_BINS/bin/dm_oracle_log"
	ln -vs $BRM_BINS/bin/dm_oracle $CUST_BINS/bin/dm_oracle_log
fi

if [ ! -f $CUST_BINS/bin/dm_oracle_web ]; then
	echo "Creando link $CUST_BINS/bin/dm_oracle_web"
	ln -vs $BRM_BINS/bin/dm_oracle $CUST_BINS/bin/dm_oracle_web
fi

if [ "$BRM_ENABLE_FUSA_SIM" == "1" ]; then
       BRM_START="$BRM_START dm_fusa answer_s answer_b"
fi

if [ "$BRM_ENABLE_IFW" == "1" ]; then
	BRM_START="$BRM_START ifw"
fi

SERVICE_NAME=`tnsping ${BRM_DB_ALIAS} | grep SERVICE_NAME | awk '{split($0, a); print a[22]}'`
export BRM_DB_SID=${SERVICE_NAME:0:${#SERVICE_NAME}-3}

DB_HOST_IP=`tnsping ${BRM_DB_ALIAS} | grep SERVICE_NAME | awk '{split($0, a); print a[12]}'`
export  IFW_DB_HOST_IP=${DB_HOST_IP:0:${#DB_HOST_IP}-6}

#export ARM_PERL_HOME=$ARM_TP/perls/perl-5.18.4
export ARM_PERL_HOME=$ARM_TP/perlbrew/perls/perl-5.24.0
export ARM_JRE_HOME=$ARM_TP/jre/1.7.0
export BRM_DB_TYPE="RDS"

export BRM_CUST_OP_FLDS="${CUST_BINS}/lib/custom_op_flds.dat"
export BRM_LOGLEVEL=1

#logging and database
VAR_HOST=`hostname`

case  $VAR_HOST  in
	brmdev-01)
		export BRM_LOGLEVEL=3
		;;
	brm)
		export IFW_DB_HOSTNAME=192.168.88.194
		export BRM_LOGLEVEL=3
		export BRM_VIRTUAL_TIME=' - - pin_virtual_time ${BRM_BINS}/lib/pin_virtual_time_file'
		;;
	brm-minacs)
		export IFW_DB_HOSTNAME=54.84.187.58
		export BRM_LOGLEVEL=1
		export BRM_VIRTUAL_TIME=' - - pin_virtual_time ${CUST_BINS}/lib/pin_virtual_time_file'
			
		if [ "$USER" == "pin" ]; then
			export BILLING_PROD_ENV=1
		fi
		if [ "$USER" == "pinjen" ]; then
			export BRM_B3_HOSTNAME=54.173.109.246
		fi	
		;;
	*)
esac

export BRM_SETUP_SCRIPTS_LOGLEVEL=3
export BRM_VIRTUAL_TIME=' - - pin_virtual_time ${BRM_BINS}/lib/pin_virtual_time_file'

export BRM_TABLESPACE=pin00

export BRM_DUMP=75PS18.brm_oob.20170630_01.dmp

export IFW_DB_PASS_ENC=$(pin_crypt_app -enc $IFW_DB_PASS | grep '^Encrypted' | awk 'match($0, />.*</) { print substr ($0, RSTART+1, RLENGTH-2) }')

export BRM_APPS=$BRM_CONF/apps
export BRM_APPS_CUSTOM=$BRM_APPS

# ports
export BRM_CM_PORT=${BRM_NUM}01
export BRM_DM_ORACLE_PORT=${BRM_NUM}02
export BRM_CMMP_PORT=${BRM_NUM}03
export BRM_DM_VERTEX_PORT=${BRM_NUM}04
export BRM_DM_FUSA_PORT=${BRM_NUM}05
export BRM_DM_FUSA_BATCH_PORT=${BRM_NUM}06
export BRM_DM_FUSA_ONLINE_PORT=${BRM_NUM}07
export BRM_ANSWER_B_PORT=$BRM_DM_FUSA_BATCH_PORT
export BRM_ANSWER_S_PORT=$BRM_DM_FUSA_ONLINE_PORT
export BRM_DISCOUNTING_EM_PORT=${BRM_NUM}08
export BRM_DM_EAI_PORT=${BRM_NUM}09
export BRM_EAI_JS_PORT=${BRM_NUM}10
export BRM_DM_IFW_SYNC_PORT=${BRM_NUM}11
export BRM_DM_PROV_TELCO_PORT=${BRM_NUM}12
export BRM_DM_PROV_TELCO_PROV_PTR_PORT=${BRM_NUM}13
export BRM_DM_AQ_PORT=${BRM_NUM}14
export BRM_FORMATTER_PORT=${BRM_NUM}15
export BRM_DM_EMAIL_PORT=${BRM_NUM}16
export BRM_RERATING_EM_PORT=${BRM_NUM}17
export BRM_INFMGR_PORT=${BRM_NUM}18
export BRM_ZONING_EM_PORT=${BRM_NUM}19
export BRM_BATCH_LOCK_SOCKET_ADDR_PORT=${BRM_NUM}20
export BRM_DM_ACH_PORT=${BRM_NUM}21
#export BRM_IFW_PORT=${BRM_NUM}22
export BRM_IFW_PORT=${BRM_DISCOUNTING_EM_PORT}
export IFW_EVENTHANDLER_PORT=${BRM_NUM}23
export BRM_DM_EBSTAX_PORT=${BRM_NUM}24
export BRM_CM_WEB_PORT=${BRM_NUM}25
export BRM_CM_LOG_PORT=${BRM_NUM}26
export BRM_DM_ORACLE_WEB_PORT=${BRM_NUM}27
export BRM_DM_ORACLE_LOG_PORT=${BRM_NUM}28

# dm_aq settings
export BRM_DM_AQ_BIGSIZE=2097152
export BRM_DM_AQ_MAX_PER_FE=16
export BRM_DM_AQ_N_BE=10
export BRM_DM_AQ_N_FE=4
export BRM_DM_AQ_SHMSIZE=33554432
export BRM_DM_AQ_TRANS_BE_MAX=8

# dm_eai settings
export BRM_DM_EAI_N_FE=4
export BRM_DM_EAI_BIGSIZE=2097152
export BRM_DM_EAI_MAX_PER_FE=16
export BRM_DM_EAI_N_BE=8
export BRM_DM_EAI_SHMSIZE=33554432
export BRM_DM_EAI_TRANS_BE_MAX=8

# dm_email settings
export BRM_DM_EMAIL_N_FE=4
export BRM_DM_EMAIL_BIGSIZE=512k
export BRM_DM_EMAIL_MAX_PER_FE=16
export BRM_DM_EMAIL_N_BE=8
export BRM_DM_EMAIL_SHMSIZE=4M

# dm_oracle settings
export BRM_DM_ORACLE_N_FE=4
export BRM_DM_ORACLE_BIGSIZE=2097152
export BRM_DM_ORACLE_IN_BATCH_SIZE=80
export BRM_DM_ORACLE_MAX_PER_FE=16
export BRM_DM_ORACLE_N_BE=10
export BRM_DM_ORACLE_HOSTNAME=$BRM_HOSTNAME
export BRM_DM_ORACLE_SHMSIZE=33554432
export BRM_DM_ORACLE_TRANS_BE_MAX=8
export BRM_DM_ORACLE_ENCRYPT_PASSWD=1

# dm_fusa settings
export BRM_DM_FUSA_BATCH_HOSTNAME=${BRM_HOSTNAME}
export BRM_DM_FUSA_ONLINE_HOSTNAME=${BRM_HOSTNAME}
export BRM_DM_FUSA_PID=090909
export BRM_DM_FUSA_PID_PWD='&aes|06|0D5E11BFDD97D2769D9B0DBFBD1BBF7E8056827E89DBA94DC95988AFD1E3633844'
export BRM_DM_FUSA_SID=090909
export BRM_DM_FUSA_SID_PWD='&aes|06|0D5E11BFDD97D2769D9B0DBFBD1BBF7E8056827E89DBA94DC95988AFD1E3633844'
export BRM_DM_FUSA_N_FE=1
export BRM_DM_FUSA_BIGSIZE=1M
export BRM_DM_FUSA_MAX_PER_FE=16
export BRM_DM_FUSA_N_BE=2
export BRM_DM_FUSA_SHMSIZE=6M

# dm_prov_telco settings
export BRM_DM_PROV_TELCO_N_FE=4
export BRM_DM_PROV_TELCO_BIGSIZE=2097152
export BRM_DM_PROV_TELCO_MAX_PER_FE=16
export BRM_DM_PROV_TELCO_N_BE=8
export BRM_DM_PROV_TELCO_HOSTNAME=$BRM_HOSTNAME
export BRM_DM_PROV_TELCO_SHMSIZE=33554432
export BRM_DM_PROV_TELCO_TRANS_BE_MAX=8
export BRM_DM_PROV_TELCO_PROV_PTR_HOSTNAME=$BRM_HOSTNAME

# dm_vertex settings
export BRM_DM_VERTEX_N_FE=4
export BRM_DM_VERTEX_BIGSIZE=512k
export BRM_DM_VERTEX_MAX_PER_FE=16
export BRM_DM_VERTEX_N_BE=8
export BRM_DM_VERTEX_RESTART_CHILDREN=0
export BRM_DM_VERTEX_SHMSIZE=4M
export BRM_DM_VERTEX_QUANTUMDB_SOURCE=stub
export BRM_DM_VERTEX_QUANTUMDB_SERVER=stub
export BRM_DM_VERTEX_QUANTUMDB_USER=stub
export BRM_DM_VERTEX_QUANTUMDB_PASSWD=stub

# dm_ifw_sunc settings
export BRM_DM_IFW_SYNC_BIGSIZE=2097152
export BRM_DM_IFW_SYNC_HOSTNAME=$BRM_HOSTNAME
export BRM_DM_IFW_SYNC_MAX_PER_FE=16
export BRM_DM_IFW_SYNC_N_BE=8
export BRM_DM_IFW_SYNC_N_FE=4
export BRM_DM_IFW_SYNC_RESTART_CHILDREN=1
export BRM_DM_IFW_SYNC_SHMSIZE=33554432
export BRM_DM_IFW_SYNC_TRANS_BE_MAX=8

# cm settings
export BRM_CM_LOGFORMAT=0
export BRM_CM_LOGLEVEL=1
export BRM_CM_HOSTNAME=$BRM_HOSTNAME

# apps/diagnostics/pin_adu_validate
export BRM_DIAGNOSTICS_PIN_ADU_VALIDATE_MTA_MAX_ERRS=5000
export BRM_DIAGNOSTICS_PIN_ADU_VALIDATE_MTA_CHILDREN=5
export BRM_DIAGNOSTICS_PIN_ADU_VALIDATE_MTA_PER_BATCH=500
export BRM_DIAGNOSTICS_PIN_ADU_VALIDATE_MTA_PER_STEP=1000
export BRM_DIAGNOSTICS_PIN_ADU_VALIDATE_MTA_FETCH_SIZE=5000

# apps/pin_billd settings
export BRM_NAP_LOGIN_NAME=root.0.0.0.1
export BRM_NAP_LOGIN_PW='&aes|08|0D5E11BFDD97D2769D9B0DBFBD1BBF7E5D40C305EDF3D77DF111AAB8F781E92122'
export BRM_PIN_BILLD_CHILDREN=5
export BRM_PIN_BILLD_FETCH_SIZE=5000
export BRM_PIN_BILLD_MERCHANT=XXX
export BRM_PIN_BILLD_MINIMUM=2.00
export BRM_PIN_BILLD_PER_BATCH=1000
export BRM_PIN_BILLD_MTA_CHILDREN=5
export BRM_PIN_BILLD_MTA_FETCH_SIZE=5000
export BRM_PIN_BILLD_MTA_MAX_ERRS=5000
export BRM_PIN_BILLD_MTA_PER_BATCH=500
export BRM_PIN_BILLD_MTA_PER_STEP=1000

# apps/*
export BRM_PIN_BALANCE_TRANSFER_MTA_MAX_ERRS=5000
export BRM_PIN_BALANCE_TRANSFER_MTA_CHILDREN=5
export BRM_PIN_BALANCE_TRANSFER_MTA_PER_BATCH=500
export BRM_PIN_BALANCE_TRANSFER_MTA_PER_STEP=1000
export BRM_PIN_BALANCE_TRANSFER_MTA_FETCH_SIZE=5000
export BRM_PIN_BILL_HANDLER_MTA_MAX_ERRS=5000
export BRM_PIN_BILL_HANDLER_MTA_CHILDREN=5
export BRM_PIN_BILL_HANDLER_MTA_PER_BATCH=500
export BRM_PIN_BILL_HANDLER_MTA_PER_STEP=1000
export BRM_PIN_BILL_HANDLER_MTA_FETCH_SIZE=5000
export BRM_PIN_BULK_ADJUST_MTA_MAX_ERRS=5000
export BRM_PIN_BULK_ADJUST_MTA_CHILDREN=5
export BRM_PIN_BULK_ADJUST_MTA_PER_BATCH=500
export BRM_PIN_BULK_ADJUST_MTA_PER_STEP=1000
export BRM_PIN_BULK_ADJUST_MTA_FETCH_SIZE=5000
export BRM_PIN_CLEAN_ASOS_MTA_MAX_ERRS=5000
export BRM_PIN_CLEAN_ASOS_MTA_CHILDREN=5
export BRM_PIN_CLEAN_ASOS_MTA_PER_BATCH=500
export BRM_PIN_CLEAN_ASOS_MTA_PER_STEP=1000
export BRM_PIN_CLEAN_ASOS_MTA_FETCH_SIZE=5000
export BRM_PIN_CLEAN_RSVNS_MTA_MAX_ERRS=5000
export BRM_PIN_CLEAN_RSVNS_MTA_CHILDREN=5
export BRM_PIN_CLEAN_RSVNS_MTA_PER_BATCH=500
export BRM_PIN_CLEAN_RSVNS_MTA_PER_STEP=1000
export BRM_PIN_CLEAN_RSVNS_MTA_FETCH_SIZE=5000
export BRM_PIN_CRYPT_MTA_MAX_ERRS=5000
export BRM_PIN_CRYPT_MTA_CHILDREN=5
export BRM_PIN_CRYPT_MTA_PER_BATCH=500
export BRM_PIN_CRYPT_MTA_PER_STEP=1000
export BRM_PIN_CRYPT_MTA_FETCH_SIZE=5000
export BRM_PIN_EXPORT_PRICE_MTA_MAX_ERRS=5000
export BRM_PIN_EXPORT_PRICE_MTA_CHILDREN=5
export BRM_PIN_EXPORT_PRICE_MTA_PER_BATCH=500
export BRM_PIN_EXPORT_PRICE_MTA_PER_STEP=1000
export BRM_PIN_EXPORT_PRICE_MTA_FETCH_SIZE=5000
export BRM_PIN_INV_MTA_MAX_ERRS=5000
export BRM_PIN_INV_MTA_CHILDREN=5
export BRM_PIN_INV_MTA_PER_BATCH=500
export BRM_PIN_INV_MTA_PER_STEP=1000
export BRM_PIN_INV_MTA_FETCH_SIZE=5000
export BRM_PIN_MONITOR_MTA_MAX_ERRS=5000
export BRM_PIN_MONITOR_MTA_CHILDREN=5
export BRM_PIN_MONITOR_MTA_PER_BATCH=500
export BRM_PIN_MONITOR_MTA_PER_STEP=1000
export BRM_PIN_MONITOR_MTA_FETCH_SIZE=5000
export BRM_PIN_OOD_HANDLER_MTA_MAX_ERRS=5000
export BRM_PIN_OOD_HANDLER_MTA_CHILDREN=5
export BRM_PIN_OOD_HANDLER_MTA_PER_BATCH=500
export BRM_PIN_OOD_HANDLER_MTA_PER_STEP=1000
export BRM_PIN_OOD_HANDLER_MTA_FETCH_SIZE=5000
export BRM_PIN_OOD_HANDLER_PROCESS_MTA_MAX_ERRS=5000
export BRM_PIN_OOD_HANDLER_PROCESS_MTA_CHILDREN=5
export BRM_PIN_OOD_HANDLER_PROCESS_MTA_PER_BATCH=500
export BRM_PIN_OOD_HANDLER_PROCESS_MTA_PER_STEP=1000
export BRM_PIN_OOD_HANDLER_PROCESS_MTA_FETCH_SIZE=5000
export BRM_PIN_RERATE_MTA_MAX_ERRS=5000
export BRM_PIN_RERATE_MTA_CHILDREN=5
export BRM_PIN_RERATE_MTA_PER_BATCH=500
export BRM_PIN_RERATE_MTA_PER_STEP=1000
export BRM_PIN_RERATE_MTA_FETCH_SIZE=5000
export BRM_PIN_STATE_CHANGE_MTA_CHILDREN=1
export BRM_PIN_STATE_CHANGE_MTA_PER_BATCH=500
export BRM_PIN_STATE_CHANGE_MTA_PER_STEP=1000
export BRM_PIN_STATE_CHANGE_MTA_FETCH_SIZE=5000
export BRM_PIN_SUBSCRIPTION_MTA_MAX_ERRS=5000
export BRM_PIN_SUBSCRIPTION_MTA_CHILDREN=5
export BRM_PIN_SUBSCRIPTION_MTA_PER_BATCH=500
export BRM_PIN_SUBSCRIPTION_MTA_PER_STEP=1000
export BRM_PIN_SUBSCRIPTION_MTA_FETCH_SIZE=5000
export BRM_PIN_TRIAL_BILL_MTA_MAX_ERRS=5000
export BRM_PIN_TRIAL_BILL_MTA_CHILDREN=5
export BRM_PIN_TRIAL_BILL_MTA_PER_BATCH=500
export BRM_PIN_TRIAL_BILL_MTA_PER_STEP=1000
export BRM_PIN_TRIAL_BILL_MTA_FETCH_SIZE=5000
export BRM_PIN_COLLECTIONS_LOGLEVEL=1
export BRM_PIN_COLLECTIONS_MTA_MAX_ERRS=500
export BRM_PIN_COLLECTIONS_MTA_CHILDREN=5
export BRM_PIN_COLLECTIONS_MTA_PER_BATCH=500
export BRM_PIN_COLLECTIONS_MTA_PER_STEP=1000
export BRM_PIN_COLLECTIONS_MTA_FETCH_SIZE=5000


# other
export BRM_DISCOUNTING_EM_HOSTNAME=$BRM_HOSTNAME
export BRM_DM_ORACLE_HOSTNAME=$BRM_HOSTNAME
export BRM_RERATING_EM_HOSTNAME=$BRM_HOSTNAME
export BRM_ZONING_EM_HOSTNAME=$BRM_HOSTNAME
export BRM_FORMATTER_HOSTNAME=$BRM_HOSTNAME
export BRM_INFMGR_HOSTNAME=$BRM_HOSTNAME
export BRM_INFMGR_PASS='&aes|08|0D5E11BFDD97D2769D9B0DBFBD1BBF7E5D40C305EDF3D77DF111AAB8F781E92122'
export BRM_DM_EMAIL_HOSTNAME=$BRM_HOSTNAME
export BRM_DM_FUSA_HOSTNAME=$BRM_HOSTNAME
export BRM_DM_VERTEX_HOSTNAME=$BRM_HOSTNAME
export BRM_DM_EAI_HOSTNAME=$BRM_HOSTNAME
export BRM_DM_IFW_SYNC_HOSTNAME=$BRM_HOSTNAME
export BRM_DM_PROV_TELCO_HOSTNAME=$BRM_HOSTNAME
export BRM_DM_AQ_HOSTNAME=$BRM_HOSTNAME

# apps/batch_controller
export BRM_BATCH_CONTROLLER_LOGLEVEL=1

# apps/preprocessor
export BRM_BATCH_PREPROCESSOR_LOGLEVEL=$BRM_LOGLEVEL
export BRM_ORACLE_HOSTNAME=$BRM_HOSTNAME
# apps/device_management
export BRM_DEVICE_MANAGEMENT_LOGLEVEL=$BRM_LOGLEVEL

# apps/load_price_list
export BRM_LOAD_PRICE_LIST_LOGLEVEL=$BRM_LOGLEVEL

# apps/pin_virtual_columns
export BRM_VCOL_LOGLEVEL=1
export BRM_VCOL_WORKER_THREADS=10

# apps/storable_class_to_xml
export BRM_STORABLE_CLASS_TO_XML_LOGLEVEL=$BRM_LOGLEVEL

# apps/telco
export BRM_TELCO_LOGLEVEL=$BRM_LOGLEVEL

# apps/uel
export BRM_UEL_LOGLEVEL=$BRM_LOGLEVEL

# sys/eai_js
export BRM_EAI_JS_LOGLEVEL=$BRM_LOGLEVEL
export BRM_EAI_JS_HOSTNAME=$BRM_HOSTNAME

# sys/formatter
export BRM_FORMATTER_LOGLEVEL=$BRM_LOGLEVEL
export BRM_FORMATTER_JS_LOGLEVEL=$BRM_LOGLEVEL
export BRM_FORMATTER_PXSLT_LOGLEVEL=$BRM_LOGLEVEL

export BRM_DATA_CONFIG_LOGLEVEL=$BRM_LOGLEVEL
export BRM_DIAGNOSTICS_PIN_ADU_VALIDATE_LOGLEVEL=$BRM_LOGLEVEL
export BRM_DM_AQ_LOGLEVEL=$BRM_LOGLEVEL
export BRM_DM_EAI_LOGLEVEL=$BRM_LOGLEVEL
export BRM_DM_IFW_SYNC_LOGLEVEL=$BRM_LOGLEVEL
export BRM_DM_PROV_TELCO_LOGLEVEL=$BRM_LOGLEVEL
export BRM_LOAD_CONFIG_LOGLEVEL=$BRM_LOGLEVEL
export BRM_LOAD_NOTIFICATION_EVENT_LOGLEVEL=$BRM_LOGLEVEL
export BRM_MTA_SAMPLE_LOGLEVEL=$BRM_LOGLEVEL
export BRM_MTA_SAMPLE_MTA_MAX_ERRS=5000
export BRM_MTA_SAMPLE_MTA_CHILDREN=5
export BRM_MTA_SAMPLE_MTA_PER_BATCH=500
export BRM_MTA_SAMPLE_MTA_PER_STEP=1000
export BRM_MTA_SAMPLE_MTA_FETCH_SIZE=5000
export BRM_PIN_AR_TAXES_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_BALANCE_TRANSFER_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_BILLD_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_BILL_HANDLER_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_BULK_ADJUST_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_CLEAN_ASOS_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_CLEAN_ASOS_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_CLEAN_RSVNS_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_CLEAN_RSVNS_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_CRYPT_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_EXPORT_PRICE_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_INV_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_MONITOR_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_MONITOR_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_OOD_HANDLER_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_OOD_HANDLER_PROCESS_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_RATE_CHANGE_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_REMIT_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_RERATE_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_RERATE_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_STATE_CHANGE_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_SUBSCRIPTION_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_SUBSCRIPTION_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_TRIAL_BILL_LOGLEVEL=$BRM_LOGLEVEL
export BRM_PIN_UNLOCK_SERVICE_LOGLEVEL=$BRM_LOGLEVEL

export BRM_GRID_INV_EXPORT_LOGLEVEL=$BRM_LOGLEVEL
export BRM_GRID_INV_ACCTS_LOGLEVEL=$BRM_LOGLEVEL

export BRM_GRID_INV_EXPORT_MTA_MAX_ERRS=5000
export BRM_GRID_INV_EXPORT_MTA_CHILDREN=5
export BRM_GRID_INV_EXPORT_MTA_PER_BATCH=500
export BRM_GRID_INV_EXPORT_MTA_PER_STEP=1000
export BRM_GRID_INV_EXPORT_MTA_FETCH_SIZE=5000
export BRM_GRID_INV_ACCTS_MTA_CHILDREN=5
export BRM_GRID_INV_ACCTS_MTA_PER_BATCH=500
export BRM_GRID_INV_ACCTS_MTA_PER_STEP=1000
export BRM_GRID_INV_ACCTS_MTA_FETCH_SIZE=5000
export BRM_GRID_INV_ACCTS_MTA_MAX_ERRS=5000

export BRM_DM_ACH_N_FE=1
export BRM_DM_ACH_MAX_PER_FE=16
export BRM_DM_ACH_N_BE=1
export BRM_DM_ACH_TRANS_BE_MAX=1
export BRM_DM_ACH_SHMSIZE=41943040
export BRM_DM_ACH_BIGSIZE=10485760
export BRM_DM_ACH_HOSTNAME=$BRM_HOSTNAME
export BRM_DM_ACH_LOGLEVEL=$BRM_LOGLEVEL

# other
export BRM_NAP_PASS='&aes|08|0D5E11BFDD97D2769D9B0DBFBD1BBF7E5D40C305EDF3D77DF111AAB8F781E92122'


# billing settings
export BILLING_PROD_ENV=0


#Introduced in 7.5PS18
export BRM_PIN_GENERATE_ANALYTICS_LOGLEVEL=2
export BRM_PIN_GENERATE_ANALYTICS_MTA_CHILDREN=5
export BRM_PIN_GENERATE_ANALYTICS_MTA_PER_BATCH=100
export BRM_PIN_GENERATE_ANALYTICS_MTA_PER_STEP=500
export BRM_PIN_GENERATE_ANALYTICS_MTA_FETCH_SIZE=5000

