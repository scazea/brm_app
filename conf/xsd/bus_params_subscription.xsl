<?xml version="1.0" encoding="UTF-8"?>
 <!--
  **************************************************************************
* Copyright (c) 2005, 2017, Oracle and/or its affiliates. All rights reserved.
  * This material is the confidential property of Oracle Corporation or its*
  * licensors and may be used, reproduced, stored or transmitted only in   *
  * accordance with a valid Oracle license or sublicense agreement.        *
  **************************************************************************
 -->
<xsl:stylesheet
    version="1.0"
    xmlns="http://www.portal.com/schemas/BusinessConfig"
    xmlns:bc="http://www.portal.com/schemas/BusinessConfig"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="bc">

  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="/">
    <BusinessConfiguration
        xmlns="http://www.portal.com/schemas/BusinessConfig"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.portal.com/schemas/BusinessConfig business_configuration.xsd">
      <BusParamConfiguration>
        <BusParamConfigurationList>
          <ParamClass desc="Business logic parameters for Subscription" name="subscription">
            <xsl:apply-templates select="/bc:BusinessConfiguration/bc:BusParamConfigurationClass/bc:BusParamsSubscription/bc:*"/>
          </ParamClass>
        </BusParamConfigurationList>
      </BusParamConfiguration>
    </BusinessConfiguration>
  </xsl:template>

  <xsl:template match="bc:DiscountBasedOnContractDaysFeature">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>discount_based_on_contract_days_feature</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
        Parameter to enable contract days counter feature. This needs to be set to 1 if the accounts contain the resource contract days counter.
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template match="bc:BestPricing">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>best_pricing</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
        Parameter to enable or disable best pricing feature. 1 means enabled.
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template match="bc:RolloverTransfer">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>rollover_transfer</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
        Parameter to enable or disable rollover transfer feature. 1 means enabled.
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

   <xsl:template match="bc:AutomatedMonitorSetup">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>automated_monitor_setup</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
        Parameter to enable or disable automated monitor setup. Affects processing logic.
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template match="bc:BillTimeDiscountWhen">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>bill_time_discount_when</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
        Parameter to indicate time to apply discount during billing. This discount could be applied either at each accounting cycle time or at the billing cycle time (i.e in the last accounting cycle in the case of multi-month billing cycle). The posible values are 0 (apply discount at each accounting cycle) and 1 (apply discount at billing cycle time). Default is 0.
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

 <xsl:template match="bc:ProductLevelValidation">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>enable_product_validation</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
        Parameter to enable or disable product level validation during Deal Dependency checks. 1 means enabled, 0 means disabled.
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

 <xsl:template match="bc:MaxServicesToSearch">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>max_services_to_search</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
         Parameter to configure the number of services to search in at a time during the retrieval of purchased offerings for a billinfo by the opcode PCM_OP_SUBSCRIPTION_GET_PURCHASED_OFFERINGS. Default is 5. Increasing the value might cause the select statement to fail, depending on the length of the poids involved in the search. 
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
        <xsl:variable name="value" select="normalize-space(text())"/>
        <xsl:element name="Value">
        <xsl:value-of select="$value"/>
      </xsl:element>
    </xsl:element>
  </xsl:template>

 <xsl:template match="bc:CancelFullDiscountImmediate">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>cancel_full_discount_immediate</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
        Parameter to enable or disable immediate cancellation of discount with proration setting of FULL. The posible values are 0(set end date to cycle end) and 1 (set end date and cancel the discount immediately). Default is 0.
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

 <xsl:template match="bc:TailormadeProductsSearch">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>tailormade_products_search</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
        Parameter to enable or disable the search for tailormade products during the application of cycle fees. 1 means enabled, 0 means disabled. This parameter should be disabled only if advanced customization of products has not been used.
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

 <xsl:template match="bc:CancelledOfferingsSearch">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>cancelled_offerings_search</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
        Parameter to enable or disable the search for cancelled offerings during billing. 1 means enabled, 0 means disabled.
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template match="bc:AllowBackdateNoRerate">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>allow_backdate_with_no_rerate</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
        Parameter to allow backdating beyond the number of billing cycles specified in the cm pin.conf entry 'fm_subs num_billing_cycles', but without creating  the auto-rerate job. The possible values are 0 (Do not allow backdating beyond 'num_billing_cycles') and 1 (Allow backdating beyond 'num_billing_cycles' without creating auto-rerate job). Default is 0. 
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template match="bc:SubsDis74BackDateValidations">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>subs_disable_74_backdated_validations</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
	Parameter to disable the backdated validations in 7.4. The possible values are 0 and 1. Default is 0. 
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template match="bc:TransferScheduledActions">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>transfer_scheduled_actions</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
        Parameter to trigger policy opcode PCM_OP_SUBSCRIPTION_POL_POST_TRANSFER_SUBSCRIPTION which will transfer /schedule objects to new account. The possible values are 0 (Do not trigger policy opcode) and 1 (Trigger policy opcode). Default is 0.
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

 <xsl:template match="bc:EventAdjustmentsDuringCancellation">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>event_adjustments_during_cancellation</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
	Parameter to allow accounting for event adjustments on cycle fees while calculating the refund during product inactivation or cancellation. The possible values are 0 (Do not consider event adjustments on the cycle fees while calculating refund) and 1 (Consider event adjustments on the cycle fees and refund based on original charge minus adjustments). Default is 0."
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
        <xsl:variable name="value" select="normalize-space(text())"/>
        <xsl:element name="Value">
        <xsl:value-of select="$value"/>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template match="bc:MultiplePlan">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>return_multiple_plan_instances_purchased</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
	           Parameter to return multiple instances of the plan purchased. The posible values are 0(Return a single instance of the plan purchased) and 1(Return all the instances of the plan purchased). Default is set to 0.
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;disabled&apos;">
            <xsl:text>0</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>1</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template match="bc:UsePrioritySubscriptionFees">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>use_priority_for_subscription_fees</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
        Parameter to make subscription charging use product priority while applying cycle fees (/event/billing/product/fee/cycle) during billing/deal purchase/cancel. This does not take care of discount priority or refunds due to any other operations like inactivations for now. MTA utilities like pin_cycle_fees does not support product prioritization when used with -defer_cancel or -cancel. This may impact performance if large number of offers are present, hence use judiciously. Possible values are 0 (do not use the priority) and 1 (use priority).
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template match="bc:RecreateDuringSubscriptionTransfer">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>recreate_during_subscription_transfer</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
	If this parameter is true then objects will be recreated in a cease+reprovide model. Tranfer across the schemas is possible only if this parameter is true.
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template match="bc:CreateTwoEventsInFirstCycle">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>create_two_events_in_first_cycle</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
	Parameter to create two events if the product end date is set in the first cycle. If this parameter is enabled, then there will be a charge for full cycle and then there will be a refund from product end to end of the cycle.By default this parameter will be disabled.
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template match="bc:ApplyProrationRules">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>apply_proration_rules</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
	Parameter to indicate whether proration rules need to be applied when purchase or cancellation occur exactly on cycle boundaries. By default this parameter is disabled and proration rules will not be applied in such cases. If enabled the rules will be applied.
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template match="bc:TimestampRounding">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>timestamp_rounding</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
	Rounds timestamps of purchased products and discounts. can have 2 values: 1(round timestamp) and (0 do not round timestamp)
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template match="bc:GetRatePlanFromCache">
    <xsl:element name="Param">
      <xsl:element name="Name">
        <xsl:text>get_rate_plan_from_cache</xsl:text>
      </xsl:element>
      <xsl:element name="Desc">
	During purchase product BRM will try to fetch all the rate plans available for the product from cache. If the RATE_PLANS are huge then it will cause performance issue during purchase product. If this parameter is disabled then BRM will not fetch RATE_PLANS thus increasing performance during purchase product which might impact billing performance. Default value is enabled
      </xsl:element>
      <xsl:element name="Type">INT</xsl:element>
      <xsl:element name="Value">
        <xsl:choose>
          <xsl:when test="normalize-space(text()) = &apos;enabled&apos;">
            <xsl:text>1</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>0</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>
