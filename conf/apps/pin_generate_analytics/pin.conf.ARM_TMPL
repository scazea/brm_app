#************************************************************************
# Configuration Entries for Logging Problems with the Billing Programs
#
#
# Use these entries to specify how the generate analytics applications log
# information about their activity.
#
# This configuration file is automatically installed and configured with
# default values during Portal installation. You can edit this file to:
#   -- change the default values of the entries.
#   -- disable an entry by inserting a crosshatch (#) at the start of
#        the line.
#   -- enable a commented entry by removing the crosshatch (#).
#
# Before you make any changes to this file, save a backup copy.
#
# When editing this file, follow the instructions in each section.
# For more information on the general syntax of configuration entries,
# see "Reference Guide to Portal Configuration Files" in the Portal
# online documentation.
#************************************************************************


#======================================================================
#
# You can edit this file to suit your specific configuration:
#  -- You can change the default values of an entry.
#  -- You can exclude an optional entry by adding the # symbol
#     at the beginning of the line.
#  -- You can include a commented entry by removing the # symbol.
#
# Before you make any changes to this file, save a backup copy.
#
# To edit this file, follow the instructions in the commented sections.
# For more information on the general syntax of configuration entries,
# see "Reference Guide to Portal Configuration Files" in the Portal
# online documentation.
#======================================================================

#=======================================================================
# ptr_virtual_time
# 
# Enables the use of pin_virtual_time to advance Portal time, and 
# specifies the shared file for use by all Portal mangers.
#
# The entry has 2 values
#
# #/Empty     	to disable / enable pin_virtual_time in all pin.conf files
#		default = #	(disable)
#
# <filepath> 	location and name of the pin_virtual_time file shared by all Portal managers.
#		default = ${BRM_BINS}/lib/pin_virtual_time_file
#
#=======================================================================
${BRM_VIRTUAL_TIME}

#========================================================================
# cm_ptr
# 
# Specifies a pointer to the CM or CMMP. 
#
# Use a separate entry for each CM or CMMP. If testnap can't find the
# first CM or CMMP, it looks for the next in the list.
#
# Each entry includes three values:
# 
#     <protocol> = "ip", for this release
#     <host>     = the name or IP address of the computer running the
#                    CM or CMMP
#     <port>     = the TCP port number of the CM or CMMP on this computer
#
# The port number should match a corresponding cm_ports entry with
# the same port number in the CM or CMMP configuration file. The 
# default, 11960, is a commonly specified port for the CM or CMMP.
#========================================================================
- nap cm_ptr ip ${BRM_CM_HOSTNAME} ${BRM_CM_PORT}


#========================================================================
# logfile
#
# Specifies the path to the log file for the sample application
#========================================================================
- sample_act    logfile sample_act.pinlog
- sample_del    logfile sample_del.pinlog
- sample_app    logfile sample_app.pinlog 
- sample_search logfile sample_search.pinlog 
- sample_who    logfile sample_who.pinlog


#========================================================================
# login_type
#
# Specifies whether the login name and password are required.
#
# The value for this entry can be:
#
#    0 = Only a user ID is required.
#    1 = (Default) Both a name and a password are required.
#
# By default, CMs require a user login and password when requesting an open
# context using PCM_CONTEXT_OPEN. However, you can remove this authentication
# requirement by configuring the CM with a cm_login_module of
# cm_login_null.so
#========================================================================
- nap login_type 1


#========================================================================
# login_name
#
# Specifies the login name to use when testnap connects to the CM. 
#========================================================================
- nap login_name ${BRM_NAP_LOGIN_NAME}


#========================================================================
# login_pw
#
# Specifies the password to use when testnap connects to the CM.
#========================================================================
- nap login_pw ${BRM_NAP_LOGIN_PW}


#========================================================================
# userid
#
# Specifies the database number and service type for the Portal
# database. 
#
# The CM uses the database number to identify the Portal database 
# and to connect to the correct Data Manager. For connections that don't
# require a login name and password, the CM also passes the service
# type to the database.
#
# The database number, in the form 0.0.0.N, is the number assigned to 
# your Portal database when you installed the system. The default
# is 0.0.0.1.
#
# The service type is /service/pcm_client and the ID is 1.
# DO NOT change these values.
#========================================================================
- - userid 0.0.0.1 /service/pcm_client 1


#========================================================================
# Deadlock Retry Count
#
# Specified the number of attempts to bill an account again in
# case of a deadlock error.
#
# This entry is required for SQL Server databases.  For other databases,
# this is an optional parameter.  For better performance this count
# should be set depending on the volumes of the accounts to be billed.
#
#========================================================================
- pin_generate_analytics  deadlock_retry_count  20


#========================================================================
# logfile
#
# Specifies the full path to the log file for the pin_generate_analytics 
# application.
#
# You can enter any valid path.
#========================================================================
- pin_generate_analytics logfile ${PIN_LOG_DIR}/pin_generate_analytics/pin_generate_analytics.pinlog


#========================================================================
# loglevel
#
# Specifies how much information the generate analytics application logs.
#
# The value for this entry can be:
#
#    0 = no logging
#    1 = (Default) log error messages only
#    2 = log error messages and warnings
#    3 = log error, warning, and debugging messages
#========================================================================
- pin_generate_analytics loglevel ${BRM_PIN_GENERATE_ANALYTICS_LOGLEVEL}


#=====================================================================
# hotlist
#
# Use this entry to set the location (path and filename) of the hot
# list file.
#
# A hot list is a file containing an array of poids, which need to be
# handled by the mta application's working threads first. This
# allows some threads to begin working on some known large or complex
# accounts/bills as early as possible. The array of poids might be
# hard-coded in the host list file, or possibly the file could be
# generated by some other application.
#
# The array of poids must be in flist format. Refer to the example
# files in the distribution for more information about this file.
#======================================================================
- pin_mta hotlist hotlist


#========================================================================
# mta_logfile
#
# Specifies the full path to the log file used by this MTA application.
#
# You can enter any valid path.
#========================================================================
- pin_mta logfile ${PIN_LOG_DIR}/pin_generate_analytics/pin_mta.pinlog


#======================================================================
# loglevel
#
# How much information the applications should log.
#
# The value for this entry can be:
#  -- 0: no logging
#  -- 1: log error messages only
#  -- 2: log error messages and warnings
#  -- 3: log error messages, warnings, and debugging messages
#======================================================================
- pin_mta loglevel ${BRM_PIN_GENERATE_ANALYTICS_LOGLEVEL}


#======================================================================
# monitor
#
# Use this entry to set the path and name of shared memory map file.
# This binary file stores information about the running mta application.
# With the help of another application (like pin_mta_monitor), you can
# view and modify the number of threads that are running in the MTA
# application without interrupting the application.
#
#======================================================================
- pin_mta monitor monitor


#======================================================================
# multi_db
#
# Enables or disables the multidatabase capability of MTA.
#
# If you enable multi_db, MTA uses global searching instead of the
# normal searching. The value for this entry can be:
#
#    0 = (Default) Disable global searching
#    1 = Enable global searching
#======================================================================
- pin_mta multi_db 0


#======================================================================
# Performance Parameters
#
# These parameters govern how the MTA applications pulls data from the
# database and tranfers it to the application space. They also
# define how many threads (children) are used to process the data in
# the application.
#
#  -- children:   number of threads used to process the accounts in the
#                 application
#  -- per_batch:  number of accounts processed by each child
#  -- per_step:   number of accounts returned in each database search
#  -- fetch_size: number of accounts cached in application memory
#                 before processing starts
#
# Not all application use all these performance parameters. For example,
# pin_inv_accts does not use per_batch.
#
# You can edit these entries to improve the performance of MTA
# applications. You typically need to configure these parameters
# differently for each application. To specify an entry for
# a specific MTA application, replace the generic "pin_mta"
# name with the specific name of the application. In the following
# example pin_inv_accts will use 50000 for the fetch size whereas other
# applications using this configuration file will use 30000 for the
# fetch_size :
#   - pin_mta          fetch_size  30000
#   - pin_inv_accts    fetch_size  50000
#
# For a complete explanation of setting these variables
# for best performance, see "Improving System Performance" in
# the online documentation.
#
#======================================================================
- pin_mta children ${BRM_PIN_GENERATE_ANALYTICS_MTA_CHILDREN}
- pin_mta per_batch ${BRM_PIN_GENERATE_ANALYTICS_MTA_PER_BATCH}
- pin_mta per_step ${BRM_PIN_GENERATE_ANALYTICS_MTA_PER_STEP}
- pin_mta fetch_size ${BRM_PIN_GENERATE_ANALYTICS_MTA_FETCH_SIZE}
