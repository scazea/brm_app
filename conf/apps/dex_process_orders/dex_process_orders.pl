#!/usr/bin/env perl
####################################################################
# This script will processes orders. Will do the following
# The sript calls the following in a infinite loop
# 1. Call the dex_process_orders MTA
# 2. Checks for if any hold lock is created, if created
#    sleep for predefined time and waits for lock to be removed 
# 3. Check if stop lock exists if exists, stops the program and exits 
####################################################################
use integer;
use POSIX qw(strftime);
use File::Basename;
use File::Copy;
use File::Path;
use pcmif;
use DBI;


$SLEEP = 15;
if("$ENV{BRM_ENV_TYPE}" eq "DEV"){
	$SLEEP = 30;
}else{
	$SLEEP = 600;
}
sub check_hold(){
	print "\n\tChecking for hold lock ...... \n";
	$HOLD_FILE = "$ENV{PIN_LOG_DIR}/dex_process_order/hold.lock";
	while ( -f $HOLD_FILE ){
		print "\n\tHold lock exists, Sleeping($SLEEP seconds) ...... \n";
		sleep $SLEEP;
	}
}

sub check_stop(){
	print "\n\tChecking for stop lock ...... \n";
	$HOLD_STOP = "$ENV{PIN_LOG_DIR}/dex_process_order/stop.lock";
	
	while ( -f $HOLD_STOP ){
		print "\n\tStop lock exists, Exiting ...... \n";
		exit 0;
	}
}

sub run_module
{
	my $exec = shift;
	my $directory = "$ENV{BRM_CONF}/apps/dex_process_orders";

	my $old_dir = $ENV{PWD};
	chdir($directory) or die "Invalid directory $directory";

	my $str_start_time = strftime("%c", localtime(pcmif::pin_perl_time()));
	print "\n** Start ($str_start_time)\n";
	print "* Executing: <$exec>...\n";


	my $start_time = time;
	system($exec);
	my $end_time = time;
	
	my $exit_value  = $? >> 8;
	my $signal_num  = $? & 127;
	my $dumped_core = $? & 128; 
	
	die "Error: Program failed to start" if $exit_value == 255;
	die "Error: Program exited becuase of signal" if $signal_num;
	die "Error: Program core dumped" if $dumped_core;
	die "Error: Program exited with nonzero status ($exit_value)" if $exit_value != 0;

	chdir($old_dir) or die "Invalid direcotry $directory";
	
	#time stats
	my $diff_time = $end_time - $start_time;
	
	my $seconds = $diff_time % 60;
	$diff_time /= 60;
	my $minutes = $diff_time % 60;
	$diff_time /= 60;
	my $hours = $diff_time;
	
	print "* Finished with <$exec>. \n";
	print "* Elapsed ($hours:$minutes:$seconds)\n";
	my $str_end_time = strftime("%c", localtime(pcmif::pin_perl_time()));
	print "** End ($str_end_time)\n";
}

do{
	run_module(dex_process_order);
	check_hold();
	check_stop();
}while(1);
