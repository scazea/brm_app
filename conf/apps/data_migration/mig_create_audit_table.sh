#Script to create audit table

echo "PROCESS STARTED"
RES=`$ORACLE_HOME/bin/sqlplus /@$BRM_DB_ALIAS << BEOF
set pages 0

--create migration Audit table

     create table DEX_MIG_AUDIT_PROCESS_T
        (program_type VARCHAR2(60),
        program_name VARCHAR2(60),
        data_file_directory VARCHAR2(255),
        source_file_name VARCHAR2(60),
        table_name VARCHAR2(60),
        total_records NUMBER,
        success_records NUMBER,
        failed_records NUMBER,
        log_directory VARCHAR2(255),
        log_filename VARCHAR2(60),
	error_directory VARCHAR2(255),
        error_file_name VARCHAR2(60),
	host VARCHAR2(60),
        user_name VARCHAR2(60),
        start_date DATE,
	end_date DATE,
        processing_time VARCHAR2(60),
        process_status VARCHAR2(60),
	batch_num NUMBER
        );

BEOF
`
echo $RES
echo "PROCESS COMPLETED"
