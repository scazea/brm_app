--OPTIONS (DIRECT=TRUE, SKIP=2)
OPTIONS (DIRECT=FALSE, SKIP=2, ERRORS=5000000)
load data
INTO TABLE DEX_MIG_OBL_ORDER_ITEM_D_T 
APPEND
FIELDS TERMINATED BY '|'
(
ACCOUNT_NO "TRIM(:ACCOUNT_NO)",
ORDER_ID  "TRIM(:ORDER_ID)",
VERSION_ID,
ACCOUNT_TYPE  "TRIM(:ACCOUNT_TYPE)",
EAID  "TRIM(:EAID)",
LEGACY_FULFILL_DATE "TRIM(:LEGACY_FULFILL_DATE)",
EITEM_ID "TRIM(:EITEM_ID)",
PARENT_EITEM_ID "TRIM(:PARENT_EITEM_ID)",
PRODUCT_CODE "TRIM(:PRODUCT_CODE)",
ITEM_DESCR "TRIM(:ITEM_DESCR)",
ACTION "TRIM(:ACTION)",
QUANTITY,
BILLING_BASIS,
MRC_OVERRIDE,
NRC_OVERRIDE,
START_DATE "TRIM(:START_DATE)",
END_DATE "TRIM(:END_DATE)",
CONTRACT_TERM,
TOTAL_CONTRACT_AMOUNT,
IS_PREPAID "TRIM(:IS_PREPAID)",
AUTH_CODE "TRIM(:AUTH_CODE)",
UDAC "TRIM(:UDAC)",
REC_ID SEQUENCE (MAX,1)
)
