--OPTIONS (DIRECT=TRUE, SKIP=2)
OPTIONS (DIRECT=FALSE, SKIP=2, ERRORS=5000000)
load data
INTO TABLE DEX_MIG_OBL_ORDER_T 
APPEND
FIELDS TERMINATED BY '|'
(
ORDER_ID  "TRIM(:ORDER_ID)",
VERSION_ID  "TRIM(:VERSION_ID)",
ACCOUNT_TYPE  "TRIM(:ACCOUNT_TYPE)",
EAID  "TRIM(:EAID)",
CMR_ID  "TRIM(:CMR_ID)",
KGEN_CUST_ID "TRIM(:KGEN_CUST_ID)",
MIGRATE_STATUS CONSTANT 0,
POID_DB CONSTANT 1,
POID_REV CONSTANT 1,
POID_TYPE CONSTANT "/dex_mig_obl_order",
CREATED_T EXPRESSION "DATE2UNIX(SYSDATE)",
MOD_T EXPRESSION "DATE2UNIX(SYSDATE)",
WRITE_ACCESS CONSTANT 'L',
READ_ACCESS CONSTANT 'L',
POID_ID0 SEQUENCE (MAX,1)
)
