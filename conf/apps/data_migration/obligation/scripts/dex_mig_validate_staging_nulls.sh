#!/bin/bash
date_tmstamp=`date +%s`
oldest_end_t=`date -d "-24 month" +%s`
sys_date_now=$(date +"%Y%m%d%H%M%S")

echo `date` >staging_null_validation_report.bad
filename=`basename $0`
echo -e "$filename:PROCESS STARTED\n"
#echo "$filename:PROCESS STARTED" >>staging_null_validation_report.bad

for TABLE in DEX_MIG_OBL_ORDER_T DEX_MIG_OBL_ORDER_ITEM_D_T DEX_MIG_OBL_ORDER_ITEM_P_T DEX_MIG_OBL_ORDER_ITEM_UDACS_T 
do
COLUMNS=`$ORACLE_HOME/bin/sqlplus -s  $BRM_DATABASE_ID/$BRM_DATABASE_PWD@$BRM_DB_ALIAS<< BEOF
set pages 0
set feedback off
select column_name from user_tab_columns 
   where table_name = '$TABLE' and 
--	column_name <> 'OBJ_ID0' and
	column_name <> 'ERROR_NUM' and
	column_name <> 'BATCH_NUM' and
	column_name <> 'CMR_ID' and
	column_name <> 'ERROR_DESCR';
BEOF
`
for COLUMN in $COLUMNS 
do
	#echo "$COLUMN"
#	echo "select count(1) from $TABLE where $COLUMN is null;"
	COUNT_NULL_OBJ=`$ORACLE_HOME/bin/sqlplus -s  $BRM_DATABASE_ID/$BRM_DATABASE_PWD@$BRM_DB_ALIAS<< BEOF
	set pages 0

	select count(1) from $TABLE where $COLUMN is null;
BEOF
	`
	COUNT_NULL_OBJ=`echo $COUNT_NULL_OBJ|sed -e 's/ //g'`
        if [ $COUNT_NULL_OBJ -ne 0 ]
        then
                echo "Found $COUNT_NULL_OBJ row(s) with NULL $COLUMN in table $TABLE "
                echo "Found $COUNT_NULL_OBJ row(s) with NULL $COLUMN in table $TABLE " >>staging_null_validation_report.bad
        fi

done
done

FILE="staging_null_validation_report$sys_date_now.bad"
echo `date` >>staging_null_validation_report.bad
echo -e "\n$filename: PROCESS ENDED"
#echo "$filename:PROCESS ENDED" >>staging_null_validation_report.bad
echo `mv staging_null_validation_report.bad $FILE`

