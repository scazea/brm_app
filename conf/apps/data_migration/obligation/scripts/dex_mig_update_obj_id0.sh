########################################################################################
#Script to update obj_id and rec_id for subscription tables in staging
########################################################################################
process_date=`date +%Y%m%d%H%M%S`
MINS=60
START=$(date +%s.%3N)

echo "Updating staging tables obj_id0, rec_id and rec_id2"
$ORACLE_HOME/bin/sqlplus  -s /@$BRM_DB_ALIAS << HERE
--
-- merge DEX_MIG_OBL_ORDER_ITEM_D_T
--
merge into DEX_MIG_OBL_ORDER_ITEM_D_T mod
  using(
     select distinct order_id, poid_id0, version_id from DEX_MIG_OBL_ORDER_T) mo
on (mod.order_id = mo.order_id and mod.version_id = mo.version_id)
when matched then
   update set mod.obj_id0 = mo.poid_id0;
commit;
--
-- merge DEX_MIG_OBL_ORDER_ITEM_P_T
--
merge into DEX_MIG_OBL_ORDER_ITEM_P_T mop
  using(
     select distinct order_id, poid_id0, version_id from DEX_MIG_OBL_ORDER_T) mo
on (mop.order_id = mo.order_id and mop.version_id = mo.version_id)
when matched then
   update set mop.obj_id0 = mo.poid_id0;
commit;
--
-- merge DEX_MIG_OBL_ORDER_ITEM_UDACS_T
--
merge into DEX_MIG_OBL_ORDER_ITEM_UDACS_T mou
  using(
     select distinct order_id, obj_id0, version_id, rec_id, issue_date, issue_num from DEX_MIG_OBL_ORDER_ITEM_P_T) mop
on (mou.order_id = mop.order_id and 
	mou.version_id = mop.version_id and
	mou.issue_date = mop.issue_date and
	mou.issue_num = mop.issue_num)
when matched then
   update set mou.obj_id0 = mop.obj_id0,
	mou.rec_id = mop.rec_id;
commit;
exit;
HERE

#Finding processed time
END=$(date +%s.%3N)
TIME=$(echo "$END - $START" | bc)
TIMEM=$(echo "$TIME/$MINS" | bc -l)

end_date=`date +%Y%m%d%H%M%S`
#Insert Audit entry for update_mig_sub.sh
RESULT=`$ORACLE_HOME/bin/sqlplus  -s /@$BRM_DB_ALIAS << BEOF

insert into  DEX_MIG_AUDIT_PROCESS_T values('update_dex_mig_obl','dex_mig_update_obj_id0.sh','N/A','DEX_MIG_OBL_ORDER_ITEM_D_T','DEX_MIG_OBL_ORDER_ITEM_D_T',0,0,0,'N/A','N/A','N/A','N/A','$HOST','$USER',to_date('$process_date','YYYY:MM:DD HH24:MI:SS'),to_date('$end_date','YYYY:MM:DD HH24:MI:SS'),'$TIMEM Minutes','SUCCESSFUL','');

commit;
BEOF
`
