#!/bin/bash
date_tmstamp=`date +%s`
oldest_end_t=`date -d "-24 month" +%s`
sys_date_now=$(date +"%Y%m%d%H%M%S")

#echo "$0:PROCESS STARTED" >>staging_extra_recs_validation_report.bad

#
# Check for extra rows in DEX_MIG_OBL_ORDER_ITEM_D_T
#
COUNT1=`$ORACLE_HOME/bin/sqlplus -s  $BRM_DATABASE_ID/$BRM_DATABASE_PWD@$BRM_DB_ALIAS<< BEOF
set pages 0
set feedback off
select count(1) from 
(select order_id, version_id, account_type, eaid from DEX_MIG_OBL_ORDER_ITEM_D_T
minus
select order_id, version_id, account_type, eaid from DEX_MIG_OBL_ORDER_T);
BEOF
`

#
# Check for extra rows in DEX_MIG_OBL_ORDER_ITEM_P_T
#
COUNT2=`$ORACLE_HOME/bin/sqlplus -s  $BRM_DATABASE_ID/$BRM_DATABASE_PWD@$BRM_DB_ALIAS<< BEOF
set pages 0
set feedback off
select count(1) from 
(select order_id, version_id, account_type, eaid from DEX_MIG_OBL_ORDER_ITEM_P_T
minus
select order_id, version_id, account_type, eaid from DEX_MIG_OBL_ORDER_T);
BEOF
`
#
# Check for extra rows in DEX_MIG_OBL_ORDER_ITEM_UDACS_T
#
COUNT3=`$ORACLE_HOME/bin/sqlplus -s  $BRM_DATABASE_ID/$BRM_DATABASE_PWD@$BRM_DB_ALIAS<< BEOF
set pages 0
set feedback off
select count(1) from 
(select order_id, version_id, account_type, eaid from DEX_MIG_OBL_ORDER_ITEM_UDACS_T
minus
select order_id, version_id, account_type, eaid from DEX_MIG_OBL_ORDER_T);
BEOF
`
#
# Check for extra rows in DEX_MIG_OBL_ORDER_ITEM_UDACS_T not in DEX_MIG_OBL_ORDER_ITEM_P_T
#
COUNT4=`$ORACLE_HOME/bin/sqlplus -s  $BRM_DATABASE_ID/$BRM_DATABASE_PWD@$BRM_DB_ALIAS<< BEOF
set pages 0
set feedback off
select count(1) from 
(select order_id, version_id, account_type, eaid, account_no from DEX_MIG_OBL_ORDER_ITEM_UDACS_T
minus
select order_id, version_id, account_type, eaid, account_no from DEX_MIG_OBL_ORDER_ITEM_P_T);
BEOF
`
#
# Check for extra rows in DEX_MIG_OBL_ORDER_T not in DEX_MIG_OBL_ORDER_ITEM_D_T or DEX_MIG_OBL_ORDER_ITEM_P_T
#
COUNT5=`$ORACLE_HOME/bin/sqlplus -s  $BRM_DATABASE_ID/$BRM_DATABASE_PWD@$BRM_DB_ALIAS<< BEOF
set pages 0
set feedback off
select count(1) from
((select order_id, version_id, account_type, eaid from DEX_MIG_OBL_ORDER_T
minus
select order_id, version_id, account_type, eaid from DEX_MIG_OBL_ORDER_ITEM_D_T)
union
(select order_id, version_id, account_type, eaid from DEX_MIG_OBL_ORDER_T
minus
select order_id, version_id, account_type, eaid from DEX_MIG_OBL_ORDER_ITEM_P_T));
BEOF
`
#
# Check for extra rows in DEX_MIG_OBL_ORDER_ITEM_P_T not in DEX_MIG_OBL_ORDER_ITEM_UDACS_T
#
COUNT6=`$ORACLE_HOME/bin/sqlplus -s  $BRM_DATABASE_ID/$BRM_DATABASE_PWD@$BRM_DB_ALIAS<< BEOF
set pages 0
set feedback off
select count(1) from 
(select order_id, version_id, account_type, eaid, account_no from DEX_MIG_OBL_ORDER_ITEM_P_T
minus
select order_id, version_id, account_type, eaid, account_no from DEX_MIG_OBL_ORDER_ITEM_UDACS_T);
BEOF
`
if [ $COUNT1 -ne 0 ] || [ $COUNT2 -ne 0 ] || [ $COUNT3 -ne 0 ] || [ $COUNT4 -ne 0 ] || [ $COUNT5 -ne 0 ] || [ $COUNT6 -ne 0 ]
then
   name=`basename $0`;
   echo -e "\n$name:PROCESS STARTED"
   echo `date` >staging_extra_recs_validation_report.bad

   if [ $COUNT1 -ne 0 ]
   then
      echo -e "\nFound $COUNT1 extra row(s) in DEX_MIG_OBL_ORDER_ITEM_D_T not in DEX_MIG_OBL_ORDER_T "
      echo -e "\nFound $COUNT1 extra row(s) in DEX_MIG_OBL_ORDER_ITEM_D_T not in DEX_MIG_OBL_ORDER_T " >>staging_extra_recs_validation_report.bad
   fi

   if [ $COUNT2 -ne 0 ]
   then
      echo -e "\nFound $COUNT2 extra row(s) in DEX_MIG_OBL_ORDER_ITEM_P_T not in DEX_MIG_OBL_ORDER_T "
      echo -e "\nFound $COUNT2 extra row(s) in DEX_MIG_OBL_ORDER_ITEM_P_T not in DEX_MIG_OBL_ORDER_T " >>staging_extra_recs_validation_report.bad
   fi

   if [ $COUNT3 -ne 0 ]
   then
      echo -e "\nFound $COUNT3 extra row(s) in DEX_MIG_OBL_ORDER_ITEM_UDACS_T not in DEX_MIG_OBL_ORDER_T "
      echo -e "\nFound $COUNT3 extra row(s) in DEX_MIG_OBL_ORDER_ITEM_UDACS_T not in DEX_MIG_OBL_ORDER_T " >>staging_extra_recs_validation_report.bad
   fi

   if [ $COUNT4 -ne 0 ]
   then
      echo -e "\nFound $COUNT4 extra row(s) in DEX_MIG_OBL_ORDER_ITEM_UDACS_T not in DEX_MIG_OBL_ORDER_ITEM_P_T "
      echo -e "\nFound $COUNT4 extra row(s) in DEX_MIG_OBL_ORDER_ITEM_UDACS_T not in DEX_MIG_OBL_ORDER_ITEM_P_T " >>staging_extra_recs_validation_report.bad
   fi

   if [ $COUNT5 -ne 0 ]
   then
      echo -e "\nFound $COUNT5 extra row(s) in DEX_MIG_OBL_ORDER_T not in DEX_MIG_OBL_ORDER_ITEM_D_T or DEX_MIG_OBL_ORDER_ITEM_P_T"
      echo -e "\nFound $COUNT5 extra row(s) in DEX_MIG_OBL_ORDER_T not in DEX_MIG_OBL_ORDER_ITEM_D_T or DEX_MIG_OBL_ORDER_ITEM_P_T" >>staging_extra_recs_validation_report.bad
   fi

   if [ $COUNT6 -ne 0 ]
   then
      echo -e "\nFound $COUNT6 extra row(s) in DEX_MIG_OBL_ORDER_ITEM_P_T not in DEX_MIG_OBL_ORDER_ITEM_UDACS_T"
      echo -e "\nFound $COUNT6 extra row(s) in DEX_MIG_OBL_ORDER_ITEM_P_T not in DEX_MIG_OBL_ORDER_ITEM_UDACS_T" >>staging_extra_recs_validation_report.bad
   fi

   FILE="staging_extra_recs_validation_report$sys_date_now.bad"
   echo `date` >>staging_extra_recs_validation_report.bad
   echo -e "\n$name:PROCESS ENDED\n"
   echo `mv staging_extra_recs_validation_report.bad $FILE`
fi


