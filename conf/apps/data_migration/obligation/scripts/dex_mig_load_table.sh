#! /bin/bash

#######################################################################################################
# This script is generic shell script to load data in to the staging tables using dat,ctl files,takes two arguments Table name & File pattern.
# Specify dat & ctl files in specific folder with proper naming pattern
# Author : Dinesh Suthar
#######################################################################################################


if [ $# -ne 2 ]
then
  echo "Provide two arugments : table name & file pattern name !!!"
exit 1
fi

CTLDIR=$BRM_CONF/apps/data_migration/obligation/ctl
LOGDIR=$PIN_LOG_DIR/dex_data_migration/obligation
DATDIR=$BRM_CONF/apps/data_migration/obligation/dat
BADDIR=$LOGDIR
FILE_PREFIX='dex_mig' #File prefix for dat,ct,log files

table_name=$1
file_name=$2
file_name=$FILE_PREFIX\_$file_name


process_date=`date +%Y%m%d%H%M%S`
MINS=60

echo "Loading Table :: $table_name";
START=$(date +%s.%3N)
	
#Finding the Number of Records Intially present in $table_name table
BEFORE=`$ORACLE_HOME/bin/sqlplus -s /@$BRM_DB_ALIAS<< BEOF
set pages 0

select count(*) from $table_name;
BEOF
`
echo "COUNT BEFORE ::"$BEFORE;

#Loading Tables usinig SQL Loader
sqlldr /@$BRM_DB_ALIAS control=$CTLDIR/$file_name.ctl data=$DATDIR/$file_name.dat log=$LOGDIR/$file_name\_$process_date.log bad=$BADDIR/$file_name\_$process_date.bad
	
#Finding processed time
END=$(date +%s.%3N)
TIME=$(echo "$END - $START" | bc)
TIMEM=$(echo "$TIME/$MINS" | bc -l)
	
total_rec=0
#total_rec=`cat $DATDIR/$file_name.dat | wc -l `
total_rec=`grep -v "\#" $DATDIR/$file_name.dat | wc -l`
	
	
#Finding total number of Records successfully loaded
AFTER=`$ORACLE_HOME/bin/sqlplus -s /@$BRM_DB_ALIAS<< BEOF
set pages 0
select count(*) from $table_name;
BEOF
`
	
echo "COUNT AFTER ::"$AFTER;
LOADED_ACCTS=$(($AFTER-$BEFORE))
FAILED_ACCTS=$(($total_rec-$LOADED_ACCTS))

	
process_status='SUCCESSFUL'
badad='N/A'
if [[ $total_rec = 0 ]]; then
	process_status='NO DATA'
fi
	
if [[ $FAILED_ACCTS > 0 ]]; then
	process_status='FAILED'
	badad=$file_name-$process_date.bad
fi
	
end_date=`date +%Y%m%d%H%M%S`
host=`hostname`
	
#Audit log into DEX_MIG_AUDIT_PROCESS_T  
echo "Inserting in Audit Table";
RESULT=`$ORACLE_HOME/bin/sqlplus -s /@$BRM_DB_ALIAS<< BEOF
set pages 0
insert into  DEX_MIG_AUDIT_PROCESS_T values('obligation_load_staging_data','dex_mig_load_table.sh','$DATDIR','$file_name.dat','$table_name',$total_rec,$LOADED_ACCTS,$FAILED_ACCTS,'$LOGDIR','$file_name-$process_date.log','$LOGDIR','$badad','$host','$USER',to_date('$process_date','YYYYMMDDHH24MISS'),to_date('$end_date','YYYYMMDDHH24MISS'),round('$TIMEM',2),'$process_status',null);
commit;
BEOF
`
echo "$RESULT Process Status::$process_status 	Loaded count::$LOADED_ACCTS 	Failed count::$FAILED_ACCTS";
