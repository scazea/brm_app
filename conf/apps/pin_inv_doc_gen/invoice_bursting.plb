DECLARE
   v_ddl_stmt VARCHAR2(100);
   v_count NUMBER;
BEGIN
   SELECT COUNT(table_name) INTO v_count FROM user_tables WHERE table_name = 'INVOICE_CHUNK_IDS';
   IF v_count <>0 THEN
   v_ddl_stmt :=  'drop table INVOICE_CHUNK_IDS';
   EXECUTE IMMEDIATE v_ddl_stmt;
   END IF;
END;
/
SHOW errors;
CREATE  global temporary TABLE INVOICE_CHUNK_IDS
(
  count  NUMBER,
  first  NUMBER,
  last  NUMBER
) on commit delete rows ;
/
SHOW errors;
CREATE OR REPLACE PACKAGE InvTypes wrapped 
a000000
367
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
140 db
kLWH4dunbiaAYp0EZPH1lzziqG4wg/DILp7hf3QCTP6OMefLhY+8CoPJFE0uoV6VPWi1bwtY
LFIEr9iHHA+b5vFO6r81Ds5Q6hV00RPF5a9dzM69iTVQA0bWNybTXn0+bsnYsZ9hMzI0LR5i
vFrJiL8yfJ+aLwI7GkoUHmR1hqxVaCSJwdOU4L5PzOwykjwCICbpBlvCOQtUvAG/HQ/vzNs=


/
SHOW errors;
CREATE OR REPLACE PACKAGE INV wrapped 
a000000
367
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
17a 107
jbVtnIBctuMCCcBAl9I/HUxRJyUwgxDQrcvhf3TpJseenMqo0XxcBnVY50uASmo8XfnEyjVn
r4ubHnOVA8aQLL3TLK8XwcmUGlSJcBQKLo1Gw1LFNLp7MJiPsrDs6w34N9moyOIkTX6HO4Mk
Rlf5W0GEimWamNP98JpnjV7pPn6mCYq/h9jyFqsSVFu6NXL/E4BeGFQhbT9X6tbHBRURoQRZ
xblsVk9O/wcQKHQuuu1u8iuJa9bXBYfNAWoGORHMXgk=

/
SHOW errors;
CREATE OR REPLACE PACKAGE BODY INV wrapped 
a000000
367
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
1140 593
lcRmRqtMLkIN7Z8iC3guAomozWMwg83qTCAF345VA6pE/ny0g1/dvXx0TZym0+kx5mwlyibb
MZnuG0jBYksdgGJ8DiQ/nImiCaeJvrQcew45ibRdvprBCZwKL52yHzbpVryAPu3pawTwYNX/
fPSZ3FCsWkTZVpWi7gE1mtxgfV22AtOWjVqq7w6ms2bQ50+faeZAccU6q0ZQzjJnhuM0D4i6
Po2Rs1tditw4K2n0L9VdS7xUUqyI0A1lsjYdo8RTTWJjf9FTcDTviH16DdiIbjIDY0QzCs2+
lC0PK3RuWgsKU8A9+xPI/YUpvCahtgpVgCDis4qOiYTIZD76aoHdvMi+osSoxtnIDy7iyQOT
fCzpkfclzHiGjSlIGleOGlw1mMfEFI5MqL21e/S57sxIfl9E+uenEcS19bQAflxCCM+0AOVV
htjvJc/T/8AHT1G/mLCwVflzNY0RXyKquZfwDfZQNv/UDL4+JLzK51JpkQOq0jYcSgUuvhA2
tnixYNvfFZyhtaO96mLASk2yuaW5KJCUpTGV9seBJ462IQjp38bNvMO7T98sh1CyFGeENHPX
zJDeggSgI1dmJJKaxCiq5ZzBsEwLb5siVnQQdEfQuRIWfzPn3rTf+NRhZPB5hMP4rH34zptP
UEOb/Uz5lvW+NRfZkKrgcwzLNeBHHnK64NUOjXhXFKURMzDIXy09TYQNRt2gQwwVL9vsfxXt
wvp7jZS/pNJbl5Hhu320giUjIJgsJGvZ+YgdaflyHG4c76YBAMIF3iXT7Rz8xXg7zifkcej6
z1QKeEpBsaA+lEbZxdA7zB+KniE1oZcCblFWd6rwjaZAa9jKS94QkEV9sLyEr+RPpFf6u9j9
fvY1ThW6FYjWr5dcviAze6Hxufdk1XiQZy6qUzp6CMmlkn5zLXtl2oetlC7Hq0k1eJMLwvE+
x1ITOTjIntGStiUFGyM0Pcu/uWBLIvKeGA2yFFisJaK3OS3/ghDX43BiZ26FrF5MmOdjjdlY
YATzuupOLp3cPIZC8PN21acUrIXleovhGn8ktSVn4Kj4qJyyZoyZAkODPLRmBhYfnoqH2xS9
bVnlVtgO8F1Mhd7yH0z67KAZO6ZXeN5bli92Z7i+hWVTIrO0DDPbq0BuyUbRXSUfsiEL1t3n
oJeRlXyMxGfD+lmXugFfvV2M/0OB8JyLMxBpSd/C+nhpctbf30fNEAqvo66P+0EehheDMmqN
Y8CVjeUeXwc73UgtsjyH1VaKbCX+6CaMGUgMXI0Baa8N2LJ3oBGGg5Rk94Hgjyp+XsDG97vX
oBkFK9QkU7hknkL70JcJ8hsLwHoVRI/uS7WgAKeYc1vDHOXRFrUjGbwmyXSaYWSzqmKgy7dF
63Ex2luvj6hpbScV/y5qsWJMxCRQC/lVmiFNpOc=

/
