DECLARE
   v_ddl_stmt VARCHAR2(100);
   v_count NUMBER;
BEGIN
   SELECT COUNT(table_name) INTO v_count FROM user_tables WHERE table_name = 'INVOICE_CHUNK_IDS';
   IF v_count <>0 THEN
   v_ddl_stmt :=  'drop table INVOICE_CHUNK_IDS';
   EXECUTE IMMEDIATE v_ddl_stmt;
   END IF;
END;
/
SHOW errors;
CREATE  global temporary TABLE INVOICE_CHUNK_IDS
(
  count  NUMBER,
  first  NUMBER,
  last  NUMBER
) on commit delete rows ;
/
SHOW errors;
CREATE OR REPLACE PACKAGE InvTypes wrapped 
a000000
367
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
140 db
kLWH4dunbiaAYp0EZPH1lzziqG4wg/DILp7hf3QCTP6OMefLhY+8CoPJFE0uoV6VPWi1bwtY
LFIEr9iHHA+b5vFO6r81Ds5Q6hV00RPF5a9dzM69iTVQA0bWNybTXn0+bsnYsZ9hMzI0LR5i
vFrJiL8yfJ+aLwI7GkoUHmR1hqxVaCSJwdOU4L5PzOwykjwCICbpBlvCOQtUvAG/HQ/vzNs=


/
SHOW errors;
CREATE OR REPLACE PACKAGE INV wrapped 
a000000
367
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
17a 107
jbVtnIBctuMCCcBAl9I/HUxRJyUwgxDQrcvhf3TpJseenMqo0XxcBnVY50uASmo8XfnEyjVn
r4ubHnOVA8aQLL3TLK8XwcmUGlSJcBQKLo1Gw1LFNLp7MJiPsrDs6w34N9moyOIkTX6HO4Mk
Rlf5W0GEimWamNP98JpnjV7pPn6mCYq/h9jyFqsSVFu6NXL/E4BeGFQhbT9X6tbHBRURoQRZ
xblsVk9O/wcQKHQuuu1u8iuJa9bXBYfNAWoGORHMXgk=

/
SHOW errors;
CREATE OR REPLACE PACKAGE BODY INV wrapped 
a000000
367
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
ff8 55a
aEMVH4FSHsEnFhGzHdUCcKn6cjwwg83qeW0FVy+5weSeVQfxztFWMQKGDr5MSwGtfw1jyvqY
MT/zKUijCacaGt5rUCyq7zE8DIpdYXZ2v2P7uxslgiByR1qG6V8PxoKs06cZjG4ZUVR0OdZE
F3UalEkRYBqsAacLUaU0UWSAHfXSoeD0wfJdIOcWZ6DFthXGpInIf5zyQdYv7EFboEqF+AIt
jV6hyRBn/7mOyZSN5y3yQTycxdx8hiYr6DdwZx5vH/BjepWUH1+/FO/XXnDXj9gTN2+0ZMSw
K9s/k3y4Zq47BoNcW36ESKjG+BtJedifLO1FYLcLab01QV4J1Nm2+Oan01XUdnILPypF7Fkb
s/XIyJPcW0zU9utVWlJWWsg1mMd4SY5MqL21e6L+d8zefnREPuenEQNzwgV62D3uBoZITk3f
0LdKWvj6V9WkHLKzxCBMimmYHQK70j972MtPJKjnHhqFJ/h9vXqKsQpqkvNZaHH987rkmklz
F2JxxOmdFnfRkdq+u2s4fWyvfHo6xIgcjMQTsLDXM1W5Wzjajg2vzRk/UNipctGH2T0xFs/k
J50ZHHeYsD34TrOqrpeWqujktfGb/dJ+vj2Hc4SAQZOEZ1+tYe1sYanCm9E0dBxMABwacH5/
fYmjYHDypWzum8nNgcHiq8JHAUyYy5LxZVivVfEjYKz41HPOskwzmvCpm6ozj9KBWxYe91X/
A2fSksPqvro86obHWBfr66Ptf/xCLhNeNVPYQnRgIUvVUOcBrR/AXxhUtSorZlPbbRjd6IEy
ilEs+Mvp652/j+OaoXi0cDWxAsRGmTkdN9nAmke4ovG597VveJBnLqpTvmVOycaLfnMNImXa
h62UecerSTV4k+DCyK3HUhM5OGdEH3NG0mH0HNmgapBYHDxbk+ktYTcUXsjskXdmpwFrxoY2
PGbb9NhXPb4MaYeYcRFG8y1CKprSMsJ23P1fj1QtVraiPCl/1pjU5PmAmaECigdxYeOs/46n
DBMLbVq3CRZLEvfxVuKeJhhTQGHxv3+wrZAybBNRgeiTTb7MBDRnuKavaXGOfDPrd+8SkbId
sERrgq3+6e9MLcyRNqwhk39HfWKO5ZFAm8rwPvsiq9FkIHrsav4ELn7g+nhpctYv/ZwzAGuV
o66PN3uxxnJTHkAMDM0YDFyxiCi61lXg5zyH1VaK2dC7LhOA7aTyPVgByK9jfnQhAfMGFV94
hcl/m6thLr4K9w8OxVH5YWX5r20GUfEfpU1msJEhFPhzSNAcnwWWHnM4AxDQy5hFmcsPJOIP
PYJAIAWxfQ+Zrzgl9ewMEsHKV0LnIVFFcLDo7hOnSIMslwuSBpZTWZgU

/
