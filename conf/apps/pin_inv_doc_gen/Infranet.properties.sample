######################################################################
#
# Copyright (c) 2012, 2015, Oracle and/or its affiliates. All rights reserved.
#
#       This material is the confidential property of Oracle Corporation.
#       or its subsidiaries or licensors and may be used, reproduced, stored
#       or transmitted only in accordance with a valid Oracle license or
#       sublicense agreement.
######################################################################


# Password should be AES encrypted for security purposes
infranet.connection=pcp://root.0.0.0.1:__PASSWORD__@__CM_HOST__:__CM_PORT__/0.0.0.1 /service/admin_client 1
infranet.login.type=1
infranet.log.level=1

infranet.log.file=_PIN_INV_DOC_GEN.log

# Use HTTPS or HTTP protocol based on BIPublisher setup
infranet.bip.url=https://__BIP_SERVER__:__BIP_PORT__/xmlpserver
infranet.bip.username=__BIP_USERNAME__
# Password should be AES encrypted for security purposes
infranet.bip.password=__BIP_PASSWORD__


infranet.threadpool.size=3
infranet.threadpool.maxsize=5
infranet.threadpool.fetchsize=5


# The thread pool configuration for invoice search
# using in the BIP bursting mechanism
infranet.burst.threadpool.size=5
infranet.burst.threadpool.maxsize=10
infranet.burst.threadpool.chunksize=1000

# Parameter to specify the sleep in milliseconds
# between polling calls while waiting for the BIP
# schedule to complete. Default is 5000
infranet.polling.sleeptime=5000

# Threshold for Search arguments , when accts_list option
# used for running pin_inv_doc_gen.
# Not to exceed 14 due to DM limitation of 32 search arguments.
infranet.dmsearchargs.size=14

# Path for duplicate document generation
infranet.dupinvdir.path=/__DUPLICATE_DIR__/

# Background Watermark for duplicate invoices
infranet.dupinvoice.watermark.text=DUPLICATE
infranet.dupinvoice.watermark.coordx=200f
infranet.dupinvoice.watermark.coordy=250f
infranet.dupinvoice.watermark.colorr=0.7f
infranet.dupinvoice.watermark.colorg=0.7f
infranet.dupinvoice.watermark.colorb=0.7f
infranet.dupinvoice.watermark.font=Arial
infranet.dupinvoice.watermark.fontsize=48
infranet.dupinvoice.watermark.angle=45

# JNDI properties to connect to WLS server
# Use t3s (secure) or "t3" protocol based on your WLS/BIPublisher setup
infranet.PROVIDER_URL=t3s://<__BIP_SERVER__>:<__BIP_PORT__>
infranet.SECURITY_PRINCIPAL=weblogic
infranet.SECURITY_CREDENTIALS=__CREDENTIALS__
infranet.INITIAL_CONTEXT_FACTORY=weblogic.jndi.WLInitialContextFactory

# This option determines if a copy of the XML data is saved in the BIP database.
# By default this is set to false for performance reasons. 
#
infranet.saveXMLData=false

########################################################################
# Configuration parameters for HTTP notifications about jobs scheduled
# in BIPublisher by 'pin_inv_doc_gen' application
########################################################################

# The name of the HTTP server (configured in BIPublisher) used for notification
# (Do not set the below configuration to disable HTTP notifications)
infranet.http.notification.server=

# The user name for the HTTP server used for notification
infranet.http.notification.userName=__NOTIFY_USER_NAME__

# The HTTP notification server password when scheduling notification
# through an HTTP server
infranet.http.notification.password=__NOTIFY_PASSWORD__

# Configuration parameters for generating HTTP notifications for a
# BIPublisher job request that succeeds or completes with a warning.
# Note that a 'true' value indicates to send HTTP notification.
infranet.notify.http.when.success=true
infranet.notify.http.when.warning=true

########################################################
SSL configuration
########################################################
# To  enable SSL, make sure to provide correct values for below config entries. e.g wallet location and filename etc.
infranet.pcp.ssl.enabled = false
infranet.pcp.ssl.wallet.location = PIN_HOME/wallet/client
infranet.pcp.ssl.wallet.filename = cwallet.sso
