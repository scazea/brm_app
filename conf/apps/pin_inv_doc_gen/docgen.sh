#!/bin/sh
#
# Copyright (c) 2010, 2016, Oracle and/or its affiliates. All rights reserved.
#
# This material is the confidential property of Oracle Corporation or its
# licensors and may be used, reproduced, stored or transmitted only in
# accordance with a valid Oracle license or sublicense agreement.
#


DOC_GEN_HOME=__DOC_GEN_HOME__
export DOC_GEN_HOME

DG_LIB="$DOC_GEN_HOME/pin_inv_doc_gen.jar:$DOC_GEN_HOME/lib/xmlp/bip_jaxws_client.jar"
PCM_LIB="$BRM_BINS/jars/pcm.jar:$BRM_BINS/jars/pcmext.jar:$BRM_BINS/jars/oraclepki.jar:$BRM_BINS/jars/osdt_cert.jar:$BRM_BINS/jars/osdt_core.jar:$BRM_BINS/jars/httpclient-4.4.jar:$BRM_BINS/jars/commons-logging-1.2.jar"
WL_LIB="$DOC_GEN_HOME/lib/wls/ojdbc6.jar:$DOC_GEN_HOME/lib/wls/wlfullclient.jar"
XERCES_LIB="$DOC_GEN_HOME/lib/xerces/xercesImpl.jar"

XMLP_LIB="$DOC_GEN_HOME/lib/xmlp/xdo-core.jar"
XMLP_LIB="$XMLP_LIB:$DOC_GEN_HOME/lib/xmlp/commons-logging.jar"
XMLP_LIB="$XMLP_LIB:$DOC_GEN_HOME/lib/xmlp/jaxrpc.jar"
XMLP_LIB="$XMLP_LIB:$DOC_GEN_HOME/lib/xmlp/orawsdl.jar"
XMLP_LIB="$XMLP_LIB:$DOC_GEN_HOME/lib/xmlp/mail.jar"
XMLP_LIB="$XMLP_LIB:$DOC_GEN_HOME/lib/xmlp/commons-discovery.jar"
XMLP_LIB="$XMLP_LIB:$DOC_GEN_HOME/lib/xmlp/xmlef.jar"
XMLP_LIB="$XMLP_LIB:$DOC_GEN_HOME/lib/xmlp/xmlparserv2.jar"
XMLP_LIB="$XMLP_LIB:$DOC_GEN_HOME/lib/xmlp/xdo-server.jar"

CLASSPATH="$WL_LIB:$XMLP_LIB:$XERCES_LIB:.:$PCM_LIB:$DG_LIB"
export CLASSPATH

# echo CLASSPATH = $CLASSPATH

echo
echo "Executing " $JAVA_HOME/bin/java invoicedocgen.pin_inv_doc_gen -status pending "$@"
$JAVA_HOME/bin/java invoicedocgen.pin_inv_doc_gen -status pending $@ 

