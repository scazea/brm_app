<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="SERVICES.xsl"/>
<xsl:include href="common.xsl"/>

<xsl:template match="/">
<xsl:choose>
	<xsl:when test="not(action/params/@pay_type)"> <xsl:message terminate="yes">Not found: action/params/@pay_type</xsl:message> </xsl:when>
	<xsl:when test="not(action/params/@taxable)"> <xsl:message terminate="yes">Not found: action/params/@taxable</xsl:message> </xsl:when>
</xsl:choose>
<flist>
	<ACCOUNT_OBJ>0.0.0.1 /account -1 0</ACCOUNT_OBJ>
	<BILLINFO elem="0">
		<PAY_TYPE> <xsl:value-of select="action/params/@pay_type"/> </PAY_TYPE>
		<BILLINFO_ID>BILL UNIT(1)</BILLINFO_ID>
		<BILL_WHEN>1</BILL_WHEN>
		<xsl:if test="action/params/@parent" >
			<PARENT><xsl:value-of select="action/params/@parent" /></PARENT>
		</xsl:if>
		<POID>0.0.0.1 /billinfo -1</POID>
	</BILLINFO>
	<FLAGS>0</FLAGS>
	<LOCALES elem="0">
		<LOCALE>en_US</LOCALE>
	</LOCALES>
	<xsl:choose>
		<xsl:when test="action/params/@diff_email_phone">
			<NAMEINFO elem="1">
				<CONTACT_TYPE>Billing</CONTACT_TYPE>
			<xsl:call-template name="BILL_NAMEINFO"/>
			</NAMEINFO>
				<NAMEINFO elem="2">
				<CONTACT_TYPE>Service</CONTACT_TYPE>
				<xsl:call-template name="SRVC_NAMEINFO"/>
			</NAMEINFO>
		</xsl:when>
		<xsl:otherwise>
			<NAMEINFO elem="1">
				<CONTACT_TYPE>Billing</CONTACT_TYPE>
				<xsl:call-template name="NAMEINFO"/>
			</NAMEINFO>
			<NAMEINFO elem="2">
				<CONTACT_TYPE>Service</CONTACT_TYPE>
				<xsl:call-template name="NAMEINFO"/>
			</NAMEINFO>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:call-template name="PAYINFO"/>
	<xsl:for-each select="action/params/plan">
	<PLAN>
		<xsl:attribute name="elem"> <xsl:value-of select="position() - 1"/> </xsl:attribute>
		<PLAN_OBJ>
			<xsl:choose>
				<xsl:when test="@poid"><xsl:value-of select="@poid"/></xsl:when>
				<xsl:otherwise>
					<bunit_function name="findPoidByName">
						<arg>/plan</arg>
						<arg><xsl:value-of select="@name"/></arg>
					</bunit_function>
				</xsl:otherwise>
			</xsl:choose>
		</PLAN_OBJ>
	</PLAN>
	</xsl:for-each>
	<POID>0.0.0.1 /plan -1</POID>
	<PROGRAM_NAME>
		<xsl:choose>
			<xsl:when test="action/params/@program_name"> <xsl:value-of select="action/params/@program_name"/> </xsl:when>
			<xsl:otherwise>${PROGRAM_NAME}</xsl:otherwise>
		</xsl:choose>
	</PROGRAM_NAME>
	<xsl:for-each select="action/params/service">
	<SERVICES>
		<xsl:attribute name="elem"> <xsl:value-of select="position()"/> </xsl:attribute>
		<xsl:call-template name="SERVICES"/>
	</SERVICES>
	</xsl:for-each>

</flist>
</xsl:template>

</xsl:stylesheet>
