<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="common.xsl"/>
<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID>
	<xsl:choose>
	<xsl:when test="action/params/@account_obj"><xsl:value-of select="action/params/@account_obj"/></xsl:when>
	<xsl:otherwise><xsl:value-of select="$ACCOUNT_OBJ"/></xsl:otherwise>
	</xsl:choose>
	</POID>

	<PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
	<BILLINFO>
		<POID><xsl:value-of select="action/params/@billinfo_obj"/></POID>
		<ACTG_FUTURE_DOM><xsl:value-of select="action/params/@new_bdom"/></ACTG_FUTURE_DOM>
		<PAY_TYPE><xsl:value-of select="action/params/@pay_type"/></PAY_TYPE>
	</BILLINFO>
</flist>
</xsl:template>

</xsl:stylesheet>
