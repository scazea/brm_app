<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
    <POID>0.0.0.1 /service/sirius/esn -1</POID>
    <LOGIN><xsl:value-of select="action/params/@login"/></LOGIN>
</flist>
</xsl:template>
</xsl:stylesheet>
