<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="common.xsl"/>

<xsl:template match="/">
<xsl:choose>
	<xsl:when test="not(action/params/@pay_type)"> <xsl:message terminate="yes">Not found: action/params/@pay_type</xsl:message> </xsl:when>
</xsl:choose>

<flist>
	<xsl:choose>
		<xsl:when test="action/params/@account_obj">
			<POID><xsl:value-of select="action/params/@account_obj"/></POID>
		</xsl:when>
		<xsl:otherwise>
			<POID>${ACCOUNT_OBJ}</POID>
		</xsl:otherwise>
	</xsl:choose>

	<PROGRAM_NAME>${PROGRAM_NAME}</PROGRAM_NAME>
	
	<START_T><xsl:value-of select="action/params/@start_t"/></START_T>
	<END_T><xsl:value-of select="action/params/@end_t"/></END_T>

	<BATCH_INFO elem="0">
		<PIN_FLD_SEPARATOR>,</PIN_FLD_SEPARATOR>
		<STATUS_FLAGS>1</STATUS_FLAGS>
		<SUBMITTER_ID>pin_collect</SUBMITTER_ID>
		<BATCH_ID>T1,1c</BATCH_ID>
	</BATCH_INFO>
		
	<CHARGES elem="0">
		<xsl:choose>
			<xsl:when test="action/params/@account_obj">
				<ACCOUNT_OBJ><xsl:value-of select="action/params/@account_obj"/></ACCOUNT_OBJ>
			</xsl:when>
			<xsl:otherwise>
				<ACCOUNT_OBJ>${ACCOUNT_OBJ}</ACCOUNT_OBJ>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:call-template name="PAYINFO"/>
		
		<PAY_TYPE><xsl:value-of select="action/params/@pay_type"/></PAY_TYPE>
		<AMOUNT>25.90</AMOUNT>
		<MERCHANT>customer</MERCHANT>
		<CURRENCY>840</CURRENCY>
		<COMMAND>4</COMMAND>
		<ACTG_TYPE>2</ACTG_TYPE>
		<SELECT_STATUS>1</SELECT_STATUS>
		<SELECT_RESULT>0</SELECT_RESULT>
	</CHARGES>
</flist>
</xsl:template>

</xsl:stylesheet>
