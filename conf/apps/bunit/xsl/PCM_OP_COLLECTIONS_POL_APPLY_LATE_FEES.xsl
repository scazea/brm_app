<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID><xsl:value-of select="action/params/@collections_action_obj"/></POID>
	<BILLINFO_OBJ><xsl:value-of select="action/params/@billinfo_obj"/></BILLINFO_OBJ>
	<DUE><xsl:value-of select="action/params/@due"/></DUE>
	<PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
	<ACCOUNT_OBJ><xsl:value-of select="$ACCOUNT_OBJ"/></ACCOUNT_OBJ>
</flist>
</xsl:template>

</xsl:stylesheet>
