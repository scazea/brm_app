<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
    <POID>0.0.0.1 /service/gsm -1</POID>
    <PASSWD_CLEAR><xsl:value-of select="action/params/@passwd_clear"/></PASSWD_CLEAR>
    <PASSWD_STATUS><xsl:value-of select="action/params/@passwd_status"/></PASSWD_STATUS>
    <FLAGS><xsl:value-of select="action/params/@flags"/></FLAGS>
</flist>
</xsl:template>
</xsl:stylesheet>
