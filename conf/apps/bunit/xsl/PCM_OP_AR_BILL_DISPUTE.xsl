<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID><xsl:value-of select="action/params/@poid"/></POID>
	<AMOUNT><xsl:value-of select="action/params/@amount"/></AMOUNT>
	<PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
	<DESCR>Customer unhappy with charges</DESCR>
	<CURRENCY>840</CURRENCY>
</flist>
</xsl:template>

</xsl:stylesheet>
