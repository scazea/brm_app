<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
        <POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
        <PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
	<CHARGES elem="0">
		<ACCOUNT_OBJ><xsl:value-of select="$ACCOUNT_OBJ"/></ACCOUNT_OBJ>
		<COMMAND>0</COMMAND>
		<PAY_TYPE>${PAY_TYPE_CHECK}</PAY_TYPE>
		<CURRENCY>${SYSTEM_CURRENCY}</CURRENCY>
	</CHARGES>
</flist>
</xsl:template>

</xsl:stylesheet>
