<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID>0.0.0.1 / -1 0</POID>
	<xsl:choose>
		<xsl:when test="action/params/@mode='command'">
			<TAXPKG_TYPE>4</TAXPKG_TYPE>
			<COMMAND>1</COMMAND>
			<SHIP_TO><xsl:value-of select="action/params/@ship_to"/></SHIP_TO>
		</xsl:when>
		<xsl:when test="action/params/@mode='calculate'">
			<TAXES>
				<TAX_CODE>
					<xsl:choose>
						<xsl:when test="action/params/@tax_code"><xsl:value-of select="action/params/@tax_code"/></xsl:when>
						<xsl:otherwise>ct_direct</xsl:otherwise>
					</xsl:choose>
				</TAX_CODE>
				<AMOUNT_TAXED><xsl:value-of select="action/params/@amount_taxed"/></AMOUNT_TAXED>
				<SHIP_TO><xsl:value-of select="action/params/@ship_to"/></SHIP_TO>
				<SHIP_FROM><xsl:value-of select="action/params/@ship_to"/></SHIP_FROM>
				<ORDER_ACCEPT><xsl:value-of select="action/params/@ship_to"/></ORDER_ACCEPT>
				<ORDER_ORIGIN><xsl:value-of select="action/params/@ship_to"/></ORDER_ORIGIN>
				<LOCATION_MODE>
					<xsl:choose>
						<xsl:when test="action/params/@location_mode"><xsl:value-of select="action/params/@location_mode"/></xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</LOCATION_MODE>
			</TAXES>
		</xsl:when>
	</xsl:choose>
</flist>
</xsl:template>

</xsl:stylesheet>
