<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
	<PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
	<xsl:choose>
		<xsl:when test="action/params/@descr">
			<DESCR><xsl:value-of select="action/params/@descr"/></DESCR>
		</xsl:when>
		<xsl:otherwise>
			<DESCR>Special circumstances - BUnit</DESCR>
		</xsl:otherwise>
	</xsl:choose>
	<STATUSES>
		<xsl:choose>
			<xsl:when test="action/params/@status_flags">
				<STATUS_FLAGS><xsl:value-of select="action/params/@status_flags"/></STATUS_FLAGS>
			</xsl:when>
			<xsl:otherwise>
				<STATUS_FLAGS>4</STATUS_FLAGS>
			</xsl:otherwise>
		</xsl:choose>
		<STATUS><xsl:value-of select="action/params/@status"/></STATUS>
	</STATUSES>
</flist>
</xsl:template>

</xsl:stylesheet>
