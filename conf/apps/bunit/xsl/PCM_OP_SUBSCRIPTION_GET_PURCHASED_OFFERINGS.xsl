<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
	<xsl:choose>
		<xsl:when test="action/params/@account_obj">
			<POID><xsl:value-of select="action/params/@account_obj"/></POID>
		</xsl:when>
		<xsl:otherwise>
			<POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:choose>
		<xsl:when test="action/params/@scope_obj">
			<SCOPE_OBJ><xsl:value-of select="action/params/@scope_obj"/></SCOPE_OBJ>
		</xsl:when>
		<xsl:otherwise>
			<SCOPE_OBJ><xsl:value-of select="$ACCOUNT_OBJ"/></SCOPE_OBJ>
		</xsl:otherwise>
	</xsl:choose>
</flist>
</xsl:template>

</xsl:stylesheet>
