<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:include href="utils/variables.xsl"/>

<xsl:template match="/">
<flist>
        <POID><xsl:value-of select="$ACCOUNT_OBJ"/></POID>
        <BILLINFO_OBJ><xsl:value-of select="$BILLINFO_OBJ"/></BILLINFO_OBJ>
        <PROGRAM_NAME><xsl:value-of select="$PROGRAM_NAME"/></PROGRAM_NAME>
        <PROCESSING_TIME>1</PROCESSING_TIME>
</flist>
</xsl:template>

</xsl:stylesheet>
