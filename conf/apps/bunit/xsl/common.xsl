<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:param name="SMS_BRAND"/>

<xsl:template name="address">
	<xsl:choose>
		<xsl:when test="action/params/@taxable = '1'"> <xsl:call-template name="address_taxable_1"/> </xsl:when>
		<xsl:otherwise> <xsl:call-template name="address_taxable_0"/> </xsl:otherwise>
	</xsl:choose>
	<COUNTRY>USA</COUNTRY>
</xsl:template>

<xsl:template name="address_taxable_0">
        <xsl:choose>
                <xsl:when test="action/params/@change_address">
                        <ADDRESS>${NEW_ADDRESS_NO_TAX}</ADDRESS>
                        <CITY>${NEW_CITY_NO_TAX}</CITY>
                        <STATE>${NEW_STATE_NO_TAX}</STATE>
                        <ZIP>${NEW_ZIP_NO_TAX}</ZIP>
                </xsl:when>
		<xsl:when test="action/params/@alternate_address">
			<ADDRESS>100 SW 1st Ave</ADDRESS>
			<CITY>Houston</CITY>
			<STATE>TX</STATE>
			<ZIP>77001-4100</ZIP>
		</xsl:when>
                <xsl:otherwise>
                        <ADDRESS>5 Penn Plaza</ADDRESS>
                        <CITY>New York</CITY>
                        <STATE>NY</STATE>
                        <ZIP>10001</ZIP>
                </xsl:otherwise>
        </xsl:choose>
</xsl:template>

<xsl:template name="address_taxable_1">
        <xsl:choose>
                <xsl:when test="action/params/@change_address">
                        <ADDRESS>${NEW_ADDRESS_TAX}</ADDRESS>
                        <CITY>${NEW_CITY_TAX}</CITY>
                        <STATE>${NEW_STATE_TAX}</STATE>
                        <ZIP>${NEW_ZIP_TAX}</ZIP>
                </xsl:when>
                <xsl:otherwise>
                        <ADDRESS>100 SW 1st Ave</ADDRESS>
                        <CITY>Miami</CITY>
                        <STATE>FL</STATE>
                        <ZIP>33130</ZIP>
                </xsl:otherwise>
        </xsl:choose>
</xsl:template>

<xsl:template name="SRVC_NAMEINFO">
	<COMPANY/>
	<xsl:choose>
		<xsl:when test="action/params/@srvc_email_addr">
			<EMAIL_ADDR><xsl:value-of select="action/params/@srvc_email_addr"/></EMAIL_ADDR>
		</xsl:when>
		<xsl:otherwise>
			<!--COUNTRY>USA</COUNTRY-->
			<EMAIL_ADDR>DoNotReply@siriusxm.com</EMAIL_ADDR>
		</xsl:otherwise>
	</xsl:choose>
	<FIRST_NAME>SiriusXM</FIRST_NAME>
	<LAST_NAME>${SCENARIO_AND_ACTION_ID}</LAST_NAME>
	<PHONES elem="1">
		<PHONE>
			<xsl:choose>
				<xsl:when test="action/params/@srvc_phone_number"><xsl:value-of select="action/params/@srvc_phone_number"/></xsl:when>
				<xsl:otherwise>609-977-5369</xsl:otherwise>
			</xsl:choose>
		</PHONE>
		<TYPE>2</TYPE>
	</PHONES>
	<ZIP>
		<xsl:choose>
			<xsl:when test="action/params/@srvc_zip"><xsl:value-of select="action/params/@srvc_zip"/></xsl:when>
			<xsl:otherwise>10001</xsl:otherwise>
		</xsl:choose>
	</ZIP>
	<TITLE/>
	<ADDRESS>5 Penn Plaza</ADDRESS>
	<CITY>New York</CITY>
	<STATE>NY</STATE>
</xsl:template>
<xsl:template name="BILL_NAMEINFO">
	<COMPANY/>
	<xsl:choose>
		<xsl:when test="action/params/@bill_email_addr">
			<EMAIL_ADDR><xsl:value-of select="action/params/@bill_email_addr"/></EMAIL_ADDR>
		</xsl:when>
		<xsl:otherwise>
			<!--COUNTRY>USA</COUNTRY-->
			<EMAIL_ADDR>DoNotReply@siriusxm.com</EMAIL_ADDR>
		</xsl:otherwise>
	</xsl:choose>
	<FIRST_NAME>SiriusXM</FIRST_NAME>
	<LAST_NAME>${SCENARIO_AND_ACTION_ID}</LAST_NAME>
	<PHONES elem="1">
		<PHONE>
			<xsl:choose>
				<xsl:when test="action/params/@bill_phone_number"><xsl:value-of select="action/params/@bill_phone_number"/></xsl:when>
				<xsl:otherwise>609-977-5369</xsl:otherwise>
			</xsl:choose>
		</PHONE>
		<TYPE>1</TYPE>
	</PHONES>
	<TITLE/>
	<ZIP>
		<xsl:choose>
			<xsl:when test="action/params/@bill_zip"><xsl:value-of select="action/params/@bill_zip"/></xsl:when>
			<xsl:otherwise>10001</xsl:otherwise>
		</xsl:choose>
	</ZIP>
	<ADDRESS>5 Penn Plaza</ADDRESS>
	<CITY>New York</CITY>
	<STATE>NY</STATE>
</xsl:template>

<xsl:template name="NAMEINFO">
	<COMPANY/>
	<xsl:choose>
		<xsl:when test="action/params/@email_addr">
			<EMAIL_ADDR><xsl:value-of select="action/params/@email_addr"/></EMAIL_ADDR>
		</xsl:when>
		<xsl:when test="action/params/@no_email_addr">
		</xsl:when>
		<xsl:otherwise>
			<!--COUNTRY>USA</COUNTRY-->
			<EMAIL_ADDR>DoNotReply@siriusxm.com</EMAIL_ADDR>
		</xsl:otherwise>
	</xsl:choose>
	<FIRST_NAME>SiriusXM</FIRST_NAME>
	<xsl:choose>
		<xsl:when test="action/params/@last_name">
			<LAST_NAME><xsl:value-of select="action/params/@last_name"/></LAST_NAME>
		</xsl:when>
		<xsl:otherwise>
			<LAST_NAME>${SCENARIO_AND_ACTION_ID}</LAST_NAME>
		</xsl:otherwise>
	</xsl:choose>
	<PHONES elem="1">
		<PHONE>
		<xsl:choose>
			<xsl:when test="action/params/@phone_number"><xsl:value-of select="action/params/@phone_number"/></xsl:when>
			<xsl:otherwise>609-977-5369</xsl:otherwise>
		</xsl:choose>
		</PHONE>
		<TYPE>2</TYPE>
	</PHONES>
	<TITLE/>
	<xsl:call-template name="address"/>
	<xsl:choose>
		<xsl:when test="action/params/@zip">
			<ZIP><xsl:value-of select="action/params/@zip"/></ZIP>
		</xsl:when>
	</xsl:choose>

</xsl:template>

<xsl:template name="PAYINFO">
	<PAYINFO elem="0">
		<xsl:choose>
			<xsl:when test="action/params/@pay_type = '10001'"> 
				<xsl:call-template name="payinfo_10001"/>
			</xsl:when>
			<xsl:when test="action/params/@pay_type = '10003'"> 
				<xsl:call-template name="payinfo_10003"/>
			</xsl:when>
		</xsl:choose>
	</PAYINFO>
</xsl:template>

<xsl:template name="payinfo_10001">
	<PAY_TYPE>10001</PAY_TYPE>
	<xsl:choose>
		<xsl:when test="action/params/@inherited = '0'">
			<INV_INFO elem="0">
				<NAME>Sirius Subscriber</NAME>
				<xsl:call-template name="address"/>
			</INV_INFO>
		</xsl:when>
		<xsl:otherwise>
			<INHERITED_INFO>
				<INV_INFO elem="0">
					<NAME>Sirius Subscriber</NAME>
					<xsl:call-template name="address"/>
				</INV_INFO>
			</INHERITED_INFO>
		</xsl:otherwise>
	</xsl:choose>
	<POID>0.0.0.1 /payinfo/invoice -1 0</POID>
</xsl:template>

<xsl:template name="payinfo_10003">
	<PAY_TYPE>10003</PAY_TYPE>
	<xsl:choose>
		<xsl:when test="action/params/@inherited = '0'">
			<xsl:call-template name="CC_INFO"/>
		</xsl:when>
		<xsl:otherwise>
			<INHERITED_INFO>
				<xsl:call-template name="CC_INFO"/>
			</INHERITED_INFO>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:choose>
		<xsl:when test="action/params/@payinfo_obj > '0'">
			<xsl:value-of select="action/params/@payinfo_obj"/>
		</xsl:when>
		<xsl:otherwise>
			<POID>0.0.0.1 /payinfo/cc -1 0</POID>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="payinfo_10021">
	<xsl:param name="pay_type_flag1"/>
	<xsl:choose><xsl:when test="action/params/@no_bill_type_flag"/>
	<xsl:otherwise><BILL_TYPE>10021</BILL_TYPE></xsl:otherwise></xsl:choose>
	<xsl:if test="$pay_type_flag1='1'">
		<PAY_TYPE>10021</PAY_TYPE>
	</xsl:if>
	<POID>0.0.0.1 /payinfo/cc -1 0</POID>
</xsl:template>

<xsl:template name="payinfo_10023">
	<xsl:param name="pay_type_flag1"/>
	<xsl:choose><xsl:when test="action/params/@no_bill_type_flag"/>
	<xsl:otherwise><BILL_TYPE>10023</BILL_TYPE></xsl:otherwise></xsl:choose>
	<xsl:if test="$pay_type_flag1='1'">
		<PAY_TYPE>10023</PAY_TYPE>
	</xsl:if>
	<POID>0.0.0.1 /payinfo/prospect -1 0</POID>
</xsl:template>

<xsl:template name="CC_INFO">
	<CC_INFO elem="0">
		<DEBIT_EXP>
			<xsl:choose>
				<xsl:when test="action/params/@debit_exp"><xsl:value-of select="action/params/@debit_exp"/></xsl:when>
				<xsl:otherwise>0168</xsl:otherwise>
			</xsl:choose>
		</DEBIT_EXP>
		<DEBIT_NUM>
			<xsl:choose>
				<xsl:when test="action/params/@debit_num"><xsl:value-of select="action/params/@debit_num"/></xsl:when>
				<xsl:otherwise>4111111111111111</xsl:otherwise>
			</xsl:choose>
		</DEBIT_NUM>
                <!-- This is added just in case Payer name is desired to be updated -->
                <NAME>
                        <xsl:choose>
                                <xsl:when test="action/params/@payer_name"><xsl:value-of select="action/params/@payer_name"/></xsl:when>
                                <xsl:otherwise>Sirius Subscriber</xsl:otherwise>
                        </xsl:choose>
                </NAME>
		<xsl:choose>
			<xsl:when test="action/params/@input_address ='1'" >
				<ADDRESS><xsl:value-of select="action/params/@address"/></ADDRESS>
				<CITY><xsl:value-of select="action/params/@city"/></CITY>
				<STATE><xsl:value-of select="action/params/@state"/></STATE>
				<ZIP><xsl:value-of select="action/params/@zip"/></ZIP>
				<COUNTRY>USA</COUNTRY>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="address"/>
			</xsl:otherwise>
		</xsl:choose>

		<SECURITY_ID>
			<xsl:choose>
				<xsl:when test="action/params/@cvv2_cid"><xsl:value-of select="action/params/@cvv2_cid"/></xsl:when>
				<xsl:otherwise></xsl:otherwise>
			</xsl:choose>
		</SECURITY_ID>
	</CC_INFO>
</xsl:template>

</xsl:stylesheet>
