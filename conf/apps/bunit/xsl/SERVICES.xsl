<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:template name="SERVICES">
	<PASSWD_CLEAR>user_pass</PASSWD_CLEAR>
	<xsl:choose>
	<xsl:when test="@type = 'ip'"> <xsl:call-template name="SERVICES_ip"/> </xsl:when>
	<xsl:when test="@type = 'email'"> <xsl:call-template name="SERVICES_email"/> </xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template name="SERVICES_ip">
	<LOGIN> <bunit_function name="getDefaultLoginWithTimestamp"/> </LOGIN>
	<POID>0.0.0.1 /service/ip -1 0</POID>
	<DEAL_OBJ>
		<bunit_function name="findDealPoidForPlan">
			<arg>/service/ip</arg>
			<arg><xsl:value-of select="../plan/@name"/></arg>
		</bunit_function>
	</DEAL_OBJ>
	<SERVICE_OBJ>0.0.0.1 /service/ip -1 0</SERVICE_OBJ>
</xsl:template>

<xsl:template name="SERVICES_email">
	<LOGIN> <bunit_function name="getDefaultLoginWithTimestamp"/> </LOGIN>
   	<POID>0.0.0.1 /service/email -1 0</POID>
	<DEAL_OBJ>
		<bunit_function name="findDealPoidForPlan">
			<arg>/service/email</arg>
			<arg><xsl:value-of select="../plan/@name"/></arg>
		</bunit_function>
	</DEAL_OBJ>
  	<SERVICE_OBJ>0.0.0.1 /service/email -1 0</SERVICE_OBJ>
</xsl:template>

</xsl:stylesheet>
