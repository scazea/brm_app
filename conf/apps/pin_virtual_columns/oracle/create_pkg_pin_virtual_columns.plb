CREATE or REPLACE PACKAGE
pin_virtual_columns wrapped 
a000000
367
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
272 148
3bN6oVkTPN3QhKd2Ps5EmKKqYWgwg5Dxrycdf3SiEv/qfXXmmajtoEwjUIP8Kzvqh5FCqEm1
tQ/bSOwCyBtGzhQvTsk5Qub+1h2v2wnJK9kEGG2o0PGm6ydEpViViIA3N9jb+RNulK/3PDLR
ijWbtLcirPEBlHXMVR/8b3qIKYUV+rSvRhioaRYUtFv80fcxtcATr6yjtFsK2vrBWE+Pwj0A
KleNMXN54j1wf6tarpXG+SZay2uieQsAGJ039YRaCXlaWH9LZFBFglU1ENHyZY66cLsspTRV
foeHmIJVqF1cPbpeKmvOtSJc/ktbeB+wtg==

/
CREATE or REPLACE PACKAGE BODY pin_virtual_columns wrapped 
a000000
367
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
11d9 64e
bMkqe48+pbDGl817AlykoTrGkzMwg9dUeSAF3y8ZsV3qcSSTOSXbUusN48H7oKKYg5HyDhoh
OqIRLbHeDH5yn6P1fdCa91EU/GIIyEQT91fTQjTEwUHaZvstqhuCnraavjfdAQaXQ8xZejj4
h3oXqMilBMpFvRZT/8j3rZTH0+N+qEbjWXqe+Y7i58QkLyKgQCCaM68By+1AvSfS5u7C4/8W
LEMjGvFrHqAeOAjP5r6Bi+1rGlGY5YuXSOqMeSJRauM5q2uh+2ouEc5x55HLZ2r9PnxJseZJ
Z8ZEnAsEQQ80mDC1i/htv+9nqOS9UyBeSYsE4cfvXx2xG/WLZeQeWYtrpYYW44xe5JRB2U09
x+opIi65JZ0lPUm0GtUIrVW5LPHaUpi8ElMPzXC3aPn3z6Mz/GtBe3eM8w2Sl2HzXL7M8EdU
xvAmcjde/XM21duy2uGqm7e4kK/0ezZeuXKvUjbiBskWZuNYHoLKQVvQHMithAj5LcH4N9JC
nm+jez0lpWpT8XIioiwWt9GtdzPnsM5GMn8o2xq9MkEMhDlSfMLgowlJZfhYfVRgz8XpDcxp
T59wItP/cd+YRhzP+DnJps6DmFYrXIrAvfHXyVh+v7idGdE4tbrCrIH8Kx1bZ+3IJ9Y7Fvzt
0cpDB7Z4Gqw2N8GDFOZ8NkNowaeS1HGH8yiuZjaJ6eSLNiMnhtMlPRfYS0RW8RCslktU44GW
nZYg/totJGqcc7HX0yza5mIGg6qh8Lh11liNctKD6DfSYDuq2zThTgKEZmboP1/gYOWZhSMA
XGFNAfV+xfY+HQkgAPBmygmM91cKqb6j+zaH8NelxVyltVtjk95ygBpamt3bhnA8PNPqOE93
7JhMRL6U5XFxLCykDEIx5ktMjXMnEd/YSiIC+wsd/7Cad++CkflCs9BfpxY7KAxOwZSMgwuK
hn0D0srJYVnFkadG/Z5vEkHIFtluytSn9YdCjb/iInuklW6UC0evBu7Hd6tg4rL77rSuJgeV
a4tMAXCRyo7FUhbyEo1RPZdy/WJgIHVY/5QynJl23XA7MHzFARBoizjpTda2ZAcd5F123Ry+
TKqTr2QFgQCRDZYlkFQAaFX0gSsiBldxVsKSp7mfSbxAUF9gRRzD2ysrXiPAH8hdBKSsvVqG
lJH9giZGmS4A4adxlqVlSspqg70rbiFYp6Qn/2+3q03UluS5BqHcpS0/q1x5/IZVKuuj4fwK
WVmxX0PZdzT52PH4Q4hh2y7xSAgQsdXs2JE4McAoDIPr3goUuQ9ZTBKbR9IAWN2EwfKL02WX
PYYUhvMamQBX9UF3tEv5z1RInSsbLpp1HO/wbvUD1A6Kl7Tmlv4tQbAgl3JIQqntUVS3vweS
FZO7oVEAZH/5W+k/qYyWuuYlzIzjlwXGXgF3Vqih8Nb4Tr316nck3jGQEDHQexLHmAtz4sWX
pHmO3P3WDmVRuMDupBRO/sgShGIUw59GOPpU7PWoMdghzgkPPufHifbewfUsP0Y38UbCXQ/o
oBgVgE/1G42+ioh3bfq9ENYKuFHU40oMWUICreW2wZXhXsx1yO+ojuz84ju5dQkaQ5rfjLv5
OTQa8Vk=

/
SHOW errors;
