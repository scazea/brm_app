/*****************************************************************
 * Copyright (c) 2000, 2014, eBS and/or its affiliates. 
 * All rights reserved. 
 *  This material is the confidential property of eBS  
 *  or its subsidiaries or licensors and may be used, reproduced, stored
 *  or transmitted only in accordance with a valid eBS license or 
 *  sublicense agreement.
 *******************************************************************/

/***********************************************************
 *   dm_ebstax.c						*
 ***********************************************************/
#include <stdio.h>
#include "pinlog.h"
#include "pin_errs.h"
#include "pcm.h"
#include "dm_sm.h"
#include "dm_debug.h"

#include <curl/curl.h>

#define CURL_OUTPUT_LEN 1048576
#define CURL_ERRBUF_LEN 524288
#define XML_OUTPUT_LEN 2054

/* Global Variables - must free any allocated memory  */
CURL *curlHandle;
char *curlOutput;
char *curlErrBuf;
char *cnfHttpUrl;

size_t write_offset = 0;

size_t dm_ebstax_write(void *buffer, size_t sz, size_t nmemb,
        void *stream);

void dm_ebstax_send_request(
	char *String,
	pin_errbuf_t *ebufp);

/*********************************************************
 *	dm_if_init_process()				   *
 *	This routine starts the initial process		*
 *
 *********************************************************/

PIN_EXPORT void
dm_if_init_process(struct dm_sm_config *confp, int32 *errp)
{
	char	msg[2056]="";
	int32	*lvl=NULL;
	int32	*timeout = NULL;

	/* set the log level */
	pin_conf("dm", "loglevel", PIN_FLDT_INT, (caddr_t *)&lvl, errp);
	if(*errp != PIN_ERR_NONE) {
		sprintf(msg, "Initialize failed : loglevel not found errp = %d", *errp);
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_WARNING, msg);
		lvl = malloc(sizeof(int32));
		*lvl=PIN_ERR_LEVEL_ERROR;
	}
	PIN_ERR_SET_LEVEL(*lvl);

	sprintf(msg, "Initialize : dm_loglevel = %d", *lvl);
	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_WARNING, msg);

	pin_conf("dm", "dm_trans_timeout", PIN_FLDT_INT, (caddr_t *)&timeout, errp);
	if (*errp == PIN_ERR_NONE) {
		curl_easy_setopt(curlHandle, CURLOPT_TIMEOUT, (long)(*timeout) * 60);
	}
	else {
		// Default to 600, 10 mins.;
		curl_easy_setopt(curlHandle, CURLOPT_TIMEOUT, (long)600);
	}
	
		/*dm_http_url*/
	pin_conf("dm", "tax_gw", PIN_FLDT_STR,
		(caddr_t *)&cnfHttpUrl, errp);
	
	if (*errp != PIN_ERR_NONE) {
		sprintf(msg, "Initialize failed: %s errp = %d , %s", "tax_gw", *errp, cnfHttpUrl);
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_ERROR, msg);
		return;
	}

	sprintf(msg, "Initialize: %s = %s", "tax_gw", cnfHttpUrl);
	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_WARNING, msg);

	if (timeout) 
		free(timeout);
	/*
	 * Initialize curl hadle
	 */
	curlHandle = curl_easy_init();
	if (curlHandle==NULL) {
		sprintf(msg, "Initialize failed : failed to initialize curl %d", *errp);
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_ERROR, msg);
		return;
	}
	curlOutput = malloc (CURL_OUTPUT_LEN);
	curlErrBuf = malloc (CURL_ERRBUF_LEN);

	curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, dm_ebstax_write);
	curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, curlOutput);
	curl_easy_setopt(curlHandle, CURLOPT_ERRORBUFFER, curlErrBuf);
	curl_easy_setopt(curlHandle, CURLOPT_URL, cnfHttpUrl);
	curl_easy_setopt(curlHandle, CURLOPT_POST, 1);
	/* Debug */
	if (*lvl >= PIN_ERR_LEVEL_DEBUG) curl_easy_setopt(curlHandle, CURLOPT_VERBOSE, 1);

	free(lvl);

	return;
}

/***********************************************************
 * dm_if_process_op()					  *
 * This routine processes an operation that comes from	 *
 * the cm						  *
 ***********************************************************/
PIN_EXPORT void
dm_if_process_op(struct dm_sm_info *dsip,
	u_int		pcm_op,
	u_int		pcm_flags,
	pin_flist_t	*in_flistp,
	pin_flist_t	**out_flistpp,
	pin_errbuf_t	*ebufp)
{ 
	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	char msg[2056]="";
        char *strp = NULL;
        int32 strsize = 0;

	/***********************************************************
	 * Copy the flist back.
	 ***********************************************************/
	*out_flistpp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, *out_flistpp,
		PIN_FLD_POID, ebufp);

	switch (pcm_op) {
		case PCM_OP_SEARCH:
			
			PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,"dm_if_process_op: Got PCM_OP_SEARCH", in_flistp);
	
			/* Convert Input flist to String */
			PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "dm_if_process_op: converting to String - in_flistp", in_flistp);
			PIN_FLIST_TO_STR(in_flistp, &strp, &strsize, ebufp);
	
			/* Send data */
			dm_ebstax_send_request(strp, ebufp);
			if (PIN_ERR_IS_ERR(ebufp)) {
				PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, "dm_if_process_op: Error after dm_ebstax_send_request", ebufp);
			}
	
			/* Convert String response to FLIST */
			
			PIN_STR_TO_FLIST(curlOutput, 1, out_flistpp, ebufp);	
			
			// Seems like CURL frees this
			//if(strp) free (strp);
			break;
		default:
			sprintf(msg, "dm_if_process_op: unrecognized command: %d", pcm_op);
			PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,msg, in_flistp);
			break;
	}


	/***********************************************************
	 * Log something so we know we got called.
	 ***********************************************************/
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"dm_if_process_op return flist", *out_flistpp);

	/***********************************************************
	 * Check for error
	 ***********************************************************/
	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF( PIN_ERR_LEVEL_ERROR,
			"dm_if_process_op error", ebufp);
	}

	return;
}

/*******************************************************************
 * dm_if_terminate_connect():
 *
 *	This routine is called to clean up the CM connection
 *
 *******************************************************************/
PIN_EXPORT void
dm_if_terminate_connect(struct dm_sm_info *dsip,
	u_int *errp)
{

	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG,"dm_if_terminate_connect");

	if (dsip->pvti != 0) {

		/* abort transaction */

		dsip->pvti = 0;
	}

	*errp = PIN_ERR_NONE;

	return;
}

/*******************************************************************
 * dm_if_terminate_process():
 *
 *	This routine is called when the DM is terminated.
 *
 *******************************************************************/
PIN_EXPORT void
dm_if_terminate_process(u_int *errp)
{
	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG,"dm_if_terminate_process: shutting down...");
	*errp = PIN_ERR_NONE;

	curl_easy_cleanup(curlHandle);
        curl_global_cleanup();
	free(curlOutput);
	free(curlErrBuf);
	free(cnfHttpUrl);

	return;
}


/*******************************************************************
 * DM_CS functions
 *******************************************************************/


/*
 * dm_ebstax_send_request - sends request using cURL
   Adds necessary headers and parameters necessary
 */
void dm_ebstax_send_request(
	char *String, 
	pin_errbuf_t *ebufp) 
{
	CURLcode res;
	struct curl_slist *headers = NULL;


	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG,"dm_ebstax sending request...");

	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG,String);
        headers = curl_slist_append(headers, "Accept: application/text");
        headers = curl_slist_append(headers, "Content-Type: application/text");
        curl_easy_setopt(curlHandle, CURLOPT_HTTPHEADER, headers);
	curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, String);
	curl_easy_setopt(curlHandle, CURLOPT_URL, cnfHttpUrl);

	memset(curlOutput, 0, CURL_OUTPUT_LEN);
	memset(curlErrBuf, 0, CURL_ERRBUF_LEN);
	write_offset = 0;
	res = curl_easy_perform(curlHandle);

	if (curlOutput!=NULL) {
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG,"dm_ebstax_send_request: cURL output");
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG,curlOutput);
	} else {
		pin_set_err(ebufp, PIN_ERRLOC_FM,
			PIN_ERRCLASS_SYSTEM_DETERMINATE,
			PIN_ERR_IS_NULL, PIN_FLD_RESPOND_STATUS, 0, 0);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"dm_ebstax_send_request error: NULL cURL output", ebufp);
	}

	if (curlErrBuf != NULL && res != CURLE_OK) {
		char dsc[CURL_ERRBUF_LEN+64];
		sprintf(dsc, "dm_ebstax_send_request: cURL error (%d - %s)", res, curlErrBuf);
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, dsc);
	}

	curl_slist_free_all(headers);
}

/*
 * dm_ebstax_write - a function to write the response from the cURL
   request 
 */
size_t dm_ebstax_write(void *buffer, size_t sz, size_t nmemb,
        void *stream) 
{
    /* stream above should be the global output */
    memcpy((char *)stream + write_offset, buffer, sz * nmemb);
    write_offset += sz * nmemb;

    return sz * nmemb;

} 
   
