/******************************************************************************
 *  
 *   Copyright (c) 2017 DexYP. All rights reserved.
 *    
 *   This material is the confidential property of DexYP
 *   or its licensors and may be used, reproduced, stored or transmitted
 *   only in accordance with a valid DexYP license or sublicense agreement.
 *    
 ******************************************************************************/
/*******************************************************************************
 *   Maintentance Log:
 *   
 *   Date: 20171013
 *   Author: Ebillsoft
 *   Identifier: N/A
 *   Notes: Configuration file for DexYP AR opcodes 
 *******************************************************************************/
/*******************************************************************************
 *   File History
 *   
 *   DATE   |  CHANGE                                           |  Author
 ***********|***************************************************|****************
 |10/08/2017|Automatic adjustment of small balance              | Rama Puppala
 |**********|***************************************************|****************
 |10/13/2017|Apply Late fees			                | Rama Puppala
 |**********|***************************************************|****************
 |10/21/2017|Move clients between CMR accounts			| Rama Puppala
 |**********|***************************************************|****************
 |10/21/2017|Move credits between clients			| Rama Puppala
 |**********|***************************************************|****************
 |10/21/2017|Move chargs between accounts			| Rama Puppala
 *********************************************************************************/
#include <stdio.h>
#include "pcm.h"
#include "cm_fm.h"
#include "ops/cust.h"
#include "dex_ops.h"
#include "dex_flds.h"

PIN_EXPORT void * fm_dex_bill_config_func();

struct cm_fm_config fm_dex_bill_config[] = {
	/* opcode-id (u_int), function name (string) */
	{ DEX_OP_SMALL_BAL_AUTO_ADJ, "op_small_bal_auto_adj" },
	{ DEX_OP_APPLY_LATE_FEES, "op_apply_late_fees" },
	{ DEX_OP_MOVE_CLIENT_BTW_CMRS, "op_move_client_btw_cmrs" },
	{ DEX_OP_MOVE_CREDIT_BTW_CLIENTS, "op_move_credit_btw_clients" },
	{ DEX_OP_MOVE_CHARGES_BTW_ACCTS, "op_move_charges_btw_accts" },
	{ 0, (char *) 0 }
};

void *
fm_dex_bill_config_func()
{
	return ((void *) (fm_dex_bill_config));
}
