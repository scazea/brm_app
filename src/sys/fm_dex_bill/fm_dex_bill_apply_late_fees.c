/******************************************************************************
 *
 * Copyright (c) 2017 DexYP. All rights reserved.
 *
 * This material is the confidential property of DexYP 
 * or its licensors and may be used, reproduced, stored or transmitted
 * only in accordance with a valid DexYP license or sublicense agreement.
 *
 *****************************************************************************/
/*******************************************************************************
 * Maintentance Log:
 *
 * Date: 20171013
 * Author: Ebillsoft
 * Identifier: N/A
 * Notes: Apply late fees based on the configured percentage 
 ***********************************************************************/
/*******************************************************************************
 * File History
 *
 * DATE     |  CHANGE                                           |  Author
 ***********|***************************************************|****************
 |10/13/2017|Applay Late Fees based on the configuration        | Rama Puppala
 |**********|***************************************************|****************
 ********************************************************************************/

#ifndef lint
static  char    Sccs_id[] = "@(#)$Id: Exp $";
#endif

#include <stdio.h>
#include "pcm.h"
#include "ops/bill.h"
#include "ops/ar.h"
#include "cm_fm.h"
#include "cm_cache.h"
#include "pin_errs.h"
#include "pin_bill.h"
#include "pin_cust.h"
#include "pinlog.h"
#include "pin_os.h"
#include "dex_ops.h"
#include "dex_flds.h"

#define LATE_FEE_EVENT "/event/billing/late_fee"

PIN_IMPORT cm_cache_t	*fm_bill_lcr_state_config_ptr; 
PIN_IMPORT cm_cache_t	*fm_bill_lcr_company_config_ptr; 

EXPORT_OP void
op_apply_late_fees(
	cm_nap_connection_t	*connp,
	u_int			opcode,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**ret_flistpp,
	pin_errbuf_t		*ebufp);

static void
dex_op_apply_late_fees(
	pcm_context_t	*ctxp,
	u_int		flags,
	pin_flist_t	*in_flistp,
	pin_flist_t	**out_flistpp,
        pin_errbuf_t	*ebufp);

PIN_IMPORT char *
fm_dex_get_acc_type(
	pcm_context_t	*ctxp,
	poid_t		*i_pdp,
	pin_errbuf_t	*ebufp); 

static char *
fm_dex_get_cust_state(
	pcm_context_t	*ctxp,
	poid_t		*i_pdp,
	pin_errbuf_t	*ebufp);

static pin_decimal_t *
fm_dex_get_cust_lcr(
	pcm_context_t	*ctxp,
	poid_t		*i_pdp,
	char		*acct_typep,
	pin_errbuf_t	*ebufp);

static pin_flist_t *
fm_dex_get_config_lcr_state_from_cache(
	char		*keyp,
	pin_errbuf_t	*ebufp); 

static pin_flist_t *
fm_dex_get_config_lcr_company_from_cache(
	char		*keyp,
	pin_errbuf_t	*ebufp);
/*******************************************************************
 * Main routine for the DEX_OP_APPLY_LATE_FEES command
 *******************************************************************/
void
op_apply_late_fees(
	cm_nap_connection_t	*connp,
	u_int			opcode,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**ret_flistpp,
	pin_errbuf_t		*ebufp)
{
	pcm_context_t		*ctxp = connp->dm_ctx;
	pin_flist_t		*r_flistp = NULL;

	/***********************************************************
	 * Null out results until we have some.
	 ***********************************************************/
	*ret_flistpp = NULL;
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Insanity check.
	 ***********************************************************/
	if (opcode != DEX_OP_APPLY_LATE_FEES) {
		pin_set_err(ebufp, PIN_ERRLOC_FM,
			PIN_ERRCLASS_SYSTEM_DETERMINATE,
			PIN_ERR_BAD_OPCODE, 0, 0, opcode);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"bad opcode in op_apply_late_fees", ebufp);
		return;
	}

	/***********************************************************
	 * Debug: What did we get?
	 ***********************************************************/
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"op_apply_late_fees input flist", in_flistp);

	/***********************************************************
	 * Call main function to do it
	 ***********************************************************/
	dex_op_apply_late_fees(ctxp, flags, in_flistp, &r_flistp, ebufp);

	/***********************************************************
	 * Results.
	 ***********************************************************/
	if (PIN_ERR_IS_ERR(ebufp)) {
		*ret_flistpp = (pin_flist_t *)NULL;
		PIN_FLIST_DESTROY(r_flistp, NULL);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"op_apply_late_fees error", ebufp);
	} else {
		*ret_flistpp = r_flistp;
		PIN_ERR_CLEAR_ERR(ebufp);
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
			"op_apply_late_fees return flist", r_flistp);
	}

	return;
}

/*******************************************************************
 * dex_op_apply_late_fees()
 *
 * Apply late fees based on the state level configuration DB table. 
 *
 *******************************************************************/
static void
dex_op_apply_late_fees(
	pcm_context_t	*ctxp,
	u_int		flags,
	pin_flist_t	*in_flistp,
	pin_flist_t	**out_flistpp,
        pin_errbuf_t	*ebufp)
{
	pin_flist_t	*i_flistp = NULL;
	pin_flist_t	*o_flistp = NULL;
	pin_flist_t	*event_flistp = NULL;
	pin_flist_t	*total_flistp = NULL;
	poid_t		*pdp = NULL;
	poid_t		*event_pdp = NULL;
	pin_decimal_t	*balp = NULL;
	pin_decimal_t	*unapplied_amtp = NULL;
	pin_decimal_t	*min_balp = NULL;
	pin_decimal_t	*lcr_prtp = NULL;
	pin_decimal_t	*hundredp = NULL;
	char		*acc_typep = NULL;
	void            *vp = NULL;
	time_t		pvt = pin_virtual_time(NULL);
	int64		db = -1;
	int32		s_flags = 0;
	int32		late_flag = 0;
	int32		currency = 840;
	int32		impact_type = 2;
	int32		status = -1;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	min_balp = pbo_decimal_from_str("0.01", ebufp);

	/************************************************************
	 * Get opening balance using billinfo object
	 ** *********************************************************/
	i_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, i_flistp, PIN_FLD_POID, ebufp); 
	PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_INCLUDE_CHILDREN, &s_flags, ebufp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "Get Balance Summary input flist", i_flistp);	

	PCM_OP(ctxp, PCM_OP_AR_GET_BAL_SUMMARY, 0, i_flistp, &o_flistp, ebufp);

	PIN_FLIST_DESTROY_EX(&i_flistp, NULL);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "Get Balance Summary output flist", o_flistp);

	balp = PIN_FLIST_FLD_TAKE(o_flistp, PIN_FLD_OPENBILL_DUE, 0, ebufp); 
	unapplied_amtp = PIN_FLIST_FLD_GET(o_flistp, PIN_FLD_UNAPPLIED_AMOUNT, 0, ebufp);
	pbo_decimal_add_assign(balp, unapplied_amtp, ebufp);

	PIN_FLIST_DESTROY_EX(&o_flistp, NULL);

	if (pbo_decimal_compare(min_balp, balp, ebufp) > 0)
	{
		pbo_decimal_destroy(&balp);
		pbo_decimal_destroy(&min_balp);
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, "Current bal is small and no late fee.. ");
		return;
	}
	pbo_decimal_destroy(&min_balp);

	/************************************************************
	 * Read field account_obj from billinfo poid 
	 ** *********************************************************/
	i_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, i_flistp, PIN_FLD_POID, ebufp); 
	PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_ACCOUNT_OBJ, NULL,  ebufp); 

	PCM_OP(ctxp, PCM_OP_READ_FLDS, PCM_OPFLG_CACHEABLE, i_flistp,
		&o_flistp, ebufp);

	PIN_FLIST_DESTROY_EX(&i_flistp, NULL);

	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_FLIST_DESTROY_EX(&o_flistp, NULL);
		pbo_decimal_destroy(&balp);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
		"dex_op_apply_late_fees RFLDS error", ebufp);
		return;
	}
	pdp = PIN_FLIST_FLD_TAKE(o_flistp, PIN_FLD_ACCOUNT_OBJ, 0, ebufp);
	
	PIN_FLIST_DESTROY_EX(&o_flistp, NULL);	

	acc_typep = fm_dex_get_acc_type(ctxp, pdp, ebufp);

	lcr_prtp = fm_dex_get_cust_lcr(ctxp, pdp, acc_typep, ebufp);

	pin_free(acc_typep);

	/***********************************************************
	 * Apply Late Fees using PCM_OP_ACT_USAGE opcode 
	 ***********************************************************/
	if (lcr_prtp && !pbo_decimal_is_zero(lcr_prtp, ebufp))
	{
		i_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_POID, pdp, ebufp);

		event_flistp = PIN_FLIST_SUBSTR_ADD(i_flistp, PIN_FLD_EVENT, ebufp);
		db = PIN_POID_GET_DB(pdp);
        	event_pdp = PIN_POID_CREATE(db, LATE_FEE_EVENT, (int64)-1, ebufp);
		PIN_FLIST_FLD_PUT(event_flistp, PIN_FLD_POID, event_pdp, ebufp);
		PIN_FLIST_FLD_SET(event_flistp, PIN_FLD_PROGRAM_NAME, 
					"pin_bill_accts", ebufp);
		PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, event_flistp, 
			PIN_FLD_BILLINFO_OBJ, ebufp);
		PIN_FLIST_FLD_SET(event_flistp, PIN_FLD_DESCR, 
				"Late Fee", ebufp);
		PIN_FLIST_FLD_SET(event_flistp, PIN_FLD_SYS_DESCR, 
				"Late Fee", ebufp);
		PIN_FLIST_FLD_SET(event_flistp, PIN_FLD_NAME, 
				"Billing Event Log", ebufp);
		PIN_FLIST_FLD_SET(event_flistp, PIN_FLD_ACCOUNT_OBJ, pdp, ebufp);
		PIN_FLIST_FLD_SET(event_flistp, PIN_FLD_START_T , &pvt, ebufp);
		PIN_FLIST_FLD_SET(event_flistp, PIN_FLD_END_T , &pvt, ebufp);

		total_flistp = PIN_FLIST_ELEM_ADD(event_flistp, PIN_FLD_TOTAL,
                                        currency, ebufp);
	
		hundredp = pbo_decimal_from_str("100.0", ebufp);
		pbo_decimal_divide_assign(lcr_prtp, hundredp, ebufp);
		pbo_decimal_multiply_assign(balp, lcr_prtp, ebufp);
		pbo_decimal_round_assign(balp, 2, ROUND_HALF_UP, ebufp);
		pbo_decimal_destroy(&hundredp);

		PIN_FLIST_FLD_PUT(total_flistp, PIN_FLD_AMOUNT, balp, ebufp);
		
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "dex_op_apply_late_fees act_usage iflist", 
			i_flistp);	
		PCM_OP(ctxp, PCM_OP_ACT_USAGE, flags, i_flistp, &o_flistp, ebufp);
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "dex_op_apply_late_fees act_usage oflist", 
			o_flistp);
	
		PIN_FLIST_DESTROY_EX(&i_flistp, NULL);

		if (PIN_ERR_IS_ERR(ebufp))
		{
			PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
				"dex_op_apply_late_fees act_usage err", ebufp);
		}
		else
		{
			status = 0;
		}
		PIN_FLIST_FLD_SET(o_flistp, PIN_FLD_STATUS, &status, ebufp);
	}
	else
	{
		o_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, o_flistp, PIN_FLD_POID, ebufp); 
		PIN_FLIST_FLD_SET(o_flistp, PIN_FLD_STATUS, &status, ebufp);
		PIN_FLIST_FLD_SET(o_flistp, PIN_FLD_ERROR_DESCR, 
			"Late Charge Not Applied", ebufp);
		PIN_FLIST_FLD_SET(o_flistp, PIN_FLD_ERROR_CODE, "20001" , ebufp);
	}

	*out_flistpp = o_flistp;
	
	/***********************************************************
	 * Error?
	 ***********************************************************/
	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"dex_op_apply_late_fees error", ebufp);
	}

	if (lcr_prtp) pbo_decimal_destroy(&lcr_prtp);
	return;
}

/*******************************************************************
 * fm_dex_get_cust_city()
 *
 * This function gets customer state from account_nameinfo[] 
 *
 * *****************************************************************/
static char *
fm_dex_get_cust_state(
	pcm_context_t	*ctxp,
	poid_t		*i_pdp,
	pin_errbuf_t	*ebufp) 
{
	pin_flist_t	*i_flistp = NULL;
	pin_flist_t	*o_flistp = NULL;
	pin_flist_t	*nameinfo_flistp = NULL;
        char		*cust_statep = NULL;

	if (PIN_ERR_IS_ERR(ebufp))
		return NULL;
	PIN_ERR_CLEAR_ERR(ebufp);	

	i_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_POID, i_pdp, ebufp); 

	nameinfo_flistp = PIN_FLIST_ELEM_ADD(i_flistp, PIN_FLD_NAMEINFO, 1, ebufp);
	PIN_FLIST_FLD_SET(nameinfo_flistp, PIN_FLD_STATE, NULL, ebufp);

	PCM_OP(ctxp, PCM_OP_READ_FLDS, PCM_OPFLG_CACHEABLE, i_flistp,
		&o_flistp, ebufp);

	PIN_FLIST_DESTROY_EX(&i_flistp, NULL);

	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_FLIST_DESTROY_EX(&o_flistp, NULL);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			 "dex_op_small_bal_auto_adj RFLDS error", ebufp);
		return NULL;
	}

        nameinfo_flistp = PIN_FLIST_ELEM_GET(o_flistp, PIN_FLD_NAMEINFO, 1, 0, ebufp);
        cust_statep = PIN_FLIST_FLD_TAKE(nameinfo_flistp, PIN_FLD_STATE, 0, ebufp);

	PIN_FLIST_DESTROY_EX(&o_flistp, NULL);
	return cust_statep;
}

/*******************************************************************
 * fm_dex_get_cust_lcr()
 *
 * This function gets late charge rate of a customer based on config
 * Parameters 
 *
 * *****************************************************************/
static pin_decimal_t *
fm_dex_get_cust_lcr(
	pcm_context_t	*ctxp,
	poid_t		*i_pdp,
	char		*acct_typep,
	pin_errbuf_t	*ebufp) 
{
	pin_flist_t	*o_flistp = NULL;
	pin_flist_t	*nameinfo_flistp = NULL;
	pin_decimal_t	*lcr_prtp = NULL;
        char		*cust_statep = NULL;
        char		*brand_areap = "W";
	char		key_str[256] = {""};
	char		*bus_entityp = "*";
	char		*acc_subtypep = "*";
	char		*ar_regionp = "*";

	if (PIN_ERR_IS_ERR(ebufp))
		return NULL;
	PIN_ERR_CLEAR_ERR(ebufp);	

	cust_statep = fm_dex_get_cust_state(ctxp, i_pdp, ebufp);

	sprintf(key_str, "%s|%s", cust_statep, brand_areap);

	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, key_str); 

	o_flistp = fm_dex_get_config_lcr_state_from_cache(key_str, ebufp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_dex_get_cust_lcr state LCR out flist", o_flistp);
	if (o_flistp)
	{
		lcr_prtp = PIN_FLIST_FLD_TAKE(o_flistp, PIN_FLD_PERCENT, 0, ebufp);
	}
	else
	{
		sprintf(key_str, "%s|%s|%s|%s|%s", bus_entityp, acct_typep,
                        acc_subtypep, ar_regionp, brand_areap);
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, key_str); 

		o_flistp = fm_dex_get_config_lcr_company_from_cache(key_str, ebufp);
	
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
			"fm_dex_get_cust_lcr company LCR out flist", o_flistp);
		if (o_flistp)
		{
			lcr_prtp = PIN_FLIST_FLD_TAKE(o_flistp, PIN_FLD_PERCENT, 0, ebufp);
		}
	}

	PIN_FLIST_DESTROY_EX(&o_flistp, NULL);

	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			 "fm_dex_get_cust_lcr error", ebufp);
		return NULL;
	}

	return lcr_prtp;
}

/*******************************************************************
 * fm_dex_get_config_lcr_state_from_cache()
 *
 * This function gets the /config/dex_lcr_state_level from cache 
 *
 * *****************************************************************/
pin_flist_t *
fm_dex_get_config_lcr_state_from_cache(
	char		*keyp,
	pin_errbuf_t	*ebufp) 
{
	pin_flist_t		*flistp = (pin_flist_t *)NULL;
	cm_cache_key_iddbstr_t	kids;
	int32			err = PIN_ERR_NONE;

	/*
 	 * If the cache is not enabled, short circuit right away
 	 */
	if (fm_bill_lcr_state_config_ptr == (cm_cache_t *)NULL) {
		return ((pin_flist_t *) NULL);
	} 	

	/*
	 * See if the entry is in our data dictionary cache
	 */
	kids.id = 0;	/* Not relevant for us */
	kids.db = 0;	/* Not relevant for us */
	kids.str = keyp;

	flistp = cm_cache_find_entry(fm_bill_lcr_state_config_ptr, &kids, &err);
	switch(err)
	{
		case PIN_ERR_NONE:
			break;
		case PIN_ERR_NOT_FOUND:
			PIN_ERR_CLEAR_ERR(ebufp);
			break;
		default:
			pinlog(__FILE__, __LINE__, LOG_FLAG_ERROR,
				"fm_dex_get_config_lcr_state_from_cache: error "
				"accessing data dictionary cache.");
			pin_set_err(ebufp, PIN_ERRLOC_CM,
				PIN_ERRCLASS_SYSTEM_DETERMINATE,
				 err, 0, 0, 0);
			return ((pin_flist_t *) NULL);
	}
	/* Return whatever we found in the cache */
	return flistp;					
}

/*******************************************************************
 * fm_dex_get_config_lcr_company_from_cache()
 *
 * This function gets the /config/dex_lcr_company_level from cache 
 *
 * *****************************************************************/
static pin_flist_t *
fm_dex_get_config_lcr_company_from_cache(
	char		*keyp,
	pin_errbuf_t	*ebufp) 
{
	pin_flist_t		*flistp = (pin_flist_t *)NULL;
	cm_cache_key_iddbstr_t	kids;
	int32			err = PIN_ERR_NONE;

	/*
 	 * If the cache is not enabled, short circuit right away
 	 */
	if (fm_bill_lcr_company_config_ptr == (cm_cache_t *)NULL) {
		return ((pin_flist_t *) NULL);
	} 	

	/*
	 * See if the entry is in our data dictionary cache
	 */
	kids.id = 0;	/* Not relevant for us */
	kids.db = 0;	/* Not relevant for us */
	kids.str = keyp;

	flistp = cm_cache_find_entry(fm_bill_lcr_company_config_ptr, &kids, &err);
	switch(err)
	{
		case PIN_ERR_NONE:
			break;
		case PIN_ERR_NOT_FOUND:
			PIN_ERR_CLEAR_ERR(ebufp);
			break;
		default:
			pinlog(__FILE__, __LINE__, LOG_FLAG_ERROR,
				"fm_dex_get_config_lcr_company_from_cache: error "
				"accessing data dictionary cache.");
			pin_set_err(ebufp, PIN_ERRLOC_CM,
				PIN_ERRCLASS_SYSTEM_DETERMINATE,
				 err, 0, 0, 0);
			return ((pin_flist_t *) NULL);
	}
	/* Return whatever we found in the cache */
	return flistp;					
}

