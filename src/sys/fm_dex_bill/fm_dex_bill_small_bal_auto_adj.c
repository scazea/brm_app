/*
 *
 *      Copyright (c) 2017 DexYP All rights reserved.
 *
 *      This material is the confidential property of DexYP or its
 *      licensors and may be used, reproduced, stored or transmitted only in
 *      accordance with a valid DexYP license or sublicense agreement.
 *
 */
/*********************************************************************
 * Maintentance Log:
 *
 * Date: 20171008
 * Author: Ebillsoft
 * Identifier: N/A
 * Notes: Automatic adjustment of small balances
 ***********************************************************************/
/*******************************************************************************
 * File History
 *
 * DATE     |  CHANGE                                           |  Author
 ******* ***|***************************************************|****************
 |10/08/2017|Automatic adjustment of small balance              | Rama Puppala
 |**********|***************************************************|****************
 ********************************************************************************/

#ifndef lint
static  char    Sccs_id[] = "@(#)$Id: Exp $";
#endif

#include <stdio.h>
#include "pcm.h"
#include "ops/bill.h"
#include "ops/ar.h"
#include "cm_fm.h"
#include "pin_errs.h"
#include "pin_bill.h"
#include "pin_cust.h"
#include "pinlog.h"
#include "pin_os.h"
#include "dex_ops.h"
#include "dex_flds.h"

PIN_IMPORT pin_decimal_t	*min_local_balp;
PIN_IMPORT pin_decimal_t	*max_local_balp;
PIN_IMPORT pin_decimal_t	*min_national_balp;
PIN_EXPORT pin_decimal_t	*max_national_balp;

EXPORT_OP void
op_small_bal_auto_adj(
	cm_nap_connection_t	*connp,
	u_int			opcode,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**ret_flistpp,
	pin_errbuf_t		*ebufp);

static void
dex_op_small_bal_auto_adj(
	pcm_context_t		*ctxp,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**out_flistpp,
        pin_errbuf_t		*ebufp);

PIN_EXPORT char *
fm_dex_get_acc_type(
	pcm_context_t		*ctxp,
	poid_t			*i_pdp,
	pin_errbuf_t		*ebufp); 

/*******************************************************************
 * Main routine for the DEX_OP_SMALL_BAL_AUTO_ADJ command
 *******************************************************************/
void
op_small_bal_auto_adj(
	cm_nap_connection_t	*connp,
	u_int			opcode,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**ret_flistpp,
	pin_errbuf_t		*ebufp)
{
	pcm_context_t		*ctxp = connp->dm_ctx;
	pin_flist_t		*r_flistp = NULL;

	/***********************************************************
	 * Null out results until we have some.
	 ***********************************************************/
	*ret_flistpp = NULL;
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Insanity check.
	 ***********************************************************/
	if (opcode != DEX_OP_SMALL_BAL_AUTO_ADJ) {
		pin_set_err(ebufp, PIN_ERRLOC_FM,
			PIN_ERRCLASS_SYSTEM_DETERMINATE,
			PIN_ERR_BAD_OPCODE, 0, 0, opcode);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"bad opcode in op_small_bal_auto_adj", ebufp);
		return;
	}

	/***********************************************************
	 * Debug: What did we get?
	 ***********************************************************/
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"op_small_bal_auto_adj input flist", in_flistp);

	/***********************************************************
	 * Call main function to do it
	 ***********************************************************/
	dex_op_small_bal_auto_adj(ctxp, flags, in_flistp, &r_flistp, ebufp);

	/***********************************************************
	 * Results.
	 ***********************************************************/
	if (PIN_ERR_IS_ERR(ebufp)) {
		*ret_flistpp = (pin_flist_t *)NULL;
		PIN_FLIST_DESTROY(r_flistp, NULL);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"op_small_bal_auto_adj error", ebufp);
	} else {
		*ret_flistpp = r_flistp;
		PIN_ERR_CLEAR_ERR(ebufp);
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
			"op_small_bal_auto_adj return flist", r_flistp);
	}

	return;
}

/*******************************************************************
 * dex_op_small_bal_auto_adj()
 *
 * Pass auto adjustment for small credit or debit balances 	
 *
 *******************************************************************/
static void
dex_op_small_bal_auto_adj(
	pcm_context_t	*ctxp,
	u_int		flags,
	pin_flist_t	*in_flistp,
	pin_flist_t	**out_flistpp,
        pin_errbuf_t	*ebufp)
{
	pin_flist_t	*i_flistp = NULL;
	pin_flist_t	*o_flistp = NULL;
	poid_t		*pdp = NULL;
	pin_decimal_t	*balp = NULL;
	pin_decimal_t	*unapplied_amtp = NULL;
	pin_decimal_t	*pending_balp = NULL;
	char		*acc_typep = NULL;
	void            *vp = NULL;
	int32		s_flags = 0;
	int32		adj_flag = 0;
	int32		ver_id = 29;
	int32		str_id = 1;
	int32		status=-1;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	/************************************************************
	 * Get opening balance using billinfo object
	 ** *********************************************************/
	i_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, i_flistp, PIN_FLD_POID, ebufp); 
	PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_INCLUDE_CHILDREN, &s_flags, ebufp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "Get Balance Summary input flist", i_flistp);	

	PCM_OP(ctxp, PCM_OP_AR_GET_BAL_SUMMARY, 0, i_flistp, &o_flistp, ebufp);

	PIN_FLIST_DESTROY_EX(&i_flistp, NULL);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "Get Balance Summary output flist", o_flistp);

	balp = PIN_FLIST_FLD_TAKE(o_flistp, PIN_FLD_OPENBILL_DUE, 0, ebufp); 
	unapplied_amtp = PIN_FLIST_FLD_GET(o_flistp, PIN_FLD_UNAPPLIED_AMOUNT, 0, ebufp);
	pbo_decimal_add_assign(balp, unapplied_amtp, ebufp);

	pending_balp = PIN_FLIST_FLD_GET(o_flistp, PIN_FLD_PENDINGBILL_DUE, 0, ebufp); 

	PIN_FLIST_DESTROY_EX(&o_flistp, NULL);
	if (!pbo_decimal_is_zero(pending_balp, ebufp))
	{
		pbo_decimal_destroy(&balp);
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, "Current bal is not zero and no adj.. ");
		return;
	}


	/************************************************************
	 * Read field account_obj from billinfo poid 
	 ** *********************************************************/
	i_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, i_flistp, PIN_FLD_POID, ebufp); 
	PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_ACCOUNT_OBJ, NULL,  ebufp); 

	PCM_OP(ctxp, PCM_OP_READ_FLDS, PCM_OPFLG_CACHEABLE, i_flistp,
		&o_flistp, ebufp);

	PIN_FLIST_DESTROY_EX(&i_flistp, NULL);

	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_FLIST_DESTROY_EX(&o_flistp, NULL);
		pbo_decimal_destroy(&balp);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"dex_op_small_bal_auto_adj RFLDS error", ebufp);
		return;
	}
	pdp = PIN_FLIST_FLD_TAKE(o_flistp, PIN_FLD_ACCOUNT_OBJ, 0, ebufp);
	
	PIN_FLIST_DESTROY_EX(&o_flistp, NULL);	

	acc_typep = fm_dex_get_acc_type(ctxp, pdp, ebufp);

	if (acc_typep && strstr(acc_typep, "Local") &&
		(pbo_decimal_compare(balp, min_local_balp, ebufp) > 0) && 
		(pbo_decimal_compare(balp, max_local_balp, ebufp) < 0))
	{
		adj_flag = 1;
	}
	else if (acc_typep && strstr(acc_typep, "National") &&
                (pbo_decimal_compare(balp, min_local_balp, ebufp) > 0) &&
                (pbo_decimal_compare(balp, max_local_balp, ebufp) < 0))
	{
		adj_flag = 1;
	}
	else
	{
	}
	
	pin_free(acc_typep);

	/***********************************************************
	 * Apply adjustment since its a small adjustment 
	 ***********************************************************/
	if (adj_flag)
	{
		i_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_POID, pdp, ebufp);
		PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_PROGRAM_NAME, 
					"Automatic Adjustment", ebufp);
		PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_DESCR, 
				"Min. Bill Amount Adjustment", ebufp);
		PIN_FLIST_FLD_PUT(i_flistp, PIN_FLD_AMOUNT,
			pbo_decimal_negate(balp, ebufp), ebufp);
		PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_STR_VERSION, &ver_id, ebufp);
		PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_STRING_ID, &str_id, ebufp);
		
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "Adjustment input flist", i_flistp);	
		PCM_OP(ctxp, PCM_OP_AR_ACCOUNT_ADJUSTMENT, 0, i_flistp, &o_flistp, ebufp);
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "Adjustment output flist", o_flistp);
	
		PIN_FLIST_DESTROY_EX(&i_flistp, NULL);

		if (PIN_ERR_IS_ERR(ebufp))
		{
			 PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
				"dex_op_small_bal_auto_adj Adjustment err", ebufp);
		}
		else
		{
			status = 0;
		}
		PIN_FLIST_FLD_SET(o_flistp, PIN_FLD_STATUS, &status, ebufp);
	}
	else
	{
		o_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, o_flistp, PIN_FLD_POID, ebufp); 
		PIN_FLIST_FLD_SET(o_flistp, PIN_FLD_STATUS, &status, ebufp);
		PIN_FLIST_FLD_SET(o_flistp, PIN_FLD_ERROR_DESCR, 
			"Account Adjustment Not Applied", ebufp);
		PIN_FLIST_FLD_SET(o_flistp, PIN_FLD_ERROR_CODE, "10001" , ebufp);
	}

	*out_flistpp = o_flistp;

	/***********************************************************
	 * Error?
	 ***********************************************************/
	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"dex_op_small_bal_auto_adj error", ebufp);
	}

	return;
}


/*******************************************************************
 * fm_dex_get_acc_type()
 *
 * This function gets account type from /pofile/account object
 *
 * *****************************************************************/
char *
fm_dex_get_acc_type(
	pcm_context_t		*ctxp,
	poid_t			*i_pdp,
	pin_errbuf_t		*ebufp) 
{
	pin_flist_t	*srch_i_flistp = NULL;
	pin_flist_t	*srch_o_flistp = NULL;
	pin_flist_t	*args_flistp   = NULL;
	pin_flist_t	*results_flistp = NULL;
	pin_flist_t	*profile_flistp = NULL;
	poid_t		*srch_pdp = NULL;
	int32		srch_flags = 256;
        char		*acc_typep = NULL;
        char            *template = "select X from /profile where F1 = V1 and profile_t.poid_type='/profile/account' ";

	if (PIN_ERR_IS_ERR(ebufp))
		return NULL;
	PIN_ERR_CLEAR_ERR(ebufp);	

        srch_pdp = PIN_POID_CREATE(PIN_POID_GET_DB(i_pdp), "/search", -1, ebufp);

        srch_i_flistp = PIN_FLIST_CREATE(ebufp);
        PIN_FLIST_FLD_PUT(srch_i_flistp, PIN_FLD_POID, srch_pdp, ebufp);
        PIN_FLIST_FLD_SET(srch_i_flistp, PIN_FLD_FLAGS, &srch_flags, ebufp);
        PIN_FLIST_FLD_SET(srch_i_flistp, PIN_FLD_TEMPLATE, template, ebufp);

        args_flistp = PIN_FLIST_ELEM_ADD(srch_i_flistp, PIN_FLD_ARGS, 1, ebufp);
        PIN_FLIST_FLD_SET(args_flistp, PIN_FLD_ACCOUNT_OBJ, i_pdp,ebufp);

        results_flistp = PIN_FLIST_ELEM_ADD(srch_i_flistp, PIN_FLD_RESULTS, 0, ebufp);
        profile_flistp = PIN_FLIST_SUBSTR_ADD(results_flistp, PIN_FLD_ATTRIBUTE_INFO, ebufp);
        PIN_FLIST_FLD_SET(profile_flistp, DEX_FLD_ACCOUNT_TYPE, NULL, ebufp);

	PIN_ERR_LOG_FLIST(3, "Profile search flist", srch_i_flistp);

	PCM_OP(ctxp, PCM_OP_SEARCH, 0, srch_i_flistp, &srch_o_flistp, ebufp);

	PIN_FLIST_DESTROY_EX(&srch_i_flistp, ebufp);

	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"Error during  profile_search", ebufp);
                goto CLEANUP;
        }

        PIN_ERR_LOG_FLIST(3, "Profile search out flist", srch_o_flistp);

        results_flistp = PIN_FLIST_ELEM_GET(srch_o_flistp, PIN_FLD_RESULTS, 0, 1, ebufp);
	if (results_flistp != NULL)
	{
        	profile_flistp = PIN_FLIST_SUBSTR_GET(results_flistp, PIN_FLD_ATTRIBUTE_INFO, 0, ebufp);

        	acc_typep = PIN_FLIST_FLD_TAKE(profile_flistp, DEX_FLD_ACCOUNT_TYPE, 0, ebufp);
	}

CLEANUP:
        PIN_FLIST_DESTROY_EX(&srch_o_flistp, ebufp);
        return acc_typep;
}

