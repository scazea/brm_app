/*
 *
 *      Copyright (c) 2017 DexYP All rights reserved.
 *
 *      This material is the confidential property of DexYP or its
 *      licensors and may be used, reproduced, stored or transmitted only in
 *      accordance with a valid DexYP license or sublicense agreement.
 *
 */
/*********************************************************************
 * Maintentance Log:
 *
 * Date: 20171022
 * Author: Ebillsoft
 * Identifier: N/A
 * Notes: Move credits between CMR clients 
 ***********************************************************************/
/*******************************************************************************
 * File History
 *
 * DATE     |  CHANGE                                           |  Author
 ******* ***|***************************************************|****************
 |10/22/2017|Move clients betwen CMR clients			| Rama Puppala
 |**********|***************************************************|****************
 ********************************************************************************/

#ifndef lint
static  char    Sccs_id[] = "@(#)$Id: Exp $";
#endif

#include <stdio.h>
#include "pcm.h"
#include "ops/bill.h"
#include "ops/ar.h"
#include "cm_fm.h"
#include "pin_errs.h"
#include "pin_bill.h"
#include "pin_cust.h"
#include "pinlog.h"
#include "pin_os.h"
#include "dex_ops.h"
#include "dex_flds.h"

EXPORT_OP void
op_move_charges_btw_accts(
	cm_nap_connection_t	*connp,
	u_int			opcode,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**ret_flistpp,
	pin_errbuf_t		*ebufp);

static void
dex_op_move_charges_btw_accts(
	pcm_context_t		*ctxp,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**out_flistpp,
        pin_errbuf_t		*ebufp);

PIN_IMPORT poid_t *
fm_dex_get_acc_poid(
	pcm_context_t		*ctxp,
	pin_flist_t		*in_flistp,
	pin_errbuf_t		*ebufp);
 
/*******************************************************************
 * Main routine for the DEX_OP_MOVE_CHARGES_BTW_ACCTS command
 *******************************************************************/
void
op_move_charges_btw_accts(
	cm_nap_connection_t	*connp,
	u_int			opcode,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**ret_flistpp,
	pin_errbuf_t		*ebufp)
{
	pcm_context_t		*ctxp = connp->dm_ctx;
	pin_flist_t		*r_flistp = NULL;

	/***********************************************************
	 * Null out results until we have some.
	 ***********************************************************/
	*ret_flistpp = NULL;
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Insanity check.
	 ***********************************************************/
	if (opcode != DEX_OP_MOVE_CHARGES_BTW_ACCTS) {
		pin_set_err(ebufp, PIN_ERRLOC_FM,
			PIN_ERRCLASS_SYSTEM_DETERMINATE,
			PIN_ERR_BAD_OPCODE, 0, 0, opcode);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"bad opcode in op_move_charges_btw_accts", ebufp);
		return;
	}

	/***********************************************************
	 * Debug: What did we get?
	 ***********************************************************/
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"op_move_charges_btw_accts input flist", in_flistp);

	/***********************************************************
	 * Call main function to do it
	 ***********************************************************/
	dex_op_move_charges_btw_accts(ctxp, flags, in_flistp, &r_flistp, ebufp);

	/***********************************************************
	 * Results.
	 ***********************************************************/
	if (PIN_ERR_IS_ERR(ebufp)) {
		*ret_flistpp = (pin_flist_t *)NULL;
		PIN_FLIST_DESTROY(r_flistp, NULL);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"op_move_charges_btw_accts error", ebufp);
	} else {
		*ret_flistpp = r_flistp;
		PIN_ERR_CLEAR_ERR(ebufp);
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
			"op_move_charges_btw_accts return flist", r_flistp);
	}

	return;
}

/*******************************************************************
 * dex_op_move_charges_btw_accts()
 *
 * Implementation of move credit amount between client accounts of
 * a CMR account 
 *
 *******************************************************************/
static void
dex_op_move_charges_btw_accts(
	pcm_context_t	*ctxp,
	u_int		flags,
	pin_flist_t	*in_flistp,
	pin_flist_t	**out_flistpp,
        pin_errbuf_t	*ebufp)
{
	pin_flist_t	*i_flistp = NULL;
	pin_flist_t	*ret_flistp = NULL;
	pin_flist_t	*o_flistp = NULL;
	pin_flist_t	*res_flistp = NULL;
	pin_flist_t	*tmp_flistp= NULL;
	pin_decimal_t	*cr_amtp = NULL;
	pin_decimal_t	*zerop = NULL;
	poid_t		*src_acc_pdp = NULL;
	poid_t		*dst_acc_pdp = NULL;
	char		*src_cmr_idp = NULL;
	char		*dst_cmr_idp = NULL;
	char		*program_namep = "DEX_OP_MOVE_CHARGES_BTW_ACCTS";
	int32		status = -1;
	int32		currency = 840;
	int32		verify_bal = 0;	

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	/************************************************************
	 * Validate input amount is credit or not 
	 ***********************************************************/
	zerop = pbo_decimal_from_str("0.0", ebufp);
	cr_amtp = PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_AMOUNT, 0, ebufp);
	if (pbo_decimal_compare(cr_amtp, zerop, ebufp) >= 0)
	{
		PIN_ERR_LOG_EBUF(3, "dex_op_move_charges_btw_accts: AMOUNT is not credit", ebufp);
		PIN_ERR_CLEAR_ERR(ebufp);
		ret_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, ret_flistp, PIN_FLD_POID, ebufp);	
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_STATUS, &status, ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_CODE, "53010", ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_DESCR, "Not Credit Amount", ebufp);
		goto CLEANUP;
	}

	/************************************************************
	 * Find account poid of source account_no 
	 ***********************************************************/
	i_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, i_flistp, PIN_FLD_POID, ebufp);	
	PIN_FLIST_FLD_COPY(in_flistp, DEX_FLD_CLIENT_ID_SRC, i_flistp, 
		PIN_FLD_ACCOUNT_NO, ebufp);	
	src_acc_pdp = fm_dex_get_acc_poid(ctxp, i_flistp, ebufp);
	PIN_FLIST_DESTROY_EX(&i_flistp, NULL);	

	if (!src_acc_pdp)
	{
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, "dex_op_move_charges_btw_accts: No source found!!");
		ret_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, ret_flistp, PIN_FLD_POID, ebufp);	
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_STATUS, &status, ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_CODE, "53000", ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_DESCR, "No source account found", ebufp);
		goto CLEANUP;
	}

	/************************************************************
	 * Find account poid of destination account_no 
	 ***********************************************************/
	i_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, i_flistp, PIN_FLD_POID, ebufp);	
	PIN_FLIST_FLD_COPY(in_flistp, DEX_FLD_CLIENT_ID_DST, i_flistp, 
		PIN_FLD_ACCOUNT_NO, ebufp);	
	dst_acc_pdp = fm_dex_get_acc_poid(ctxp, i_flistp, ebufp);
	PIN_FLIST_DESTROY_EX(&i_flistp, NULL);	

	if (!dst_acc_pdp)
	{
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, "dex_op_move_charges_btw_accts: No destination found!!");
		ret_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, ret_flistp, PIN_FLD_POID, ebufp);	
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_STATUS, &status, ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_CODE, "53000", ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_DESCR, "No destination account found", ebufp);
		goto CLEANUP;
	}

	/************************************************************
	 * Find CMR_ID of the source account 
	 ***********************************************************/
	i_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_POID, src_acc_pdp, ebufp);
        PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_ACCOUNT_OBJ, src_acc_pdp, ebufp);
        PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_TYPE_STR, "/profile/account", ebufp);
	tmp_flistp = PIN_FLIST_ELEM_ADD(i_flistp, PIN_FLD_RESULTS, 0, ebufp);
	tmp_flistp = PIN_FLIST_SUBSTR_ADD(tmp_flistp, PIN_FLD_ATTRIBUTE_INFO, ebufp);
	PIN_FLIST_FLD_SET(tmp_flistp, DEX_FLD_CMR_ID, NULL, ebufp);

	PIN_ERR_LOG_FLIST(3, "dex_op_move_charges_btw_accts: find profile input", i_flistp);

	PCM_OP(ctxp, PCM_OP_CUST_FIND_PROFILE, PCM_OPFLG_READ_RESULT, i_flistp, &o_flistp, ebufp);

	PIN_FLIST_DESTROY_EX(&i_flistp, NULL);	

	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_ERR_LOG_EBUF(1, "dex_op_move_charges_btw_accts: Error in FIND_PROFILE", ebufp);
		PIN_ERR_CLEAR_ERR(ebufp);
		ret_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, ret_flistp, PIN_FLD_POID, ebufp);	
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_STATUS, &status, ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_CODE, "53001", ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_DESCR, "Error Reading Profile", ebufp);
		goto CLEANUP;
	}

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "dex_op_move_charges_btw_accts: FIND_PROFILE", o_flistp);

	res_flistp = PIN_FLIST_ELEM_GET(o_flistp, PIN_FLD_RESULTS, 0, 1, ebufp);
	if (!res_flistp)
	{
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, "dex_op_move_charges_btw_accts: No profile found!!");
		ret_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, ret_flistp, PIN_FLD_POID, ebufp);	
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_STATUS, &status, ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_CODE, "53002", ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_DESCR, "No profile found", ebufp);
		goto CLEANUP;
	}

	tmp_flistp = PIN_FLIST_SUBSTR_GET(res_flistp, PIN_FLD_ATTRIBUTE_INFO, 0, ebufp);
	src_cmr_idp = PIN_FLIST_FLD_TAKE(tmp_flistp, DEX_FLD_CMR_ID, 0, ebufp);

	PIN_FLIST_DESTROY_EX(&o_flistp, NULL);	

	/************************************************************
	 * Find CMR_ID of the destination account 
	 ***********************************************************/
	i_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_POID, dst_acc_pdp, ebufp);
        PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_ACCOUNT_OBJ, dst_acc_pdp, ebufp);
        PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_TYPE_STR, "/profile/account", ebufp);
	tmp_flistp = PIN_FLIST_ELEM_ADD(i_flistp, PIN_FLD_RESULTS, 0, ebufp);
	tmp_flistp = PIN_FLIST_SUBSTR_ADD(tmp_flistp, PIN_FLD_ATTRIBUTE_INFO, ebufp);
	PIN_FLIST_FLD_SET(tmp_flistp, DEX_FLD_CMR_ID, NULL, ebufp);

	PIN_ERR_LOG_FLIST(3, "dex_op_move_charges_btw_accts: find profile input", i_flistp);

	PCM_OP(ctxp, PCM_OP_CUST_FIND_PROFILE, PCM_OPFLG_READ_RESULT, i_flistp, &o_flistp, ebufp);

	PIN_FLIST_DESTROY_EX(&i_flistp, NULL);	

	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_ERR_LOG_EBUF(1, "dex_op_move_charges_btw_accts: Error in FIND_PROFILE", ebufp);
		PIN_ERR_CLEAR_ERR(ebufp);
		ret_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, ret_flistp, PIN_FLD_POID, ebufp);	
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_STATUS, &status, ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_CODE, "53001", ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_DESCR, "Error Reading Profile", ebufp);
		goto CLEANUP;
	}

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "dex_op_move_charges_btw_accts: FIND_PROFILE", o_flistp);

	res_flistp = PIN_FLIST_ELEM_GET(o_flistp, PIN_FLD_RESULTS, 0, 1, ebufp);
	if (!res_flistp)
	{
		PIN_ERR_LOG_MSG(3, "dex_op_move_charges_btw_accts: No profile found!!");
		ret_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, ret_flistp, PIN_FLD_POID, ebufp);	
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_STATUS, &status, ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_CODE, "53002", ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_DESCR, "No profile found", ebufp);
		goto CLEANUP;
	}

	tmp_flistp = PIN_FLIST_SUBSTR_GET(res_flistp, PIN_FLD_ATTRIBUTE_INFO, 0, ebufp);
	dst_cmr_idp = PIN_FLIST_FLD_TAKE(tmp_flistp, DEX_FLD_CMR_ID, 0, ebufp);

	PIN_FLIST_DESTROY_EX(&o_flistp, NULL);	

	/************************************************************
	 * Check clients belong to same CMR account or not 
	 ***********************************************************/
	if ((src_cmr_idp == NULL) || (dst_cmr_idp == NULL) ||
		(src_cmr_idp && dst_cmr_idp && strcmp(src_cmr_idp, dst_cmr_idp) != 0))
	{
		PIN_ERR_LOG_MSG(3, "dex_op_move_charges_btw_accts: clients not belong to same CMR!!");
		ret_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, ret_flistp, PIN_FLD_POID, ebufp);	
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_STATUS, &status, ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_CODE, "53003", ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_DESCR, "Clients not from same CMR", ebufp);
		goto CLEANUP;
	} 
	
	/************************************************************
	 * Transfer balance from source to destination accounts 
	 ***********************************************************/
	i_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_PROGRAM_NAME, program_namep, ebufp);	
	PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_POID, src_acc_pdp, ebufp);
	PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_TRANSFER_TARGET, dst_acc_pdp, ebufp);
	PIN_FLIST_FLD_PUT(i_flistp, PIN_FLD_AMOUNT,
		pbo_decimal_negate(cr_amtp, ebufp), ebufp);
	PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_CURRENCY, &currency, ebufp);
	PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_VERIFY_BALANCE, &verify_bal, ebufp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "PCM_OP_BILL_TRANSFER_BALANCE Input flist:", i_flistp);	

	PCM_OP(ctxp, PCM_OP_BILL_TRANSFER_BALANCE, flags, i_flistp, &o_flistp, ebufp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "PCM_OP_BILL_TRANSFER_BALANCE Output flist:", o_flistp);

	PIN_FLIST_DESTROY_EX(&i_flistp, NULL);
	
	ret_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, ret_flistp, PIN_FLD_POID, ebufp);	
	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_CODE, "53004", ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_DESCR, "Error in PCM_OP_BILL_TRANSFER_BALANCE", ebufp);
	}
	else
	{
		status = 0;
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_CODE, "00", ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_DESCR, "Moved credit Successfully", ebufp);
	}
	PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_STATUS, &status, ebufp);

	/***********************************************************
	 * Error?
	 ***********************************************************/
	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"dex_op_move_charges_btw_accts error", ebufp);
	}

CLEANUP:
	if (src_acc_pdp) PIN_POID_DESTROY(src_acc_pdp, NULL);
	if (dst_acc_pdp) PIN_POID_DESTROY(dst_acc_pdp, NULL);
	if (src_cmr_idp) pin_free(src_cmr_idp);
	if (dst_cmr_idp) pin_free(dst_cmr_idp);
	pbo_decimal_destroy(&zerop);
	PIN_FLIST_DESTROY_EX(&o_flistp, NULL);
	*out_flistpp = ret_flistp;

	return;
}
