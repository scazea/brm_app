/*
 *
 *      Copyright (c) 2017 DexYP All rights reserved.
 *
 *      This material is the confidential property of DexYP or its
 *      licensors and may be used, reproduced, stored or transmitted only in
 *      accordance with a valid DexYP license or sublicense agreement.
 *
 */
/*********************************************************************
 * Maintentance Log:
 *
 * Date: 20171017
 * Author: Ebillsoft
 * Identifier: N/A
 * Notes: Move clients between CMR accounts 
 ***********************************************************************/
/*******************************************************************************
 * File History
 *
 * DATE     |  CHANGE                                           |  Author
 ******* ***|***************************************************|****************
 |10/17/2017|Move clients betwen CMR accounts			| Rama Puppala
 |**********|***************************************************|****************
 ********************************************************************************/

#ifndef lint
static  char    Sccs_id[] = "@(#)$Id: Exp $";
#endif

#include <stdio.h>
#include "pcm.h"
#include "ops/bill.h"
#include "ops/ar.h"
#include "cm_fm.h"
#include "pin_errs.h"
#include "pin_bill.h"
#include "pin_cust.h"
#include "pinlog.h"
#include "pin_os.h"
#include "dex_ops.h"
#include "dex_flds.h"

PIN_IMPORT pin_decimal_t	*min_local_balp;
PIN_IMPORT pin_decimal_t	*max_local_balp;
PIN_IMPORT pin_decimal_t	*min_national_balp;
PIN_EXPORT pin_decimal_t	*max_national_balp;

EXPORT_OP void
op_move_client_btw_cmrs(
	cm_nap_connection_t	*connp,
	u_int			opcode,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**ret_flistpp,
	pin_errbuf_t		*ebufp);

static void
dex_op_move_client_btw_cmrs(
	pcm_context_t		*ctxp,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**out_flistpp,
        pin_errbuf_t		*ebufp);

PIN_EXPORT poid_t *
fm_dex_get_acc_poid(
	pcm_context_t		*ctxp,
	pin_flist_t		*in_flistp,
	pin_errbuf_t		*ebufp);
 
/*******************************************************************
 * Main routine for the DEX_OP_MOVE_CLIENT_BTW_CMRS command
 *******************************************************************/
void
op_move_client_btw_cmrs(
	cm_nap_connection_t	*connp,
	u_int			opcode,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**ret_flistpp,
	pin_errbuf_t		*ebufp)
{
	pcm_context_t		*ctxp = connp->dm_ctx;
	pin_flist_t		*r_flistp = NULL;

	/***********************************************************
	 * Null out results until we have some.
	 ***********************************************************/
	*ret_flistpp = NULL;
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Insanity check.
	 ***********************************************************/
	if (opcode != DEX_OP_MOVE_CLIENT_BTW_CMRS) {
		pin_set_err(ebufp, PIN_ERRLOC_FM,
			PIN_ERRCLASS_SYSTEM_DETERMINATE,
			PIN_ERR_BAD_OPCODE, 0, 0, opcode);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"bad opcode in op_move_client_btw_cmrs", ebufp);
		return;
	}

	/***********************************************************
	 * Debug: What did we get?
	 ***********************************************************/
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"op_move_client_btw_cmrs input flist", in_flistp);

	/***********************************************************
	 * Call main function to do it
	 ***********************************************************/
	dex_op_move_client_btw_cmrs(ctxp, flags, in_flistp, &r_flistp, ebufp);

	/***********************************************************
	 * Results.
	 ***********************************************************/
	if (PIN_ERR_IS_ERR(ebufp)) {
		*ret_flistpp = (pin_flist_t *)NULL;
		PIN_FLIST_DESTROY(r_flistp, NULL);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"op_move_client_btw_cmrs error", ebufp);
	} else {
		*ret_flistpp = r_flistp;
		PIN_ERR_CLEAR_ERR(ebufp);
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
			"op_move_client_btw_cmrs return flist", r_flistp);
	}

	return;
}

/*******************************************************************
 * dex_op_move_client_btw_cmrs()
 *
 * Pass auto adjustment for small credit or debit balances 	
 *
 *******************************************************************/
static void
dex_op_move_client_btw_cmrs(
	pcm_context_t	*ctxp,
	u_int		flags,
	pin_flist_t	*in_flistp,
	pin_flist_t	**out_flistpp,
        pin_errbuf_t	*ebufp)
{
	pin_flist_t	*i_flistp = NULL;
	pin_flist_t	*ret_flistp = NULL;
	pin_flist_t	*o_flistp = NULL;
	pin_flist_t	*res_flistp = NULL;
	pin_flist_t	*modprof_flistp = NULL;
	pin_flist_t	*profile_flistp = NULL;
	pin_flist_t	*tmp_flistp= NULL;
	poid_t		*acc_pdp = NULL;
	int32		status = -1;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	acc_pdp = fm_dex_get_acc_poid(ctxp, in_flistp, ebufp);
	if (!acc_pdp)
	{
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, "dex_op_move_client_btw_cmrs: No client found!!");
		ret_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, ret_flistp, PIN_FLD_POID, ebufp);	
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_STATUS, &status, ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_CODE, "52000", ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_DESCR, "No client account found", ebufp);
		goto CLEANUP;
	}

	/************************************************************
	 * Find profile object  
	 ***********************************************************/
	i_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_POID, acc_pdp, ebufp);
        PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_ACCOUNT_OBJ, acc_pdp, ebufp);
        PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_TYPE_STR, "/profile/account", ebufp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "dex_op_move_client_btw_cmrs: find profile input", i_flistp);

	PCM_OP(ctxp, PCM_OP_CUST_FIND_PROFILE, PCM_OPFLG_READ_RESULT, i_flistp, &o_flistp, ebufp);

	PIN_FLIST_DESTROY_EX(&i_flistp, NULL);	

	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, "dex_op_move_client_btw_cmrs: Error in FIND_PROFILE", ebufp);
		PIN_ERR_CLEAR_ERR(ebufp);
		ret_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, ret_flistp, PIN_FLD_POID, ebufp);	
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_STATUS, &status, ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_CODE, "52001", ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_DESCR, "Error Reading Profile", ebufp);
		goto CLEANUP;
	}

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "dex_op_move_client_btw_cmrs: output flist", o_flistp);

	res_flistp = PIN_FLIST_ELEM_GET(o_flistp, PIN_FLD_RESULTS, 0, 1, ebufp);
	if (!res_flistp)
	{
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, "dex_op_move_client_btw_cmrs: No profile found!!");
		ret_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, ret_flistp, PIN_FLD_POID, ebufp);	
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_STATUS, &status, ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_CODE, "52002", ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_DESCR, "No profile found", ebufp);
		goto CLEANUP;
	}

	/************************************************************
	 * Update profile object  
	 ***********************************************************/
	i_flistp = PIN_FLIST_CREATE(ebufp);
	
	PIN_FLIST_FLD_COPY(res_flistp, PIN_FLD_POID, i_flistp, PIN_FLD_POID, ebufp); 
	PIN_FLIST_FLD_SET(i_flistp, PIN_FLD_ACCOUNT_OBJ, acc_pdp, ebufp);
	profile_flistp = PIN_FLIST_ELEM_ADD(i_flistp, PIN_FLD_PROFILES, 0, ebufp);
	PIN_FLIST_FLD_COPY(res_flistp, PIN_FLD_POID, profile_flistp, PIN_FLD_PROFILE_OBJ, ebufp); 
	tmp_flistp = PIN_FLIST_SUBSTR_ADD(profile_flistp, PIN_FLD_INHERITED_INFO, ebufp);
	tmp_flistp = PIN_FLIST_SUBSTR_ADD(tmp_flistp, PIN_FLD_ATTRIBUTE_INFO, ebufp);
	PIN_FLIST_FLD_COPY(in_flistp, DEX_FLD_CMR_ID_DST, tmp_flistp, DEX_FLD_CMR_ID, ebufp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "Modify Profile input flist", i_flistp);	

	PCM_OP(ctxp, PCM_OP_CUST_MODIFY_PROFILE, flags, i_flistp, &modprof_flistp, ebufp);

	PIN_FLIST_DESTROY_EX(&i_flistp, NULL);
	PIN_FLIST_DESTROY_EX(&modprof_flistp, NULL);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "Modify Profile output flist", modprof_flistp);

	ret_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, ret_flistp, PIN_FLD_POID, ebufp);	
	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_CODE, "52003", ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_DESCR, "Error in updating profile details", ebufp);
	}
	else
	{
		status = 0;
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_CODE, "00", ebufp);
		PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_ERROR_DESCR, "Moved client Successfully", ebufp);
	}
	PIN_FLIST_FLD_SET(ret_flistp, PIN_FLD_STATUS, &status, ebufp);

	/*************************************************************
 	 * Move balance from source CMR to destination CMR if required
 	 * ***********************************************************/ 	

	/***********************************************************
	 * Error?
	 ***********************************************************/
	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"dex_op_move_client_btw_cmrs error", ebufp);
	}

CLEANUP:
	if (acc_pdp) PIN_POID_DESTROY(acc_pdp, NULL);
	PIN_FLIST_DESTROY_EX(&o_flistp, NULL);
	*out_flistpp = ret_flistp;

	return;
}


/*******************************************************************
 * fm_dex_get_acc_poid()
 *
 * This function gets account POID /account object
 *
 * *****************************************************************/
poid_t *
fm_dex_get_acc_poid(
	pcm_context_t	*ctxp,
	pin_flist_t	*in_flistp,
	pin_errbuf_t	*ebufp) 
{
	pin_flist_t	*srch_i_flistp = NULL;
	pin_flist_t	*srch_o_flistp = NULL;
	pin_flist_t	*args_flistp   = NULL;
	pin_flist_t	*results_flistp = NULL;
	poid_t		*srch_pdp = NULL;
	poid_t		*i_pdp = NULL;
	poid_t		*acc_pdp = NULL;
	int32		srch_flags = 1;
        char		*acc_nop = NULL;
        char            *templatep = "select X from /account where F1 = V1 ";

	if (PIN_ERR_IS_ERR(ebufp))
		return NULL;
	PIN_ERR_CLEAR_ERR(ebufp);	

	i_pdp = PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_POID, 0, ebufp);	
	acc_nop = PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_ACCOUNT_NO, 0, ebufp);	

        srch_pdp = PIN_POID_CREATE(PIN_POID_GET_DB(i_pdp), "/search", -1, ebufp);

        srch_i_flistp = PIN_FLIST_CREATE(ebufp);
        PIN_FLIST_FLD_PUT(srch_i_flistp, PIN_FLD_POID, srch_pdp, ebufp);
        PIN_FLIST_FLD_SET(srch_i_flistp, PIN_FLD_FLAGS, &srch_flags, ebufp);
        PIN_FLIST_FLD_SET(srch_i_flistp, PIN_FLD_TEMPLATE, templatep, ebufp);

        args_flistp = PIN_FLIST_ELEM_ADD(srch_i_flistp, PIN_FLD_ARGS, 1, ebufp);
        PIN_FLIST_FLD_SET(args_flistp, PIN_FLD_ACCOUNT_NO, acc_nop, ebufp);

	results_flistp = PIN_FLIST_ELEM_ADD(srch_i_flistp, PIN_FLD_RESULTS, 0, ebufp);
        PIN_FLIST_FLD_SET(results_flistp, PIN_FLD_BUSINESS_TYPE, NULL, ebufp);

	PIN_ERR_LOG_FLIST(3, "Account search flist", srch_i_flistp);

	PCM_OP(ctxp, PCM_OP_SEARCH, 0, srch_i_flistp, &srch_o_flistp, ebufp);

	PIN_FLIST_DESTROY_EX(&srch_i_flistp, ebufp);

	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, "Error during account_search", ebufp);
                goto CLEANUP;
        }

        PIN_ERR_LOG_FLIST(3, "Account search out flist", srch_o_flistp);

	results_flistp = PIN_FLIST_ELEM_GET(srch_o_flistp, PIN_FLD_RESULTS, 0, 1, ebufp);

	if (results_flistp)
	{
        	acc_pdp = PIN_FLIST_FLD_TAKE(results_flistp, PIN_FLD_POID, 0, ebufp);
	}

CLEANUP:
        PIN_FLIST_DESTROY_EX(&srch_o_flistp, ebufp);
        return acc_pdp;
}
