/******************************************************************************
 *
 *   Copyright (c) 2017 DexYP. All rights reserved.
 *
 *   This material is the confidential property of DexYP
 *   or its licensors and may be used, reproduced, stored or transmitted
 *   only in accordance with a valid DexYP license or sublicense agreement.
 *
 ******************************************************************************/
/*******************************************************************************
 *   Maintentance Log:
 *
 *   Date: 20171103
 *   Author: Ebillsoft
 *   Identifier: N/A
 *   Notes: Configuration file for DexYP AR opcodes
 *******************************************************************************/
/*******************************************************************************
 *   File History
 *
 *   DATE   |  CHANGE                                           |  Author
 ***********|***************************************************|****************
 |11/03/2017|Compensation opcode added				| Victor 
 |**********|***************************************************|****************
 |11/04/2017|Unallocate subsequent payment opcode added 	| Rama Puppala
 *********************************************************************************/
#ifndef lint
static const char Sccs_id[] = "@(#)$Id: fm_dex_ar_config.c /cgbubrm_1.0.0.ar/6 2012/01/17 08:53:14 arevav01 Exp $";
#endif

#include <stdio.h>
#include <string.h>

#include <pcm.h>
#include <pinlog.h>

#include "cm_fm.h"
#include "ops/ar.h"

#include "dex_ops.h"

#ifdef MSDOS
__declspec(dllexport) void * fm_dex_ar_config_func();
#endif

/*******************************************************************
 * NOTE THAT THE DISPATCH ENTRIES ARE COMMENTED. WHEN YOU OVERRIDE
 * AN IMPLEMENTATION, UNCOMMENT THE LINE BELOW THAT MATCHES THE
 * OPCODE FOR WHICH YOU HAVE PROVIDED AN ALTERNATE IMPLEMENTATION.
 *******************************************************************/

struct cm_fm_config fm_dex_ar_config[] = {
	/* opcode as a int32, function name (as a string) */

 	{ DEX_OP_AR_PROCESS_COMPENSATION, "op_dex_ar_process_compensation" }, 
	{ DEX_OP_AR_UNALLOCATE_PYMTS, "op_ar_unallocate_pymts" },
	{ 0,	(char *)0 }
};

#ifdef MSDOS
void *
fm_dex_ar_config_func()
{
  return ((void *) (fm_dex_ar_config));
}
#endif
