/*******************************************************************************
 *
 *      Copyright (c) 2017 DexYP All rights reserved.
 *
 *      This material is the confidential property of DexYP or its
 *      licensors and may be used, reproduced, stored or transmitted only in
 *      accordance with a valid DexYP license or sublicense agreement.
 *
 ******************************************************************************/
/*******************************************************************************
 * Maintentance Log:
 *
 * Date: 20171031
 * Author: Ebillsoft
 * Identifier: N/A
 * Notes: Unallocate payments after a given date 
 ******************************************************************************/
/*******************************************************************************
 * File History
 *
 * DATE     |  CHANGE                                           |  Author
 ******* ***|***************************************************|****************
 |10/31/2017| Unallocate payments after a given date		| Rama Puppala
 |**********|***************************************************|****************
 ********************************************************************************/

#ifndef lint
static  char    Sccs_id[] = "@(#)$Id: Exp $";
#endif

#include <stdio.h>
#include "pcm.h"
#include "ops/bill.h"
#include "ops/ar.h"
#include "cm_fm.h"
#include "pin_errs.h"
#include "pin_bill.h"
#include "pin_cust.h"
#include "pinlog.h"
#include "pin_os.h"
#include "dex_ops.h"
#include "dex_flds.h"

#define PIN_ERR_NOT_PROPER_FORMAT 93

EXPORT_OP void
op_ar_unallocate_pymts(
	cm_nap_connection_t	*connp,
	u_int			opcode,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**ret_flistpp,
	pin_errbuf_t		*ebufp);

static void
dex_op_ar_unallocate_pymts(
	pcm_context_t		*ctxp,
	cm_op_info_t		*opstackp,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**out_flistpp,
        pin_errbuf_t		*ebufp);

static void
fm_dex_get_pymts(
	pcm_context_t		*ctxp,
	pin_flist_t		*i_flistp,
	pin_flist_t		**r_flistpp,
	pin_errbuf_t		*ebufp);

static void
fm_dex_unallocate_pymt_item(
	pcm_context_t	*ctxp,
	u_int		flags,
	pin_flist_t	*i_flistp,
	pin_errbuf_t	*ebufp);

static void
fm_dex_transfer_to_pymt_item(
	pcm_context_t	*ctxp,
	u_int		flags,
	pin_flist_t	*i_flistp,
	pin_errbuf_t	*ebufp);

PIN_IMPORT char *
fm_dex_get_acc_type(
	pcm_context_t		*ctxp,
	poid_t			*i_pdp,
	pin_errbuf_t		*ebufp); 

/*******************************************************************
 * Main routine for the DEX_OP_AR_UNALLOCATE_PYMTS command
 *******************************************************************/
void
op_ar_unallocate_pymts(
	cm_nap_connection_t	*connp,
	u_int			opcode,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**ret_flistpp,
	pin_errbuf_t		*ebufp)
{
	pcm_context_t		*ctxp = connp->dm_ctx;
	pin_flist_t		*r_flistp = NULL;
	cm_op_info_t		*opstackp = connp->coip;

	/***********************************************************
	 * Null out results until we have some.
	 ***********************************************************/
	*ret_flistpp = NULL;
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Insanity check.
	 ***********************************************************/
	if (opcode != DEX_OP_AR_UNALLOCATE_PYMTS) {
		pin_set_err(ebufp, PIN_ERRLOC_FM,
			PIN_ERRCLASS_SYSTEM_DETERMINATE,
			PIN_ERR_BAD_OPCODE, 0, 0, opcode);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"bad opcode in op_ar_unallocate_pymts", ebufp);
		return;
	}

	/***********************************************************
	 * Debug: What did we get?
	 ***********************************************************/
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"op_ar_unallocate_pymts input flist", in_flistp);

	/***********************************************************
	 * Call main function to do it
	 ***********************************************************/
	dex_op_ar_unallocate_pymts(ctxp, opstackp, flags, in_flistp, &r_flistp, ebufp);

	/***********************************************************
	 * Results.
	 ***********************************************************/
	if (PIN_ERR_IS_ERR(ebufp)) {
		*ret_flistpp = (pin_flist_t *)NULL;
		PIN_FLIST_DESTROY(r_flistp, NULL);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"op_ar_unallocate_pymts error", ebufp);
	} else {
		*ret_flistpp = r_flistp;
		PIN_ERR_CLEAR_ERR(ebufp);
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
			"op_ar_unallocate_pymts return flist", r_flistp);
	}

	return;
}

/*******************************************************************
 * dex_op_ar_unallocate_pymts()
 *
 * Implementation of unallocate sub sequent payments	
 *
 *******************************************************************/
static void
dex_op_ar_unallocate_pymts(
	pcm_context_t	*ctxp,
	cm_op_info_t	*opstackp,
	u_int		flags,
	pin_flist_t	*in_flistp,
	pin_flist_t	**out_flistpp,
        pin_errbuf_t	*ebufp)
{
	pin_flist_t	*i_flistp = NULL;
	pin_flist_t	*o_flistp = NULL;
	pin_flist_t	*initial_flistp = NULL;
	pin_flist_t	*item_flistp = NULL;
	pin_cookie_t	cookie = NULL;
	poid_t		*pdp = NULL;
	char		*acc_typep = NULL;
	char		debug_msg[255];
	void            *vp = NULL;
	int32		rec_id = 0;
	int32		status=-1;
	int32		stack_opcode = 0;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	pdp = PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_ACCOUNT_OBJ, 0, ebufp);
	
	acc_typep = fm_dex_get_acc_type(ctxp, pdp, ebufp);

	if (acc_typep && strstr(acc_typep, "National"))
	{
		pin_free(acc_typep);
		return;
	}

	pin_free(acc_typep);

	while(opstackp != NULL)
	{
		stack_opcode = opstackp->opcode;

		sprintf(debug_msg, "OPCODE=%d", stack_opcode);
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, debug_msg);

		initial_flistp = opstackp->in_flistp; 
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "stack input FLIST", initial_flistp);		 
	
		/*********************************************************
		 * Check if opcode is PCM_OP_BILL_REVERSE_PAYMENT, 
		 * then get item poid 
		 * ******************************************************/ 	
		if (stack_opcode == 138)
		{
			break;
		}
		opstackp = opstackp->next;
	}

	fm_dex_get_pymts(ctxp, initial_flistp, &o_flistp, ebufp);

	while ((item_flistp = PIN_FLIST_ELEM_GET_NEXT(o_flistp, PIN_FLD_RESULTS,
                &rec_id, 1, &cookie, ebufp)) != (pin_flist_t *)NULL)
        {
		fm_dex_unallocate_pymt_item(ctxp, flags, item_flistp, ebufp);
	}	

	PIN_FLIST_DESTROY_EX(&o_flistp, NULL);
	/***********************************************************
	 * Error?
	 ***********************************************************/
	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"dex_op_ar_unallocate_pymts error", ebufp);

		*out_flistpp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, 
			*out_flistpp, PIN_FLD_POID, ebufp);
		PIN_FLIST_FLD_SET(*out_flistpp, PIN_FLD_STATUS, &status, ebufp);
		PIN_FLIST_FLD_SET(*out_flistpp, PIN_FLD_ERROR_DESCR,
			"Failed", ebufp);
		 PIN_FLIST_FLD_SET(*out_flistpp, PIN_FLD_ERROR_CODE, "20001", ebufp);
	}
	else
	{
		status = 0;
		*out_flistpp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_COPY(in_flistp, PIN_FLD_POID, 
			*out_flistpp, PIN_FLD_POID, ebufp);
		PIN_FLIST_FLD_SET(*out_flistpp, PIN_FLD_STATUS, &status, ebufp);
		PIN_FLIST_FLD_SET(*out_flistpp, PIN_FLD_DESCR, "Success", ebufp);
		
	}
	return;
}


/*******************************************************************
 * fm_dex_get_pymts()
 *
 * This function gets payment items after effective_t 
 *
 *******************************************************************/
static void
fm_dex_get_pymts(
	pcm_context_t	*ctxp,
	pin_flist_t	*i_flistp,
	pin_flist_t	**r_flistpp,
	pin_errbuf_t	*ebufp) 
{
	pin_flist_t	*srch_i_flistp = NULL;
	pin_flist_t	*srch_o_flistp = NULL;
	pin_flist_t	*args_flistp   = NULL;
	pin_flist_t	*results_flistp = NULL;
	poid_t		*srch_pdp = NULL;
	pin_decimal_t	*zerop = NULL;
        char            *templatep = "select X from /item where F1 = V1 and F2.type = V2 and F3 > V3 and F4 = V4";
	time_t		*eff_tp = NULL;
	int64		db = -1;
	int32		srch_flags = 256;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);	

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "fm_dex_get_pymts input FLIST", i_flistp);		 

	srch_i_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_COPY(i_flistp, PIN_FLD_OBJECT, srch_i_flistp, PIN_FLD_POID, ebufp);
	PIN_FLIST_FLD_SET(srch_i_flistp, PIN_FLD_EFFECTIVE_T, NULL, ebufp);

	PCM_OP(ctxp, PCM_OP_READ_FLDS, PCM_OPFLG_CACHEABLE, srch_i_flistp, &srch_o_flistp, ebufp);

	PIN_FLIST_DESTROY_EX(&srch_i_flistp, NULL);
	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_FLIST_DESTROY_EX(&srch_o_flistp, NULL);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"fm_dex_get_pymts RFLDS error", ebufp);
		return;
	}
	
	eff_tp = PIN_FLIST_FLD_TAKE(srch_o_flistp, PIN_FLD_EFFECTIVE_T, 0, ebufp);

	PIN_FLIST_DESTROY_EX(&srch_o_flistp, NULL);
	db = PIN_POID_GET_DB(PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_POID, 0, ebufp));
        srch_pdp = PIN_POID_CREATE(db, "/search", -1, ebufp);

        srch_i_flistp = PIN_FLIST_CREATE(ebufp);
        PIN_FLIST_FLD_PUT(srch_i_flistp, PIN_FLD_POID, srch_pdp, ebufp);
        PIN_FLIST_FLD_SET(srch_i_flistp, PIN_FLD_FLAGS, &srch_flags, ebufp);
        PIN_FLIST_FLD_SET(srch_i_flistp, PIN_FLD_TEMPLATE, templatep, ebufp);

        args_flistp = PIN_FLIST_ELEM_ADD(srch_i_flistp, PIN_FLD_ARGS, 1, ebufp);
	PIN_FLIST_FLD_COPY(i_flistp, PIN_FLD_POID, args_flistp, PIN_FLD_ACCOUNT_OBJ, ebufp);

        args_flistp = PIN_FLIST_ELEM_ADD(srch_i_flistp, PIN_FLD_ARGS, 2, ebufp);
	PIN_FLIST_FLD_PUT(args_flistp, PIN_FLD_POID, 
		PIN_POID_CREATE(db, "/item/payment", -1, ebufp), ebufp);

        args_flistp = PIN_FLIST_ELEM_ADD(srch_i_flistp, PIN_FLD_ARGS, 3, ebufp);
	PIN_FLIST_FLD_PUT(args_flistp, PIN_FLD_EFFECTIVE_T, eff_tp, ebufp);

	zerop = pbo_decimal_from_str("0.0", ebufp);
        args_flistp = PIN_FLIST_ELEM_ADD(srch_i_flistp, PIN_FLD_ARGS, 4, ebufp);
	PIN_FLIST_FLD_PUT(args_flistp, PIN_FLD_RECVD, zerop, ebufp);

        results_flistp = PIN_FLIST_ELEM_ADD(srch_i_flistp, PIN_FLD_RESULTS, 0, ebufp);
        PIN_FLIST_FLD_SET(results_flistp, PIN_FLD_ITEM_TOTAL, NULL, ebufp);

	PIN_ERR_LOG_FLIST(3, "fm_dex_get_pymts search flist", srch_i_flistp);

	PCM_OP(ctxp, PCM_OP_SEARCH, 0, srch_i_flistp, r_flistpp, ebufp);

	PIN_FLIST_DESTROY_EX(&srch_i_flistp, ebufp);

	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_FLIST_DESTROY_EX(r_flistpp, ebufp);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"Error during fm_dex_get_pymts search", ebufp);
        }

        PIN_ERR_LOG_FLIST(3, "fm_dex_get_pymts search out flist", *r_flistpp);
	
	return;
}

/*******************************************************************
 * fm_dex_unallocate_pymt_item()
 *
 * This function will find out transfer events and reverse the same 
 *
 *******************************************************************/
static void
fm_dex_unallocate_pymt_item(
	pcm_context_t	*ctxp,
	u_int		flags,
	pin_flist_t	*i_flistp,
	pin_errbuf_t	*ebufp) 
{
	pin_flist_t	*srch_i_flistp = NULL;
	pin_flist_t	*srch_o_flistp = NULL;
	pin_flist_t	*args_flistp   = NULL;
	pin_flist_t	*results_flistp = NULL;
	pin_flist_t	*evt_flistp = NULL;
	pin_flist_t	*wfld_i_flistp = NULL;
	pin_flist_t	*wfld_o_flistp = NULL;
	pin_cookie_t	cookie = NULL;
	poid_t		*srch_pdp = NULL;
	poid_t		*item_pdp = NULL;
        char            *templatep = "select X from /event where F1 = V1 and F2 like V2 ";
	int64		db = -1;
	int32		srch_flags = 256;
	int32		elemid = 0;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);	

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "fm_dex_unallocate_pymt_item input FLIST", i_flistp);		 
	db = PIN_POID_GET_DB(PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_POID, 0, ebufp));
        srch_pdp = PIN_POID_CREATE(db, "/search", -1, ebufp);

        srch_i_flistp = PIN_FLIST_CREATE(ebufp);
        PIN_FLIST_FLD_PUT(srch_i_flistp, PIN_FLD_POID, srch_pdp, ebufp);
        PIN_FLIST_FLD_SET(srch_i_flistp, PIN_FLD_FLAGS, &srch_flags, ebufp);
        PIN_FLIST_FLD_SET(srch_i_flistp, PIN_FLD_TEMPLATE, templatep, ebufp);

        args_flistp = PIN_FLIST_ELEM_ADD(srch_i_flistp, PIN_FLD_ARGS, 1, ebufp);
	PIN_FLIST_FLD_COPY(i_flistp, PIN_FLD_POID, args_flistp, PIN_FLD_ITEM_OBJ, ebufp);

        args_flistp = PIN_FLIST_ELEM_ADD(srch_i_flistp, PIN_FLD_ARGS, 2, ebufp);
	PIN_FLIST_FLD_PUT(args_flistp, PIN_FLD_POID, 
		PIN_POID_CREATE(db, "/event/billing/item/transfer", -1, ebufp), ebufp);

        results_flistp = PIN_FLIST_ELEM_ADD(srch_i_flistp, PIN_FLD_RESULTS, 0, ebufp);
        PIN_FLIST_FLD_SET(results_flistp, PIN_FLD_ACCOUNT_OBJ, NULL, ebufp);
        PIN_FLIST_FLD_SET(results_flistp, PIN_FLD_ITEM_OBJ, NULL, ebufp);
        results_flistp = PIN_FLIST_ELEM_ADD(results_flistp, PIN_FLD_ACTION_INFO, 0, ebufp);

	PIN_ERR_LOG_FLIST(3, "fm_dex_unallocate_pymt_item search flist", srch_i_flistp);

	PCM_OP(ctxp, PCM_OP_SEARCH, 0, srch_i_flistp, &srch_o_flistp, ebufp);

	PIN_FLIST_DESTROY_EX(&srch_i_flistp, ebufp);

	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_FLIST_DESTROY_EX(&srch_o_flistp, NULL);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"Error during fm_dex_unallocate_pymt_item search", ebufp);
        }

        PIN_ERR_LOG_FLIST(3, "fm_dex_unallocate_pymt_item search out flist", srch_o_flistp);
	
        item_pdp = PIN_POID_CREATE(db, "/item/payment", -1, ebufp);

	while ((evt_flistp = PIN_FLIST_ELEM_GET_NEXT(srch_o_flistp, PIN_FLD_RESULTS,
                &elemid, 1, &cookie, ebufp)) != (pin_flist_t *)NULL)
        {
		fm_dex_transfer_to_pymt_item(ctxp, flags, evt_flistp, ebufp);

		if (PIN_ERR_IS_ERR(ebufp))
		{
			PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
				"Error from fm_dex_transfer_to_pymt_item", ebufp);
			break;
        	}	

		/**********************************************
 		 * Delink payment item from transfer event
 		 **********************************************/ 	
		wfld_i_flistp = PIN_FLIST_CREATE(ebufp);	
		PIN_FLIST_FLD_COPY(evt_flistp, PIN_FLD_POID, wfld_i_flistp, PIN_FLD_POID, ebufp);
        	PIN_FLIST_FLD_SET(wfld_i_flistp, PIN_FLD_ITEM_OBJ, item_pdp, ebufp);

		PIN_ERR_LOG_FLIST(3, "Input flist for updating event", wfld_i_flistp);
        
		PCM_OP(ctxp, PCM_OP_WRITE_FLDS, flags, wfld_i_flistp, &wfld_o_flistp, ebufp);
		PIN_FLIST_DESTROY_EX(&wfld_i_flistp, NULL);        
 
        	PIN_ERR_LOG_FLIST(3, "Output flist from updating evt", wfld_o_flistp);

		if (PIN_ERR_IS_ERR(ebufp))
		{
			PIN_FLIST_DESTROY_EX(&wfld_o_flistp, NULL);        
			PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, "Error in update evt", ebufp);
			break;
		}
		PIN_FLIST_DESTROY_EX(&wfld_o_flistp, NULL);        
	}	

	PIN_FLIST_DESTROY_EX(&srch_o_flistp, NULL);
	PIN_POID_DESTROY(item_pdp, ebufp);
	return;
}

/*******************************************************************
 * fm_dex_transfer_to_pymt_item()
 *
 * This function will reverse the transfer 
 *
 *******************************************************************/
static void
fm_dex_transfer_to_pymt_item(
	pcm_context_t	*ctxp,
	u_int		flags,
	pin_flist_t	*i_flistp,
	pin_errbuf_t	*ebufp) 
{
	pin_flist_t	*trans_i_flistp = NULL;
	pin_flist_t	*trans_o_flistp = NULL;
	pin_flist_t	*act_info_flistp   = NULL;
	pin_flist_t	*trans_flistp = NULL;
	pin_flist_t	*item_flistp = NULL;
	pin_flist_t	*wfld_i_flistp = NULL;
	pin_flist_t	*wfld_o_flistp = NULL;
	pin_decimal_t	*amtp = NULL;
	pin_buf_t	*pin_bufp = NULL;
	pin_cookie_t	cookie = NULL;
	poid_t		*item_pdp = NULL;
        char            *program_namep = "dex_op_ar_unallocate_pymts";
        char            *descp = "item transfer (unallocate)";
	int64		db = -1;
	int32		srch_flags = 256;
	int32		elemid = 0;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);	

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "fm_dex_transfer_to_pymt_item input FLIST", i_flistp);
	
	act_info_flistp = PIN_FLIST_ELEM_GET(i_flistp, PIN_FLD_ACTION_INFO, 0, 0, ebufp);
	pin_bufp = (pin_buf_t *)PIN_FLIST_FLD_GET(act_info_flistp, PIN_FLD_BUFFER,
                                                   0, ebufp);

	if ( pin_bufp == NULL || pin_bufp->data == NULL )
	{
		/*******************************************
 		 * Buffer could be null and we will bail out
 		 * *****************************************/ 	
		return;
	}
	else
	{
		pin_str_to_flist(pin_bufp->data, 0, &trans_flistp, ebufp);

		if( PIN_ERR_IS_ERR( ebufp ))
		{
			pin_errbuf_set_err(ebufp, PIN_ERRLOC_FM, PIN_ERRCLASS_APPLICATION,
                                PIN_ERR_BAD_VALUE, PIN_FLD_FORMATS, 0, 0,PIN_DOMAIN_ERRORS,
                                PIN_ERR_NOT_PROPER_FORMAT, 1,0, NULL);
                        PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
                                "Error putting buffer onto flist", ebufp);
			return;
		}
	}

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "fm_dex_transfer_to_pymt trans items", trans_flistp);

	while ((item_flistp = PIN_FLIST_ELEM_GET_NEXT(trans_flistp, PIN_FLD_ITEMS,
		&elemid, 1, &cookie, ebufp)) != (pin_flist_t *)NULL)
	{
		/**********************************************
 		 * Now reverse transfer amount 
 		 **********************************************/ 	
		amtp = PIN_FLIST_FLD_TAKE(item_flistp, PIN_FLD_AMOUNT, 0, ebufp);
		pbo_decimal_negate_assign(amtp, ebufp);	
		PIN_FLIST_FLD_PUT(item_flistp, PIN_FLD_AMOUNT, amtp, ebufp);

		item_pdp = PIN_FLIST_FLD_TAKE(item_flistp, PIN_FLD_ITEM_OBJ, 0, ebufp);
		PIN_FLIST_FLD_PUT(item_flistp, PIN_FLD_POID, item_pdp, ebufp);
	}
	
	/***********************************************************
 	 * Now call BILL_ITEM_TRANSFER opcode to reverse allocation
 	 ***********************************************************/
	trans_i_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_COPY(i_flistp, PIN_FLD_ACCOUNT_OBJ, trans_i_flistp, PIN_FLD_POID, ebufp);
	PIN_FLIST_FLD_SET(trans_i_flistp, PIN_FLD_PROGRAM_NAME, program_namep, ebufp); 
	PIN_FLIST_FLD_SET(trans_i_flistp, PIN_FLD_DESCR, descp, ebufp); 
	PIN_FLIST_FLD_COPY(i_flistp, PIN_FLD_ITEM_OBJ, trans_i_flistp, PIN_FLD_ITEM_OBJ, ebufp);
	PIN_FLIST_CONCAT(trans_i_flistp, trans_flistp, ebufp);

	PIN_ERR_LOG_FLIST(3, "Input flist for transfer item", trans_i_flistp);

	PCM_OP(ctxp, PCM_OP_BILL_ITEM_TRANSFER, flags, trans_i_flistp, &trans_o_flistp, ebufp);

	PIN_FLIST_DESTROY_EX(&trans_i_flistp, NULL);        
	PIN_FLIST_DESTROY_EX(&trans_flistp, NULL);        

	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_FLIST_DESTROY_EX(&trans_o_flistp, NULL);        
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, "Error in transfer item", ebufp);
		return;
	}

	PIN_ERR_LOG_FLIST(3, "Output flist from transfer item", trans_o_flistp);

	item_flistp = PIN_FLIST_ELEM_GET(trans_o_flistp, PIN_FLD_RESULTS, 0, 0, ebufp);

	/**********************************************
 	 * Delink payment item from transfer event
 	 **********************************************/ 	
	wfld_i_flistp = PIN_FLIST_CREATE(ebufp);	

	PIN_FLIST_FLD_COPY(item_flistp, PIN_FLD_POID, wfld_i_flistp, PIN_FLD_POID, ebufp);
        item_pdp = PIN_POID_CREATE(db, "/item/payment", -1, ebufp);
       	PIN_FLIST_FLD_PUT(wfld_i_flistp, PIN_FLD_ITEM_OBJ, item_pdp, ebufp);

	PIN_ERR_LOG_FLIST(3, "Input flist for updating event", wfld_i_flistp);
        
	PCM_OP(ctxp, PCM_OP_WRITE_FLDS, flags, wfld_i_flistp, &wfld_o_flistp, ebufp);
        
	PIN_FLIST_DESTROY_EX(&wfld_i_flistp, NULL);        

       	PIN_ERR_LOG_FLIST(3, "Output flist from updating evt", wfld_o_flistp);

	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, "Error in update evt", ebufp);
	}

	PIN_FLIST_DESTROY_EX(&wfld_o_flistp, NULL);        
	PIN_FLIST_DESTROY_EX(&trans_o_flistp, NULL);

	return;
}
