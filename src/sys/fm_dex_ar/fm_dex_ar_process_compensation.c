/*
 *      Copyright (c) 2001-2006 Oracle. All rights reserved.
 *
 *      This material is the confidential property of Oracle Corporation
 *      or its licensors and may be used, reproduced, stored or transmitted
 *      only in accordance with a valid Oracle license or sublicense agreement.
 */

#ifndef lint
static const char Sccs_id[] = "@(#) %full_filespec: fm_dex_ar_process_compensation.c~2:csrc:1 %";
#endif

#include <stdio.h>
#include <string.h>

#include <pcm.h>
#include <pinlog.h>

#include "cm_fm.h"
#include "cm_cache.h"
#include "fm_utils.h"
#include "pin_cust.h"
#include "fm_collections.h"

#include "dex_ops.h"
#include "dex_flds.h"

#ifdef MSDOS
#include <WINDOWS.h>
#endif


/*******************************************************************
 * Routines defined here.
 *******************************************************************/

EXPORT_OP void
op_dex_ar_process_compensation(
	cm_nap_connection_t	*connp, 
	int32			opcode, 
	int32			flags, 
	pin_flist_t		*in_flistp, 
	pin_flist_t		**ret_flistpp, 
	pin_errbuf_t	*ebufp);

static void
fm_dex_ar_process_compensation(
	pcm_context_t	*ctxp, 
	int32			flags, 
	pin_flist_t		*in_flistp, 
	pin_flist_t		**ret_flistpp, 
	pin_errbuf_t	*ebufp);

static char *
fm_dex_ar_process_compensation_get_eaid(
	pcm_context_t	*ctxp, 
	poid_t	        *a_pdp, 
	pin_errbuf_t	*ebufp);

static void
fm_dex_ar_process_compensation_create_obj(
	pcm_context_t		*ctxp, 
	char			*eaid,
	pin_flist_t		*in_flistp, 
	pin_flist_t		*tmp_flistp, 
	pin_errbuf_t		*ebufp);


/*******************************************************************
 * Main routine for the DEX_OP_AR_PROCESS_COMPENSATION 
 *******************************************************************/
void
op_dex_ar_process_compensation(
	cm_nap_connection_t	*connp, 
	int32			opcode, 
	int32			flags, 
	pin_flist_t		*in_flistp, 
	pin_flist_t		**ret_flistpp, 
	pin_errbuf_t		*ebufp)
{
	pcm_context_t		*ctxp = connp->dm_ctx;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Insanity check.
	 ***********************************************************/
	if (opcode != DEX_OP_AR_PROCESS_COMPENSATION) {
		pin_set_err(ebufp, PIN_ERRLOC_FM,
			PIN_ERRCLASS_SYSTEM_DETERMINATE,
			PIN_ERR_BAD_OPCODE, 0, 0, opcode);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"op_dex_ar_process_compensation opcode error", 
			ebufp);
		return;
	}

	/***********************************************************
	 * Debug: What we got.
	 ***********************************************************/

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"op_dex_ar_process_compensation input flist", in_flistp);

	/***********************************************************
	 * Do the actual op in a sub. Copy it since we may need to
	 * replace it later.
	 ***********************************************************/

	fm_dex_ar_process_compensation(ctxp, flags, in_flistp, 
			ret_flistpp, ebufp);

	/***********************************************************
	 * Error?
	 ***********************************************************/
	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"op_dex_ar_process_compensation error", ebufp);
	} else {
		/***************************************************
		 * Debug: What we're sending back.
		 ***************************************************/
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
			"op_dex_ar_process_compensation return flist", 
			*ret_flistpp);
	}
	return;
}

/********************************************************************************
 * fm_dex_ar_process_compensation()
 *
 * This function calls process_compensation and create /dex_compensation objects
 *
 ********************************************************************************/
static void
fm_dex_ar_process_compensation(
	pcm_context_t		*ctxp, 
	int32			flags, 
	pin_flist_t		*in_flistp, 
	pin_flist_t		**ret_flistpp, 
	pin_errbuf_t		*ebufp)
{
	int32			result = PIN_BOOLEAN_FALSE;
	char			msg[1024];
	char			func_name[] = "fm_dex_ar_process_compensation";
	
	pin_flist_t		*action_info_flistp;
	pin_flist_t		*item_flistp; 
        pin_flist_t    		*flpBuf	= NULL;
	pin_flist_t		*pymt_flistp;

	poid_t			*a_pdp = NULL;
	poid_t			*e_pdp = NULL;
	poid_t			*i_pdp = NULL;
	poid_t			*comp_pdp =  NULL;
	char			*eaid_value = NULL;
	char			*trans_id = NULL;
	void			*amount = NULL;

	pin_cookie_t		cookie = NULL;
	int32			rec_id = 0;

        pin_buf_t      		*pin_bufp = NULL;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	sprintf(msg, "%s %s", func_name, "input flist");
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, in_flistp);

	// Get the input values
	e_pdp = (poid_t *)PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_POID, 0, ebufp);
	a_pdp = (poid_t *)PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_ACCOUNT_OBJ, 0, ebufp);
	i_pdp = (poid_t *)PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_ITEM_OBJ, 0, ebufp);
	
 
    	// Retrieve the eaid value for the account as part of input values
	eaid_value = fm_dex_ar_process_compensation_get_eaid (ctxp, a_pdp, ebufp);
	
	if (PIN_ERR_IS_ERR(ebufp)){
		sprintf(msg, "%s %s", func_name, "Error in fm_dex_ar_process_compensation_get_eaid function");
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		goto CLEANUP;
	}

	
	if (strncmp((char *)PIN_POID_GET_TYPE(e_pdp), "/event/billing/payment/", sizeof("/event/billing/payment")) == 0) {   
		sprintf(msg, "%s", "the event type is /event/billing/payment/");
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);

		pymt_flistp = PIN_FLIST_SUBSTR_GET (in_flistp, PIN_FLD_PAYMENT, PIN_ELEMID_ANY, ebufp);
		fm_dex_ar_process_compensation_create_obj(ctxp, eaid_value, in_flistp, pymt_flistp, ebufp);
        }
        else if ((strncmp((char *)PIN_POID_GET_TYPE(e_pdp), "/event/billing/item/transfer", sizeof("/event/billing/item/transfer")) == 0) &&
             (strcmp(PIN_POID_GET_TYPE(i_pdp), "/item/payment") == 0) )
        {
		// Get the PIN_FLD_ACTION_INFO array from the input flist
		action_info_flistp = PIN_FLIST_ELEM_GET(in_flistp, PIN_FLD_ACTION_INFO, PIN_ELEMID_ANY, 0, ebufp);
		pin_bufp = (pin_buf_t *)PIN_FLIST_FLD_GET(action_info_flistp, PIN_FLD_BUFFER, 0, ebufp );
		if ( pin_bufp == NULL || pin_bufp->data == NULL ) {

			/* not strictly an error.  The buffer could be null
 			 ** But we'll output a null pointer anyway.
 			**/
			flpBuf = NULL;
		}
		else {
			PIN_STR_TO_FLIST(pin_bufp->data, 0, &flpBuf, ebufp );
			if( PIN_ERR_IS_ERR( ebufp )) {
				PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, "CV - Error putting buffer onto flist", ebufp);
				PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, "Error putting buffer onto flist", ebufp);
				PIN_FLIST_DESTROY_EX(&flpBuf, NULL);  
				goto CLEANUP;
			}
		}
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "Item object from BUFFER", flpBuf);
		
		// IF the buffer is not null then loop thru the flist to retrieve
		// each PIN_FLD_ITEMS and create the object
		rec_id = 0;
		cookie = NULL;
		while ((item_flistp = PIN_FLIST_ELEM_GET_NEXT(flpBuf, PIN_FLD_ITEMS, &rec_id, 1, &cookie, ebufp)) != NULL )
		{
	   		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, "I am in while");
			fm_dex_ar_process_compensation_create_obj(ctxp, eaid_value, in_flistp, item_flistp, ebufp);
		}
        }
        else
        {
           	// Set the values in the input flist for creating the object
	   	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, "I am in else");
		fm_dex_ar_process_compensation_create_obj(ctxp, eaid_value, in_flistp, NULL, ebufp);

        }
	if (PIN_ERR_IS_ERR(ebufp)){
		sprintf(msg, "%s %s", func_name, "Error returned from the function fm_dex_ar_process_compensation_create_obj");
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		goto CLEANUP;
	}


        // Prepare return flist
	*ret_flistpp = PIN_FLIST_CREATE(ebufp);
	result = PIN_BOOLEAN_TRUE;
	PIN_FLIST_FLD_SET(*ret_flistpp, PIN_FLD_RESULT, &result, ebufp);

CLEANUP:
	PIN_FLIST_DESTROY_EX(&flpBuf, NULL); 
	sprintf(msg, "%s %s", func_name, " return flist ");
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, *ret_flistpp);

}

/*******************************************************************
 * fm_dex_ar_process_compensation_get_eaid()
 *
 * This function searchs for EAID value 
 *
 *******************************************************************/
static char *
fm_dex_ar_process_compensation_get_eaid(
	pcm_context_t		*ctxp, 
	poid_t			*a_pdp, 
	pin_errbuf_t		*ebufp)
{
	poid_t			*srch_pdp = NULL;
	pin_flist_t		*srch_in_flistp = NULL; 
	pin_flist_t		*srch_out_flistp = NULL; 
	pin_flist_t		*att_flistp = NULL;
	pin_flist_t		*a_flistp = NULL; 
	pin_flist_t		*res_flistp = NULL; 
	int32			sflag = 0;
	int32			rec_id = 0;
	pin_cookie_t    	cookie = NULL;
	char			template[] = "select X from /profile/account where F1 = V1 ";
	char			msg[1024];
	char			func_name[] = "fm_dex_ar_process_compensation_search_profile";
	char			*eaid = NULL;

	if (PIN_ERR_IS_ERR(ebufp))
		return NULL;
	PIN_ERR_CLEAR_ERR(ebufp);

	/*************************************************
	 * Search on /profile/account to get the EAID
	 *************************************************/
	srch_in_flistp = PIN_FLIST_CREATE(ebufp);
	srch_pdp = PIN_POID_CREATE(PIN_POID_GET_DB(a_pdp), "/search", -1, ebufp);
	PIN_FLIST_FLD_PUT(srch_in_flistp, PIN_FLD_POID, (void *)srch_pdp, ebufp);
	PIN_FLIST_FLD_SET(srch_in_flistp, PIN_FLD_FLAGS, (void *)&sflag, ebufp);

	PIN_FLIST_FLD_SET(srch_in_flistp, PIN_FLD_TEMPLATE, (void *)template, ebufp);

	a_flistp = PIN_FLIST_ELEM_ADD(srch_in_flistp, PIN_FLD_ARGS, 1, ebufp);
	PIN_FLIST_FLD_SET(a_flistp, PIN_FLD_ACCOUNT_OBJ, a_pdp, ebufp);

	a_flistp = PIN_FLIST_ELEM_ADD(srch_in_flistp, PIN_FLD_RESULTS, 0, ebufp);
	PIN_FLIST_FLD_SET(a_flistp, PIN_FLD_POID, NULL, ebufp);
	PIN_FLIST_FLD_SET(a_flistp, PIN_FLD_ACCOUNT_OBJ, NULL, ebufp);
	PIN_FLIST_ELEM_SET(a_flistp, att_flistp, PIN_FLD_ATTRIBUTE_INFO, 0, ebufp);

	
	PCM_OP(ctxp, PCM_OP_SEARCH, 0, srch_in_flistp, &srch_out_flistp, ebufp);
	if (PIN_ERR_IS_ERR(ebufp)){
		sprintf(msg, "%s %s", func_name, "Error in /profile/account search");
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		PIN_FLIST_DESTROY_EX(&srch_in_flistp, NULL);
		PIN_FLIST_DESTROY_EX(&srch_out_flistp, NULL);
		goto CLEANUP;
	}
	sprintf(msg, "%s %s", func_name, "srch_out_flistp flist");
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, srch_out_flistp);
	
	//loop through search results and get the EAID value
	while ( (res_flistp = PIN_FLIST_ELEM_GET_NEXT (srch_out_flistp,
    		PIN_FLD_RESULTS, &rec_id, 1,&cookie, ebufp)) != (pin_flist_t *)NULL ) {
		
		att_flistp = PIN_FLIST_SUBSTR_GET (res_flistp, PIN_FLD_ATTRIBUTE_INFO, PIN_ELEMID_ANY, ebufp);

		eaid = PIN_FLIST_FLD_GET(att_flistp, DEX_FLD_EAID, 1, ebufp);
	}

CLEANUP:
	sprintf(msg, "%s %s %s", func_name, "eaid returned", eaid);
	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);
	PIN_FLIST_DESTROY_EX(&srch_in_flistp, NULL);
	PIN_FLIST_DESTROY_EX(&srch_out_flistp, NULL);
	sprintf(msg, "%s %s", func_name, "returning ");
	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);

	return eaid;
}





/*******************************************************************
 * fm_dex_ar_process_compensation_create_obj()
 *
 * This function create /dex_compensation object 
 *
 *******************************************************************/
static void
fm_dex_ar_process_compensation_create_obj(
	pcm_context_t		*ctxp, 
	char			*eaid,
	pin_flist_t		*in_flistp, 
	pin_flist_t		*tmp_flistp, 
	pin_errbuf_t		*ebufp)
{
	pin_flist_t		*create_in_flistp = NULL;
	pin_flist_t		*create_out_flistp = NULL;
	pin_flist_t		*pymt_flistp = NULL;
	//poid_t			*e_pdp = NULL;
	poid_t			*a_pdp = NULL;
	//poid_t			*i_pdp = NULL;

	poid_t			*comp_pdp =  NULL;
	char			*eaid_value = NULL;
	char			*trans_id = NULL;
	char			func_name[] = "fm_dex_ar_process_compensation_create_obj";
	char			msg[1024];
	void			*amount = NULL;

	
	// Get the input values
	//e_pdp = (poid_t *)PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_POID, 0, ebufp);
	a_pdp = (poid_t *)PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_ACCOUNT_OBJ, 0, ebufp);
	//i_pdp = (poid_t *)PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_ITEM_OBJ, 0, ebufp);
	
	/*************************************************
	 * CREATE_OBJ
	 *************************************************/
	create_in_flistp = PIN_FLIST_CREATE(ebufp);

	if ( tmp_flistp != NULL)
	{	
		trans_id = (char *)PIN_FLIST_FLD_GET(tmp_flistp, PIN_FLD_TRANS_ID, 1, ebufp);
		if ( trans_id != NULL)
		{
			PIN_FLIST_FLD_SET(create_in_flistp, DEX_FLD_PAYMENT_ID, trans_id, ebufp);
		}
		amount = (pin_decimal_t *)PIN_FLIST_FLD_GET(pymt_flistp, PIN_FLD_AMOUNT, 0, ebufp);
	}
	else
	{
		//Read the payment item to get the other values

	}

	
	comp_pdp = PIN_POID_CREATE(PIN_POID_GET_DB(a_pdp), "/dex_compensation", -1, ebufp);
	PIN_FLIST_FLD_PUT(create_in_flistp, PIN_FLD_POID, comp_pdp, ebufp);
	PIN_FLIST_FLD_SET(create_in_flistp, PIN_FLD_ACCOUNT_OBJ, a_pdp, ebufp);
	PIN_FLIST_FLD_SET(create_in_flistp, DEX_FLD_EAID, eaid, ebufp);
	PIN_FLIST_FLD_SET(create_in_flistp, DEX_FLD_CDL_AMT, (pin_decimal_t *)amount, ebufp);	

	if (PIN_ERR_IS_ERR(ebufp)){
		sprintf(msg, "%s %s", func_name, "Error in creating input flist for creating the object");
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		goto CLEANUP;
	}

	
	PCM_OP(ctxp, PCM_OP_CREATE_OBJ, 0, create_in_flistp, &create_out_flistp, ebufp);
	if (PIN_ERR_IS_ERR(ebufp)){
		sprintf(msg, "%s %s", func_name, "Error in create_obj");
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		goto CLEANUP;
	}
	sprintf(msg, "%s %s", func_name, "create_out_flistp");
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, create_out_flistp);

CLEANUP:
	PIN_FLIST_DESTROY_EX(&create_in_flistp, NULL);
	PIN_FLIST_DESTROY_EX(&create_out_flistp, NULL);
	sprintf(msg, "%s %s", func_name, "returning ");
	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);

}

