/**
 * fm_dex_ar_config.c
 *
 * Config for DEX_AR custom FM
 */

#include <stdio.h>
#include "pcm.h"
#include "cm_fm.h"
#include "ops/cust.h"
#include "cust_ops.h"
#include "dex_ops.h"

PIN_EXPORT void * fm_dex_order_config_func();

struct cm_fm_config fm_dex_order_config[] = {

	/* opcode-id (u_int), function name (string) */
	{ DEX_OP_ORDER_PROCESS_TOFULFILL, "op_dex_order_process_tofulfill" },
	{ 0, (char *) 0 }
};

void *
fm_dex_order_config_func()
{
	return ((void *) (fm_dex_order_config));
}
