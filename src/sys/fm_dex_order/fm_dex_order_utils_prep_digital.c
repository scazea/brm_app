#include "pcm.h"
#include "pcm_ops.h"
#include "pinlog.h"
#include "pin_act.h"
#include "pin_ar.h"
#include "pin_bill.h"
#include "pin_currency.h"
#include "pin_cust.h"
#include "pin_gsm.h"
#include "pin_pymt.h"
#include "pin_rate.h"
#include "pin_subscription.h"
#include "pin_type.h"
#include "ops/ar.h"
#include "ops/bill.h"
#include "ops/cust.h"
#include "ops/price.h"
#include "ops/pymt.h"
#include "ops/subscription.h"
#include "cm_fm.h"
#include "psiu_business_params.h"
#include "dex_flds.h"
#include "dex_constants.h"

void
fm_dex_order_utils_find_deal_by_name_type(
    pcm_context_t   *ctxp,
    poid_t          *a_pdp,
    char            *offer_namep,
    char            *service_typep,
    pin_flist_t     **out_flistpp,
    pin_errbuf_t    *ebufp );

void
fm_dex_order_utils_prep_modify_customer(
    pcm_context_t   *ctxp,
    poid_t          *a_pdp,
    pin_flist_t     *in_deal_flistp,
    pin_flist_t     *in_order_flistp,
    pin_flist_t     **opi_flistp,
    pin_errbuf_t    *ebufp );

extern char *deal_delimiterp;
/*******************************************************
 *This is main function to process order, This will do
 * 1. Get the deal from order and call function to get
 *  actual deal/products from BRM
 * 2. Get the service for the account, if found
 * Prepare purchase_deal flist
 * If not found, prepare modify customer flist
 ********************************************************/
void
fm_dex_order_utils_prep_tofulfill_digital(
    pcm_context_t   *ctxp,
    poid_t          *a_pdp,
    pin_flist_t     *in_flistp,
    pin_flist_t     **out_flistp,
    pin_errbuf_t    *ebufp )
{
    pin_flist_t     *deal_flistp = NULL;
    pin_flist_t     *udac_flistp = NULL;
    int32           status = DEX_STATUS_SUCCESS;
    char            func_name[] = "fm_dex_order_utils_prep_tofulfill_digital";
    char            msg[1024];
    char            item_descr[256];
    char            product_code[256];
    char            issue_num[256];
    char            udac[256];
    char            deal_name[1024];
    char            deal_name1[1024];
    void            *vp = NULL;


    if( PIN_ERR_IS_ERR(ebufp))
        return;
    PIN_ERR_CLEAR_ERR(ebufp);

    snprintf( msg, LOG_BUFFER_LENGTH - 1, "%s: input flist ", func_name);
    PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, msg, in_flistp);

    *out_flistp = PIN_FLIST_CREATE(ebufp);
    PIN_FLIST_FLD_SET(*out_flistp, PIN_FLD_POID, a_pdp, ebufp );

    /*******************************************************
     * Deal Name is combination
     * DEX_PRODUCT_CODE|DEX_UDAC|DESCR
     *******************************************************/
    vp = PIN_FLIST_FLD_GET(in_flistp, DEX_FLD_ITEM_DESCR, 0, ebufp);
    if(vp){
        sprintf(item_descr, "%s", (char *)vp);
    }

    vp = PIN_FLIST_FLD_GET(in_flistp, DEX_FLD_ITEM_DESCR, 0, ebufp);
    if(vp){
        sprintf(item_descr, "%s", (char *)vp);
    }

    udac_flistp = PIN_FLIST_ELEM_GET(in_flistp, DEX_FLD_UDACS, PIN_ELEMID_ANY, 1, ebufp);
    if(udac_flistp != (pin_flist_t *) NULL){
        vp = PIN_FLIST_FLD_GET(udac_flistp, DEX_FLD_UDAC, 1, ebufp);
        if(vp){
            sprintf(udac, "%s", (char *)vp);
        }
    }
    sprintf(deal_name, "%s%s%s%s%s", DEX_DEFAULT_DIGITAL_PRODUCT_CODE, 
			 deal_delimiterp, udac, deal_delimiterp, item_descr);

    snprintf( msg, LOG_BUFFER_LENGTH - 1, "%s: deal name %s", func_name, deal_name1);
    PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);

    /*******************************************************
     * Call the function to get the deal details from deal name
 	********************************************************/
    fm_dex_order_utils_find_deal_by_name_type(ctxp, a_pdp, deal_name, DEX_SERVICE_TYPE_DIGITAL, &deal_flistp, ebufp);
    if(deal_flistp == (pin_flist_t *)NULL){
        snprintf(msg, LOG_BUFFER_LENGTH - 1, "%s: Deal '%s' not found", func_name, deal_name);
        PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);
        status = DEX_STATUS_FAIL;
        goto CLEANUP;
    }

    fm_dex_order_utils_prep_modify_customer(ctxp, a_pdp, deal_flistp, in_flistp, out_flistp, ebufp);

    snprintf( msg, LOG_BUFFER_LENGTH - 1, "%s: return flist", func_name);
    PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, msg, *out_flistp);

CLEANUP:
    PIN_FLIST_FLD_SET(*out_flistp, PIN_FLD_RESULT, &status, ebufp);
    PIN_FLIST_DESTROY_EX(&deal_flistp, NULL);
}

/*******************************************************
 * prepare flist for modify customer
 ********************************************************/
void
fm_dex_order_utils_prep_modify_customer(
    pcm_context_t   *ctxp,
    poid_t          *a_pdp,
    pin_flist_t     *in_deal_flistp,
    pin_flist_t     *in_order_flistp,
    pin_flist_t     **opi_flistp,
    pin_errbuf_t    *ebufp )
{
    pin_flist_t     *opinfo_flistp = NULL;
    pin_flist_t     *dealinfo_flistp = NULL;
    pin_flist_t     *deal_flistp = NULL;
    pin_flist_t     *prod_flistp = NULL;
    pin_flist_t     *p_flistp = NULL;
    pin_flist_t     *serv_flistp = NULL;
    poid_t          *serv_pdp = NULL;
    pin_cookie_t    cookie = NULL;
    int             trans_id = 0;
    int32           elem_id = 0;
    int32           start_t = 0;
    int32           return_basic_fields = 1;
    int32           status = 1;
    int32           opcode = PCM_OP_CUST_MODIFY_CUSTOMER;
    int32           status_flags = 0;
    char            func_name[] = "fm_dex_order_utils_prep_modify_customer";
    char            msg[1024];
    char            *eitem_id = NULL;
    void            *vp = NULL;
    pin_decimal_t   *mrc_amount = NULL;
    pin_decimal_t   *nrc_amount = NULL;
    pin_decimal_t   *quantity = NULL;

    if( PIN_ERR_IS_ERR( ebufp ) )
        return;

    PIN_ERR_CLEAR_ERR( ebufp );

    snprintf( msg, LOG_BUFFER_LENGTH - 1, "%s: input deal flist ", func_name);
    PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, msg, in_deal_flistp);

    snprintf( msg, LOG_BUFFER_LENGTH - 1, "%s: input order flist ", func_name);
    PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, msg, in_order_flistp);

    quantity = (void *)PIN_FLIST_FLD_GET(in_order_flistp, PIN_FLD_QUANTITY, 1, ebufp );
    if(pbo_decimal_is_null(quantity, ebufp)){
        quantity = pbo_decimal_from_str("1.0",ebufp);
    }

    mrc_amount = (pin_decimal_t *) PIN_FLIST_FLD_GET(in_order_flistp, DEX_FLD_MRC_OVERRIDE, 1, ebufp);
    nrc_amount = (pin_decimal_t *) PIN_FLIST_FLD_GET(in_order_flistp, DEX_FLD_NRC_OVERRIDE, 1, ebufp);
    eitem_id = PIN_FLIST_FLD_GET(in_order_flistp, DEX_FLD_EITEM_ID, 0, ebufp);

    /*******************************************************************
	 * Add MODIFY_CUSTOMER input flist as PIN_FLD_OPERATION_INFO
	 ********************************************************************/
    PIN_FLIST_FLD_SET(*opi_flistp, PIN_FLD_OPCODE, &opcode, ebufp);

    opinfo_flistp = PIN_FLIST_SUBSTR_ADD(*opi_flistp, PIN_FLD_OPERATION_INFO, ebufp);
    PIN_FLIST_FLD_SET(opinfo_flistp, PIN_FLD_POID, a_pdp, ebufp);
    PIN_FLIST_FLD_SET(opinfo_flistp, PIN_FLD_PROGRAM_NAME, "fm_dex_order", ebufp);
    PIN_FLIST_FLD_SET(opinfo_flistp, PIN_FLD_ACCOUNT_OBJ, a_pdp, ebufp);

    /*******************************************************************
 	 * Add PIN_FLD_SERVICES to PIN_FLD_OPERATION_INFO
 	 ********************************************************************/
    serv_flistp = PIN_FLIST_ELEM_ADD(opinfo_flistp, PIN_FLD_SERVICES, 0, ebufp);
    serv_pdp = PIN_POID_CREATE(PIN_POID_GET_DB(a_pdp), DEX_SERVICE_TYPE_DIGITAL, -1, ebufp);
    PIN_FLIST_FLD_PUT(serv_flistp, PIN_FLD_SERVICE_OBJ, serv_pdp, ebufp);
    PIN_FLIST_FLD_SET(serv_flistp, PIN_FLD_LOGIN, eitem_id, ebufp);


    dealinfo_flistp = PIN_FLIST_CREATE(ebufp);
    /*******************************************************************
 	 * For digital assumption is only one array i.e DEAL to PRODUCT
	 * mapping is one-one.
	 * Add PIN_FLD_PRODUCTS to PIN_FLD_DEAL
	 ********************************************************************/
    while(p_flistp = PIN_FLIST_ELEM_GET_NEXT(in_deal_flistp, PIN_FLD_PRODUCTS,
                &elem_id, 1, &cookie, ebufp))
    {
        prod_flistp = PIN_FLIST_ELEM_ADD(dealinfo_flistp, PIN_FLD_PRODUCTS, elem_id, ebufp);

        /*******************************************************************
         * If the input MRC or NRC override values are provided override them
         *******************************************************************/
        if((mrc_amount) && (nrc_amount)) {
            PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_CYCLE_FEE_AMT, mrc_amount, ebufp);
            PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_PURCHASE_FEE_AMT, nrc_amount, ebufp);
            status_flags = ( PIN_PROD_STATUS_FLAGS_OVERRIDE_PURCHASE_FEE |
                                          PIN_PROD_STATUS_FLAGS_OVERRIDE_CYCLE_FEE );
        }else if(mrc_amount){
            PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_CYCLE_FEE_AMT, mrc_amount, ebufp);
            status_flags = PIN_PROD_STATUS_FLAGS_OVERRIDE_CYCLE_FEE;
        }else if(nrc_amount){
            PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_PURCHASE_FEE_AMT, nrc_amount, ebufp);
            status_flags =  PIN_PROD_STATUS_FLAGS_OVERRIDE_PURCHASE_FEE;
        }

        PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_DESCR, eitem_id, ebufp)
        PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_PURCHASE_START_T, &start_t, ebufp);
        PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_PURCHASE_END_T, &start_t, ebufp);
        PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_USAGE_START_T, &start_t, ebufp);
        PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_USAGE_END_T, &start_t, ebufp);
        PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_CYCLE_START_T, &start_t, ebufp);
        PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_CYCLE_END_T, &start_t, ebufp);
        PIN_FLIST_FLD_COPY(p_flistp, PIN_FLD_PRODUCT_OBJ, prod_flistp, PIN_FLD_PRODUCT_OBJ, ebufp);

        PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_QUANTITY, quantity, ebufp);
        PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_STATUS, &status, ebufp)
        PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_STATUS_FLAGS, &status_flags, ebufp)
        PIN_FLIST_ELEM_SET(dealinfo_flistp, prod_flistp, PIN_FLD_PRODUCTS, elem_id, ebufp);
    }

    /*******************************************************************
     * Add PIN_FLD_DEALS to PIN_FLD_SERVICES
     *******************************************************************/
    vp = PIN_FLIST_FLD_GET(in_deal_flistp, PIN_FLD_POID, 0, ebufp);
    PIN_FLIST_FLD_SET(dealinfo_flistp, PIN_FLD_POID, vp, ebufp);

    vp = PIN_FLIST_FLD_GET(in_deal_flistp, PIN_FLD_NAME, 0, ebufp);
    PIN_FLIST_FLD_SET(dealinfo_flistp, PIN_FLD_NAME, vp, ebufp);

    vp = PIN_FLIST_FLD_GET(in_deal_flistp, PIN_FLD_DESCR, 1, ebufp);
    if(vp) {
        PIN_FLIST_FLD_SET(dealinfo_flistp, PIN_FLD_DESCR, vp, ebufp);
    }else {
        vp = PIN_FLIST_FLD_GET(in_deal_flistp, PIN_FLD_NAME, 0, ebufp);
        PIN_FLIST_FLD_SET(dealinfo_flistp, PIN_FLD_DESCR, vp, ebufp);
    }
    PIN_FLIST_FLD_SET(dealinfo_flistp, PIN_FLD_START_T, &start_t, ebufp);
    PIN_FLIST_FLD_SET(dealinfo_flistp, PIN_FLD_END_T, &start_t, ebufp);

    deal_flistp = PIN_FLIST_COPY(dealinfo_flistp, ebufp);

    PIN_FLIST_SUBSTR_PUT(serv_flistp, dealinfo_flistp, PIN_FLD_DEAL_INFO, ebufp);

    vp = PIN_FLIST_FLD_GET(in_deal_flistp, PIN_FLD_POID, 0, ebufp);
    PIN_FLIST_FLD_SET(deal_flistp, PIN_FLD_DEAL_OBJ, vp, ebufp);
    PIN_FLIST_ELEM_PUT(serv_flistp, deal_flistp, PIN_FLD_DEALS, 0, ebufp);

    snprintf( msg, LOG_BUFFER_LENGTH - 1, "%s: return flist ", func_name);
    PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, msg, *opi_flistp);
}
