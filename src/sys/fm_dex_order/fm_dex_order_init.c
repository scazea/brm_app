/********************************************************************
 *
* Copyright (c) 2000, 2016, Oracle and/or its affiliates. All rights reserved.
 *
 *      This material is the confidential property of Oracle Corporation
 *      or its licensors and may be used, reproduced, stored or transmitted
 *      only in accordance with a valid Oracle license or sublicense agreement.
 *
 *******************************************************************/

#ifndef lint
static  char    Sccs_id[] = "@(#)%Portal Version: fm_dex_order_init.c:RWSmod7.3.1Int:1:2007-Aug-01 23:18:05 %";
#endif

/*******************************************************************
 * This file contains the fm_dex_order_init() function which is called
 * during CM initialization.  This function caches the 
 * /config/invoice_templates object poids in a cm_cache for later
 * (faster) retrieval
 *******************************************************************/

#include <stdio.h> 
#include <string.h> 
#include <stdlib.h>

 
#include "pcm.h"
#include "ops/inv.h"
#include "cm_fm.h"
#include "pin_errs.h"
#include "pinlog.h"

#include "pin_inv.h"
#include "cm_cache.h"

#define FILE_SOURCE_ID          "fm_dex_order_init.c(8)"

/*******************************************************************
 * Global for holding cache ptr
 *******************************************************************/
PIN_EXPORT char *deal_delimiterp = NULL;
PIN_EXPORT char *contract_amt_product = NULL;
PIN_EXPORT char *contract_term_product = NULL;

PIN_EXPORT cm_cache_t   *fm_dex_order_product_name_cache_ptr = (cm_cache_t*) 0;

PIN_EXPORT void fm_dex_order_init(
	int32			*errp);

void
fm_dex_order_init_cache_products_by_name(
	pcm_context_t		*ctxp,
	pin_errbuf_t 		*ebufp);

/*******************************************************************
 * fm_dex_order_init();
 *
 *******************************************************************/
void fm_dex_order_init(
	int32			*errp) 
{
	pcm_context_t   *ctxp = NULL;
	pin_errbuf_t    ebuf;
	pin_flist_t		*cache_flistp = NULL;
	int64			database;
	poid_t			*pdp = NULL;
	int32			err = 0;
	char			msg[1024];
	char			key[512];

	PIN_ERR_CLEAR_ERR(&ebuf);
	
	sprintf(msg, "%s", "Entering fm_dex_order_init function");
	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);

	PCM_CONTEXT_OPEN(&ctxp, (pin_flist_t *)0, &ebuf);
    if(PIN_ERR_IS_ERR(&ebuf)) {
        pin_set_err(&ebuf, PIN_ERRLOC_FM,
            PIN_ERRCLASS_SYSTEM_DETERMINATE,
            PIN_ERR_DM_CONNECT_FAILED, 0, 0, ebuf.pin_err);
        PIN_FLIST_LOG_ERR("fm_dex_order_init pcm_context_open err",
                &ebuf);
        *errp = PIN_ERR_DM_CONNECT_FAILED;
        return;
    }

	/*******************************************************
	 * read Deal name delimiter from pin.conf
	 ********************************************************/
    pin_conf("fm_dex_order", "deal_delimiter", PIN_FLDT_STR,(caddr_t *)&deal_delimiterp, &err);

	/*******************************************************
	 * read counter product name
	 ********************************************************/
    pin_conf("fm_dex_order", "digital_contract_term_product", PIN_FLDT_STR,(caddr_t *)&contract_term_product, &err);

	/*******************************************************
	 * read contract amount product name
	 ********************************************************/
    pin_conf("fm_dex_order", "digital_contract_amt_product", PIN_FLDT_STR,(caddr_t *)&contract_amt_product, &err);

	sprintf(msg, "Delimiter [%s] contract term product [%s] contract amount product [%s]", deal_delimiterp, 
					contract_term_product, contract_amt_product);
	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);

	fm_dex_order_init_cache_products_by_name(ctxp, &ebuf);
	/***********************************************************
	 * close the context and return.
	 ***********************************************************/
    PCM_CONTEXT_CLOSE(ctxp, 0, &ebuf);
    *errp = ebuf.pin_err;

	/*******************************************
	 * Print the Cache for test
	 *******************************************/
	cache_flistp = cm_cache_find_entry(fm_dex_order_product_name_cache_ptr, contract_amt_product, &err);
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "fm_dex_order_init test cache result flistp ", cache_flistp);
	PIN_FLIST_DESTROY_EX(&cache_flistp, NULL);

	sprintf(msg, "%s", "Exiting fm_dex_order_init function");
	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);

    return;

}

/***********************************************************************
 * Get the products and add to the cache
 ***********************************************************************/
void
fm_dex_order_init_cache_products_by_name(
	pcm_context_t		*ctxp,
	pin_errbuf_t 		*ebufp)
{
	poid_t              *s_pdp = NULL;
	poid_t              *pdp = NULL;
	pin_flist_t         *s_flistp = NULL;
	pin_flist_t         *res_flistp = NULL;
	pin_flist_t         *arg_flistp = NULL;
	pin_flist_t         *sub_flistp = NULL;
	pin_cookie_t		cookie = NULL;
	int32               err = PIN_ERR_NONE;
	int32               s_flags = 256;
	int32               cnt = 0;
	int32               elem_id = 0;
	int					no_of_buckets = 556;
	char                *key = NULL;
	char                *name = NULL;
	char                temp_buf[256];
	char				template[102];
	char				cache_key[512];
	int64 				database = 0;
	cm_cache_key_iddbstr_t  kids;

	if (PIN_ERR_IS_ERR(ebufp))
		return;

	PIN_ERR_CLEAR_ERR(ebufp);

	pdp = PCM_GET_USERID(ctxp);

	database = PIN_POID_GET_DB(pdp);

	/* Create Search flist */
	s_flistp = PIN_FLIST_CREATE(ebufp);

	s_pdp = PIN_POID_CREATE(database, "/search", -1, ebufp);
	PIN_FLIST_FLD_PUT(s_flistp, PIN_FLD_POID, (void *)s_pdp, ebufp);
	PIN_FLIST_FLD_SET(s_flistp, PIN_FLD_FLAGS, (void *)&s_flags, ebufp);

	sprintf(template, "select X from /product where F1 in ( V1 , '%s', '%s') ", contract_amt_product, contract_term_product);

	PIN_FLIST_FLD_SET(s_flistp, PIN_FLD_TEMPLATE, template, ebufp);

	/* setup arguments */
	arg_flistp = PIN_FLIST_ELEM_ADD(s_flistp, PIN_FLD_ARGS, 1, ebufp);
	PIN_FLIST_FLD_SET(arg_flistp, PIN_FLD_NAME, (void *)contract_amt_product, ebufp);

	sub_flistp = PIN_FLIST_ELEM_ADD(s_flistp, PIN_FLD_RESULTS, PIN_ELEMID_ANY, ebufp);
    PIN_FLIST_FLD_SET(sub_flistp, PIN_FLD_POID, NULL, ebufp);
    PIN_FLIST_FLD_SET(sub_flistp, PIN_FLD_NAME, NULL, ebufp);
    PIN_FLIST_FLD_SET(sub_flistp, PIN_FLD_PERMITTED, NULL, ebufp);
	
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_dex_order_init_cache_products_by_name: Search flist", s_flistp);

	/***********************************************************
 	*  * Do the database search.
 	*   ***********************************************************/
	PCM_OP(ctxp, PCM_OP_SEARCH, 0, s_flistp, &res_flistp, ebufp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
	"fm_dex_order_init_cache_products_by_name: Return flist", res_flistp);

	/***********************************************************
 	*  * If Inv Strings are not configured, bail out from here.
 	*   ***********************************************************/
	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
				"fm_dex_order_init_cache_products_by_name() "
				"error loading /product object", ebufp);
		pin_set_err(ebufp, PIN_ERRLOC_FM,
		PIN_ERRCLASS_SYSTEM_DETERMINATE, PIN_ERR_BAD_VALUE, 0, 0, err);
		goto cleanup;
	}

	cnt = PIN_FLIST_ELEM_COUNT(res_flistp, PIN_FLD_RESULTS, ebufp);

	/*
	 * If there's no data, there's no point initializing it.
	 */
	if (cnt < 1) {
		goto cleanup;
	}
	
	fm_dex_order_product_name_cache_ptr =
		cm_cache_init("fm_dex_order_product_name_cache", 15,
		(cnt * 1024) + 1024, 15, CM_CACHE_KEY_STR, &err);

	if ((err != PIN_ERR_NONE) || !fm_dex_order_product_name_cache_ptr) {
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_ERROR, 
		"Error: Couldn't initialize "
		"cache in fm_dex_order_product_name_cache");
		pin_set_err(ebufp, PIN_ERRLOC_FM, 
				PIN_ERRCLASS_SYSTEM_DETERMINATE,
				PIN_ERR_NO_MEM, 0, 0, err);
		goto cleanup;
	}

	elem_id = 0;
	cookie = NULL;
	while((arg_flistp = PIN_FLIST_ELEM_GET_NEXT(res_flistp, PIN_FLD_RESULTS,
			&elem_id, 1, &cookie, ebufp)) != NULL) {
		name = PIN_FLIST_FLD_GET(arg_flistp, PIN_FLD_NAME, 0, ebufp);

		memset(cache_key, '\0', 512);
		strncpy(cache_key, name, strlen(name));
		//sprintf(cache_key, "%s", name);
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, cache_key);

        cm_cache_add_entry(fm_dex_order_product_name_cache_ptr, cache_key, arg_flistp, &err);
		if (err != PIN_ERR_NONE) {
			PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_ERROR,
					"Error: Couldn't add "
					"cache in fm_dex_order_product_name_cache");
			pin_set_err(ebufp, PIN_ERRLOC_FM,
					PIN_ERRCLASS_SYSTEM_DETERMINATE,
					PIN_ERR_NO_MEM, 0, 0, err);
		} else {
			PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG,
					"fm_dex_order_product_name_cache cache loaded successfully");
		}
	}

cleanup:
	PIN_FLIST_DESTROY_EX(&s_flistp, NULL);
	PIN_FLIST_DESTROY_EX(&res_flistp, NULL);
}
