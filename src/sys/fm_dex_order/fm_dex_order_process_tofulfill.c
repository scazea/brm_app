/**
 * This is an DEX custom API, a confidential property of EbillSoft LLC.
 * Unauthorized code replication or usage is strictly prohibited.
 */

#ifndef lint
static  char    Sccs_id[] = "@(#)$Id: Exp $";
#endif

/**
 * Includes
 */

// Std. C headers
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>

// Oracle BRM ootb headers
#include "pcm.h"
#include "ops/bill.h"
#include "ops/pymt.h"
#include "ops/cust.h"
#include "ops/base.h"
#include "cm_fm.h"
#include "pin_errs.h"
#include "pin_cust.h"
#include "pin_inv.h"
#include "pin_currency.h"
#include "pin_cc.h"
#include "pin_cc_patterns.h"
#include "pinlog.h"
#include "fm_utils.h"
#include "fm_bill_utils.h"
#include "pin_bill.h"
#include "pin_pymt.h"
#include "pin_type.h"

// DEX custom headers
#include "dex_ops.h"
#include "dex_flds.h"
#include "dex_constants.h"

// Extrnal function
void
fm_dex_order_utils_find_account_by_no(
    pcm_context_t	*ctxp,
    char			*account_nop,
    poid_t			*pdp,
    pin_flist_t		**out_flistpp,
    pin_errbuf_t	*ebufp );

void
fm_dex_order_utils_find_deal_by_name_type(
    pcm_context_t	*ctxp,
    poid_t			*a_pdp,
    char			*offer_namep,
    char			*service_typep,
    pin_flist_t		**out_flistpp,
    pin_errbuf_t	*ebufp );
void
fm_dex_order_utils_find_subscription_by_account(
    pcm_context_t	*ctxp,
    poid_t			*in_account_poidp,
    char			*service_typep,   
    u_int32			return_basic_fields,
    pin_flist_t		**out_flistpp,
    pin_errbuf_t	*ebufp );

void
fm_dex_order_utils_execute_op(
    pcm_context_t	*ctxp,
    pin_flist_t		*i_flistp,
    pin_flist_t		**o_flistpp,
    pin_errbuf_t	*ebufp);

void
fm_dex_order_utils_prep_tofulfill_digital(
	pcm_context_t	*ctxp,
	poid_t			*a_pdp,
	pin_flist_t		*in_flistp,
	pin_flist_t		**out_flistpp,
	pin_errbuf_t	*ebufp);

void 
fm_dex_order_utils_add_counter_products(
	pcm_context_t	*ctxp, 
	pin_flist_t		*order_item_flistp, 
	pin_flist_t		*cust_modify_flistp, 
	pin_flist_t		**out_flistpp, 
	pin_errbuf_t	*ebufp);

/**
 * Local function definitions
 */
EXPORT_OP void
op_dex_order_process_tofulfill(
	cm_nap_connection_t	*connp,
	int32			opcode,
	int32			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**o_flistpp,
	pin_errbuf_t	*ebufp);

static void
fm_dex_order_process_tofulfill(
	pcm_context_t	*ctxp,
	pin_flist_t		*in_flistp,
	pin_flist_t		**r_flistpp,
	pin_errbuf_t	*ebufp);


void
fm_dex_order_process_tofulfill_update_order(
    pcm_context_t   *ctxp,
    poid_t          *o_pdp,
    int32           elem_id,
    int32           status,
    pin_flist_t     *in_flistp,
    pin_errbuf_t    *ebufp);
/**
 * Opcode function
 */
void
op_dex_order_process_tofulfill (
	cm_nap_connection_t	*connp,
	int32			opcode,
	int32			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**o_flistpp,
	pin_errbuf_t		*ebufp )
{

	pcm_context_t		*ctxp = connp->dm_ctx;

	if( PIN_ERR_IS_ERR( ebufp ) )
		return;

	*o_flistpp = NULL;
	PIN_ERR_CLEAR_ERR( ebufp );

	if( opcode != DEX_OP_ORDER_PROCESS_TOFULFILL )
	{

		pin_set_err( ebufp, PIN_ERRLOC_FM, PIN_ERRCLASS_SYSTEM_DETERMINATE,
				PIN_ERR_BAD_OPCODE, 0, 0, opcode );
		PIN_ERR_LOG_EBUF( PIN_ERR_LEVEL_ERROR, "op_dex_order_process_tofulfill: bad opcode!", ebufp );
		return;
	}

	PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, "op_dex_order_process_tofulfill: input flist -", in_flistp );
	fm_dex_order_process_tofulfill( ctxp, in_flistp, o_flistpp, ebufp );

	if( PIN_ERR_IS_ERR( ebufp ) )
	{

		PIN_ERR_LOG_EBUF( PIN_ERR_LEVEL_ERROR, "op_dex_order_process_tofulfill: error buffer!", ebufp );
		if( *o_flistpp != NULL )
		{
			PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_ERROR, "op_dex_order_process_tofulfill: error output", *o_flistpp );
			PIN_FLIST_DESTROY_EX(o_flistpp, NULL );
		}
	}

	PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, "op_dex_order_process_tofulfill: output -", *o_flistpp );
	return;
}


/**
 * Main function implementation
 */
void
fm_dex_order_process_tofulfill (
	pcm_context_t	*ctxp,
	pin_flist_t		*in_flistp,
	pin_flist_t		**r_flistpp,
	pin_errbuf_t	*ebufp )
{
	pin_flist_t		*i_flistp = NULL;
	pin_flist_t		*order_item_flistp = NULL;
	pin_flist_t		*acct_flistp = NULL;
	pin_flist_t		*opi_flistp = NULL;
	pin_flist_t		*opo_flistp = NULL;
	pin_flist_t		*o_flistp = NULL;
	pin_flist_t		*a_flistp = NULL;
	poid_t			*pdp = NULL;
	poid_t			*a_pdp = NULL;
	pin_cookie_t	cookie = NULL;
	int             trans_id = 0;
	int32			elem_id = 0;
	int32			status = DEX_STATUS_FAIL;
	int32           err = 0;
	char			func_name[] = "fm_dex_order_process_tofulfill";
	char			msg[1024];
	char			*account_no = NULL;
	char			*issue_date = NULL;
	char			*deal_delimiterp = NULL;
	void			*vp = NULL;


	if(PIN_ERR_IS_ERR(ebufp))
		return;

	PIN_ERR_CLEAR_ERR( ebufp );

	//Make a copy of flist
	i_flistp = PIN_FLIST_COPY(in_flistp, ebufp);

	PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, "fm_dex_order_process_tofulfill: input flist", i_flistp);

	pdp = (poid_t *)PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_POID, 0, ebufp);

	// create return flist
	*r_flistpp = NULL;
	*r_flistpp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_SET(*r_flistpp, PIN_FLD_POID, pdp, ebufp);

	/*******************************************************
	 * Process each order item
	 *******************************************************/
	while ((order_item_flistp = PIN_FLIST_ELEM_GET_NEXT(i_flistp, DEX_FLD_ORDER_ITEMS, 
		&elem_id, 1, &cookie, ebufp)) != NULL){

		// Set the vale to fail
		status = DEX_STATUS_FAIL;

		snprintf(msg, LOG_BUFFER_LENGTH - 1, "%s: current processing order", func_name);
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, order_item_flistp);

		a_flistp = PIN_FLIST_ELEM_ADD(*r_flistpp, DEX_FLD_ORDER_ITEMS, elem_id, ebufp);
		
		/*******************************************************
		 * Get the account no and call the function to get the
		 * account poid
		 *******************************************************/
		account_no = PIN_FLIST_FLD_GET(order_item_flistp, PIN_FLD_ACCOUNT_NO, 0, ebufp);
		fm_dex_order_utils_find_account_by_no(ctxp, account_no, pdp, &acct_flistp, ebufp);
		if(acct_flistp == (pin_flist_t *)NULL || PIN_ERRBUF_IS_ERR(ebufp) )
		{
			// Account not found update the order_item status to error
			status = DEX_STATUS_FAIL;
			PIN_ERR_CLEAR_ERR(ebufp);
			fm_dex_order_process_tofulfill_update_order(ctxp, pdp, elem_id, status, opo_flistp, ebufp);
			snprintf(msg, LOG_BUFFER_LENGTH - 1, "%s: ACCOUNT_NO '%s' not found", func_name, account_no);
			PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);
			PIN_FLIST_FLD_SET(a_flistp, PIN_FLD_RESULT, &status, ebufp);
			continue;
		}
		a_pdp = PIN_FLIST_FLD_GET(acct_flistp, PIN_FLD_POID, 0, ebufp);
		
		issue_date = (char *)PIN_FLIST_FLD_GET(order_item_flistp, DEX_FLD_ISSUE_DATE, 1, ebufp);

		if((issue_date) && (strlen(issue_date) > 0)){
			//Print Order
			snprintf(msg, LOG_BUFFER_LENGTH - 1, "%s: processing print order", func_name);
			PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);
		}else{
			//Digital Order
			snprintf(msg, LOG_BUFFER_LENGTH - 1, "%s: processing digital order", func_name);
			PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);

			/*******************************************************
			 * Call the function actual processing
			 *******************************************************/
			fm_dex_order_utils_prep_tofulfill_digital(ctxp, a_pdp, order_item_flistp, &opi_flistp, ebufp);
		}

		PIN_FLIST_DESTROY_EX(&acct_flistp, NULL);

		if(opi_flistp != NULL){
			status = *(int32  *)PIN_FLIST_FLD_GET(opi_flistp, PIN_FLD_RESULT, 0, ebufp);
		}

		trans_id = fm_utils_trans_open(ctxp, PCM_TRANS_OPEN_READWRITE, pdp, ebufp);
		if(PIN_ERRBUF_IS_ERR(ebufp)){
			PIN_FLIST_DESTROY_EX(&opi_flistp, NULL);
			snprintf(msg, LOG_BUFFER_LENGTH - 1, "%s: trans_open error", func_name);
			PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
			goto CLEANUP;
		}

		/*******************************************************
		 * Status  success means no issue with the order item
		 *******************************************************/
		if(status == DEX_STATUS_SUCCESS){
			/*******************************************************
		 	 * Status  success means no issue with the order item
		 	 * so call the opcode to modify customer
		 	 * If no ebuf call function to add counter products
		 	 *******************************************************/
			fm_dex_order_utils_execute_op(ctxp, opi_flistp, &opo_flistp, ebufp);
			PIN_FLIST_DESTROY_EX(&opi_flistp, NULL);
			if(PIN_ERR_IS_ERR(ebufp)){
				fm_utils_trans_close( ctxp, trans_id, ebufp );
				PIN_ERR_CLEAR_ERR(ebufp);
				status = DEX_STATUS_FAIL;
			}else{
				fm_dex_order_utils_add_counter_products(ctxp, order_item_flistp, opo_flistp, &opi_flistp, ebufp);
				fm_dex_order_utils_execute_op(ctxp, opi_flistp, &o_flistp, ebufp);
				if(PIN_ERR_IS_ERR(ebufp)){
					fm_utils_trans_close( ctxp, trans_id, ebufp );
					PIN_ERR_CLEAR_ERR(ebufp);
					status = DEX_STATUS_FAIL;
				}
			}
		}

		fm_dex_order_process_tofulfill_update_order(ctxp, pdp, elem_id, status, opo_flistp, ebufp);
		if(PIN_ERR_IS_ERR(ebufp)) {
			snprintf(msg, LOG_BUFFER_LENGTH - 1, "%s: update orer item error", func_name);
			PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp );
		}
		fm_utils_trans_close( ctxp, trans_id, ebufp );
		
		// clean ebufp, so that next order processes in while loop
		PIN_ERR_CLEAR_ERR(ebufp);

		PIN_FLIST_DESTROY_EX(&opi_flistp, NULL);
		PIN_FLIST_DESTROY_EX(&opo_flistp, NULL);
		PIN_FLIST_DESTROY_EX(&o_flistp, NULL);
		PIN_FLIST_FLD_SET(a_flistp, PIN_FLD_RESULT, &status, ebufp);
	}

CLEANUP:
	PIN_FLIST_DESTROY_EX(&i_flistp, NULL);
	snprintf(msg, LOG_BUFFER_LENGTH - 1, "%s: return flist", func_name);
	PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, msg, *r_flistpp );
	return;
}

/*******************************************************
 * Function to update the order_items 
 *******************************************************/
void
fm_dex_order_process_tofulfill_update_order(
	pcm_context_t	*ctxp,
	poid_t			*o_pdp,
	int32			elem_id,
	int32			status,
	pin_flist_t		*in_flistp,
	pin_errbuf_t    *ebufp)
{
	char			msg[1024];
	char			status_str[64];
	char			func_name[] = "fm_dex_order_process_tofulfill_update_order";
	int32			opcode = PCM_OP_WRITE_FLDS;
	pin_flist_t		*opi_flistp = NULL;
	pin_flist_t		*opo_flistp = NULL;
	pin_flist_t		*opinfo_flistp = NULL;
	pin_flist_t		*product_flistp = NULL;
	pin_flist_t		*serv_flistp = NULL;
	pin_flist_t		*dealinfo_flistp = NULL;
	pin_flist_t		*flistp = NULL;

	snprintf( msg, LOG_BUFFER_LENGTH - 1, "%s: input flist", func_name);
	PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, msg, in_flistp);

	/*******************************************************************
 	 * Add WRITE_FLDS input flist as PIN_FLD_OPERATION_INFO
 	 *******************************************************************/
	opi_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_SET(opi_flistp, PIN_FLD_POID, o_pdp, ebufp);
	PIN_FLIST_FLD_SET(opi_flistp, PIN_FLD_OPCODE, &opcode, ebufp);

	opinfo_flistp = PIN_FLIST_SUBSTR_ADD(opi_flistp, PIN_FLD_OPERATION_INFO, ebufp);
	PIN_FLIST_FLD_SET(opinfo_flistp, PIN_FLD_POID, o_pdp, ebufp);
	flistp = PIN_FLIST_ELEM_ADD(opinfo_flistp, DEX_FLD_ORDER_ITEMS, elem_id, ebufp);
	
	if(status == DEX_STATUS_FAIL || (in_flistp == (pin_flist_t *)NULL)){
		PIN_FLIST_FLD_SET(flistp, PIN_FLD_STATUS_STR, "failed", ebufp);
	}else{
		PIN_FLIST_FLD_SET(flistp, PIN_FLD_STATUS_STR, "fulfilled", ebufp);

		/****************************************************************************
 		 * MODIFY_CUSTOMER, so get package id from
 		 * PIN_FLD_SERVICES[0]->PIN_FLD_DEAL_INFO[0]->PIN_FLD_PRODUCTS[0]->PACKAGE_ID
 		 *****************************************************************************/
		serv_flistp = PIN_FLIST_ELEM_GET(in_flistp, PIN_FLD_SERVICES, PIN_ELEMID_ANY, 1, ebufp);
		if(serv_flistp != (pin_flist_t *)NULL){
			dealinfo_flistp = PIN_FLIST_SUBSTR_GET(serv_flistp, PIN_FLD_DEAL_INFO, 1, ebufp);
			if(dealinfo_flistp != (pin_flist_t *)NULL){
				product_flistp = PIN_FLIST_ELEM_GET(dealinfo_flistp, PIN_FLD_PRODUCTS, PIN_ELEMID_ANY, 1, ebufp);
				if(product_flistp != (pin_flist_t *)NULL){
		 			PIN_FLIST_FLD_COPY(product_flistp, PIN_FLD_PACKAGE_ID, flistp, PIN_FLD_PACKAGE_ID, ebufp);
				}
			}
		}
	}
	fm_dex_order_utils_execute_op(ctxp, opi_flistp, &opo_flistp, ebufp);

	if(PIN_ERR_IS_ERR(ebufp)) {
		snprintf( msg, LOG_BUFFER_LENGTH - 1, "%s: error calling write_flds ", func_name);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp );
	}
	PIN_FLIST_DESTROY_EX(&opi_flistp, NULL);
	PIN_FLIST_DESTROY_EX(&opo_flistp, NULL);

	snprintf(msg, LOG_BUFFER_LENGTH - 1, "%s: return ", func_name);
	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);
}
