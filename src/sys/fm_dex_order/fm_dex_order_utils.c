#include "pcm.h"
#include "pcm_ops.h"
#include "pinlog.h"
#include "pin_act.h"
#include "pin_ar.h"
#include "pin_bill.h"
#include "pin_currency.h"
#include "pin_cust.h"
#include "pin_gsm.h"
#include "pin_pymt.h"
#include "pin_rate.h"
#include "pin_subscription.h"
#include "pin_type.h"
#include "ops/ar.h"
#include "ops/bill.h"
#include "ops/cust.h"
#include "ops/price.h"
#include "ops/pymt.h"
#include "ops/subscription.h"
#include "cm_fm.h"
#include "cm_cache.h"
#include "psiu_business_params.h"

#include "dex_flds.h"
#include "dex_constants.h"


extern char *contract_amt_product;
extern char *contract_term_product;
extern cm_cache_t   *fm_dex_order_product_name_cache_ptr;
void
fm_dex_order_utils_find_offering_by_name(
	pcm_context_t        *ctxp,
	poid_t				 *pdp,
	char                 *offer_namep,
	char                 *service_typep,      /* optional */
	pin_flist_t          **out_flistpp,
	pin_errbuf_t         *ebufp);

void 
fm_dex_order_utils_find_account_by_no(
	pcm_context_t        *ctxp,
	char                 *account_nop,
	poid_t               *pdp,
	pin_flist_t          **out_flistpp,
	pin_errbuf_t         *ebufp)
{
	const char           prog_name[] = "fm_dex_order_utils_find_account_by_no";
	pin_flist_t          *s_in_flp = NULL;        /* search input flistp */
	pin_flist_t          *s_out_flp = NULL;       /* search output flistp */
	pin_flist_t          *sub_flp = NULL;         /* sub flistp */
	char                 s_template[ 64 ] = "select X from /account where F1 = V1 ";
	int32                search_flags = SRCH_EXACT;
	char                 msg[ 1024 ];
	void                 *vp = NULL;

	if(PIN_ERRBUF_IS_ERR(ebufp)) return;
	PIN_ERRBUF_CLEAR(ebufp);

	/*******************************************************************
  	 * initialize the *out_flistpp first
  	 *******************************************************************/
	*out_flistpp = NULL;

	if(account_nop == NULL || strlen(account_nop) < 1)
	{
		pin_set_err(ebufp, PIN_ERRLOC_FM, PIN_ERRCLASS_APPLICATION,
		                    PIN_ERR_MISSING_ARG, PIN_FLD_ACCOUNT_NO, 0, 0);
		snprintf(msg, 1023, "%s: input ACCOUNT_NO invalid", prog_name);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		return;
	}

	snprintf(msg, 1023, "%s: input ACCOUNT_NO = '%s'", prog_name, account_nop);
	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);

	/*******************************************************************
  	 * build a search input
  	 *******************************************************************/
	s_in_flp = PIN_FLIST_CREATE(ebufp);

	vp = PIN_POID_CREATE(PIN_POID_GET_DB(pdp), "/search", -1, ebufp);
	PIN_FLIST_FLD_PUT(s_in_flp, PIN_FLD_POID, vp, ebufp);

	PIN_FLIST_FLD_SET(s_in_flp, PIN_FLD_TEMPLATE, s_template, ebufp);
	PIN_FLIST_FLD_SET(s_in_flp, PIN_FLD_FLAGS, &search_flags, ebufp);

	sub_flp = PIN_FLIST_ELEM_ADD(s_in_flp, PIN_FLD_ARGS, 1, ebufp);
	PIN_FLIST_FLD_SET(sub_flp, PIN_FLD_ACCOUNT_NO, account_nop, ebufp);

	PIN_FLIST_ELEM_SET(s_in_flp, NULL, PIN_FLD_RESULTS, PIN_ELEMID_ANY, ebufp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "search input", s_in_flp);
	PCM_OP(ctxp, PCM_OP_SEARCH, 0, s_in_flp, &s_out_flp, ebufp);
	PIN_FLIST_DESTROY_EX(&s_in_flp, NULL);

	if(PIN_ERRBUF_IS_ERR(ebufp))
	{
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, "search error", ebufp);
		PIN_FLIST_DESTROY_EX(&s_out_flp, NULL);
		return;
	}

	/*******************************************************************
  	 * return should be unique, if exists
  	 *******************************************************************/
	*out_flistpp = PIN_FLIST_ELEM_TAKE(s_out_flp, PIN_FLD_RESULTS, PIN_ELEMID_ANY, 1, ebufp);
	PIN_FLIST_DESTROY_EX(&s_out_flp, NULL);

	/*******************************************************************
  	 * drop useless fields
  	 *******************************************************************/
	if(*out_flistpp != NULL)
	{
		PIN_FLIST_FLD_DROP(*out_flistpp, PIN_FLD_READ_ACCESS,    ebufp);
		PIN_FLIST_FLD_DROP(*out_flistpp, PIN_FLD_WRITE_ACCESS,   ebufp);
		PIN_FLIST_FLD_DROP(*out_flistpp, PIN_FLD_INTERNAL_NOTES, ebufp);
	}

	/*******************************************************************
  	 * check error
  	 *******************************************************************/
	if(PIN_ERRBUF_IS_ERR(ebufp))
	{
		snprintf(msg, 1023, "%s: error", prog_name);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		PIN_FLIST_DESTROY_EX(out_flistpp, NULL);
	}
	snprintf(msg, 1023, "%s: output", prog_name);
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, *out_flistpp);
	return;
}

void
fm_dex_order_utils_find_deal_by_name_type(
	pcm_context_t        *ctxp,
	poid_t				 *a_pdp,
	char                 *offer_namep,
	char                 *service_typep,      /* optional */
	pin_flist_t          **out_flistpp,
	pin_errbuf_t         *ebufp)
{
	const char           prog_name[] = "fm_dex_order_utils_find_deal_by_name_type";
	pin_flist_t          *s_in_flp = NULL;        /* search input flistp */
	pin_flist_t          *s_out_flp = NULL;       /* search output flistp */
	pin_flist_t          *sub_flp = NULL;         /* sub flistp */
	char                 s_template[ 256 ];
	int32                search_flags = SRCH_DISTINCT | SRCH_EXACT;
	char                 *permittedp = NULL;
	char                 msg[ 1024 ];
	void                 *vp = NULL;

	if(PIN_ERRBUF_IS_ERR(ebufp)) return;
	PIN_ERRBUF_CLEAR(ebufp);

	/*******************************************************************
  	 * initialize *out_flistpp, in case of error
  	 *******************************************************************/
	*out_flistpp = NULL;

	if(offer_namep == NULL || strlen(offer_namep) < 1)
	{
		/*******************************************************************
  		 * do not set ebuf here, let the caller decide the action.
  		 *******************************************************************/
		snprintf(msg, 1023, "%s: input offering NAME invalid", prog_name);
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_WARNING, msg);
		return;
	}

	snprintf(msg, 1023, "%s: input  NAME = '%s'",
	                     prog_name, offer_namep);
	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);

	if(service_typep != NULL && strlen(service_typep) > 1)
	{
		snprintf(msg, 1023, "%s: input service TYPE = '%s'",
		                     prog_name, service_typep);
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);
	}

	/*******************************************************************
  	 * build a search input
  	 *******************************************************************/
	snprintf(s_template, 255, "select X from /deal where F1 = V1 ");

	s_in_flp = PIN_FLIST_CREATE(ebufp);

	vp = PIN_POID_CREATE(PIN_POID_GET_DB(a_pdp), "/search", -1, ebufp);
	PIN_FLIST_FLD_PUT(s_in_flp, PIN_FLD_POID, vp, ebufp);

	PIN_FLIST_FLD_SET(s_in_flp, PIN_FLD_TEMPLATE, s_template, ebufp);
	PIN_FLIST_FLD_SET(s_in_flp, PIN_FLD_FLAGS, &search_flags, ebufp);

	sub_flp = PIN_FLIST_ELEM_ADD(s_in_flp, PIN_FLD_ARGS, 1, ebufp);
	PIN_FLIST_FLD_SET(sub_flp, PIN_FLD_NAME, offer_namep, ebufp);

	PIN_FLIST_ELEM_SET(s_in_flp, NULL, PIN_FLD_RESULTS, PIN_ELEMID_ANY, ebufp);
	

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "search input", s_in_flp);
	PCM_OP(ctxp, PCM_OP_SEARCH, 0, s_in_flp, &s_out_flp, ebufp);
	PIN_FLIST_DESTROY_EX(&s_in_flp, NULL);

	if(PIN_ERRBUF_IS_ERR(ebufp))
	{
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, "search error", ebufp);
		PIN_FLIST_DESTROY_EX(&s_out_flp, NULL);
		return;
	}

	/*******************************************************************
  	 * return should be unique, if exists
  	 *******************************************************************/
	*out_flistpp = PIN_FLIST_ELEM_TAKE(s_out_flp, PIN_FLD_RESULTS, PIN_ELEMID_ANY, 1, ebufp);
	PIN_FLIST_DESTROY_EX(&s_out_flp, NULL);

	/*******************************************************************
  	 * check error
  	 *******************************************************************/
	if(PIN_ERRBUF_IS_ERR(ebufp))
	{
		snprintf(msg, 1023, "%s: error", prog_name);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		PIN_FLIST_DESTROY_EX(out_flistpp, NULL);
	}
	else if(*out_flistpp == (pin_flist_t *)NULL)
	{
		snprintf(msg, 1023, "%s: deal of name '%s' not found",
		                     prog_name, offer_namep);
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);
	}
	else if(service_typep != NULL && strlen(service_typep) > 1)
	{
		/*******************************************************************
 		 * need to check the service type of the found offering
 		 *******************************************************************/
		permittedp = PIN_FLIST_FLD_GET(*out_flistpp, PIN_FLD_PERMITTED, 1, ebufp);
		if(permittedp != NULL &&
		    0 != strncmp(permittedp, service_typep, strlen(permittedp)))
		{
			snprintf(msg, 1023, "%s: deal not for service TYPE '%s'",
			                     prog_name,  service_typep);
			pin_set_err(ebufp, PIN_ERRLOC_FM, PIN_ERRCLASS_APPLICATION,
			                    PIN_ERR_NO_MATCH, PIN_FLD_PERMITTED, 0, 0);
			PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_ERROR, msg, *out_flistpp);

			PIN_FLIST_DESTROY_EX(out_flistpp, NULL);
		}
	}
	else
	{
		snprintf(msg, 1023, "%s: output", prog_name);
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, *out_flistpp);
	}
	return;
} 

void
fm_dex_order_utils_find_subscription_by_account(
	pcm_context_t        *ctxp,
	poid_t               *a_pdp,
	char                 *service_typep,
	u_int32              return_basic_fields,
	pin_flist_t          **out_flistpp,
	pin_errbuf_t         *ebufp)
{
	const char           func_name[] = "fm_dex_order_utils_find_subscription_by_account";
	pin_flist_t          *s_in_flp = NULL;        /* search input flistp */
	pin_flist_t          *s_out_flp = NULL;       /* search output flistp */
	pin_flist_t          *sub_flp = NULL;         /* a sub-flistp */
	char                 s_template[ 255 ] = "select X from /service where F1 = V1 and F2 = V2 ";
	int32                search_flags = SRCH_DISTINCT;
	char                 msg[ 1024 ];
	void                 *vp = NULL;

	if(PIN_ERRBUF_IS_ERR(ebufp)) return;
	PIN_ERRBUF_CLEAR(ebufp);

	/*******************************************************************
 	 * initialize *out_flistpp, in case of error return
 	 *******************************************************************/
	*out_flistpp = NULL;

	/*******************************************************************
	 * if a_pdp is invalid, simply return
 	 *******************************************************************/
	if(PIN_POID_IS_NULL(a_pdp)) return;

	/*******************************************************************
  	 * build a search input flist
  	 *******************************************************************/
	s_in_flp = PIN_FLIST_CREATE(ebufp);

	vp = PIN_POID_CREATE(PIN_POID_GET_DB(a_pdp), "/search", -1, ebufp);
	PIN_FLIST_FLD_PUT(s_in_flp, PIN_FLD_POID, vp, ebufp);

	PIN_FLIST_FLD_SET(s_in_flp, PIN_FLD_TEMPLATE, s_template, ebufp);
	PIN_FLIST_FLD_SET(s_in_flp, PIN_FLD_FLAGS, &search_flags, ebufp);

	sub_flp = PIN_FLIST_ELEM_ADD(s_in_flp, PIN_FLD_ARGS, 1, ebufp);
	PIN_FLIST_FLD_SET(sub_flp, PIN_FLD_ACCOUNT_OBJ, a_pdp, ebufp);

	vp = PIN_POID_CREATE(PIN_POID_GET_DB(a_pdp), service_typep, -1, ebufp);
	sub_flp = PIN_FLIST_ELEM_ADD(s_in_flp, PIN_FLD_ARGS, 2, ebufp);
	PIN_FLIST_FLD_SET(sub_flp, PIN_FLD_POID, vp, ebufp);

	if(return_basic_fields)
	{
		sub_flp = PIN_FLIST_ELEM_ADD(s_in_flp, PIN_FLD_RESULTS, PIN_ELEMID_ANY, ebufp);
		PIN_FLIST_FLD_SET(sub_flp, PIN_FLD_POID, NULL, ebufp);
		PIN_FLIST_FLD_SET(sub_flp, PIN_FLD_ACCOUNT_OBJ, NULL, ebufp);
		PIN_FLIST_FLD_SET(sub_flp, PIN_FLD_BAL_GRP_OBJ, NULL, ebufp);
		PIN_FLIST_FLD_SET(sub_flp, PIN_FLD_LOGIN, NULL, ebufp);
		PIN_FLIST_FLD_SET(sub_flp, PIN_FLD_STATUS, NULL, ebufp);
	}
	else
	{
		PIN_FLIST_ELEM_SET(s_in_flp, NULL, PIN_FLD_RESULTS, PIN_ELEMID_ANY, ebufp);
	}

	snprintf(msg, 1023, "%s: search subscription input", func_name);
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, s_in_flp);
	PCM_OP(ctxp, PCM_OP_SEARCH, 0, s_in_flp, &s_out_flp, ebufp);
	PIN_FLIST_DESTROY_EX(&s_in_flp, NULL);

	/*******************************************************************
 	 * return should be unique, if exists
 	 *******************************************************************/
	*out_flistpp = PIN_FLIST_ELEM_TAKE(s_out_flp, PIN_FLD_RESULTS, PIN_ELEMID_ANY, 1, ebufp);
	PIN_FLIST_DESTROY_EX(&s_out_flp, NULL);

	/*******************************************************************
 	 * check error
 	 *******************************************************************/
	if(PIN_ERRBUF_IS_ERR(ebufp))
	{
		snprintf(msg, 1023, "%s: error", func_name);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		PIN_FLIST_DESTROY_EX(out_flistpp, NULL);
	}
	else
	{
		snprintf(msg, 1023, "%s: output", func_name);
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, *out_flistpp);
	}
	return;
}

/*******************************************************************
 * fm_dex_order_utils_execute_op(): wrapper of PCM_OP
 * caller is responsible for memory mgmt of input & output flists
 *******************************************************************/
void
fm_dex_order_utils_execute_op(
    pcm_context_t        *ctxp,
    pin_flist_t          *in_flistp,
    pin_flist_t          **o_flistpp,
    pin_errbuf_t         *ebufp)
{
    char                 func_name[ 40 ] = "fm_dex_order_utils_execute_op";
    const char           *opnamep = NULL;    /* opcode namep */
    char                 msg[ 1024 ];
    pin_flist_t          *i_flistp = NULL;
    int32                opcode = 0;
    int32                opflags = 0;
    void                *vp = NULL;

    if(PIN_ERRBUF_IS_ERR(ebufp)) return;
    PIN_ERRBUF_CLEAR(ebufp);

    opcode = *(int32 *) PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_OPCODE, 0, ebufp);
    vp = (void *) PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_FLAGS, 1, ebufp);
    if(vp){
        opflags = *(int32 *)vp;
    }

    opnamep = pcm_opcode_to_opname(opcode);
    i_flistp = PIN_FLIST_SUBSTR_TAKE(in_flistp, PIN_FLD_OPERATION_INFO, 0, ebufp);

    sprintf(msg, "%s: %s input (opflags = %d)", func_name, opnamep, opflags);
    PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, i_flistp);

    PCM_OP(ctxp, opcode, opflags, i_flistp, o_flistpp, ebufp);

    PIN_FLIST_DESTROY_EX(&i_flistp, NULL);
    sprintf(msg, "%s: %s output", func_name, opnamep);
    PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, *o_flistpp);

    if(PIN_ERRBUF_IS_ERR(ebufp))
    {
        sprintf(msg, "%s: %s error", func_name, opnamep);
        PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
    }
    return;
}

void
fm_dex_order_utils_add_counter_products(
    pcm_context_t   	*ctxp,
    pin_flist_t     	*order_in_flistp,
    pin_flist_t     	*cust_modify_flistp,
    pin_flist_t     	**opi_flistpp,
    pin_errbuf_t    	*ebufp)
{
	pin_flist_t			*opinfo_flistp = NULL;
	pin_flist_t			*serv_flistp = NULL;
	pin_flist_t			*dealinfo_flistp = NULL;
	pin_flist_t			*prod_flistp = NULL;
	pin_flist_t			*product_flistp = NULL;
	pin_flist_t			*cache_flistp = NULL;
	poid_t				*a_pdp = NULL;
	poid_t				*serv_pdp = NULL;
	poid_t				*deal_pdp = NULL;
    char                func_name[ 40 ] = "fm_dex_order_utils_add_counter_products";
	char				*eitem_id = NULL;
	char				contract_term_str[12];
	char				msg[1024];
	char				prod_name[512];
	pin_decimal_t   	*total_contract_amountp = NULL;
	pin_decimal_t   	*contract_termp = NULL;
	pin_decimal_t   	*quantity = NULL;
	int32				contract_term = 0;
	int32				status = PIN_PRODUCT_STATUS_ACTIVE;
	int32				status_flags = 0;
	int32				opcode = PCM_OP_SUBSCRIPTION_PURCHASE_DEAL;
	int32				err = PIN_ERR_NONE;
	time_t				start_t = 0;

    if(PIN_ERRBUF_IS_ERR(ebufp)) return;
    PIN_ERRBUF_CLEAR(ebufp);

    sprintf(msg, "%s: input (order) flist", func_name);
    PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, order_in_flistp);

    sprintf(msg, "%s: input (cust modify) flist", func_name);
    PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, cust_modify_flistp);

    contract_term = *(int32 *) PIN_FLIST_FLD_GET(order_in_flistp, DEX_FLD_CONTRACT_TERM, 0, ebufp);
	sprintf(contract_term_str, "%d", contract_term);
    total_contract_amountp = PIN_FLIST_FLD_GET(order_in_flistp, PIN_FLD_TOTAL_DUE, 0, ebufp);
	eitem_id = PIN_FLIST_FLD_GET(order_in_flistp, DEX_FLD_EITEM_ID, 0, ebufp);

	a_pdp = PIN_FLIST_FLD_GET(cust_modify_flistp, PIN_FLD_ACCOUNT_OBJ, 0, ebufp);
	serv_flistp = PIN_FLIST_ELEM_GET(cust_modify_flistp, PIN_FLD_SERVICES, PIN_ELEMID_ANY, 0, ebufp);
	serv_pdp = PIN_FLIST_FLD_GET(serv_flistp, PIN_FLD_SERVICE_OBJ, 0, ebufp);

	*opi_flistpp = PIN_FLIST_CREATE(ebufp);
    PIN_FLIST_FLD_SET(*opi_flistpp, PIN_FLD_POID, a_pdp, ebufp);
    PIN_FLIST_FLD_SET(*opi_flistpp, PIN_FLD_OPCODE, &opcode, ebufp);

	opinfo_flistp = PIN_FLIST_SUBSTR_ADD(*opi_flistpp, PIN_FLD_OPERATION_INFO, ebufp);
    PIN_FLIST_FLD_SET(opinfo_flistp, PIN_FLD_POID, a_pdp, ebufp);
    PIN_FLIST_FLD_SET(opinfo_flistp, PIN_FLD_PROGRAM_NAME, "fm_dex_order", ebufp);
    PIN_FLIST_FLD_SET(opinfo_flistp, PIN_FLD_ACCOUNT_OBJ, a_pdp, ebufp);
    PIN_FLIST_FLD_SET(opinfo_flistp, PIN_FLD_SERVICE_OBJ, serv_pdp, ebufp);

	dealinfo_flistp = PIN_FLIST_SUBSTR_ADD(opinfo_flistp, PIN_FLD_DEAL_INFO, ebufp);
	deal_pdp = PIN_POID_CREATE(PIN_POID_GET_DB(a_pdp), "/deal", -1, ebufp);
    PIN_FLIST_FLD_PUT(dealinfo_flistp, PIN_FLD_POID, deal_pdp, ebufp);
    PIN_FLIST_FLD_SET(dealinfo_flistp, PIN_FLD_NAME, "fm_dex_order", ebufp);
    PIN_FLIST_FLD_SET(dealinfo_flistp, PIN_FLD_DESCR, "fm_dex_order", ebufp);
	PIN_FLIST_FLD_SET(dealinfo_flistp, PIN_FLD_START_T, &start_t, ebufp);
	PIN_FLIST_FLD_SET(dealinfo_flistp, PIN_FLD_END_T, &start_t, ebufp);
	
	/************************************************************
	 * We have to add two products for counter and total contract
	 * amount
	 ************************************************************/
	prod_flistp = PIN_FLIST_ELEM_ADD(dealinfo_flistp, PIN_FLD_PRODUCTS, 0, ebufp);
	PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_DESCR, eitem_id, ebufp)
	PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_PURCHASE_FEE_AMT, total_contract_amountp, ebufp);
	status_flags = (PIN_PROD_STATUS_FLAGS_OVERRIDE_PURCHASE_FEE |
                                          PIN_PROD_STATUS_FLAGS_OVERRIDE_CYCLE_FEE);

	quantity  = pbo_decimal_from_str("1.0", ebufp);
	PIN_FLIST_FLD_PUT(prod_flistp, PIN_FLD_QUANTITY, quantity, ebufp);
	PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_STATUS, &status, ebufp)
	PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_STATUS_FLAGS, &status_flags, ebufp)

	memset(prod_name, '\0', 512);
	strncpy(prod_name, contract_amt_product, strlen(contract_amt_product));
	cache_flistp = cm_cache_find_entry(fm_dex_order_product_name_cache_ptr, prod_name, &err);
	PIN_FLIST_FLD_COPY(cache_flistp, PIN_FLD_POID, prod_flistp, PIN_FLD_PRODUCT_OBJ, ebufp);
	PIN_FLIST_DESTROY_EX(&cache_flistp, NULL);

	prod_flistp = PIN_FLIST_ELEM_ADD(dealinfo_flistp, PIN_FLD_PRODUCTS, 1, ebufp);
	PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_DESCR, eitem_id, ebufp)
	contract_termp  = pbo_decimal_from_str(contract_term_str, ebufp);
	PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_PURCHASE_FEE_AMT, contract_termp, ebufp);
	status_flags = (PIN_PROD_STATUS_FLAGS_OVERRIDE_PURCHASE_FEE |
                                          PIN_PROD_STATUS_FLAGS_OVERRIDE_CYCLE_FEE);

	quantity  = pbo_decimal_from_str("1.0",ebufp);
	PIN_FLIST_FLD_PUT(prod_flistp, PIN_FLD_QUANTITY, quantity, ebufp);
	PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_STATUS, &status, ebufp)
	PIN_FLIST_FLD_SET(prod_flistp, PIN_FLD_STATUS_FLAGS, &status_flags, ebufp)

	memset(prod_name, '\0', 512);
	strncpy(prod_name, contract_term_product, strlen(contract_term_product));
	cache_flistp = cm_cache_find_entry(fm_dex_order_product_name_cache_ptr, prod_name, &err);
	PIN_FLIST_FLD_COPY(cache_flistp, PIN_FLD_POID, prod_flistp, PIN_FLD_PRODUCT_OBJ, ebufp);
	PIN_FLIST_DESTROY_EX(&cache_flistp, NULL);

    sprintf(msg, "%s: return (purchase deal) flist", func_name);
    PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, *opi_flistpp);

}
