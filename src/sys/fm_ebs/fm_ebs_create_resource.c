/**
 * This is an EBS custom API, a confidential property of EbillSoft LLC.
 * Unauthorized code replication or usage is strictly prohibited.
 */

#ifndef lint
static  char    Sccs_id[] = "@(#)$Id: Exp $";
#endif

/**
 * Includes
 */

// Std. C headers
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>

// Oracle BRM ootb headers
#include "pcm.h"
#include "ops/bill.h"
#include "ops/pymt.h"
#include "ops/cust.h"
#include "cm_fm.h"
#include "pin_errs.h"
#include "pin_cust.h"
#include "pin_inv.h"
#include "pin_currency.h"
#include "pin_cc.h"
#include "pin_cc_patterns.h"
#include "pinlog.h"
#include "fm_utils.h"
#include "fm_bill_utils.h"
#include "pin_bill.h"
#include "pin_pymt.h"
#include "pin_type.h"

// Oracle BRM custom headers
#include "cust_ops.h"


/**
 * Local function definitions
 */
EXPORT_OP void
op_ebs_create_resource (
	cm_nap_connection_t	*connp,
	int32			opcode,
	int32			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**o_flistpp,
	pin_errbuf_t		*ebufp );

static void
fm_ebs_create_resource (
	pcm_context_t		*ctxp,
	pin_flist_t		*in_flistp,
	pin_flist_t		**r_flistpp,
	pin_errbuf_t		*ebufp );

static void
process_input (
	pcm_context_t		*ctxp,
	pin_flist_t		*in_flistp,
	pin_flist_t		**r_flistpp,
	pin_errbuf_t		*ebufp );

static void
check_uniqueness (
	pcm_context_t		*ctxp,
	char			*in_str_code,
	char			*in_symbol,
	pin_flist_t		**r_flistpp,
	pin_errbuf_t		*ebufp );

static void
enhance_input (
	pcm_context_t		*ctxp,
	pin_flist_t		*in_flistp,
	pin_flist_t		**io_flistpp,
	pin_errbuf_t		*ebufp );


/**
 * Opcode function
 */
void
op_ebs_create_resource (
	cm_nap_connection_t	*connp,
	int32			opcode,
	int32			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**o_flistpp,
	pin_errbuf_t		*ebufp )
{

	pcm_context_t		*ctxp = connp->dm_ctx;

	if( PIN_ERR_IS_ERR( ebufp ) )
		return;

	*o_flistpp = NULL;
	PIN_ERR_CLEAR_ERR( ebufp );

	if( opcode != EBS_OP_CREATE_RESOURCE )
	{

		pin_set_err( ebufp, PIN_ERRLOC_FM, PIN_ERRCLASS_SYSTEM_DETERMINATE,
				PIN_ERR_BAD_OPCODE, 0, 0, opcode );
		PIN_ERR_LOG_EBUF( PIN_ERR_LEVEL_ERROR, "op_ebs_create_resource: bad opcode!", ebufp );
		return;
	}

	PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, "op_ebs_create_resource: input flist -", in_flistp );
	fm_ebs_create_resource( ctxp, in_flistp, o_flistpp, ebufp );

	if( PIN_ERR_IS_ERR( ebufp ) )
	{

		PIN_ERR_LOG_EBUF( PIN_ERR_LEVEL_ERROR, "op_ebs_create_resource: error buffer!", ebufp );
		if( *o_flistpp != NULL )
		{
			PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_ERROR, "op_ebs_create_resource: error output", *o_flistpp );
			PIN_FLIST_DESTROY_EX( o_flistpp, NULL );
		}
	}

	PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, "op_ebs_create_resource: output -", *o_flistpp );
	return;
}


/**
 * Main function implementation
 */
void
fm_ebs_create_resource (
	pcm_context_t		*ctxp,
	pin_flist_t		*in_flistp,
	pin_flist_t		**r_flistpp,
	pin_errbuf_t		*ebufp )
{

	pin_flist_t		*work_flistp = NULL;

	int32			op_flgs = PCM_OPFLG_ADD_ENTRY | PCM_OPFLG_READ_RESULT;


	if( PIN_ERR_IS_ERR( ebufp ) )
		return;

	*r_flistpp = NULL;
	PIN_ERR_CLEAR_ERR( ebufp );

	process_input( ctxp, in_flistp, &work_flistp, ebufp );
	if( PIN_ERR_IS_ERR( ebufp ) )
	{
		PIN_ERR_LOG_EBUF( PIN_ERR_LEVEL_ERROR, "fm_ebs_create_resource: error validating input!", ebufp );
		PIN_FLIST_DESTROY_EX( &work_flistp, NULL );
		return;
	}

	enhance_input( ctxp, in_flistp, &work_flistp, ebufp );

	PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, "fm_ebs_create_resource: write_flds input -", work_flistp );
	PCM_OP( ctxp, PCM_OP_WRITE_FLDS, op_flgs, work_flistp, r_flistpp, ebufp );
	PIN_FLIST_DESTROY_EX( &work_flistp, NULL );
	PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, "fm_ebs_create_resource: write_flds output -", *r_flistpp );

	if( PIN_ERR_IS_ERR( ebufp ) )
	{
		PIN_ERR_LOG_EBUF( PIN_ERR_LEVEL_ERROR, "fm_ebs_create_resource: error creating the resource!", ebufp );
		PIN_FLIST_DESTROY_EX( r_flistpp, NULL );
	}

	return;
}

void
process_input (
	pcm_context_t		*ctxp,
	pin_flist_t		*in_flistp,
	pin_flist_t		**r_flistpp,
	pin_errbuf_t		*ebufp )
{

	pin_flist_t		*bal_flp = NULL;
	pin_flist_t		*rules_flp = NULL;

	int32			stage = 0;
	int32			elem_id = 0;
	int32			rules_count = 0;
	int32			*resource_typep = NULL;

	char			*in_symbol = NULL;
	char			*in_str_code = NULL;

	pin_cookie_t		cookie = NULL;

	void			*vp = NULL;


	if( PIN_ERR_IS_ERR( ebufp ) )
		return;

	*r_flistpp = NULL;
	PIN_ERR_CLEAR_ERR( ebufp );


	// Check mandatory fields
	vp = PIN_FLIST_FLD_GET( in_flistp, PIN_FLD_NAME, 0, ebufp );
	vp = PIN_FLIST_FLD_GET( in_flistp, PIN_FLD_APPLY_MODE, 0, ebufp );
	vp = PIN_FLIST_FLD_GET( in_flistp, PIN_FLD_CONSUMPTION_RULE, 0, ebufp );
	resource_typep = PIN_FLIST_FLD_GET( in_flistp, PIN_FLD_TYPE, 0, ebufp );	// 0 - non-currency, 1 - currency
	in_str_code = PIN_FLIST_FLD_GET( in_flistp, PIN_FLD_BEID_STR_CODE, 0, ebufp );
	in_symbol = PIN_FLIST_FLD_GET( in_flistp, PIN_FLD_SYMBOL, 0, ebufp );

	if( in_str_code == NULL || in_symbol == NULL || strlen( in_str_code ) == 0 || strlen( in_symbol ) == 0 )
	{
		pin_set_err( ebufp, PIN_ERRLOC_FM, PIN_ERRCLASS_SYSTEM_DETERMINATE,
					PIN_ERR_IS_NULL, 0, 0, 0 );
		PIN_ERR_LOG_EBUF( PIN_ERR_LEVEL_ERROR, "process_input: str_code and/or symbol NULL!", ebufp );
	}

	if( !PIN_ERR_IS_ERR( ebufp ) && resource_typep != NULL && *(int32 *)resource_typep == 1 )
	{
		// rounding instructions are expected for currency resources
		rules_count = PIN_FLIST_ELEM_COUNT( in_flistp, PIN_FLD_RULES, ebufp );
		if( rules_count != 4 )
		{
			pin_set_err( ebufp, PIN_ERRLOC_FM, PIN_ERRCLASS_SYSTEM_DETERMINATE,
						PIN_ERR_NONEXISTANT_ELEMENT, PIN_FLD_RULES, 0, 0 );
			PIN_ERR_LOG_EBUF( PIN_ERR_LEVEL_ERROR, "process_input: missing rounding rule(s) for currency resource", ebufp );
		}

		cookie = NULL;
		while( ( rules_flp = PIN_FLIST_ELEM_GET_NEXT( in_flistp, PIN_FLD_RULES,
					&elem_id, 1, &cookie, ebufp ) ) != NULL )
		{
			vp = PIN_FLIST_FLD_GET( rules_flp, PIN_FLD_PROCESSING_STAGE, 0, ebufp );
			vp = PIN_FLIST_FLD_GET( rules_flp, PIN_FLD_ROUNDING, 0, ebufp );
			vp = PIN_FLIST_FLD_GET( rules_flp, PIN_FLD_ROUNDING_MODE, 0, ebufp );
		}
	}

	// Check uniqueness of STR_CODE and SYMBOL
	if( !PIN_ERR_IS_ERR( ebufp ) )
		check_uniqueness( ctxp, in_str_code, in_symbol, r_flistpp, ebufp );

	if( PIN_ERR_IS_ERR( ebufp ) )
	{
		PIN_ERR_LOG_EBUF( PIN_ERR_LEVEL_ERROR, "process_input: input validation failed!", ebufp );
	}

	return;
}

void
check_uniqueness (
	pcm_context_t		*ctxp,
	char			*in_str_code,
	char			*in_symbol,
	pin_flist_t		**r_flistpp,
	pin_errbuf_t		*ebufp )
{

	pin_flist_t		*bal_flp = NULL;
	pin_flist_t		*args_flp = NULL;
	pin_flist_t		*result_flp = NULL;
	pin_flist_t		*srch_i_flp = NULL;
	pin_flist_t		*srch_o_flp = NULL;

	poid_t			*dummy_pdp = NULL;

	char			*temp_symbol = NULL;
	char			*temp_str_code = NULL;
	char			srch_template[256] = "select X from /config where F1 = V1 ";

	int32			elem_id = 0;
	int32			srch_flgs = SRCH_DISTINCT;
	int64			i64_dbno = cm_fm_get_startup_db_no();

	pin_cookie_t		cookie = NULL;


	srch_i_flp = PIN_FLIST_CREATE( ebufp );
	dummy_pdp = PIN_POID_CREATE( i64_dbno, "/search", -1, ebufp );
	PIN_FLIST_FLD_PUT( srch_i_flp, PIN_FLD_POID, dummy_pdp, ebufp );
	PIN_FLIST_FLD_SET( srch_i_flp, PIN_FLD_TEMPLATE, srch_template, ebufp );
	PIN_FLIST_FLD_SET( srch_i_flp, PIN_FLD_FLAGS, &srch_flgs, ebufp );

	args_flp = PIN_FLIST_ELEM_ADD( srch_i_flp, PIN_FLD_ARGS, 1, ebufp );
	dummy_pdp = PIN_POID_CREATE( i64_dbno, "/config/beid", -1, ebufp );
	PIN_FLIST_FLD_PUT( args_flp, PIN_FLD_POID, dummy_pdp, ebufp );

	result_flp = PIN_FLIST_ELEM_ADD( srch_i_flp, PIN_FLD_RESULTS, PIN_ELEMID_ANY, ebufp );
	PIN_FLIST_FLD_SET( result_flp, PIN_FLD_POID, NULL, ebufp );
	args_flp = PIN_FLIST_ELEM_ADD( result_flp, PIN_FLD_BALANCES, PIN_ELEMID_ANY, ebufp );
	PIN_FLIST_FLD_SET( args_flp, PIN_FLD_BEID_STR_CODE, NULL, ebufp );
	PIN_FLIST_FLD_SET( args_flp, PIN_FLD_SYMBOL, NULL, ebufp );
	PIN_FLIST_FLD_SET( args_flp, PIN_FLD_NAME, NULL, ebufp );

	PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, "process_input: config search input -", srch_i_flp );
	PCM_OP( ctxp, PCM_OP_SEARCH, 0, srch_i_flp, &srch_o_flp, ebufp );
	PIN_FLIST_DESTROY_EX( &srch_i_flp, NULL );
	PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, "process_input: config search output -", srch_o_flp );

	result_flp = PIN_FLIST_FLD_GET( srch_o_flp, PIN_FLD_RESULTS, PIN_ELEMID_ANY, ebufp ); // search should only return one config object

	cookie = NULL;
	while( ( bal_flp = PIN_FLIST_ELEM_GET_NEXT( result_flp, PIN_FLD_BALANCES,
				&elem_id, 1, &cookie, ebufp ) ) != NULL )
	{
		temp_str_code = PIN_FLIST_FLD_GET( bal_flp, PIN_FLD_BEID_STR_CODE, 0, ebufp );
		temp_symbol = PIN_FLIST_FLD_GET( bal_flp, PIN_FLD_SYMBOL, 0, ebufp );

		if( !strcmp( temp_str_code, in_str_code ) || !strcmp( temp_symbol, in_symbol ) )
		{
			PIN_ERR_LOG_MSG( PIN_ERR_LEVEL_ERROR, "process_input: input not unique!" );
			PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_ERROR, "process_input: validated against -", bal_flp );
			pin_set_err( ebufp, PIN_ERRLOC_FM, PIN_ERRCLASS_SYSTEM_DETERMINATE,
						PIN_ERR_DUPLICATE, 0, 0, 0 );
			PIN_ERR_LOG_EBUF( PIN_ERR_LEVEL_ERROR, "process_input: validation failed!", ebufp );
			break;
		}
	}

	// Construct the work flist for resource creation
	if( !PIN_ERR_IS_ERR( ebufp ) )
	{
		*r_flistpp = PIN_FLIST_CREATE( ebufp );
		PIN_FLIST_FLD_COPY( result_flp, PIN_FLD_POID, *r_flistpp, PIN_FLD_POID, ebufp );
	}

	PIN_FLIST_DESTROY_EX( &srch_o_flp, NULL );
	return;
}

void
enhance_input (
	pcm_context_t		*ctxp,
	pin_flist_t		*in_flistp,
	pin_flist_t		**io_flistpp,
	pin_errbuf_t		*ebufp )
{

	pin_flist_t		*bal_flp = NULL;
	pin_flist_t		*rules_flp = NULL;

	int32			stage = 0;
	int32			elem_id = 0;
	int32			round_to = 2;
	int32			rules_count = 0;
	int32			*resource_typep = NULL;
	int32			resource_validity = 366;
	int32			round_mode = PIN_BEID_ROUND_NEAREST;
	int32			beid_active = PIN_BEID_STATUS_ACTIVE;

	pin_cookie_t		cookie = NULL;

	pin_decimal_t		*zero_decp = NULL;


	if( PIN_ERR_IS_ERR( ebufp ) )
		return;

	if( *io_flistpp == NULL )
	{
		pin_set_err( ebufp, PIN_ERRLOC_FM, PIN_ERRCLASS_SYSTEM_DETERMINATE,
				PIN_ERR_BAD_VALUE, 0, 0, 0 );
		PIN_ERR_LOG_EBUF( PIN_ERR_LEVEL_ERROR, "enhance input: NULL work flist!", ebufp );
		return;
	}
		
				
	PIN_ERR_CLEAR_ERR( ebufp );


	resource_typep = PIN_FLIST_FLD_GET( in_flistp, PIN_FLD_TYPE, 0, ebufp );	// 0 - non-currency, 1 - currency

	bal_flp = PIN_FLIST_ELEM_ADD( *io_flistpp, PIN_FLD_BALANCES, PIN_ELEMID_ASSIGN, ebufp );
	PIN_FLIST_FLD_COPY( in_flistp, PIN_FLD_APPLY_MODE, bal_flp, PIN_FLD_APPLY_MODE, ebufp );
	PIN_FLIST_FLD_COPY( in_flistp, PIN_FLD_BEID_STR_CODE, bal_flp, PIN_FLD_BEID_STR_CODE, ebufp );
	PIN_FLIST_FLD_COPY( in_flistp, PIN_FLD_CONSUMPTION_RULE, bal_flp, PIN_FLD_CONSUMPTION_RULE, ebufp );
	PIN_FLIST_FLD_COPY( in_flistp, PIN_FLD_NAME, bal_flp, PIN_FLD_NAME, ebufp );
	PIN_FLIST_FLD_COPY( in_flistp, PIN_FLD_SYMBOL, bal_flp, PIN_FLD_SYMBOL, ebufp );

	PIN_FLIST_FLD_SET( bal_flp, PIN_FLD_STATUS, &beid_active, ebufp );
	PIN_FLIST_FLD_SET( bal_flp, PIN_FLD_VALIDITY_IN_DAYS, &resource_validity, ebufp );

	if( resource_typep != NULL && *(int32 *)resource_typep == 1 )
	{
		PIN_FLIST_FLD_COPY( in_flistp, PIN_FLD_RULES, bal_flp, PIN_FLD_RULES, ebufp );
		cookie = NULL;
		while( ( rules_flp = PIN_FLIST_ELEM_GET_NEXT( bal_flp, PIN_FLD_RULES,
					&elem_id, 1, &cookie, ebufp ) ) != NULL )
		{
			zero_decp = pbo_decimal_from_str( "0.0", ebufp );

			PIN_FLIST_FLD_SET( rules_flp, PIN_FLD_EVENT_TYPE, "*", ebufp );
			PIN_FLIST_FLD_SET( rules_flp, PIN_FLD_TOLERANCE_PERCENT, zero_decp, ebufp );
			PIN_FLIST_FLD_SET( rules_flp, PIN_FLD_TOLERANCE_AMOUNT_MAX, zero_decp, ebufp );
			PIN_FLIST_FLD_PUT( rules_flp, PIN_FLD_TOLERANCE_AMOUNT_MIN, zero_decp, ebufp );
		}
	}
	else
	{
		for( stage = 0; stage <= 3; stage++ )
		{
			rules_flp = PIN_FLIST_CREATE( ebufp );
			zero_decp = pbo_decimal_from_str( "0.0", ebufp );

			PIN_FLIST_FLD_SET( rules_flp, PIN_FLD_EVENT_TYPE, "*", ebufp );
			PIN_FLIST_FLD_SET( rules_flp, PIN_FLD_TOLERANCE_PERCENT, zero_decp, ebufp );
			PIN_FLIST_FLD_SET( rules_flp, PIN_FLD_TOLERANCE_AMOUNT_MAX, zero_decp, ebufp );
			PIN_FLIST_FLD_PUT( rules_flp, PIN_FLD_TOLERANCE_AMOUNT_MIN, zero_decp, ebufp );

			PIN_FLIST_FLD_SET( rules_flp, PIN_FLD_ROUNDING, &round_to, ebufp );
			PIN_FLIST_FLD_SET( rules_flp, PIN_FLD_ROUNDING_MODE, &round_mode, ebufp );
			PIN_FLIST_FLD_SET( rules_flp, PIN_FLD_PROCESSING_STAGE, &stage, ebufp );

			PIN_FLIST_ELEM_PUT( bal_flp, rules_flp, PIN_FLD_RULES, stage, ebufp );
		}
	}

	PIN_ERR_LOG_FLIST( PIN_ERR_LEVEL_DEBUG, "process_input: processed input -", *io_flistpp );
	return;
}
