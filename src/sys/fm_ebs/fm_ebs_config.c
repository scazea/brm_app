/**
 * fm_ebs_config.c
 *
 * Config for EBS custom FM
 */

#include <stdio.h>
#include "pcm.h"
#include "cm_fm.h"
#include "ops/cust.h"
#include "cust_ops.h"

PIN_EXPORT void * fm_ebs_config_func();

struct cm_fm_config fm_ebs_config[] = {

	/* opcode-id (u_int), function name (string) */
	{ EBS_OP_CREATE_RESOURCE, "op_ebs_create_resource" },
	{ 0, (char *) 0 }
};

void *
fm_ebs_config_func()
{
	return ((void *) (fm_ebs_config));
}
