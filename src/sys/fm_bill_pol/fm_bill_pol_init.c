/*
 *
 *      Copyright (c) 2004 - 2006 Oracle. All rights reserved.
 *
 *      This material is the confidential property of Oracle Corporation or its
 *      licensors and may be used, reproduced, stored or transmitted only in
 *      accordance with a valid Oracle license or sublicense agreement.
 *
 */
/*********************************************************************
 * Maintentance Log:
 * 
 * Date: 20171008
 * Author: Ebillsoft
 * Identifier: N/A
 * Notes: Get tolerance values from cm pin.conf 
 **********************************************************************/
/*******************************************************************************
 * File History
 *
 *  DATE    |  CHANGE                                           |  Author
 ***********|***************************************************|****************
 |10/08/2017|Accelerate charges for small contracts             | Rama Puppala
 |**********|***************************************************|****************
 ********************************************************************************/
#ifndef lint
static const char Sccs_id[] = "@(#) %full_filespec: fm_bill_pol_init.c~2:csrc:1 %";
#endif

#include <stdio.h>
#include <string.h>

#include <pcm.h>
#include <pinlog.h>

#define FILE_LOGNAME "fm_bill_pol_init.c(1)"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "pcm.h"
#include "ops/cust.h"
#include "pinlog.h"
#include "pin_errs.h"
#include "cm_fm.h"
#include "cm_cache.h"
#include "dex_flds.h"


/***********************************************************************
 *Forward declaration
 ***********************************************************************/
EXPORT_OP void fm_bill_pol_init(int32 *errp);

PIN_EXPORT char		*fm_bill_pol_dd_vendor = NULL;
PIN_EXPORT pin_decimal_t	*min_local_balp = NULL;
PIN_EXPORT pin_decimal_t	*max_local_balp = NULL;
PIN_EXPORT pin_decimal_t	*min_national_balp = NULL;
PIN_EXPORT pin_decimal_t	*max_national_balp = NULL;
PIN_EXPORT pin_decimal_t	*min_contract_amtp = NULL;

/*******************************************************************
 * Globals for /config/suppress objects in memory/cache stored objects.
 *******************************************************************/
PIN_EXPORT cm_cache_t	*fm_bill_suppression_config_ptr = (cm_cache_t*) 0; 
PIN_EXPORT cm_cache_t	*fm_bill_lcr_state_config_ptr = (cm_cache_t*) 0; 
PIN_EXPORT cm_cache_t	*fm_bill_lcr_company_config_ptr = (cm_cache_t*) 0; 

static void
fm_bill_pol_config_suppression_cache(
	pcm_context_t	*ctxp,
	int64		database,		
	pin_errbuf_t	*ebufp);

static void
fm_bill_pol_config_lcr_company_cache(
	pcm_context_t	*ctxp,
	int64		database,		
	pin_errbuf_t	*ebufp);

static void
fm_bill_pol_config_lcr_state_cache(
	pcm_context_t	*ctxp,
	int64		database,		
	pin_errbuf_t	*ebufp);

/***********************************************************************
 *fm_bill_pol_init: One time initialization for fm_bill_pol
 ***********************************************************************/
void fm_bill_pol_init(int32 *errp)
{
        pcm_context_t   *ctxp = NULL;
        pin_errbuf_t    ebuf;
	poid_t		*pdp = NULL;
	char		*local_tolp = NULL;
	char		*national_tolp = NULL;
	char		*tmp_strp = NULL;
	char		*min_contract_strp = NULL;
        int             err;
	int64		database = -1;
 
        /***********************************************************
         * open the context.
         ***********************************************************/
        PIN_ERR_CLEAR_ERR(&ebuf);
        PCM_CONTEXT_OPEN(&ctxp, (pin_flist_t *)0, &ebuf);

        if(PIN_ERR_IS_ERR(&ebuf)) {
                pin_set_err(&ebuf, PIN_ERRLOC_FM,
                        PIN_ERRCLASS_SYSTEM_DETERMINATE,
                        PIN_ERR_DM_CONNECT_FAILED, 0, 0, ebuf.pin_err);
                PIN_FLIST_LOG_ERR("fm_bill_pol_init pcm_context_open err", 
				&ebuf);

                *errp = PIN_ERR_DM_CONNECT_FAILED;
                return;
        }

	pin_conf("cm", "dd_vendor", PIN_FLDT_STR, &fm_bill_pol_dd_vendor, &err);

	pin_conf("fm_bill_pol", "local_tolerance", PIN_FLDT_STR, &local_tolp, &err);
	if (local_tolp != NULL)
	{
		tmp_strp = strtok(local_tolp, ",");
		min_local_balp = pbo_decimal_from_str(tmp_strp, &ebuf);		
		tmp_strp = strtok (NULL, ",");
		max_local_balp = pbo_decimal_from_str(tmp_strp, &ebuf);		
		pin_free(local_tolp);
		local_tolp = NULL;
	}

	pin_conf("fm_bill_pol", "national_tolerance", PIN_FLDT_STR, &national_tolp, &err);
	if (national_tolp != NULL)
	{
		tmp_strp = strtok(national_tolp, ",");
		min_national_balp = pbo_decimal_from_str(tmp_strp, &ebuf);		
		tmp_strp = strtok (NULL, ",");
		max_national_balp = pbo_decimal_from_str(tmp_strp, &ebuf);		
		pin_free(national_tolp);
		national_tolp = NULL;
	}

	pin_conf("fm_bill_pol", "min_contract_sum", PIN_FLDT_DECIMAL, (caddr_t *)&min_contract_strp, errp);

	/***********************************************************
	 * HACK: we need a db number. this will eventually
	 * be handled by the 'get beid object' method from
	 * likely the rating FM. But for now, assume db
	 * matches userid found in pin.conf
	 ***********************************************************/
	pdp = PCM_GET_USERID(ctxp);
	database = PIN_POID_GET_DB(pdp);
	PIN_POID_DESTROY(pdp, NULL);

	/* Cache the /config/suppression config object */
	fm_bill_pol_config_suppression_cache(ctxp, database, &ebuf);
	if (PIN_ERR_IS_ERR(&ebuf)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_DEBUG, 
		"fm_bill_pol_config_suppression_cache load error ", &ebuf);
	}

	/* Cache the /config/dex_lcr_company_level config object */
	fm_bill_pol_config_lcr_company_cache(ctxp, database, &ebuf);
	if (PIN_ERR_IS_ERR(&ebuf)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_DEBUG, 
		"fm_bill_pol_config_lcr_company_cache load error ", &ebuf);
	}

	/* Cache the /config/dex_lcr_state_level config object */
	fm_bill_pol_config_lcr_state_cache(ctxp, database, &ebuf);
	if (PIN_ERR_IS_ERR(&ebuf)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_DEBUG, 
		"fm_bill_pol_config_lcr_state_cache load error ", &ebuf);
	}

        PCM_CONTEXT_CLOSE(ctxp, 0, &ebuf);
        *errp = ebuf.pin_err;
        return;
}


/**********************************************************************
* Read the config objects /config/suppression into cm cache 
**********************************************************************/
static void
fm_bill_pol_config_suppression_cache(
	pcm_context_t	*ctxp,
	int64		database,		
	pin_errbuf_t	*ebufp)
{
	poid_t                  *s_pdp = NULL;
	poid_t                  *a_pdp = NULL;
	pin_flist_t             *s_flistp = NULL;
	pin_flist_t             *r_flistp = NULL;
	pin_flist_t             *res_flistp = NULL;
	pin_flist_t             *arg_flistp = NULL;

	int32                   err = PIN_ERR_NONE;
	int32                   cnt;
	cm_cache_key_iddbstr_t  kids;
	int32                   s_flags = 256;
	int			no_of_buckets = 1;
	
	s_flistp = PIN_FLIST_CREATE(ebufp);

	s_pdp = PIN_POID_CREATE(database, "/search", -1, ebufp);
	PIN_FLIST_FLD_PUT(s_flistp, PIN_FLD_POID, (void *)s_pdp, ebufp);
	PIN_FLIST_FLD_SET(s_flistp, PIN_FLD_FLAGS, (void *)&s_flags, ebufp);
	PIN_FLIST_FLD_SET(s_flistp, PIN_FLD_TEMPLATE, 
			"select X from /config where F1 = V1 ", ebufp);

	/* setup arguments */
	arg_flistp = PIN_FLIST_ELEM_ADD(s_flistp, PIN_FLD_ARGS, 1, ebufp);

	a_pdp = PIN_POID_CREATE(database, "/config/suppression", -1, ebufp);
	PIN_FLIST_FLD_PUT(arg_flistp, PIN_FLD_POID, (void *)a_pdp, ebufp);
	PIN_FLIST_ELEM_SET( s_flistp, NULL, PIN_FLD_RESULTS, 0, ebufp);

	PCM_OP(ctxp, PCM_OP_SEARCH, 0, s_flistp, &r_flistp, ebufp);
	if (PIN_ERR_IS_ERR(ebufp)) {
                PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
                        "fm_bill_config_suppression_object_from_db: "
			"error loading /config/suppression object",
                        ebufp);

                goto cleanup;
        }
	cnt = PIN_FLIST_COUNT(r_flistp, ebufp);

	if (cnt <= 1) {
		goto cleanup;
	}
	res_flistp = PIN_FLIST_ELEM_TAKE(r_flistp, PIN_FLD_RESULTS, 
					PIN_ELEMID_ANY, 1, ebufp);

	/*
	 * Get the number of items in the flist so we can create an
	 * appropriately sized cache
	 */
	cnt  = 0;
	if (res_flistp) {
		cnt = PIN_FLIST_COUNT(res_flistp, ebufp);
	}

	/*
	 * If there's no data, there's no point initializing it, is there.
	 * Especially since this is optional!
	 */
	if (cnt < 1) {
		goto cleanup;
	}
	
	/*
	 * This cache doesn't need configuration since the sizes of the
	 * entries are computed
	 */
	fm_bill_suppression_config_ptr =
		cm_cache_init("fm_bill_config_suppression_object", no_of_buckets,
				  cnt * 1024, 8, CM_CACHE_KEY_IDDBSTR, &err);

	if (err != PIN_ERR_NONE) {
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_ERROR, "Error: Couldn't initialize "
		"cache in fm_bill_pol_config_suppression_cache");
		pin_set_err(ebufp, PIN_ERRLOC_FM, 
				PIN_ERRCLASS_SYSTEM_DETERMINATE,
				PIN_ERR_NO_MEM, 0, 0, err);
		goto cleanup;
	}

	if (!fm_bill_suppression_config_ptr) {
		goto cleanup;
	}

	kids.id  = 0; 
	kids.db  = 0;
	kids.str = "/config/suppression";
	cm_cache_add_entry(fm_bill_suppression_config_ptr, 
				&kids, res_flistp, &err);

	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, 
				"fm_bill_config_suppression_object cache error",
				 ebufp);
	}
	else {
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, 
			"config_suppression_object cache loaded successfully");
	}
	
 cleanup:
        PIN_FLIST_DESTROY_EX(&s_flistp, NULL);
        PIN_FLIST_DESTROY_EX(&r_flistp, NULL);
        PIN_FLIST_DESTROY_EX(&res_flistp, NULL);
}

/**********************************************************************
* Read the config objects /config/dex_lcr_company_level into cm cache 
**********************************************************************/
static void
fm_bill_pol_config_lcr_company_cache(
	pcm_context_t	*ctxp,
	int64		database,		
	pin_errbuf_t	*ebufp)
{
	poid_t		*s_pdp = NULL;
	poid_t		*a_pdp = NULL;
	pin_flist_t	*s_flistp = NULL;
	pin_flist_t	*r_flistp = NULL;
	pin_flist_t	*res_flistp = NULL;
	pin_flist_t	*arg_flistp = NULL;
	pin_flist_t	*rates_flistp = NULL;

	pin_cookie_t	cookie = NULL;
	char		*bus_entityp = NULL;		
	char		*acc_typep = NULL;		
	char		*acc_subtypep = NULL;		
	char		*ar_regionp = NULL;		
	char		*brand_areap = NULL;		
	char		key_str[256]= {""};		
	int32		err = PIN_ERR_NONE;
	int32		cnt = 0;
	int32		s_flags = 256;
	int32		elemid = 0;
	
	s_flistp = PIN_FLIST_CREATE(ebufp);

	s_pdp = PIN_POID_CREATE(database, "/search", -1, ebufp);
	PIN_FLIST_FLD_PUT(s_flistp, PIN_FLD_POID, (void *)s_pdp, ebufp);
	PIN_FLIST_FLD_SET(s_flistp, PIN_FLD_FLAGS, (void *)&s_flags, ebufp);
	PIN_FLIST_FLD_SET(s_flistp, PIN_FLD_TEMPLATE, 
			"select X from /config where F1 = V1 ", ebufp);

	/* setup arguments */
	arg_flistp = PIN_FLIST_ELEM_ADD(s_flistp, PIN_FLD_ARGS, 1, ebufp);

	a_pdp = PIN_POID_CREATE(database, "/config/dex_lcr_company_level", -1, ebufp);
	PIN_FLIST_FLD_PUT(arg_flistp, PIN_FLD_POID, (void *)a_pdp, ebufp);
	PIN_FLIST_ELEM_SET( s_flistp, NULL, PIN_FLD_RESULTS, 0, ebufp);

	PCM_OP(ctxp, PCM_OP_SEARCH, 0, s_flistp, &r_flistp, ebufp);

	if (PIN_ERR_IS_ERR(ebufp)) {
                PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
                        "fm_bill_config_lcr_company_object_from_db: "
			"error loading /config/dex_lcr_company_level object",
                        ebufp);
                goto cleanup;
        }
	cnt = PIN_FLIST_ELEM_COUNT(r_flistp, PIN_FLD_RESULTS, ebufp);

	/*
	 * If there's no data, there's no point initializing it, is there.
	 * Especially since this is optional!
	 */
	if (cnt < 1) {
		goto cleanup;
	}

	/*
	 * This cache doesn't need configuration since the sizes of the
	 * entries are computed
	 */
	fm_bill_lcr_company_config_ptr =
		cm_cache_init("fm_bill_config_lcr_company_object", cnt,
				  cnt * 4096, 8, CM_CACHE_KEY_IDDBSTR, &err);

	if (err != PIN_ERR_NONE) {
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_ERROR, "Error: Couldn't initialize "
		"cache in fm_bill_pol_config_lcr_company_cache");
		pin_set_err(ebufp, PIN_ERRLOC_FM, 
				PIN_ERRCLASS_SYSTEM_DETERMINATE,
				PIN_ERR_NO_MEM, 0, 0, err);
		goto cleanup;
	}

	if (!fm_bill_lcr_company_config_ptr) {
		goto cleanup;
	}
	
	while (res_flistp = 
		PIN_FLIST_ELEM_GET_NEXT(r_flistp, PIN_FLD_RESULTS, &elemid, 1, &cookie, ebufp))
	{
		cm_cache_key_iddbstr_t  kids;
		kids.id  = 0; 
		kids.db  = 0;
		
		rates_flistp = PIN_FLIST_ELEM_GET(res_flistp, PIN_FLD_RATES, 
					PIN_ELEMID_ANY, 0, ebufp);
		bus_entityp = PIN_FLIST_FLD_GET(rates_flistp, PIN_FLD_BUSINESS_PARTNER, 
				0, ebufp);
		acc_typep = PIN_FLIST_FLD_GET(rates_flistp, DEX_FLD_ACCOUNT_TYPE, 
				0, ebufp);
		acc_subtypep = PIN_FLIST_FLD_GET(rates_flistp, DEX_FLD_ACCOUNT_SUBTYPE, 
				0, ebufp);
		ar_regionp = PIN_FLIST_FLD_GET(rates_flistp, DEX_FLD_FINANCE_ID, 
				0, ebufp);
		brand_areap = PIN_FLIST_FLD_GET(rates_flistp, PIN_FLD_BRAND_NAME, 
				0, ebufp);
		sprintf(key_str, "%s|%s|%s|%s|%s", bus_entityp, acc_typep,
			acc_subtypep, ar_regionp, brand_areap);
		kids.str = key_str;

		if (kids.str)
		{
			cm_cache_add_entry(fm_bill_lcr_company_config_ptr, 
				&kids, rates_flistp, &err);
			switch(err) {
			case PIN_ERR_NONE:
				break;
			case PIN_ERR_OP_ALREADY_DONE:
				pinlog(__FILE__, __LINE__, LOG_FLAG_WARNING,
				"fm_bill_pol_config_lcr_company_cache: cache already done: <%s>", 
					kids.str);
                                break;
                        default:
				pinlog(__FILE__, __LINE__, LOG_FLAG_ERROR, 
				"fm_bill_pol_config_lcr_company_cache: error adding cache <%s>",
					kids.str);
                                break;
                        }
                }
	}

	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, 
			"fm_bill_pol_config_lcr_company_cache cache error",
				 ebufp);
	}
	else {
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, 
			"fm_bill_pol_config_lcr_company_cache cache loaded successfully");
	}
	
 cleanup:
        PIN_FLIST_DESTROY_EX(&s_flistp, NULL);
        PIN_FLIST_DESTROY_EX(&r_flistp, NULL);
	return;
}

/**********************************************************************
* Read the config objects /config/dex_lcr_state_level into cm cache 
**********************************************************************/
static void
fm_bill_pol_config_lcr_state_cache(
	pcm_context_t	*ctxp,
	int64		database,		
	pin_errbuf_t	*ebufp)
{
	poid_t		*s_pdp = NULL;
	poid_t		*a_pdp = NULL;
	pin_flist_t	*s_flistp = NULL;
	pin_flist_t	*r_flistp = NULL;
	pin_flist_t	*res_flistp = NULL;
	pin_flist_t	*arg_flistp = NULL;
	pin_flist_t	*rates_flistp = NULL;

	pin_cookie_t	cookie = NULL;
	char		*state_abbrp = NULL;		
	char		*brand_areap = NULL;		
	char		key_str[256]= {""};		
	int32		err = PIN_ERR_NONE;
	int32		cnt = 0;
	int32		s_flags = 256;
	int32		elemid = 0;
	
	s_flistp = PIN_FLIST_CREATE(ebufp);

	s_pdp = PIN_POID_CREATE(database, "/search", -1, ebufp);
	PIN_FLIST_FLD_PUT(s_flistp, PIN_FLD_POID, (void *)s_pdp, ebufp);
	PIN_FLIST_FLD_SET(s_flistp, PIN_FLD_FLAGS, (void *)&s_flags, ebufp);
	PIN_FLIST_FLD_SET(s_flistp, PIN_FLD_TEMPLATE, 
			"select X from /config where F1 = V1 ", ebufp);

	/* setup arguments */
	arg_flistp = PIN_FLIST_ELEM_ADD(s_flistp, PIN_FLD_ARGS, 1, ebufp);

	a_pdp = PIN_POID_CREATE(database, "/config/dex_lcr_state_level", -1, ebufp);
	PIN_FLIST_FLD_PUT(arg_flistp, PIN_FLD_POID, (void *)a_pdp, ebufp);
	PIN_FLIST_ELEM_SET( s_flistp, NULL, PIN_FLD_RESULTS, 0, ebufp);

	PCM_OP(ctxp, PCM_OP_SEARCH, 0, s_flistp, &r_flistp, ebufp);

	if (PIN_ERR_IS_ERR(ebufp)) {
                PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
                        "fm_bill_config_lcr_state_object_from_db: "
			"error loading /config/dex_lcr_state_level object",
                        ebufp);
                goto cleanup;
        }
	cnt = PIN_FLIST_ELEM_COUNT(r_flistp, PIN_FLD_RESULTS, ebufp);

	/*
	 * If there's no data, there's no point initializing it, is there.
	 * Especially since this is optional!
	 */
	if (cnt < 1) {
		goto cleanup;
	}

	/*
	 * This cache doesn't need configuration since the sizes of the
	 * entries are computed
	 */
	fm_bill_lcr_state_config_ptr =
		cm_cache_init("fm_bill_config_lcr_state_object", cnt,
				  cnt * 4096, 8, CM_CACHE_KEY_IDDBSTR, &err);

	if (err != PIN_ERR_NONE) {
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_ERROR, "Error: Couldn't initialize "
		"cache in fm_bill_pol_config_lcr_state_cache");
		pin_set_err(ebufp, PIN_ERRLOC_FM, 
				PIN_ERRCLASS_SYSTEM_DETERMINATE,
				PIN_ERR_NO_MEM, 0, 0, err);
		goto cleanup;
	}

	if (!fm_bill_lcr_state_config_ptr) {
		goto cleanup;
	}
	
	while (res_flistp = 
		PIN_FLIST_ELEM_GET_NEXT(r_flistp, PIN_FLD_RESULTS, &elemid, 1, &cookie, ebufp))
	{
		cm_cache_key_iddbstr_t  kids;
		kids.id  = 0; 
		kids.db  = 0;
		
		rates_flistp = PIN_FLIST_ELEM_GET(res_flistp, PIN_FLD_RATES, 
					PIN_ELEMID_ANY, 0, ebufp);
		state_abbrp = PIN_FLIST_FLD_GET(rates_flistp, PIN_FLD_STATE, 
				0, ebufp);
		brand_areap = PIN_FLIST_FLD_GET(rates_flistp, PIN_FLD_BRAND_NAME, 
				0, ebufp);
		sprintf(key_str, "%s|%s", state_abbrp, brand_areap);
		kids.str = key_str;

		if (kids.str)
		{
			cm_cache_add_entry(fm_bill_lcr_state_config_ptr, 
				&kids, rates_flistp, &err);
			switch(err) {
			case PIN_ERR_NONE:
				break;
			case PIN_ERR_OP_ALREADY_DONE:
				pinlog(__FILE__, __LINE__, LOG_FLAG_WARNING,
				"fm_bill_pol_config_lcr_state_cache: cache already done: <%s>", 
					kids.str);
                                break;
                        default:
				pinlog(__FILE__, __LINE__, LOG_FLAG_ERROR, 
				"fm_bill_pol_config_lcr_state_cache: error adding cache <%s>",
					kids.str);
                                break;
                        }
                }
	}

	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, 
			"fm_bill_pol_config_lcr_state_cache cache error",
				 ebufp);
	}
	else {
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, 
			"fm_bill_pol_config_lcr_state_cache cache loaded successfully");
	}
	
 cleanup:
        PIN_FLIST_DESTROY_EX(&s_flistp, NULL);
        PIN_FLIST_DESTROY_EX(&r_flistp, NULL);
	return;
}
