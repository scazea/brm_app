/*******************************************************************
 *
 * @(#)%Portal Version: fm_rate_pol_post_rating.c:IDCmod7.3PatchInt:1:2007-Mar-26 14:15:36 %
 *
 *      Copyright (c) 1999 - 2007 Oracle. All rights reserved.
 *
 *      This material is the confidential property of Oracle Corporation
 *      or its subsidiaries or licensors and may be used, reproduced, stored
 *      or transmitted only in accordance with a valid Oracle license or
 *      sublicense agreement.
 *
 *******************************************************************/

#ifndef lint
static  char    Sccs_id[] = "@(#)%Portal Version: fm_rate_pol_post_rating.c:IDCmod7.3PatchInt:1:2007-Mar-26 14:15:36 %";
#endif

/*******************************************************************
 * Contains the PCM_OP_RATE_POL_POST_RATING operation. 
 *******************************************************************/

#include <stdio.h> 
#include <string.h> 

#include "pcm.h"
#include "ops/rate.h"
#include "pin_bill.h"
#include "pin_cust.h"
#include "cm_fm.h"
#include "pin_errs.h"
#include "pinlog.h"
#include "pin_act.h"
#include "ops/pymt.h"
#include "dex_flds.h"

#define FILE_SOURCE_ID  "fm_rate_pol_post_rating.c(5)"


/*******************************************************************
 * Routines contained within.
 *******************************************************************/
EXPORT_OP void
op_rate_pol_post_rating(
        cm_nap_connection_t	*connp,
	u_int			opcode,
        u_int			flags,
        pin_flist_t		*i_flistp,
        pin_flist_t		**o_flistpp,
        pin_errbuf_t		*ebufp);

static void
fm_rate_pol_post_rating_get_masked_cc(
		pcm_context_t	*connp,
		pin_flist_t		*i_flistp,
		pin_flist_t		*r_flistp,
		pin_errbuf_t	*ebufp);
/*******************************************************************
 * Routines needed from elsewhere.
 *******************************************************************/

/*******************************************************************
 * Main routine for the PCM_OP_RATE_POL_POST_RATING operation.
 *******************************************************************/
void
op_rate_pol_post_rating(
        cm_nap_connection_t	*connp,
	u_int			opcode,
        u_int			flags,
        pin_flist_t		*i_flistp,
        pin_flist_t		**o_flistpp,
        pin_errbuf_t		*ebufp)
{
	pcm_context_t		*ctxp = connp->dm_ctx;
	pin_flist_t		*r_flistp = NULL;
	void                    *vp = NULL;
	
	pin_decimal_t 		*quantity = NULL;
	poid_t                  *e_pdp = NULL;

	// Start: Dex Customization
	cm_op_info_t    *cmopinfo2_t = connp->coip;
	pin_flist_t		*parent_flistp = NULL;
	// End: Dex Customization

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Insanity check.
	 ***********************************************************/
	if (opcode != PCM_OP_RATE_POL_POST_RATING) {
		pin_set_err(ebufp, PIN_ERRLOC_FM,
			PIN_ERRCLASS_SYSTEM_DETERMINATE,
			PIN_ERR_BAD_OPCODE, 0, 0, opcode);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"op_rate_pol_post_rating opcode error", ebufp);
		return;
	}

	/***********************************************************
	 * Debug: What we got.
	 ***********************************************************/
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"op_rate_pol_post_rating input flist", i_flistp);

	/***********************************************************
	 * Just a straight copy of the input flist. 
	 ***********************************************************/
	r_flistp = PIN_FLIST_COPY(i_flistp, ebufp);
	
	/***********************************************************
	 * Drop the Dropped call specific fields if present.
	 *
	 * - PIN_FLD_DROPPED_CALL_QUANTITY
	 * - PIN_FLD_CALL_TYPE
	 ***********************************************************/
	vp = PIN_FLIST_FLD_GET(r_flistp, PIN_FLD_DROPPED_CALL_QUANTITY,
				1, ebufp);
	if (vp != NULL) {

		PIN_FLIST_FLD_DROP(r_flistp,
			PIN_FLD_DROPPED_CALL_QUANTITY, ebufp);
	}

	vp = PIN_FLIST_FLD_GET(r_flistp, PIN_FLD_CALL_TYPE, 1, ebufp);
	if (vp != NULL) {
		PIN_FLIST_FLD_DROP(r_flistp, PIN_FLD_CALL_TYPE, ebufp);
	}

	/***********************************************************
	 * Drop the EXTENDED_INFO field if present.
	 ***********************************************************/

	vp = PIN_FLIST_SUBSTR_GET(r_flistp, PIN_FLD_EXTENDED_INFO, 1, ebufp);
	if(vp) {
		PIN_FLIST_SUBSTR_DROP(r_flistp, PIN_FLD_EXTENDED_INFO, ebufp);
	}

	/***********************************************************
	 * Results.
	 ***********************************************************/
	if (PIN_ERR_IS_ERR(ebufp)) {

		/***************************************************
		 * Log something and return nothing.
		 ***************************************************/
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"op_rate_pol_post_rating error", ebufp);
		PIN_FLIST_DESTROY_EX(&r_flistp, NULL);
		*o_flistpp = NULL;

	} else {

		/***************************************************
		 * Point the real return flist to the right thing.
		 ***************************************************/
		PIN_ERR_CLEAR_ERR(ebufp);
	
		/**************************************************************************
         * Check if the event poid is of the type /event/session or its sub type, then remove the 
         * PIN_FLD_QUANTITY field from the flist 
         **************************************************************************/

		e_pdp = (poid_t *)PIN_FLIST_FLD_GET(r_flistp, PIN_FLD_POID, 0, ebufp);


		// Start: Dex Customization
		/**************************************************************************
		 * If the event type is of CC, Get the MASKED_CC from parent opoce i.e
		 * PYMT_COLLECT if prsent, Otherwise read from the accounts payinfo
		 * Then set the MASKED_CC on EVENT_NO
		 **************************************************************************/
		if (fm_utils_is_subtype(e_pdp, "/event/billing/payment/cc") == 1) {
			if ( fm_utils_op_is_ancestor(connp->coip, PCM_OP_PYMT_COLLECT)){
				while(cmopinfo2_t != NULL) {
					if(cmopinfo2_t->opcode ==   PCM_OP_PYMT_COLLECT)
					{
						parent_flistp = cmopinfo2_t->in_flistp;
						break;
					}
					cmopinfo2_t = cmopinfo2_t->next;
					parent_flistp = NULL;
				}//end of while
		
			}
			PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "op_rate_pol_post_rating parent input flist", parent_flistp);
			vp = NULL;
			if(parent_flistp != NULL){
				vp = PIN_FLIST_FLD_GET(parent_flistp, DEX_FLD_MASKED_CC, 1, ebufp);
			}
			if((vp) && (strlen(vp) !=0)){
				PIN_FLIST_FLD_SET(r_flistp, PIN_FLD_EVENT_NO, vp, ebufp);
			}else{
				fm_rate_pol_post_rating_get_masked_cc(ctxp, i_flistp, r_flistp, ebufp);
			}
		}
		// End: Dex Customization
	
		if (fm_utils_is_subtype(e_pdp, PIN_OBJ_TYPE_EVENT_SESSION) == 1) {
		
			quantity = (pin_decimal_t *)PIN_FLIST_FLD_TAKE(r_flistp, 
				PIN_FLD_QUANTITY, 1, ebufp);
			pbo_decimal_destroy(&quantity);
		}
		
		if (PIN_ERR_IS_ERR(ebufp)) {
                            
                    /***************************************************
                     * Log something and return nothing.
                     ***************************************************/
                    PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
                            "op_rate_pol_post_rating error while\
				 removing PIN_FLD_QUANTITY field", ebufp);

                    PIN_FLIST_DESTROY_EX(&r_flistp, NULL);
                    *o_flistpp = NULL;                    
                 }
		
	
		*o_flistpp = r_flistp;

		/***************************************************
		 * Debug: What we're sending back.
		 ***************************************************/
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
			"op_rate_pol_post_rating return flist", *o_flistpp);

	}

	return;
}

static void
fm_rate_pol_post_rating_get_masked_cc(
	pcm_context_t	*ctxp,
	pin_flist_t		*i_flistp,
	pin_flist_t		*r_flistp,
	pin_errbuf_t	*ebufp)
{
	pin_flist_t		*read_in_flistp = NULL;
	pin_flist_t		*read_out_flistp = NULL;
	poid_t			*bi_pdp = NULL;
	poid_t			*p_pdp = NULL;
	char			func_name[] = "fm_rate_pol_post_rating_get_masked_cc";
	char			msg[1024];
	char			*name = NULL;
	char			*tok = NULL;

	sprintf(msg, "%s %s", func_name, "Entering input ");
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, i_flistp);

	/*************************************************
 	* Read Billinfo
 	*************************************************/
	bi_pdp = (poid_t *)PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_BILLINFO_OBJ, 0, ebufp);

	read_in_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_SET(read_in_flistp, PIN_FLD_POID, bi_pdp, ebufp);
	PIN_FLIST_FLD_SET(read_in_flistp, PIN_FLD_PAYINFO_OBJ, NULL, ebufp);

	sprintf(msg, "%s %s", func_name, "billinfo read_flds input ");
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, read_in_flistp);

	PCM_OP(ctxp, PCM_OP_READ_FLDS, 0, read_in_flistp, &read_out_flistp, ebufp);
	if (PIN_ERR_IS_ERR(ebufp)){
		sprintf(msg, "%s %s", func_name, "Error in billinfo read_flds");
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		PIN_FLIST_DESTROY_EX(&read_in_flistp, NULL);
		goto CLEANUP;
	}
	PIN_FLIST_DESTROY_EX(&read_in_flistp, NULL);

	sprintf(msg, "%s %s", func_name, "billinfo read_flds output ");
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, read_out_flistp);

	p_pdp = (poid_t *)PIN_FLIST_FLD_TAKE(read_out_flistp, PIN_FLD_PAYINFO_OBJ, 0, ebufp);
	PIN_FLIST_DESTROY_EX(&read_out_flistp, NULL);

	if (PIN_POID_IS_NULL(p_pdp)){
		PIN_POID_DESTROY( p_pdp, NULL );
		goto CLEANUP;
	}

	/*************************************************
 	* Read Payinfo
 	*************************************************/

	read_in_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_PUT(read_in_flistp, PIN_FLD_POID, p_pdp, ebufp);
	PIN_FLIST_FLD_SET(read_in_flistp, PIN_FLD_NAME, NULL, ebufp);

	sprintf(msg, "%s %s", func_name, "payinfo read_flds input ");
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, read_in_flistp);

	PCM_OP(ctxp, PCM_OP_READ_FLDS, 0, read_in_flistp, &read_out_flistp, ebufp);
	if (PIN_ERR_IS_ERR(ebufp)){
		sprintf(msg, "%s %s", func_name, "Error in payinfo read_flds");
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		PIN_FLIST_DESTROY_EX(&read_in_flistp, NULL);
		goto CLEANUP;
	}

	sprintf(msg, "%s %s", func_name, "payinfo read_flds output ");
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, read_out_flistp);
	name = PIN_FLIST_FLD_GET(read_out_flistp, PIN_FLD_NAME, 0, ebufp);
	sprintf(msg, "%s %s %s", func_name, "payinfo name ", name);
	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg );

	/*************************************************
	* The second string is the actual masked_cc
 	* Set the value in return flist 
 	*************************************************/
	tok = (char *)strtok(name, "|");
	tok = (char *)strtok(NULL, "|");
	if(tok != NULL){
		PIN_FLIST_FLD_SET(r_flistp, PIN_FLD_EVENT_NO, (void *)tok, ebufp);
	}

	PIN_FLIST_DESTROY_EX(&read_in_flistp, NULL);
	PIN_FLIST_DESTROY_EX(&read_out_flistp, NULL);

CLEANUP:
	sprintf(msg, "%s %s", func_name, "Exiting ");
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, r_flistp);
}
