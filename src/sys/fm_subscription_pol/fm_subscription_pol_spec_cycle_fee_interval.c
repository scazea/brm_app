/**********************************************************************
 *
 * Copyright (c) 1999, 2009, Oracle and/or its affiliates.All rights reserved. 
 *      
 *      This material is the confidential property of Oracle Corporation
 *      or its licensors and may be used, reproduced, stored or transmitted
 *      only in accordance with a valid Oracle license or sublicense agreement.
 *
 **********************************************************************/
 /*********************************************************************
 *  Maintentance Log:
 *
 *   Date: 20171011
 *   Author: Ebillsoft
 *   Identifier: N/A
 *   Notes: Accelerate charges for small contracts
 **********************************************************************/
/*******************************************************************************
 * File History
 *
 *  DATE    |  CHANGE                                           |  Author
 ***********|***************************************************|****************
 |10/11/2017|Accelerate charges for small contracts		| Rama Puppala
 |**********|***************************************************|****************
 ********************************************************************************/

#ifndef lint
static char Sccs_id[] = "@(#)$Id: fm_subscription_pol_spec_cycle_fee_interval.c /cgbubrm_7.5.0.portalbase/1 2015/11/27 06:17:47 nishahan Exp $";
#endif

/*******************************************************************
 * Contains the PCM_OP_SUBSCRIPTION_POL_SPEC_CYCLE_FEE_INTERVAL operation. 
 *******************************************************************/

#include <stdio.h> 
#include <string.h> 
 
#include "pcm.h"
#include "ops/bill.h"
#include "ops/subscription.h"
#include "ops/price.h"
#include "pin_bill.h"
#include "pin_rate.h"
#include "cm_fm.h"
#include "pin_errs.h"
#include "pinlog.h"

#define TCS_RES_ID 1000200
#define TCM_RES_ID 1000097 

PIN_IMPORT pin_decimal_t        *min_contract_amtp;

/*******************************************************************
 * Routines contained within.
 *******************************************************************/
EXPORT_OP void
op_subscription_pol_spec_cycle_fee_interval(
	cm_nap_connection_t	*connp,
	int32			opcode,
	int32			flags,
	pin_flist_t		*i_flistp,
	pin_flist_t		**r_flistpp,
	pin_errbuf_t		*ebufp);

static int64 
fm_subscription_pol_get_tcm_small_contract(
	pcm_context_t		*ctxp,
	poid_t			*i_pdp,
	pin_errbuf_t		*ebufp);

/*******************************************************************
 * Main routine for the PCM_OP_SUBSCRIPTION_POL_SPEC_CYCLE_FEE_INTERVAL operation.
 *******************************************************************/
void
op_subscription_pol_spec_cycle_fee_interval(
	cm_nap_connection_t	*connp,
	int32			opcode,
	int32			flags,
	pin_flist_t		*i_flistp,
	pin_flist_t		**r_flistpp,
	pin_errbuf_t		*ebufp)
{
	void			*vp = NULL;
	pcm_context_t		*ctxp = connp->dm_ctx;
	poid_t			*prod_pdp = NULL;
	struct tm		*timeeff = NULL;
	int64			total_contract_months = -1; 
	time_t			charged_to_t = 0;

	*r_flistpp = NULL;
	if (PIN_ERR_IS_ERR(ebufp)) {
		return;
	}
	PIN_ERR_CLEAR_ERR(ebufp);

	/*
	 * Insanity check.
	 */
	if (opcode != PCM_OP_SUBSCRIPTION_POL_SPEC_CYCLE_FEE_INTERVAL) {
		pin_set_err(ebufp, PIN_ERRLOC_FM,
			PIN_ERRCLASS_SYSTEM_DETERMINATE,
			PIN_ERR_BAD_OPCODE, 0, 0, opcode);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"op_subscription_pol_spec_cycle_fee_interval bad opcode error", 
			ebufp);
		return;
	}

	/*
	 * Debug: What we got.
	 */
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"op_subscription_pol_spec_cycle_fee_interval input flist", i_flistp);

	prod_pdp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_PRODUCT_OBJ, 1, ebufp);
	if (prod_pdp)
	{
		total_contract_months = fm_subscription_pol_get_tcm_small_contract(ctxp, prod_pdp, ebufp);
	}

	/*
	 * Prepare return flist.
	 */
	*r_flistpp = PIN_FLIST_CREATE(ebufp);
	vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_POID, 0, ebufp);
	PIN_FLIST_FLD_SET(*r_flistpp, PIN_FLD_POID, vp, ebufp);

	vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_CHARGED_FROM_T,
		0, ebufp);
	if (vp && total_contract_months > 0)
	{
		timeeff = localtime((time_t *)vp);
		timeeff->tm_mon = timeeff->tm_mon + total_contract_months;
		charged_to_t = mktime (timeeff);
	} 

	PIN_FLIST_FLD_SET(*r_flistpp, PIN_FLD_CHARGED_FROM_T, 
		vp, ebufp);
	vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_CHARGED_TO_T,
		0, ebufp);

	if (charged_to_t > 0)
	{
		PIN_FLIST_FLD_SET(*r_flistpp, PIN_FLD_CHARGED_TO_T, 
			&charged_to_t, ebufp);

		PIN_FLIST_FLD_SET(*r_flistpp, PIN_FLD_CYCLE_FEE_END_T, 
			&charged_to_t, ebufp);
	}
	else
	{
		PIN_FLIST_FLD_SET(*r_flistpp, PIN_FLD_CHARGED_TO_T, 
			vp, ebufp);
	}
	vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_SCALE, 1, ebufp);
	if (vp) {
        	PIN_FLIST_FLD_SET(*r_flistpp, PIN_FLD_SCALE,
                				vp, ebufp);
	}
        /*
	 * Error?
	 */
	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
		"op_subscription_pol_spec_cycle_fee_interval error", ebufp);
	} else {

		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"op_subscription_pol_spec_cycle_fee_interval output flist", *r_flistpp);
	}
	return;
}

/*******************************************************************
 * fm_subscription_pol_is_small_contract()
 *
 * This function checks this product is s small contract product and
 * gets total contract months from product configuration
 *
 * *****************************************************************/
static int64  
fm_subscription_pol_get_tcm_small_contract(
	pcm_context_t		*ctxp,
	poid_t			*i_pdp,
	pin_errbuf_t		*ebufp)
{
	pin_flist_t		*prod_iflistp = NULL;
	pin_flist_t		*prod_oflistp = NULL;
	pin_flist_t		*tmp_flistp = NULL;	
	pin_flist_t		*rp_flistp = NULL;	
	pin_flist_t		*rate_flistp = NULL;	
	pin_flist_t		*tier_flistp = NULL;	
	pin_flist_t		*bi_flistp = NULL;	
	pin_cookie_t		cookie = NULL;
	pin_cookie_t		i_cookie = NULL;
	pin_decimal_t		*tcs_amtp = NULL;
	pin_decimal_t		*tcm_amtp = NULL;
	char			*event_typep = NULL;
	int64			contract_months = -1;
	int32			elem_id = 0;
	int32			i_elem_id = 0;
	int32			res_id = 0;
	
	if (PIN_ERR_IS_ERR(ebufp))
		return contract_months;
	PIN_ERR_CLEAR_ERR(ebufp);

	prod_iflistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_SET(prod_iflistp, PIN_FLD_POID, i_pdp, ebufp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, 
		"fm_subscription_pol_get_tcm_small_contract prod_info in flist", prod_iflistp);

	PCM_OP(ctxp, PCM_OP_PRICE_GET_PRODUCT_INFO, 0, prod_iflistp, &prod_oflistp, ebufp);

	PIN_FLIST_DESTROY_EX(&prod_iflistp, NULL);
	if (PIN_ERR_IS_ERR(ebufp))
	{
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, "Error in getting prod details", ebufp);
		PIN_FLIST_DESTROY_EX(&prod_oflistp, NULL);
		return contract_months;
	}

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_subscription_pol_get_tcm_small_contract prod_info  out flistp", prod_oflistp);

	tmp_flistp = PIN_FLIST_ELEM_GET(prod_oflistp, PIN_FLD_PRODUCTS, 0, 0, ebufp);
	while((rp_flistp = PIN_FLIST_ELEM_GET_NEXT(tmp_flistp, PIN_FLD_RATE_PLANS,
		&elem_id, 1, &cookie, ebufp)) != NULL )
	{
		event_typep = PIN_FLIST_FLD_GET(rp_flistp, PIN_FLD_EVENT_TYPE, 0, ebufp);
		if (strstr(event_typep, PIN_OBJ_TYPE_EVENT_PRODUCT_FEE_PURCHASE) == NULL)
		{
			continue;	
		}
		rate_flistp =PIN_FLIST_ELEM_GET(rp_flistp, PIN_FLD_RATES, 0, 0, ebufp); 
		tier_flistp =PIN_FLIST_ELEM_GET(rate_flistp, PIN_FLD_QUANTITY_TIERS, 0, 0, ebufp); 

		/*********************************************************
 		 * Capture contract resource amounts 
 		 * *******************************************************/ 	
		i_elem_id = 0;
		i_cookie = NULL;	
		while((bi_flistp = PIN_FLIST_ELEM_GET_NEXT(tier_flistp, PIN_FLD_BAL_IMPACTS,
			&i_elem_id, 1, &i_cookie, ebufp)) != NULL )
		{
			res_id = *(int32 *)PIN_FLIST_FLD_GET(bi_flistp, PIN_FLD_ELEMENT_ID, 0, ebufp);
			if (res_id == TCM_RES_ID)
			{
				tcm_amtp = PIN_FLIST_FLD_GET(bi_flistp, PIN_FLD_SCALED_AMOUNT,
							 0, ebufp);
			}
			else if (res_id == TCS_RES_ID)
			{
				tcs_amtp = PIN_FLIST_FLD_GET(bi_flistp, PIN_FLD_SCALED_AMOUNT,
							 0, ebufp);
			}
		}
	}

	if (tcs_amtp && pbo_decimal_compare(tcs_amtp, min_contract_amtp, ebufp) <= 0)
	{
		pbo_decimal_negate_assign(tcm_amtp, ebufp);
		contract_months = pin_decimal_to_int64(tcm_amtp, ebufp);
	}

	
	PIN_FLIST_DESTROY_EX(&prod_oflistp, NULL);
	return contract_months;
}
