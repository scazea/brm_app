/*******************************************************************************
* Copyright (c) 2008, 2012, Oracle and/or its affiliates. All rights reserved. 
 *
 *      This material is the confidential property of Oracle Corporation
 *      or its licensors and may be used, reproduced, stored or transmitted
 *      only in accordance with a valid Oracle license or sublicense agreement.
 ********************************************************************************/

#ifndef lint
static const char Sccs_id[] = "@(#)$Id: fm_dex_collections_config.c /cgbubrm_1.0.0.collections/6 2012/01/17 08:53:14 praghura Exp $";
#endif

#include <stdio.h>
#include <string.h>

#include <pcm.h>
#include <pinlog.h>

#include "cm_fm.h"
#include "ops/collections.h"

#include "dex_ops.h"

#ifdef MSDOS
__declspec(dllexport) void * fm_dex_collections_config_func();
#endif

/*******************************************************************
 * NOTE THAT THE DISPATCH ENTRIES ARE COMMENTED. WHEN YOU OVERRIDE
 * AN IMPLEMENTATION, UNCOMMENT THE LINE BELOW THAT MATCHES THE
 * OPCODE FOR WHICH YOU HAVE PROVIDED AN ALTERNATE IMPLEMENTATION.
 *******************************************************************/

struct cm_fm_config fm_dex_collections_config[] = {
	/* opcode as a int32, function name (as a string) */

 	{ DEX_OP_COLLECTIONS_HOLD, 
		"op_dex_collections_hold" }, 
	{ 0,	(char *)0 }
};

#ifdef MSDOS
void *
fm_dex_collections_config_func()
{
  return ((void *) (fm_dex_collections_config));
}
#endif
