/*
 *      Copyright (c) 2001-2006 Oracle. All rights reserved.
 *
 *      This material is the confidential property of Oracle Corporation
 *      or its licensors and may be used, reproduced, stored or transmitted
 *      only in accordance with a valid Oracle license or sublicense agreement.
 */

#ifndef lint
static const char Sccs_id[] = "@(#) %full_filespec: fm_dex_collections_hold.c~2:csrc:1 %";
#endif

#include <stdio.h>
#include <string.h>

#include <pcm.h>
#include <pinlog.h>

#include "cm_fm.h"
#include "cm_cache.h"
#include "ops/collections.h"
#include "fm_utils.h"
#include "pin_cust.h"
#include "fm_collections.h"

#include "dex_ops.h"
#include "dex_flds.h"

#ifdef MSDOS
#include <WINDOWS.h>
#endif


/*******************************************************************
 * Routines defined here.
 *******************************************************************/

EXPORT_OP void
op_dex_collections_hold(
	cm_nap_connection_t	*connp, 
	int32			opcode, 
	int32			flags, 
	pin_flist_t		*in_flistp, 
	pin_flist_t		**ret_flistpp, 
	pin_errbuf_t	*ebufp);

static void
fm_dex_collections_hold(
	pcm_context_t	*ctxp, 
	int32			flags, 
	pin_flist_t		*in_flistp, 
	pin_flist_t		**ret_flistpp, 
	pin_errbuf_t	*ebufp);

static void
fm_dex_collections_hold_set_action_status(
	pcm_context_t	*ctxp, 
	int32			flags, 
	pin_flist_t		*in_flistp, 
	pin_errbuf_t	*ebufp);

static void
fm_dex_collections_hold_write_flds(
	pcm_context_t	*ctxp, 
	poid_t			*i_pdp, 
	int32			fldnum,
	int32			status,
	char			*descr,
	pin_errbuf_t	*ebufp);
/*******************************************************************
 * Variables/functions defined elsewhere
 *******************************************************************/

/*******************************************************************
 * Main routine for the PCM_OP_COLLECTIONS_POL_SELECT_PROFILE 
 *******************************************************************/
void
op_dex_collections_hold(
	cm_nap_connection_t	*connp, 
	int32			opcode, 
	int32			flags, 
	pin_flist_t		*in_flistp, 
	pin_flist_t		**ret_flistpp, 
	pin_errbuf_t		*ebufp)
{
	pcm_context_t		*ctxp = connp->dm_ctx;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Insanity check.
	 ***********************************************************/
	if (opcode != DEX_OP_COLLECTIONS_HOLD) {
		pin_set_err(ebufp, PIN_ERRLOC_FM,
			PIN_ERRCLASS_SYSTEM_DETERMINATE,
			PIN_ERR_BAD_OPCODE, 0, 0, opcode);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"op_dex_collections_hold opcode error", 
			ebufp);
		return;
	}

	/***********************************************************
	 * Debug: What we got.
	 ***********************************************************/

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"op_dex_collections_hold input flist", in_flistp);

	/***********************************************************
	 * Do the actual op in a sub. Copy it since we may need to
	 * replace it later.
	 ***********************************************************/

	fm_dex_collections_hold(ctxp, flags, in_flistp, 
			ret_flistpp, ebufp);

	/***********************************************************
	 * Error?
	 ***********************************************************/
	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"op_dex_collections_hold error", ebufp);
	} else {
		/***************************************************
		 * Debug: What we're sending back.
		 ***************************************************/
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
			"op_dex_collections_hold return flist", 
			*ret_flistpp);
	}
	return;
}

/*******************************************************************
 * fm_dex_collections_hold()
 *
 * This function calls corresponding action opcode and updates status.
 *
 *******************************************************************/
static void
fm_dex_collections_hold(
	pcm_context_t		*ctxp, 
	int32			flags, 
	pin_flist_t		*in_flistp, 
	pin_flist_t		**ret_flistpp, 
	pin_errbuf_t		*ebufp)
{
	poid_t			*a_pdp = NULL;
	poid_t			*b_pdp = NULL;
	poid_t			*p_pdp = NULL;
	pin_flist_t		*billinfo_flistp = NULL; 
	pin_flist_t		*a_flistp = NULL; 
	pin_flist_t		*prof_in_flistp = NULL; 
	pin_flist_t		*prof_out_flistp = NULL; 
	int32			collectionhold = 0;
	int32			db_collectionhold = 0;
	int32			result = PIN_BOOLEAN_FALSE;
	char			msg[1024];
	char			func_name[] = "fm_dex_collections_hold";

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	sprintf(msg, "%s %s", func_name, "input flist");
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, in_flistp);

	// Get the input values
	a_pdp = (poid_t *)PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_POID, 0, ebufp);
	b_pdp = (poid_t *)PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_BILLINFO_OBJ, 0, ebufp);
	p_pdp = (poid_t *)PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_PROFILE_OBJ, 0, ebufp);
	collectionhold = *(int32 *)PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_BILLINFO_OBJ, 0, ebufp);

	// Prepare return flist
	*ret_flistpp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_SET(*ret_flistpp, PIN_FLD_POID, a_pdp, ebufp);

	PIN_FLIST_FLD_SET(*ret_flistpp, PIN_FLD_BILLINFO_OBJ, b_pdp, ebufp);


	/*************************************************
	 * Read Profile
	 *************************************************/
	prof_in_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_SET(prof_in_flistp, PIN_FLD_POID, p_pdp, ebufp);

	a_flistp = PIN_FLIST_ELEM_ADD( prof_in_flistp, PIN_FLD_COLLECTIONS_PARAMS, 0, ebufp);
	PIN_FLIST_FLD_SET( a_flistp, DEX_FLD_COLLECTION_HOLD, NULL, ebufp);

	PCM_OP(ctxp, PCM_OP_READ_FLDS, 0, prof_in_flistp, &prof_out_flistp, ebufp);
	if (PIN_ERR_IS_ERR(ebufp)){
		sprintf(msg, "%s %s", func_name, "Error in profile read_flds");
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		PIN_FLIST_DESTROY_EX(&prof_in_flistp, NULL);
		PIN_FLIST_DESTROY_EX(ret_flistpp, NULL);
		goto CLEANUP;
	}
	PIN_FLIST_DESTROY_EX(&prof_in_flistp, NULL);
	if(prof_out_flistp == (pin_flist_t *)NULL){
		PIN_FLIST_FLD_SET(*ret_flistpp, PIN_FLD_RESULT, &result, ebufp);
		goto CLEANUP;
	}
	a_flistp = PIN_FLIST_ELEM_GET(prof_out_flistp, PIN_FLD_COLLECTIONS_PARAMS, 0, 1, ebufp);
	if(a_flistp != (pin_flist_t *)NULL){
		db_collectionhold = *(int32 *) PIN_FLIST_FLD_GET(a_flistp, DEX_FLD_COLLECTION_HOLD, 0, ebufp);
	}

	// input hold value is same as db value do nothing
	if(collectionhold == db_collectionhold){
		sprintf(msg, "%s %s", func_name, "input value is same as db value, no update required ");
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_WARNING, msg);
		result = PIN_BOOLEAN_TRUE;
		PIN_FLIST_FLD_SET(*ret_flistpp, PIN_FLD_RESULT, &result, ebufp);
		goto CLEANUP;
	}
	PIN_FLIST_DESTROY_EX(&prof_out_flistp, NULL);

	/*************************************************
	 * Do write_flds to update the profile value
	 *************************************************/
	prof_in_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_SET(prof_in_flistp, PIN_FLD_POID, p_pdp, ebufp);

	a_flistp = PIN_FLIST_ELEM_ADD( prof_in_flistp, PIN_FLD_COLLECTIONS_PARAMS, 0, ebufp);
	PIN_FLIST_FLD_SET( a_flistp, DEX_FLD_COLLECTION_HOLD, &collectionhold, ebufp);

	PCM_OP(ctxp, PCM_OP_WRITE_FLDS, 0, prof_in_flistp, &prof_out_flistp, ebufp);
	if (PIN_ERR_IS_ERR(ebufp)){
		sprintf(msg, "%s %s", func_name, "Error in profile writ_flds");
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		PIN_FLIST_DESTROY_EX(&prof_in_flistp, NULL);
		PIN_FLIST_DESTROY_EX(ret_flistpp, NULL);
		goto CLEANUP;
	}
	PIN_FLIST_DESTROY_EX(&prof_in_flistp, NULL);
	PIN_FLIST_DESTROY_EX(&prof_out_flistp, NULL);
	
	/*************************************************
	 * Collection hold, call the function to set the collection 
	 * action status to no execute
	 *************************************************/
	if(collectionhold == 1){
		fm_dex_collections_hold_set_action_status(ctxp, flags, in_flistp, ebufp);

	}
	if (PIN_ERR_IS_ERR(ebufp)){
		sprintf(msg, "%s %s", func_name, "Error in scenario manipulation");
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		PIN_FLIST_DESTROY_EX(ret_flistpp, NULL);
		goto CLEANUP;
	}
	result = PIN_BOOLEAN_TRUE;
	PIN_FLIST_FLD_SET(*ret_flistpp, PIN_FLD_RESULT, &result, ebufp);

CLEANUP:
		sprintf(msg, "%s %s", func_name, "return flist ");
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, *ret_flistpp);

}

/*******************************************************************
 * fm_dex_collections_hold_set_action_status()
 *
 * This function searchs for corresponding action opcode and 
 * updates status.
 *
 *******************************************************************/
static void
fm_dex_collections_hold_set_action_status(
	pcm_context_t		*ctxp, 
	int32			flags, 
	pin_flist_t		*in_flistp, 
	pin_errbuf_t	*ebufp)
{
	poid_t			*a_pdp = NULL;
	poid_t			*b_pdp = NULL;
	poid_t			*s_pdp = NULL;
	poid_t			*srch_pdp = NULL;
	poid_t			*scenario_pdp = NULL;
	poid_t			*schedule_pdp = NULL;
	pin_flist_t		*srch_in_flistp = NULL; 
	pin_flist_t		*srch_out_flistp = NULL; 
	pin_flist_t		*read_in_flistp = NULL; 
	pin_flist_t		*read_out_flistp = NULL; 
	pin_flist_t		*billinfo_in_flistp = NULL; 
	pin_flist_t		*billinfo_out_flistp = NULL; 
	pin_flist_t		*a_flistp = NULL; 
	int32			status = PIN_ACTION_PENDING;
	int32			sflag = 256;
	int32			rec_id = 0;
	pin_cookie_t    cookie = NULL;
	char			template[] = "select X from /collections_action where F1 = V1 and F2 = V2 and F3 = V3 and F4 = V4 ";
	char			msg[1024];
	char			func_name[] = "fm_dex_collections_hold_set_action_status";
	char			descr[1024];
	void			*vp = NULL;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	sprintf(msg, "%s %s", func_name, "input flist");
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, in_flistp);
	
	// Get the input values
	a_pdp = (poid_t *)PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_POID, 0, ebufp);
	b_pdp = (poid_t *)PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_BILLINFO_OBJ, 0, ebufp);


	/*************************************************
	 * Read Billinfo
	 *************************************************/
	billinfo_in_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_SET(billinfo_in_flistp, PIN_FLD_POID, b_pdp, ebufp);
	PIN_FLIST_FLD_SET(billinfo_in_flistp, PIN_FLD_SCENARIO_OBJ, NULL, ebufp);

	PCM_OP(ctxp, PCM_OP_READ_FLDS, 0, billinfo_in_flistp, &billinfo_out_flistp, ebufp);
	if (PIN_ERR_IS_ERR(ebufp)){
		sprintf(msg, "%s %s", func_name, "Error in billinfo read_flds");
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		PIN_FLIST_DESTROY_EX(&billinfo_in_flistp, NULL);
		goto CLEANUP;
	}
	PIN_FLIST_DESTROY_EX(&billinfo_in_flistp, NULL);

	if(billinfo_out_flistp == (pin_flist_t *)NULL){
		sprintf(msg, "%s %s", func_name, "no billinfo ");
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_ERROR, msg);
		goto CLEANUP;
	}

	s_pdp = (poid_t *)PIN_FLIST_FLD_GET(billinfo_out_flistp, PIN_FLD_SCENARIO_OBJ, 1, ebufp);
	if(PIN_POID_IS_NULL(s_pdp)) {
		PIN_FLIST_DESTROY_EX(&billinfo_out_flistp, NULL);
		sprintf(msg, "%s %s", func_name, "scenario object not found, no action required ");
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_WARNING, msg);
		goto CLEANUP;
	}

	/*************************************************
	 * Search on secario actions to get the actions and
	 * schedule actions if any
	 *************************************************/

	srch_in_flistp = PIN_FLIST_CREATE(ebufp);
	srch_pdp = PIN_POID_CREATE(PIN_POID_GET_DB(a_pdp), "/search/pin", 0, ebufp);
	PIN_FLIST_FLD_PUT(srch_in_flistp, PIN_FLD_POID, (void *)srch_pdp, ebufp);
	PIN_FLIST_FLD_SET(srch_in_flistp, PIN_FLD_FLAGS, (void *)&sflag, ebufp);

	PIN_FLIST_FLD_SET(srch_in_flistp, PIN_FLD_TEMPLATE, (void *)template, ebufp);

	a_flistp = PIN_FLIST_ELEM_ADD(srch_in_flistp, PIN_FLD_ARGS, 1, ebufp);
	PIN_FLIST_FLD_SET(a_flistp, PIN_FLD_ACCOUNT_OBJ, a_pdp, ebufp);

	a_flistp = PIN_FLIST_ELEM_ADD(srch_in_flistp, PIN_FLD_ARGS, 2, ebufp);
	PIN_FLIST_FLD_SET(a_flistp, PIN_FLD_BILLINFO_OBJ, b_pdp, ebufp);

	a_flistp = PIN_FLIST_ELEM_ADD(srch_in_flistp, PIN_FLD_ARGS, 3, ebufp);
	PIN_FLIST_FLD_SET(a_flistp, PIN_FLD_SCENARIO_OBJ, s_pdp, ebufp);

	a_flistp = PIN_FLIST_ELEM_ADD(srch_in_flistp, PIN_FLD_ARGS, 4, ebufp);
	PIN_FLIST_FLD_SET(a_flistp, PIN_FLD_STATUS, &status, ebufp);

	a_flistp = PIN_FLIST_ELEM_ADD(srch_in_flistp, PIN_FLD_RESULTS, 0, ebufp);
	PIN_FLIST_FLD_SET(a_flistp, PIN_FLD_POID, NULL, ebufp);
	PIN_FLIST_FLD_SET(a_flistp, PIN_FLD_SCHEDULE_OBJ, NULL, ebufp);

	PCM_OP(ctxp, PCM_OP_SEARCH, 0, srch_in_flistp, &srch_out_flistp, ebufp);
	if (PIN_ERR_IS_ERR(ebufp)){
		sprintf(msg, "%s %s", func_name, "Error in scenario action search");
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		PIN_FLIST_DESTROY_EX(&srch_in_flistp, NULL);
		PIN_FLIST_DESTROY_EX(&billinfo_out_flistp, NULL);
		goto CLEANUP;
	}

	//loop through search results and update status
	while ( (a_flistp = PIN_FLIST_ELEM_GET_NEXT (srch_out_flistp,
    		PIN_FLD_RESULTS, &rec_id, 1,&cookie, ebufp)) != (pin_flist_t *)NULL ) {

		scenario_pdp = (poid_t *)PIN_FLIST_FLD_GET(a_flistp, PIN_FLD_POID, 1, ebufp);
		if(!PIN_POID_IS_NULL(scenario_pdp)) {
			status = PIN_ACTION_NO_EXECUTE;
			fm_dex_collections_hold_write_flds( ctxp, scenario_pdp, PIN_FLD_STATUS, status, NULL, ebufp);
		}
		
		schedule_pdp = (poid_t *)PIN_FLIST_FLD_GET(a_flistp, PIN_FLD_SCHEDULE_OBJ, 1, ebufp);
		if(!PIN_POID_IS_NULL(schedule_pdp)) {
			status = PIN_SCHEDULE_STATUS_ERROR;

			read_in_flistp = PIN_FLIST_CREATE(ebufp);
			PIN_FLIST_FLD_SET(read_in_flistp, PIN_FLD_POID, (void *)schedule_pdp, ebufp);
			PIN_FLIST_FLD_SET(read_in_flistp, PIN_FLD_DESCR, (void *)NULL, ebufp);
			PCM_OP(ctxp, PCM_OP_READ_FLDS, 0, read_in_flistp, &read_out_flistp, ebufp);
			if (PIN_ERR_IS_ERR(ebufp)){
				sprintf(msg, "%s %s", func_name, "Error in schedule read_flds ");
				PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
				PIN_FLIST_DESTROY_EX(&read_in_flistp, NULL);
				goto CLEANUP;
			}
			vp = PIN_FLIST_FLD_GET(read_out_flistp, PIN_FLD_DESCR, 1, ebufp);

			memset(descr, 0, 1024);
			if(vp && strlen(vp) != 0){
				sprintf(descr, "%s-%s", (char *)vp, "COLLECTION_HOLD");
			}else{
				sprintf(descr, "%s", "COLLECTION_HOLD");
			}
			fm_dex_collections_hold_write_flds( ctxp, schedule_pdp, PIN_FLD_STATUS, status, descr, ebufp);

			PIN_FLIST_DESTROY_EX(&read_in_flistp, NULL);
			PIN_FLIST_DESTROY_EX(&read_out_flistp, NULL);
		}
	}
	
CLEANUP:
		PIN_FLIST_DESTROY_EX(&billinfo_out_flistp, NULL);
		PIN_FLIST_DESTROY_EX(&srch_in_flistp, NULL);
		PIN_FLIST_DESTROY_EX(&srch_out_flistp, NULL);
		sprintf(msg, "%s %s", func_name, "returning ");
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_DEBUG, msg);
}

/*******************************************************************
 * fm_dex_collections_hold_set_action_status()
 *
 * This function searchs for corresponding action opcode and 
 * updates status.
 *
 *******************************************************************/
static void
fm_dex_collections_hold_write_flds(
	pcm_context_t		*ctxp, 
	poid_t				*i_pdp, 
	int32				fldnum,
	int32				status,
	char				*descr,
	pin_errbuf_t	*ebufp)
{
	pin_flist_t			*write_in_flistp = NULL;
	pin_flist_t			*write_out_flistp = NULL;
	char				func_name[] = "fm_dex_collections_hold_write_flds";
	char				msg[1024];

	/*************************************************
	 * WRITE_FLDS
	 *************************************************/
	write_in_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_SET(write_in_flistp, PIN_FLD_POID, i_pdp, ebufp);
	PIN_FLIST_FLD_SET(write_in_flistp, fldnum, &status, ebufp);
	if(descr != NULL){
		PIN_FLIST_FLD_SET(write_in_flistp, PIN_FLD_DESCR, (void *)descr, ebufp);
	}

	sprintf(msg, "%s %s", func_name, "write_flds input");
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, write_in_flistp);

	PCM_OP(ctxp, PCM_OP_WRITE_FLDS, 0, write_in_flistp, &write_out_flistp, ebufp);
	if (PIN_ERR_IS_ERR(ebufp)){
		sprintf(msg, "%s %s", func_name, "Error in write_flds");
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, msg, ebufp);
		PIN_FLIST_DESTROY_EX(&write_in_flistp, NULL);
	}
	sprintf(msg, "%s %s", func_name, "write_flds output");
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, msg, write_out_flistp);

	PIN_FLIST_DESTROY_EX(&write_in_flistp, NULL);
	PIN_FLIST_DESTROY_EX(&write_out_flistp, NULL);
}
