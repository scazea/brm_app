/*
 * This file contains the opcode definitions for the EBS custom PCM APIs
 */

#ifndef _EBS_PCM_OPS_H
#define _EBS_PCM_OPS_H

/*
 * Define the opcodes.
 *
 *
 * do NOT! change these values!
 *
 *
 * NOTE: the opcode number space is divided into space reserved for
 *       definition by Portal and space reserved for customer definition.
 *       The spaces are allocated as follows:
 *
 *              opcode number           reserved for
 *              ------------------------------------
 *              0 - 9999                Portal Only
 *              10,000 - 999,999        Customer Use
 *              1,000,000 - 9,999,999   Portal Only
 *              10,000,000+             Customer Use
 *
 * DO NOT define custom opcodes in the ranges reserved for Portal.
 *
 * DO NOT ADD ANY defines OTHER THAN OPCODES IN THIS FILE. THIS FILE IS USED
 * TO AUTOMATICALLY GENERATE OPCODE MAPPINGS.
 *
 */

#define EBS_OP_CREATE_RESOURCE		10001

#endif
