#ifndef _CUST_FLDS_H
#define _CUST_FLDS_H

/**
 * Definition of EBS custom fields
 *
 * do NOT CHANGE the values - NEVER replace, ALWAYS ADD to end of a
 * functional block.
 *
 * NOTE: the field number space is divided into space reserved for
 *       definition by Portal and space reserved for customer definition.
 *       The spaces are allocated as follows:
 *
 *             field number            reserved for
 *             ------------------------------------
 *             0 - 9999                Portal Only
 *             10,000 - 999,999        Customer Use
 *             1,000,000 - 9,999,999   Portal Only
 *             10,000,000+             Customer Use
 *
 * DO NOT define custom fields in the ranges reserved for Portal.
 */

// Sample custom field definition
// #define EBS_FLD_TEST_FIELD		PIN_MAKE_FLD(PIN_FLDT_STR, 20001)

#endif
