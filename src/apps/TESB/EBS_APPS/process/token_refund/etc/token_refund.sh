#!/bin/bash
die () {
    echo "$0:" "$@" >&2;  exit 1;
}

chmod 755 token_refund/token_refund_run.sh

OPTS=`getopt -o h -l "pay_type:,vendor:,active,inactive,close,verbose,test,help" -- "$@"`
if [ $? != 0 ] 
then
    exit 1
fi

status_set=false
eval set -- "$OPTS"
usage="usage: $0 --pay_type <number> --vendor <vendor_name> [--active|--inactive|--close] [--verbose] [--test]"

while true ; do
    case "$1" in    	    
	--pay_type) pay_type=$2; shift 2;;
	--vendor) vendor=$2; shift 2;;
	--active) $status_set && die "Status already given $status"
		status_set=true; status=10100; shift;;
	--inactive) $status_set && die "Status already given $status"
		status_set=true; status=10102; shift;;
	--close) $status_set && die "Status already given $status"
		status_set=true; status=10103; shift;;		
	--verbose) echo "Verbose on"; ver=true; shift;;
	--test) echo "test on"; test=true; shift;;
	-h|--help)
	    echo $usage >&2
	    exit 2
	    ;;
        --) shift; break;;
    esac
done

([[ $pay_type == "" ]] || [[ $vendor == "" ]] || [[ $status_set != "true" ]]) && die "Missing options. $usage"

#echo "Paytype  $pay_type"
#echo "Vendor $vendor"
#echo "Status $status"
#echo "Verbose $ver"
#echo "Test $test"

if [[ $ver ]]
then 
	ctx_ver="--context_param verbose=true"
fi
if [[ $test ]]
then
	ctx_test="--context_param test=true"
fi

token_refund/token_refund_run.sh --context_param pay_type=$pay_type --context_param vendor=$vendor --context_param status=$status $ctx_ver $ctx_test

