create or replace TYPE item_row AS OBJECT (
       obj_id0 NUMBER(38,0),
       item_obj_id0 NUMBER(38,0)
    )
/
create or replace TYPE item_tab AS TABLE OF item_row
/
create or replace FUNCTION get_trans_items(item_poid NUMBER) RETURN item_tab IS
     l_result item_tab;
  BEGIN
     SELECT item_row(obj_id0, item_obj_id0)
       BULK COLLECT INTO l_result
      FROM (SELECT obj_id0, to_number(REGEXP_SUBSTR(list_of_items,'.* /item.+ ([0-9]+) .*', 1, LEVEL,NULL,1)) as item_obj_id0
            FROM (
                select eib.obj_id0, utl_raw.cast_to_varchar2(eib.buffer_buf) list_of_items
                from event_item_transfer_buf_t eib
                where eib.obj_id0 = item_poid
              )
            CONNECT BY LEVEL <= REGEXP_COUNT(list_of_items, '/item'));
     RETURN l_result;
END get_trans_items;
/
quit
