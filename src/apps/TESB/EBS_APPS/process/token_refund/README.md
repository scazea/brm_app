==token_refund .sh tweak==
In order for the script to log and connect to a BRM server the following files need to be copied and configured into the token_refund/ folder where the token_refund_run.sh is located:
	* commons-logging.properties
	* Infranet.properties
	* log4j2.xml
	