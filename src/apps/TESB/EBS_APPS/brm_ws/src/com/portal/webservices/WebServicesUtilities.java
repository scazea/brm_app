package com.portal.webservices;

import com.portal.pcm.EBufException;
import com.portal.pcm.PortalOp;
import com.portal.pfc.infranet.connpool.EbsPortalContextFactory;
import com.portal.pfc.infranet.connpool.ConnectionPool;
//import com.portal.pfc.infranet.connpool.PortalContextFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.commons.logging.*;

public class WebServicesUtilities
{
	private static Properties sInfranetProps;
	private static boolean sLogEnabled = false;
	private static Log myLog = LogFactory.getLog("ebsWS");
	private static String sFldPrefix = null;
	private static String sFldShrt = null;
	
	public static void init()
	{
	}

	public static Log getLogger()
	{
		return myLog;
	}
	public static boolean isLoggingEnabled()
	{
		return sLogEnabled;
	}

	public static String getPrefix()
	{
		return sFldPrefix;
	}

	public static String getShort()
	{
		return sFldShrt;
	}
	
	public static int getOpcode(String opcodeName)
	{
		int opcode = -1;
		char c = opcodeName.charAt(0);
		
		if (opcodeName.startsWith("PCM_OP_")) {
			opcodeName = opcodeName.substring(7);
			opcode = PortalOp.stringToOp(opcodeName);
		} else {
//			opcode = CustomOp.stringToOp(opcodeName);
		}
		if (opcode == -1 && Character.isDigit(c)) {
			try
			{
				opcode = Integer.parseInt(opcodeName);
			} catch (NumberFormatException ex) {
				myLog.error("Invalid opcode: " + opcodeName, ex);
				throw ex;
			}
		}
	return opcode;
	}

	static
	{
		sInfranetProps = new Properties();
		try
		{
			InputStream is = WebServicesUtilities.class.getResourceAsStream("/Infranet.properties");
		
			if (is != null) {
				sInfranetProps.load(is);
			}
			else {
				String infranetPropsFile = System.getProperty("user.home") + System.getProperty("file.separator") + "Infranet.properties";
			
				FileInputStream fis = new FileInputStream(infranetPropsFile);
				sInfranetProps.load(fis);
			}
		} catch (IOException ioex) {
			sInfranetProps.put("infranet.connection", "pcp://root.0.0.0.1:password@localhost:11960/service/admin_client 1");
			sInfranetProps.put("infranet.login.type", "1");
		}
	
		sLogEnabled = Boolean.valueOf(sInfranetProps.getProperty("webservices.log.enabled", "false")).booleanValue();
		sFldPrefix = sInfranetProps.getProperty("webservices.cust-field.prefix", "");
		sFldShrt = sInfranetProps.getProperty("webservices.cust-field.short", "");
		
		try
		{
			ConnectionPool.createInstance(new EbsPortalContextFactory(), sInfranetProps);
			
			myLog.debug("Infranet CM connection: " + sInfranetProps.getProperty("infranet.connection"));			
		}
		catch (EBufException ex)
		{
			myLog.error("Cannot initialize ", ex);
		}
	}
}
