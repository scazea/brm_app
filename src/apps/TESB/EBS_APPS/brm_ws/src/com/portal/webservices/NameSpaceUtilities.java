package com.portal.webservices;

import com.portal.pcm.EBufException;
import com.portal.pcm.FList;
import java.io.ByteArrayInputStream;
import java.io.PrintStream;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.apache.commons.logging.*;

public class NameSpaceUtilities
{
  private static final String XMLNS = "xmlns";
  private static final String XSI = ":xsi";
  private static final String PRSF_QUALIFIER = "prsf";
  private static Log myLog = WebServicesUtilities.getLogger();

  public static String addNameSpaceToOutput(String input, FList outFL)
  {
    WebServicesUtilities.init();

    String inNS = null; String inQualifier = null;

    String retVal = null;
    try {
      Node child;
      NamedNodeMap nnm;
      int i;
      DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
      DocumentBuilder docBuilder = fact.newDocumentBuilder();

      Document inDoc = docBuilder.parse(new ByteArrayInputStream(input.getBytes()));

      if (inDoc.getNodeType() == 9) {
        child = inDoc.getFirstChild();
        nnm = child.getAttributes();
        i = 0; for (int len = nnm.getLength(); i < len; ++i) {
          Node node = nnm.item(i);
          String attribName = node.getNodeName();
          if ((attribName.startsWith("xmlns")) && (!(attribName.endsWith(":xsi"))))
          {
            if (attribName.indexOf(":") != -1)
              inQualifier = attribName.split(":")[1];
            inNS = ((Element)child).getAttribute(attribName);
          }

        }

      }

      if (inNS != null) {
        Document outDoc = outFL.toXMLDocument();
        Node flist = outDoc.getFirstChild();
        Element elemFL = (Element)flist;

        StringBuffer attribName = new StringBuffer("xmlns");
        if (inQualifier == null)
          inQualifier = "prsf";

        attribName.append(":").append(inQualifier);
        elemFL.setAttributeNS("http://www.w3.org/2000/xmlns/", attribName.toString(), inNS + ".output");

        retVal = getXMLDocAsString(outDoc);

        retVal = retVal.replaceAll("flist", inQualifier + ":flist");
      }
    }
    catch (Exception ex)
    {
    	myLog.error("caught exception:", ex);
    }

    if (retVal == null)
      try {
        retVal = outFL.toXMLString();
      } catch (EBufException ex) {
        retVal = "";
      }

    return retVal;
  }

  private static String getXMLDocAsString(Document doc) {
    StringWriter sw = new StringWriter();
    TransformerFactory tFactory = TransformerFactory.newInstance();
    if ((tFactory.getFeature("http://javax.xml.transform.dom.DOMSource/feature")) && (tFactory.getFeature("http://javax.xml.transform.stream.StreamResult/feature")))
    {
      Source src = new DOMSource(doc);
      Result result = new StreamResult(sw);
      try
      {
        Transformer t = tFactory.newTransformer();
        t.setOutputProperty("indent", "yes");
        t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        t.transform(src, result);
      } catch (Exception ex) {
        return null;
      }
    }
    return sw.toString();
  }
}