package com.portal.webservices;

import com.portal.pcm.EbsPortalContext;
import com.portal.pcm.DefaultLog;
import com.portal.pcm.EBufException;
import com.portal.pcm.FList;
import com.portal.pcm.Poid;
import com.portal.pcm.PortalContext;
import com.portal.pcm.PortalOp;
import com.portal.pcm.fields.*;
import com.portal.pfc.infranet.connpool.ConnectionPool;
import org.apache.commons.logging.*;
import java.io.StringReader;
import java.util.Date;
import java.util.Arrays;


public class OpcodeCaller implements AutoCloseable {
	private EbsPortalContext _ctx;
	private ConnectionPool _cpool;
	private static Log _log;
	private static String _prefix = null;
	private static String _short = null;
	private static	boolean _replace = false;
	private boolean _persist = false;

	public OpcodeCaller() {
		this(false);
	}
	
	public OpcodeCaller(boolean keep) {
		WebServicesUtilities.init();
		_log = WebServicesUtilities.getLogger();
		_prefix = WebServicesUtilities.getPrefix();
		_short = WebServicesUtilities.getShort();
		_replace = (_short != null && _prefix != null);
		_persist = keep;

		if (keep){
			try {
				this._cpool = ConnectionPool.getInstance();
				_log.debug("Current ConnectionPool headroom: " + this._cpool.getLowestHeadroom());
				_log.debug("Current ConnectionPool free ones: " + this._cpool.getNumberOfFreeConnections());

				while (this._ctx== null) {
						this._ctx = (EbsPortalContext) this._cpool.getConnection();
				}
			} catch (EBufException ebufex) {
				_log.error(Arrays.toString(ebufex.getStackTrace()));
			}

		}
	}
	
	public void releaseContext() {
		if (_persist)
			this._cpool.releaseConnection(this._ctx);
			
	}
	
	@Override
	public void close() {
		_log.info("AutoClosing OpcodeCaller");
		releaseContext();
	}

	public String opcode(String opcodeName, String inputXML) {
		return opcodeWithFlags(opcodeName, 0, inputXML);
	}

	public String opcode(int opcode, String inputXML) {
		return opcodeWithFlags(opcode, 0, inputXML, null);
	}

	public String opcodeWithFlags(String opcodeName, int flags, String inputXML) {
		return opcodeWithFlags(opcodeName, flags, inputXML, null);

	}

	public String opcodeWithFlags(String opcodeName, int flags, String inputXML, String correlationId) {
		int opcode = WebServicesUtilities.getOpcode(opcodeName);
		return opcodeWithFlags(opcode, flags, inputXML, correlationId);

	}
	
	public String opcodeWithFlags(int opcode, int flags, String inputXML, String correlationId) {
		String ret_string = null;
		try {
			ret_string = opcodeWithFlags(opcode, flags, inputXML, correlationId, false);
		} catch (EBufException e) {
			_log.error(Arrays.toString(e.getStackTrace()));
		}
		return ret_string;
	}
	
	public String opcodeWithFlagsEx(String opcodeName, int flags, String inputXML, String correlationId) throws EBufException {
		int opcode = WebServicesUtilities.getOpcode(opcodeName);
		return opcodeWithFlags(opcode, flags, inputXML, correlationId, true);
	}
	
	public String opcodeWithFlags(int opcode, int flags, String inputXML, String correlationId, boolean bubble) throws EBufException {
		String opName = PortalOp.opToString(opcode);
		if (opName == null) {
//TODO: enable when dealing with custom opcode/fields			opName = CustomOp.opToString(opcode);
		}

		if (opName == null) {
			opName = Integer.toString(opcode);
		}

		String outputXML = null;
		if (this._persist == false)
			this._ctx = null;

		inputXML = inputXML.replaceAll("<\\?xml.+\\?>\n","").replaceAll("<ns.+>\n", "")
				.replaceAll("\n</ns.+>", "");
		inputXML = inputXML.replaceAll("><([A-Za-z0-9])",">\n<$1");
		if (_replace) {
			_log.debug("prefix: " + _prefix + "\tshort: "	+ _short);	
			inputXML = inputXML.replaceAll("<" + _short, "<" + _prefix).replaceAll("</" + _short, "</" + _prefix);
		}

		// Some data is sensitive, so only show it in trace mode.
		_log.trace("Executing BRM API " + opName + "\tinput xml:\n" + "\t" + inputXML);

		if ((inputXML == null) || (inputXML.equals(""))) {
			throw new NullPointerException("input XML may not be null or empty");
		}

		if (opcode <= 0) {
			throw new NullPointerException("opcode may not be zero or negative");
		}

		
		StringReader is = new StringReader(inputXML);
		FList outFL = null;
		String name = null;
		try {
			@SuppressWarnings("deprecation")
			FList inFL = FList.createFromXML(is);
						
			if ( ! inFL.hasField(FldPoid.getInst()) ) {
				_log.info("No POID given");
				inFL.set(FldPoid.getInst(), new Poid(1, -1, "/account"));
			}

			_log.info("opcode to call: " + opName + (flags != 0 ? "\tflags: " + flags : ""));
			_log.info(">>> input flist:\n" + inFL.toString());

			name = Thread.currentThread().getName();
			if ((name == null) || (name.length() == 0)) {
				name = "WS: ";
			} else {
				name = "WS: " + name;
			}

			_log.debug(name + " Retrieving connection from pool.");

			if (this._persist == false)
				this._cpool = ConnectionPool.getInstance();
			this._cpool = ConnectionPool.getInstance();
			_log.debug("Current ConnectionPool headroom: " + this._cpool.getLowestHeadroom());
			_log.debug("Current ConnectionPool free ones: " + this._cpool.getNumberOfFreeConnections());

			while (this._ctx == null) {
				this._ctx = (EbsPortalContext) this._cpool.getConnection();
			}

			outFL =  this._ctx.opcode(opcode, flags, inFL, correlationId);
		
			_log.info(">>> output flist:\n" + outFL.toString());
			_log.debug("Current ConnectionPool headroom: " + this._cpool.getLowestHeadroom());


			if (this._persist == false) 
				this._cpool.releaseConnection(this._ctx);
		} catch (EBufException ebufex) {
			if (bubble)
				throw ebufex;
			_log.info(name + " EBufException - connection is " + this._ctx);

			if (ebufex.getError() == 27) {
				if ((ebufex.getMessageID() != 1) && (this._cpool != null)  && (this._persist == false)) {
					this._cpool.releaseBadConnection(this._ctx);
				}

			} else if (this._cpool != null && (this._persist == false)) {
				this._cpool.releaseConnection(this._ctx);
			}

			_log.error(ebufex.getErrorString() + "\n" +
								ebufex.getFacilityString() + "\n" +
								ebufex.getFieldString() + "\n" +
								ebufex.getFileName() + "\n" +
								ebufex.getLine() + "\n" +
								ebufex.getLocalizedMessage() + "\n" +
								ebufex.getLocationString() + "\n" +
								ebufex.getMessage());
			
			FList ebsErrFL = new FList();			
			ebsErrFL.set(FldOpcode.getInst(), opcode);
			ebsErrFL.set(FldErrorNum.getInst(), ebufex.getError());
			ebsErrFL.set(FldAddress.getInst(), ebufex.getLocationString());
			ebsErrFL.set(FldMessage.getInst(), ebufex.getMessage());
			ebsErrFL.set(FldTypeStr.getInst(), ebufex.getFacilityString());

			if (1 == 0) {
				_log.info("Man that sucks!\n");
			}
			else if (ebufex.getError() == 27) {
				ebsErrFL.set(FldErrorDescr.getInst(), "Backend server not responding");
				ebsErrFL.set(FldFieldName.getInst(), "");
			} else {
				ebsErrFL.set(FldErrorDescr.getInst(), ebufex.getErrorString());
				ebsErrFL.set(FldFieldName.getInst(), ebufex.getFieldString());
			}
			
			outFL = new FList();
			outFL.setElement(FldErrorInfo.getInst(), 0, ebsErrFL);
		}

		if (outFL != null) {
			outputXML = NameSpaceUtilities.addNameSpaceToOutput(inputXML, outFL);
			if (_replace) {
				outputXML = outputXML.replaceAll(_prefix, _short);
			}
		}

		_log.info(">>> output xml: \n" + outputXML);
		return outputXML;
	}
}
