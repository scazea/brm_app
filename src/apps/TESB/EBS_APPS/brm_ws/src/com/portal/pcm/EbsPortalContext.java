package com.portal.pcm;

import com.portal.pcm.fields.FldOpCorrelationId;

public class EbsPortalContext extends PortalContext {
private PCPContext my_ctx;

	public EbsPortalContext(PortalContext arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
	public EbsPortalContext() 
			throws EBufException 
	{
		super();
	}
	
	public synchronized FList opcode(int opcode, int flags, FList paramFList, String correlation)
		    throws EBufException
	  {
		if (correlation != null)
		{
			setCorrelationId(correlation);
		}
		else
		{
			setCorrelationId(genCorrelationId(opcode));
		}
	    sendCorrelation(paramFList);
	    my_ctx = this.getContext();
	    
	    if (this.my_ctx != null)
	    {
	      FList localFList = this.my_ctx.op(opcode, flags, paramFList);
	      if (localFList.hasField(FldOpCorrelationId.getInst()))
	        localFList.remove(FldOpCorrelationId.getInst());
	      return localFList;
	    }
	    throw new EBufException(1, 56, this);
	  }
	
}
