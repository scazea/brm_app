package com.portal.pfc.infranet.connpool;
import com.portal.pcm.EBufException;
import com.portal.pcm.EbsPortalContext;

public class EbsPortalContextFactory 
	 implements ConnectionFactory
	 {
	   public EbsPortalContext create()
	     throws EBufException
	   {
	     return new EbsPortalContext();
	   }
	 }
