package routines;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.DatatypeConfigurationException;

import org.dom4j.DocumentException;
import com.portal.webservices.*;
import com.portal.pcm.*;

import routines.system.Document;
import routines.system.ParserUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;


/*
 * user specification: the function's comment should contain keys as follows: 1. write about the function's comment.but
 * it must be before the "{talendTypes}" key.
 * 
 * 2. {talendTypes} 's value must be talend Type, it is required . its value should be one of: String, char | Character,
 * long | Long, int | Integer, boolean | Boolean, byte | Byte, Date, double | Double, float | Float, Object, short |
 * Short
 * 
 * 3. {Category} define a category for the Function. it is required. its value is user-defined .
 * 
 * 4. {param} 's format is: {param} <type>[(<default value or closed list values>)] <name>[ : <comment>]
 * 
 * <type> 's value should be one of: string, int, list, double, object, boolean, long, char, date. <name>'s value is the
 * Function's parameter name. the {param} is optional. so if you the Function without the parameters. the {param} don't
 * added. you can have many parameters for the Function.
 * 
 * 5. {example} gives a example for the Function. it is optional.
 */
public class pcm {

	private static DatatypeFactory myDataFactory;
	static
	{
		try {
			myDataFactory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			throw new RuntimeException("Init Error!", e);
		}
	}

    /**
     * Date2Ux: returns {@code Unix epoch-based timestamp} of {@code dateTimeStr}
     * 
     * {talendTypes} Long
     * {Category} User Defined
     * {param} string("2012-08-19T13:21:21Z") input: The string datetime.
     * {example} Date2Ux("2012-08-19T13:21:21Z") # 118213412322
     * {return} unix timestamp.
     *
     * @param  dateTimeStr String
     *         The dateTime to get the timestamp from.
     * @return  {@code Long timestamp} if the given dateTime is valid.
     * @see  #Ux2Date(Long)
     */
	public static synchronized Long Date2Ux(String dateTimeStr)
	{
		if (dateTimeStr == null)
			return null;
		XMLGregorianCalendar dateTime = myDataFactory.newXMLGregorianCalendar(dateTimeStr);
		return (Long) (dateTime.toGregorianCalendar().getTimeInMillis()/1000);			
	}

    /**
     * Ux2Date: returns {@code dateTimeStr} of {@code epoch} Unix epoch-based timestamp
     * 
     * {talendTypes} String
     * {Category} User Defined
     * {param} Long("118213412322") input: The unix timestamp.
     * {example} Ux2Date("118213412322") # "2012-08-19T13:21:21Z"
     * {return} XML dateTime.
     *
     * @param  epoch Long, or epoch String
     *         The unix timestamp to get the dateTime from.
     * @param  (Optional) boolean, to flag epoch includes milliseconds or not
     * @return  {@code String dateTime} if the given unix timestamp is valid.
     * @see  #Date2Ux(String)
     */
	public static synchronized String Ux2Date(Long epoch)
		{
		if (epoch == null)
			return null;
		GregorianCalendar myGreg = new GregorianCalendar();
		myGreg.setTimeInMillis(epoch * (epoch > 2147483647 ? 1 : 1000));
		if (epoch == 0)
			myGreg.set(GregorianCalendar.YEAR, myGreg.getMaximum(GregorianCalendar.YEAR)-1);
		
		return myDataFactory.newXMLGregorianCalendar(myGreg).normalize().toXMLFormat();
	}
	public static String Ux2Date(String epoch)
	{
		if (epoch == null)
			return null;
		return Ux2Date(Long.parseLong(epoch));
	}

	public static synchronized Long JavaDate2Ux(String dateTimeStr)
	{
		if (dateTimeStr == null)
			return null;
		Calendar mydate = new GregorianCalendar();		
		Date thedate;
		try {
			thedate = new SimpleDateFormat("MM/dd/YY", Locale.US).parse(dateTimeStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block			
			e.printStackTrace();
			return new Long(0);
		}
		mydate.setTime(thedate);
		return (Long) (mydate.getTime().getTime()/1000);			
	}

	public static synchronized Long CurrentUxDate()
	{
		return (Long) (Calendar.getInstance().getTime().getTime()/1000);			
	}
	
    /**
     * getPoidID: returns {@code ID} of POID or {@code null} if POID is not valid.
     * 
     * {talendTypes} String
     * {Category} User Defined
     * {param} string("0.0.0.1 /account -1 0") input: The POID to extract the ID from.
     * {example} getPoidID("0.0.0.1 /account -1 0") # -1
     * {return} ID of POID or null if not valid.
     * @param  POID String
     *         The POID to extract the ID from
     * @return  {@code ID} if the given POID is not invalid, {@code null} otherwise
     * @see  #setPoid(String, String)
     */
    public static String getPoidID(String poid) {    	
    	return poid == null ? null : poid.replaceAll("(.+) (.+) ([^ ]+)", "$3");
    }

    /**
     * setPoid: returns POID as String or null if ID is not valid.
     * 
     * {talendTypes} String
     * {Category} User Defined
     * {param} String("/deal") input: The POID type to create.
     * {param} String("1234") input: The POID ID to create.
     * {example} setPOID("/deal", "1231") # -1
     * {return} POID as String or null if not valid.
     */
    public static String setPoid(String type, String id) {    	
    	return id == null || type == null ? null : "0.0.0.1 " + type + " " + id;
    }
    
    /**
     * callOpcode: Calls a BRM opcode with an input Document and returns a result Document.
     * 
     * {talendTypes} Document
     * {Category} User Defined
     * {param} Document(AnXMLPayload) input: The XML Flist.
     * {example} callOpcode(input_row.body) # Return XML Flist
     * {return} Document or null if input null.
     * @param  Opcode String
     * 			The opcode Name to invoke
     * @param  flags integer
     *         The opcode flags
     * @param  Document
     *         The input XML Flist
     * @return  {@code Document} if input is valid, {@code null} otherwise
     */
  //Code generated according to input schema and output schema
    public static Document callOpcode(String opcode, int flags, Document inFlist, String correlation) {
        OpcodeCaller oc = new OpcodeCaller();        
        String out_XML_str = oc.opcodeWithFlags(opcode, flags, inFlist.toString(), correlation);
        Document outFlist = null;
        try {
        	outFlist = ParserUtils.parseTo_Document(out_XML_str);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return outFlist;
    }
    
    public static Document callOpcodeEx(String opcode, int flags, Document inFlist, String correlation) throws EBufException {
        OpcodeCaller oc = new OpcodeCaller();
        String out_XML_str = oc.opcodeWithFlagsEx(opcode, flags, inFlist.toString(), correlation);
        Document outFlist = null;
        try {
        	outFlist = ParserUtils.parseTo_Document(out_XML_str);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return outFlist;
    }

    public static Document callOpcodeWithCallerEx(OpcodeCaller oc, String opcode, int flags, Document inFlist, String correlation) throws EBufException {
        String out_XML_str = oc.opcodeWithFlagsEx(opcode, flags, inFlist.toString(), correlation);
        Document outFlist = null;
        try {
        	outFlist = ParserUtils.parseTo_Document(out_XML_str);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return outFlist;
    }      
}
