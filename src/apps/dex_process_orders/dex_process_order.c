/********************************************************************
 *
 *      Copyright (c) 1999 - 2006 Portal Software, Inc. All rights reserved.
 *
 *      This material is the confidential property of
 *      Portal Software, Inc. or its subsidiaries or licensors and
 *      may be used, reproduced, stored or transmitted only in  accordance
 *      with a valid Portal license or sublicense agreement.
 *
 *******************************************************************/

#ifndef lint
static  char    Sccs_id[] = "@(#)% %";
#endif

#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <pthread.h>

#include "pcm.h"
#include "pinlog.h"
#include "pin_act.h"
#include "pin_os.h"
#include "pin_mta.h"
#include "pcm_ops.h"
#include "ops/bill.h"
#include "pin_cust.h"
#include "pin_errs.h"
#include "fm_utils.h"
//#include "pin_thread.h"
#include "dex_flds.h"
#include "dex_ops.h"
#include "dex_process_order.h"


void 
populate_cmd_line_args(
	pin_flist_t *param_flistp,
	pin_flist_t *app_flistp,
	pin_errbuf_t *ebufp);

static time_t min_time = 0;
static int64  g_poid_id0 = 1;
static pthread_mutex_t  lock;
/*******************************************************************
 * Usage information for the specific app.
 * Called prior MTA_USAGE policy opcode
 *******************************************************************/

void 
pin_mta_usage(prog)
char	*prog;
{
	fprintf(stderr, "Usage: %s %s %s %s \n\n",
		prog,
	        "[ -retry]",	
                "[-verbose]",
		"[-test]");
	return;
}

/*******************************************************************
 * Configuration of application
 * Called prior MTA_CONFIG policy opcode
 *******************************************************************/

void 
pin_mta_config (
	pin_flist_t	*param_flistp,
	pin_flist_t	*app_flistp,
	pin_errbuf_t	*ebufp)
{
	void		*vp = NULL;
	int32		err;
	int32		*i_ptr = NULL;
	int32		enable_ara = 0;
	int32		mta_flag = 0;
	char		*error_statusp = NULL;
	char		*statusp = NULL;
	char		error_status[64];
	char		status_str[64];

	if (PIN_ERR_IS_ERR(ebufp)) 
		return;

	PIN_ERRBUF_CLEAR (ebufp);

	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG, 
			"pin_mta_config parameters flist", param_flistp);
	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG, 
			"pin_mta_config application info flist", app_flistp);

	/*********************************************
     * Read deadlock retry  parameter
     **********************************************/
     pin_conf(DEX_PROCESS_ORDER_MTA_PROG, DEX_PROCESS_ORDER_DEADLOCK_RETRY,
			PIN_FLDT_INT,(caddr_t *)&(i_ptr),&err);

	if (i_ptr != (int32 *)NULL)
	{
		PIN_FLIST_FLD_SET(app_flistp, PIN_FLD_DEADLOCK_RETRY, i_ptr,
				ebufp);
		free(i_ptr);
		i_ptr = (int32 *)NULL;
	}



	pin_conf(DEX_PROCESS_ORDER_MTA_PROG, ORDER_STATUS_ERROR, PIN_FLDT_STR,
				&error_statusp, &err);
	if (error_statusp != (char *) NULL) {
		strcpy(error_status, error_statusp);
		free(error_statusp);
	}else{
		strcpy(error_status, "error");
	}
	PIN_FLIST_FLD_SET(app_flistp, PIN_FLD_STATUS_MSG, (void *)error_status, ebufp);

	pin_conf(DEX_PROCESS_ORDER_MTA_PROG, ORDER_STATUS_FULFILL, PIN_FLDT_STR,
				&statusp, &err);
	if (statusp != (char *) NULL) {
		strcpy(status_str, statusp);
		free(statusp);
	}else{
		strcpy(status_str, "tofufill");
	}
	PIN_FLIST_FLD_SET(app_flistp, PIN_FLD_STATUS_STR, (void *)status_str, ebufp);

	/*********************************************
	 * Get the command line params from the
	 * param_flistp and populate into the app_flistp.
	 *********************************************/	
	populate_cmd_line_args(param_flistp,app_flistp,ebufp);

	/*********************************************
	 * Configure application to return an error to
	 * the shell if any worker thread fails.
	 *********************************************/

        vp = PIN_FLIST_FLD_GET(app_flistp, PIN_FLD_FLAGS, MTA_MANDATORY, ebufp);

        if(vp) {
                mta_flag = *(int32 *) vp;
        }

        mta_flag = mta_flag | MTA_FLAG_RETURN_WORKER_ERROR;

        PIN_FLIST_FLD_SET(app_flistp, PIN_FLD_FLAGS,
			(void *) &mta_flag, ebufp);

	if (PIN_ERR_IS_ERR(ebufp)) 
	{
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
				"pin_mta_config error", ebufp);
	}

	pin_mutex_init(&lock, ebufp);
	return;
}


/*******************************************************************
 * Post configuration of application
 * Called after MTA_CONFIG policy opcode
 *******************************************************************/
void pin_mta_post_config (pin_flist_t	*param_flistp,
			pin_flist_t	*app_flistp,
			pin_errbuf_t	*ebufp)
{
	time_t			end_t = 0;
   	time_t			now_t = 0;
	time_t			start_t = 0;
	pin_flist_t		*flistp = NULL;
	pin_cookie_t	cookie = 0;
	int				rec_id = 0;
	void			*vp = NULL;
	
	if (PIN_ERR_IS_ERR(ebufp)) 
	{
		return;
	}
	PIN_ERRBUF_CLEAR (ebufp);

	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG,
		"pin_mta_post_config parameters flist", param_flistp);
	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG,
		"pin_mta_post_config application info flist", app_flistp);


	flistp = PIN_FLIST_ELEM_GET_NEXT(app_flistp,
			PIN_FLD_PARAMS,&rec_id,1,&cookie,ebufp);
	if(flistp != NULL)
	{
		vp = PIN_FLIST_FLD_GET(flistp,PIN_FLD_START_T,1,ebufp);
		if(vp)
		{
			start_t = *(time_t *)vp;
		}
		vp = PIN_FLIST_FLD_GET(flistp,PIN_FLD_END_T,1,ebufp);
		if(vp)
		{
			end_t = *(time_t *)vp;
		}
		else
		{
			end_t = pin_virtual_time((time_t *)NULL);
		}
	}
	
	now_t = pin_virtual_time((time_t *)NULL);

	if ((end_t && (start_t >= end_t)) ||
		(start_t >= now_t) || (end_t > now_t)) 
	{
		fprintf(stdout, "Start time specified is greater than or "
			"equal to the End time  or the end time is greater "
			"than the current time.\n");
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_ERROR, "bad config: time");
   	 	exit(PIN_ERR_INVALID_CONF);
	}
	PIN_FLIST_FLD_SET(flistp,PIN_FLD_END_T,&end_t,ebufp);
	if (PIN_ERR_IS_ERR(ebufp)) 
	{
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, 
			"pin_mta_post_config error", ebufp);
	}

	return;

}


/*******************************************************************
 * Initialization of application
 * Called prior MTA_INIT_APP policy opcode
 *******************************************************************/

PIN_EXPORT void 
pin_mta_init_app (
	pin_flist_t	*app_flistp,
	pin_errbuf_t	*ebufp)
{
	void		*vp = NULL;
	pin_flist_t	*oper_flistp = NULL;
	time_t		now_t = 0;

	if (PIN_ERR_IS_ERR(ebufp)) 
	{
		return;
	}
	PIN_ERRBUF_CLEAR (ebufp);

	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG,
		"\n\npin_mta_init_app application info flist\n",app_flistp);


	/*********************************************************************
	 * Add the Audit Revenue Assurance fields required
	 ********************************************************************/

	/*********************************************************************
	 * Get the Operation Info by locking 
	 ********************************************************************/
	oper_flistp = pin_mta_global_flist_node_get_with_lock(
			PIN_FLD_OPERATION_INFO, ebufp);

	/*********************************************************************
	 * Fields of the application scope.
	 ********************************************************************/
	PIN_FLIST_FLD_SET(oper_flistp, PIN_FLD_PROCESS_NAME,
			"pin_deferred_act ", ebufp);
	PIN_FLIST_FLD_SET(oper_flistp, PIN_FLD_PROGRAM_NAME,
			DEX_PROCESS_ORDER_MTA_PROG, ebufp);
	now_t = pin_virtual_time((time_t *)NULL);
	PIN_FLIST_FLD_SET(oper_flistp,PIN_FLD_PROCESS_START_T, &now_t, ebufp);
	vp = (time_t *)NULL;
    	PIN_FLIST_FLD_SET(oper_flistp,PIN_FLD_PROCESS_END_T, vp, ebufp);
	vp = (void *)NULL;
    	PIN_FLIST_FLD_SET(oper_flistp, PIN_FLD_SUCCESSFUL_RECORDS, vp, ebufp);
    	PIN_FLIST_FLD_SET(oper_flistp, PIN_FLD_FAILED_RECORDS, vp, ebufp);
    	PIN_FLIST_FLD_SET(oper_flistp, PIN_FLD_NUM_TOTAL_RECORDS, vp, ebufp);

	/*******************************************************************
	* Release global flist
 	*******************************************************************/
    	pin_mta_global_flist_node_release(PIN_FLD_OPERATION_INFO,ebufp);


	if (PIN_ERR_IS_ERR(ebufp)) 
	{
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"pin_mta_init_app error", ebufp);
	}

	return;
	
}


/*******************************************************************
 * Application defined search criteria.
 * Called prior MTA_INIT_SEARCH policy opcode
 *******************************************************************/

PIN_EXPORT void 
pin_mta_init_search(
	pin_flist_t	*app_flistp,
	pin_flist_t	**search_flistpp,
			pin_errbuf_t	*ebufp)
{
	char			status[128];
	pin_flist_t		*search_flistp = NULL;
	pin_flist_t		*flistp = NULL;
	pin_flist_t		*sub_flistp = NULL;
	poid_t			*sch_pdp = NULL;
	poid_t			*s_pdp = NULL;
	int64			database = 1; 
	int32			batch = 0;
	void			*vp = NULL;
	int32			rec_id = 0;
	pin_cookie_t	cookie = NULL;
    int32           mta_flags = 0;
    char           	template[256] = "";
    int32          	sflags = 768;

	if (PIN_ERR_IS_ERR(ebufp)) 
	{
		return;
	}
	
	PIN_ERR_CLEAR_ERR(ebufp);

	*search_flistpp = 0;

	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG, 
		"\n\n pin_mta_init_search application info flist\n",app_flistp);


	/***********************************************************
	 * Allocate the search flist                 
	 ***********************************************************/

 	vp = PIN_FLIST_FLD_GET (app_flistp, PIN_FLD_POID_VAL, 0, ebufp);
	if(vp)
	{
		database = PIN_POID_GET_DB ((poid_t*)vp);
	}

	search_flistp = PIN_FLIST_CREATE(ebufp);

	s_pdp = PIN_POID_CREATE(database, "/search", -1, ebufp);

	PIN_FLIST_FLD_PUT(search_flistp, PIN_FLD_POID, (void *)s_pdp, ebufp);

	PIN_FLIST_FLD_SET(search_flistp, PIN_FLD_FLAGS, (void *)&sflags, ebufp);

	sprintf(template,"select X from /dex_order where F1 = V1 and dex_order_t.poid_id0 > %" 
		I64_PRINTF_PATTERN"d order by poid_id0 asc", g_poid_id0);

    PIN_FLIST_FLD_SET(search_flistp, PIN_FLD_TEMPLATE, 
			(void *)template, ebufp);


	/***********************************************************
	 * Add the search criteria
	 ***********************************************************/
	/* 
	 * STATUS_STR
	 */
	flistp = PIN_FLIST_ELEM_ADD(search_flistp, PIN_FLD_ARGS, 1, ebufp);
	sub_flistp = PIN_FLIST_ELEM_GET_NEXT(app_flistp, 
			PIN_FLD_PARAMS, &rec_id,1,&cookie, ebufp);

    vp = PIN_FLIST_FLD_GET (sub_flistp, PIN_FLD_FLAGS,1,ebufp);
    if(vp)
    {
            mta_flags = *((int32*)vp);
    }	
    flistp = PIN_FLIST_ELEM_ADD(flistp, DEX_FLD_ORDER_ITEMS, -1, ebufp);
	memset(status, 0, sizeof(status));
    if(mta_flags & PROCESS_ORDER_FLAG_RETRY) {
 		vp = PIN_FLIST_FLD_GET (app_flistp, PIN_FLD_STATUS_MSG, 0, ebufp);
      	strcpy(status,  vp );
    } else {	
 		vp = PIN_FLIST_FLD_GET (app_flistp, PIN_FLD_STATUS_STR, 0, ebufp);
       	strcpy(status,  vp );
    }	
    PIN_FLIST_FLD_SET(flistp, PIN_FLD_STATUS_STR, (void *)&status, ebufp);
	
	/***********************************************************
	 * Add the search results we want
	 ***********************************************************/
	
	flistp = PIN_FLIST_ELEM_ADD(search_flistp, PIN_FLD_RESULTS, 0, ebufp);
	flistp = PIN_FLIST_ELEM_ADD(flistp, DEX_FLD_ORDER_ITEMS, -1, ebufp);
	vp = (void *)NULL;
	
	if (PIN_ERR_IS_ERR(ebufp)) 
	{
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"pin_mta_init_search error", ebufp);
		PIN_FLIST_DESTROY_EX (&search_flistp,0);
	}
	else
	{
		*search_flistpp = search_flistp;
		PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG, 
			"\n\n pin_mta_init_search search flist\n",
			search_flistp);
	}
	return;
}



/*******************************************************************
 * Search results may be updated, modified or enriched
 * Called prior MTA_TUNE policy opcode
 *******************************************************************/

PIN_EXPORT void 
pin_mta_tune (
	pin_flist_t	*app_flistp,
	pin_flist_t	*srch_res_flistp,
	pin_errbuf_t	*ebufp) 
{

	int32		total_count = 0;
	int		rec_id = 0;
	pin_cookie_t	cookie = 0;
	pin_flist_t	*oper_flistp = NULL;
	pin_flist_t	*flistp = NULL;
	void		*vp = NULL;

	if (PIN_ERR_IS_ERR(ebufp)) 
	{
		return;
	}
	PIN_ERRBUF_CLEAR (ebufp);

	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG,
		"\n\n pin_mta_tune application info flist\n", app_flistp);

	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG,
		"\n\n pin_mta_tune search results flist\n", srch_res_flistp);


	/********************************************************************
	* Get the count of the no of accounts (no of results and update the 
	* RAF object for the track of total scheduled accounts
	********************************************************************/
	
	while((flistp = PIN_FLIST_ELEM_GET_NEXT(srch_res_flistp,
		PIN_FLD_MULTI_RESULTS, &rec_id , 1, &cookie, ebufp) ) != NULL)  
	{
		total_count += PIN_FLIST_ELEM_COUNT(flistp, 
			PIN_FLD_RESULTS, ebufp);
	}
	
	oper_flistp = pin_mta_global_flist_node_get_with_lock(
			PIN_FLD_OPERATION_INFO,ebufp);	
	vp = PIN_FLIST_FLD_GET(oper_flistp,PIN_FLD_NUM_TOTAL_RECORDS,0,ebufp);
	if(vp)
	{
		total_count+=*(int32 *)vp;
	}
	PIN_FLIST_FLD_SET(oper_flistp,PIN_FLD_NUM_TOTAL_RECORDS,
			&total_count,ebufp);
	pin_mta_global_flist_node_release(PIN_FLD_OPERATION_INFO,ebufp);

	return;
}


/*******************************************************************
 * Function executed at application exit
 * Called after MTA_EXIT policy opcode
 *******************************************************************/

PIN_EXPORT void 
pin_mta_post_exit (
	pin_flist_t	*app_flistp,
	pin_errbuf_t	*ebufp)
{
	int32			rec_id = 0;
	int32			success_count = 0;
	int32			failed_count = 0;
	int32			total_count = 0;
	int32			mta_flags = 0;
	int32           	enable_ara=0;
	pin_cookie_t		cookie = 0;
	pin_cookie_t		sub_cookie = 0;
	pin_flist_t		*oper_flistp = NULL;
	pin_flist_t		*tmp_flistp = NULL;
	pin_flist_t		*sub_flistp = NULL;
	pin_flist_t		*failed_flistp = NULL;
	void			*vp = NULL;
	time_t			now_t = 0;
	int32			count = 0;
	int32			sub_rec_id = 0;


	if (PIN_ERR_IS_ERR(ebufp)) 
	{
		return;
	}
	PIN_ERRBUF_CLEAR (ebufp);

	vp = PIN_FLIST_FLD_GET(app_flistp,PIN_FLD_FLAGS,0,ebufp);
	if(vp)
	{
		mta_flags = *(int32 *)vp;
	}
	if(mta_flags & MTA_FLAG_USAGE_MSG)
	{
		return;
	}

	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG, 
		"\n\npin_mta_post_exit application info flist \n", app_flistp);

	now_t = pin_virtual_time((time_t *)NULL);
	
	/********************************************************************
	 * Make the ARA object by consolidting the threads information for 
	 * the ARA
	 ********************************************************************/
	oper_flistp = pin_mta_global_flist_node_get_with_lock(
			PIN_FLD_OPERATION_INFO,ebufp); 
	vp = PIN_FLIST_FLD_GET(oper_flistp,PIN_FLD_NUM_TOTAL_RECORDS,0,ebufp);
	if(vp)
	{
		total_count = *(int32 *)vp;
	}

	/********************************************************************
	 * Loop through the thread specific info and consolidate
	 ********************************************************************/
	
		rec_id = 0;
		cookie = NULL;
		count = 0;
		while ( (tmp_flistp = PIN_FLIST_ELEM_GET_NEXT(oper_flistp,
			PIN_FLD_THREAD_INFO,&rec_id,1,&cookie,ebufp)) != NULL ) 
		{
			vp = PIN_FLIST_FLD_GET(tmp_flistp,
				PIN_FLD_SUCCESSFUL_RECORDS,0,ebufp);
			if(vp)
			{
				success_count += *(int32 *)vp;
			}
			vp = PIN_FLIST_FLD_GET(tmp_flistp,
				PIN_FLD_FAILED_RECORDS,0,ebufp);
			if(vp)
			{	
				failed_count += *(int32 *)vp;
			}
		} /* While close */
		PIN_FLIST_FLD_SET(oper_flistp, PIN_FLD_SUCCESSFUL_RECORDS,
			&success_count,ebufp);
		PIN_FLIST_FLD_SET(oper_flistp, PIN_FLD_FAILED_RECORDS,
			&failed_count,ebufp);
		PIN_FLIST_FLD_SET(oper_flistp, PIN_FLD_PROCESS_END_T,
			&now_t,ebufp);

		pin_mta_global_flist_node_release(PIN_FLD_OPERATION_INFO,ebufp);

	return;
}


/*******************************************************************
 * Initialization of worker thread
 * Called prior MTA_WORKER_INIT policy opcode
 *******************************************************************/

PIN_EXPORT void 
pin_mta_worker_init (
	pcm_context_t	*ctxp,
	pin_flist_t	*ti_flistp,
	pin_errbuf_t	*ebufp)
{
	void		*vp = NULL;

	if (PIN_ERR_IS_ERR(ebufp)) 
	{
		return;
	}
	PIN_ERRBUF_CLEAR (ebufp);

	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG, 
		"\n\n pin_mta_worker_init thread info flist \n", ti_flistp);

	/*****************************************************************
	 * Add the ARA specific fields to the local thread info flist
	 ****************************************************************/
	
	vp = (void *)NULL;
	PIN_FLIST_FLD_SET(ti_flistp, PIN_FLD_SUCCESSFUL_RECORDS, vp, ebufp);
	PIN_FLIST_FLD_SET(ti_flistp, PIN_FLD_FAILED_RECORDS, vp, ebufp);
	PIN_FLIST_FLD_SET(ti_flistp, PIN_FLD_NUM_TOTAL_RECORDS, vp, ebufp);

	if (PIN_ERR_IS_ERR(ebufp)) 
	{
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, 
			"pin_mta_worker_init error", ebufp);
	}

	return;

}

/*******************************************************************
 * Function called when new job is avaialble to worker
 * Called prior MTA_WORKER_JOB policy opcode
 *******************************************************************/

PIN_EXPORT void 
pin_mta_worker_job (
	pcm_context_t		*ctxp,
	pin_flist_t		*srch_res_flistp,
	pin_flist_t		**op_in_flistpp,
	pin_flist_t		*ti_flistp,
	pin_errbuf_t		*ebufp)
{
	void			*vp = NULL;	
	int32			rec_id = 0;
	int			opflag = 0;
	int32			mta_flags = 0;	
	time_t			*end_tp = NULL;
	time_t			*when_tp = NULL;
	pin_flist_t		*flistp = NULL;
	pin_flist_t		*app_flistp = NULL;
	pin_cookie_t		cookie = NULL;
	pin_schedule_status_t	*statusp = NULL;

	if (PIN_ERR_IS_ERR(ebufp)) 
	{
		return;
	}
	PIN_ERRBUF_CLEAR (ebufp);

	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG, 
		"\n\n pin_mta_worker_job: search results flist\n",
		srch_res_flistp);

	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG,
		"\n\n pin_mta_worker_job thread info flist", ti_flistp);

	app_flistp = pin_mta_global_flist_node_get_with_lock(
			PIN_FLD_APPLICATION_INFO, ebufp);
	vp = PIN_FLIST_FLD_GET(app_flistp,PIN_FLD_FLAGS,0,ebufp);	
	if(vp)
	{
		mta_flags = *(int *)vp;
	}
	pin_mta_global_flist_node_release(PIN_FLD_APPLICATION_INFO, ebufp);


	if (mta_flags & MTA_FLAG_TEST_MODE) 
	{
		opflag |= PCM_OPFLG_CALC_ONLY;
	}

	*op_in_flistpp = PIN_FLIST_COPY (srch_res_flistp, ebufp);

	vp = (void *) DEX_PROCESS_ORDER_MTA_PROG;
	PIN_FLIST_FLD_SET(*op_in_flistpp,PIN_FLD_PROGRAM_NAME,vp,ebufp);
		
	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG,
		"\n\n pin_mta_worker_job prepared flist for main opcode \n",
		*op_in_flistpp);

	if (PIN_ERR_IS_ERR(ebufp)) 
	{
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
		"pin_mta_worker_job error", ebufp);
	}
	return;
}


/*******************************************************************
 * Main application opcode is called here
 *******************************************************************/

PIN_EXPORT void 
pin_mta_worker_opcode (
	pcm_context_t		*ctxp,
	pin_flist_t		*srch_res_flistp,
	pin_flist_t		*op_in_flistp,
	pin_flist_t		**op_out_flistpp,
	pin_flist_t		*ti_flistp,
	pin_errbuf_t		*ebufp)
{
	void		*vp = NULL;
	int			opcode = 0;
	//int			cnt = 0;
	int			success_count = 0;
	int			failed_count = 0;
	int32			mta_flags = 0;
	pin_flist_t		*app_flistp = NULL;
	int32			opflag = 0;
	//char			msg[512];
    pin_cookie_t    cookie = 0;
    int             elem_id = 0;
	//pin_errbuf_t	ebuf1;
    pin_flist_t		*flistp = NULL; 
    pin_flist_t		*o_flistp = NULL; 
	int64			poid_id = 1;
	poid_t			*o_pdp = NULL;


	/*
	 * Call the DEX_OP_PROCESS_ORDER
	 */
	if (PIN_ERR_IS_ERR(ebufp)) 
	{
		return;
	}
	PIN_ERRBUF_CLEAR (ebufp);


	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG, 
		"\n\n pin_mta_worker_opcode search results flist \n", 
		srch_res_flistp);

	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG, 
		"\n\n pin_mta_worker_opcode prepared flist for main opcode \n", 
		op_in_flistp);

	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG, 
		"\n\n pin_mta_worker_opcode thread info flist \n", 
		ti_flistp);

	app_flistp = pin_mta_global_flist_node_get_with_lock (
		PIN_FLD_APPLICATION_INFO, ebufp);
    flistp = PIN_FLIST_ELEM_GET_NEXT(app_flistp, PIN_FLD_PARAMS,
		&elem_id, 1, &cookie, ebufp);
	vp = PIN_FLIST_FLD_GET (flistp, PIN_FLD_FLAGS, 1, ebufp);
	if(vp)
	{
		mta_flags = *((int32*)vp);
	}
	pin_mta_global_flist_node_release (PIN_FLD_APPLICATION_INFO, ebufp);

	opcode = DEX_OP_ORDER_PROCESS_TOFULFILL;
	vp = PIN_FLIST_FLD_GET(ti_flistp,PIN_FLD_SUCCESSFUL_RECORDS,0,ebufp);
	if(vp)
	{
		success_count = *(int32 *)vp;
	}
	vp = PIN_FLIST_FLD_GET(ti_flistp,PIN_FLD_FAILED_RECORDS,0,ebufp);
	if(vp!=NULL)
	{
		failed_count = *(int32 *)vp;
	}
	
	PIN_ERR_CLEAR_ERR(ebufp);
	PCM_OP(ctxp, opcode, opflag, op_in_flistp, op_out_flistpp, ebufp);

	PIN_ERR_LOG_FLIST (PIN_ERR_LEVEL_DEBUG, 
		"\n\n pin_mta_worker_opcode output flist from main opcode \n", 
		*op_out_flistpp);

	elem_id = 0;
	cookie = NULL;
	while(o_flistp = PIN_FLIST_ELEM_GET_NEXT(*op_out_flistpp, DEX_FLD_ORDER_ITEMS,
                &elem_id, 1, &cookie, ebufp))
    {
		vp = PIN_FLIST_FLD_GET(o_flistp, PIN_FLD_RESULT, 1,ebufp);
		if(vp && (*(int32 *)vp == 0 )){
			success_count += 1;
			PIN_FLIST_FLD_SET(ti_flistp, PIN_FLD_FAILED_RECORDS, &failed_count, ebufp);
			PIN_FLIST_FLD_SET(ti_flistp, PIN_FLD_SUCCESSFUL_RECORDS, &success_count, ebufp);
		} else {
			failed_count += 1;
			PIN_FLIST_FLD_SET(ti_flistp, PIN_FLD_FAILED_RECORDS, &failed_count, ebufp);
			PIN_FLIST_FLD_SET(ti_flistp, PIN_FLD_SUCCESSFUL_RECORDS, &success_count, ebufp);
		}
	}

	o_pdp = PIN_FLIST_FLD_GET(op_in_flistp, PIN_FLD_POID, 0,ebufp);
	poid_id = PIN_POID_GET_ID(o_pdp);
	/***************************************************
 	* Set g_poid_id0 to the largest value
 	* Use mutex control to avoid concurrent writes.
  	***************************************************/
	pthread_mutex_lock(&lock);
	if (PIN_ERR_IS_ERR(ebufp)) {
        PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR, "pthread_mutex_lock error", ebufp);
	}
	if (poid_id > g_poid_id0) {
        g_poid_id0 = poid_id;
	}
	pthread_mutex_unlock(&lock);  
	
	return;

}

/* Get the command line params from the param_flistp and populate into the app_flistp.
 * Loop through the Array(param_flistp) and get all the command line parameters from
 * PARAM_FLD_NAME and PARAM_FLD_VALUE fields and assign it to the app_flistp custom_info field.
 */
void populate_cmd_line_args(param_flistp,app_flistp,ebufp)
	pin_flist_t     *param_flistp;
	pin_flist_t     *app_flistp;
	pin_errbuf_t    *ebufp;
{
	int32           err = 0;
	void            *vp = NULL;
	int         rec_id = 0;
	int32           mta_flags = 0;
	pin_flist_t     *flistp = NULL;
	pin_flist_t     *sub_flistp = NULL;
	pin_cookie_t        cookie = 0;
	pin_cookie_t        prev_cookie = 0;
	time_t          start_t = 0;
	time_t          end_t = 0;
	
	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);
	vp = PIN_FLIST_FLD_GET (app_flistp, PIN_FLD_FLAGS, 1,ebufp);
	if(vp)
	{
		mta_flags = *((int32*)vp);
	}
	mta_flags = mta_flags | MTA_FLAG_VERSION_NEW;

	PIN_FLIST_FLD_SET (app_flistp, PIN_FLD_FLAGS, &mta_flags, ebufp);
	flistp = PIN_FLIST_ELEM_ADD(app_flistp,PIN_FLD_PARAMS,0,ebufp);
	while ( (sub_flistp = (PIN_FLIST_ELEM_GET_NEXT(param_flistp,PIN_FLD_PARAMS,&rec_id,1,&cookie,ebufp))) != NULL ) {
		vp = PIN_FLIST_FLD_GET(sub_flistp,PIN_FLD_PARAM_NAME,1,ebufp);
		if(vp) {
			if ((strcmp((char *)vp,"-retry")==0)) {
				vp = PIN_FLIST_FLD_GET(flistp, PIN_FLD_FLAGS,1,ebufp);
				if(vp) {
					mta_flags = *((int32*)vp);
				}
				mta_flags |= PROCESS_ORDER_FLAG_RETRY ;
				PIN_FLIST_FLD_SET(flistp,PIN_FLD_FLAGS,&mta_flags,ebufp);
				PIN_FLIST_ELEM_DROP(param_flistp,PIN_FLD_PARAMS,rec_id,ebufp);
				cookie = prev_cookie;
				continue;
			}
			prev_cookie = cookie ;
		}
	}

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,"\nApplication flist from the cmd_line function\n\n",app_flistp);
    if (PIN_ERR_IS_ERR(ebufp)) {
    	PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,"pin_mta_config error", ebufp);
	}
	return;
}

