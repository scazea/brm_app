/********************************************************************
 *
 *      @(#)%Portal Version: dex_process_oder.h:
 *
 *      Copyright (c) 1999 - 2006 Portal Software, Inc. All rights reserved.
 *
 *      This material is the confidential property of
 *      Portal Software, Inc. or its subsidiaries or licensors and
 *      may be used, reproduced, stored or transmitted only in  accordance
 *      with a valid Portal license or sublicense agreement.
 *
 *******************************************************************/

#ifndef _DEX_PROCESS_ORDER_H_
#define _DEX_PROCESS_ORDER_H_

#ifdef __cplusplus
extern "C" {
#endif

#define PIN_MTA_PC_PROG_PROCESS_ORDER "dex_process_order"
#define DEX_PROCESS_ORDER_DEADLOCK_RETRY	"deadlock_retry_count"
#define DEX_PROCESS_ORDER_MTA_PROG		"dex_process_order"

#define ORDER_STATUS_ERROR		"status_error"
#define ORDER_STATUS_FULFILL		"status_fulfill"
#define PROCESS_ORDER_FLAG_RETRY         0x00002000

#define SRV_POID_LIST_ALLOC_SIZE	256

#ifdef __cplusplus
}
#endif

#endif /*_DEX_PROCESS_ORDER_H_*/
