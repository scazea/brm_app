/*******************************************************************
 *
 * @(#)%Portal Version: fm_gsm_aaa_pol_acc_on_off_search.c:PortalBase7.3.1Int:1:2007-Jun-15 09:36:03 %
 *
 * Copyright (c) 2004, 2009, Oracle and/or its affiliates.All rights reserved. 
 *
 * This material is the confidential property of Oracle Corporation or its 
 * licensors and may be used, reproduced, stored or transmitted only in 
 * accordance with a valid Oracle license or sublicense agreement.
 *
 *
 *******************************************************************/

#ifndef lint
static  char Sccs_Id[] = "@(#)%Portal Version: fm_gsm_aaa_pol_acc_on_off_search.c:PortalBase7.3.1Int:1:2007-Jun-15 09:36:03 %";
#endif

#include <stdio.h>

#include <pin_errs.h>
#include <pinlog.h>
#include "pcm.h"
#include "ops/gsm_aaa.h"
#include "pin_tcf.h"
#include "pin_gsm_aaa.h"
#include "pin_asm.h"
#include "cm_fm.h"

#define FILE_SOURCE_ID  "fm_gsm_aaa_pol_acc_on_off_search.c(1.1)"

/*******************************************************************
 * Routines contained herein.
 *******************************************************************/

static void
fm_gsm_aaa_pol_acc_on_off_search(
	pcm_context_t		*ctxp,
	pin_flist_t		*in_flistp,
	pin_flist_t		**out_flistpp,
        pin_errbuf_t		*ebufp);
void
fm_tcf_aaa_utils_accounting_on_off_get_type_of_poid(
        poid_t      *pp,
        char        *obj_str) ;

void
fm_tcf_aaa_utils_accounting_on_off_prep_search_template(
        char      *type_of_poid,
        char      *template_str,
        time_t    *start_t);
 
/*******************************************************************
 * Main routine for the PCM_OP_GSM_AAA_POL_ACC_ON_OFF_SEARCH  command
 *******************************************************************/
void
op_gsm_aaa_pol_acc_on_off_search(
        cm_nap_connection_t	*connp,
	u_int			opcode,
        int32                   flags,
        pin_flist_t		*in_flistp,
        pin_flist_t		**ret_flistpp,
        pin_errbuf_t		*ebufp)
{

	pcm_context_t		*ctxp = connp->dm_ctx;

        /* To receive the Output from the main Function */

	pin_flist_t		*r_flistp = NULL;

        if (PIN_ERR_IS_ERR(ebufp)){
                return;
               /*******/
        }

	PIN_ERR_CLEAR_ERR(ebufp);

	/*
	 * Null out results until we have some.
	 */

	*ret_flistpp = NULL;

	/*
	 * Insanity check.
	 */

	if (opcode != PCM_OP_GSM_AAA_POL_ACC_ON_OFF_SEARCH) {

		pin_set_err(ebufp, PIN_ERRLOC_FM,
			PIN_ERRCLASS_SYSTEM_DETERMINATE,
			PIN_ERR_BAD_OPCODE, 0, 0, opcode);

		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"bad opcode in pcm_op_gsm_aaa_pol_acc_on_off_search", ebufp);

		return;
	}

        /*
         * Debug: what did we get?
         */

        PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
                "op_gsm_aaa_pol_acc_on_off_search input flist", in_flistp);



	/*
	 * Call main function to do it
	 */

	fm_gsm_aaa_pol_acc_on_off_search(ctxp, in_flistp, &r_flistp, ebufp);

	/*
	 * Results.
	 */

	if (PIN_ERR_IS_ERR(ebufp)) {

		*ret_flistpp = (pin_flist_t *)NULL;
		PIN_FLIST_DESTROY_EX(&r_flistp, NULL);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"pcm_op_gsm_aaa_pol_acc_on_off_search error", ebufp);
	} else {

		*ret_flistpp = r_flistp;
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
			"pcm_op_gsm_aaa_pol_acc_on_off_search return flist", r_flistp);
	}

	return;
}

/************************************************************************
 * fm_gsm_aaa_pol_acc_on_off_search()
 * This does the following functions
 * 1. Create a Search Flist
 * 2. As it is a helper Opcode, it Allows the End User to change the 
 *    search criteria.
 *
 ************************************************************************/
static void
fm_gsm_aaa_pol_acc_on_off_search(
	pcm_context_t		*ctxp,
	pin_flist_t		*in_flistp,
	pin_flist_t		**out_flistpp,
        pin_errbuf_t		*ebufp)
{

	/* These are used to create Search Template */

        poid_t                	*session_objp   		= NULL;
	char 			type_of_poid[MAX_STRING_LENGTH]	= {""};
        time_t                  *start_time          		= NULL;
        void                    *vp                             = NULL;

	/* These are used to create the Search FList */

        int32            	s_flags 			= SRCH_DISTINCT;
        char             	template_str[MAX_STRING_LENGTH] = {""};
        char             	msg[MAX_STRING_LENGTH] 		= {""};
        pin_flist_t      	*session_search_flistp 		= NULL;
        int64            	db_id 				= 0;
        poid_t           	*svc_pdp 			= NULL;
       	pin_flist_t      	*args_flistp 			= NULL;
       	pin_flist_t      	*args_flistp_origin_nw		= NULL;
        pin_flist_t      	*svc_flistp 			= NULL;

	/* These are set as a parameter of args[] in Search Flist */

        void                    *origin_nw                      = NULL;
        int32               	aso_created 			= PIN_ASO_STATUS_CREATED;
        int32               	aso_started 			= PIN_ASO_STATUS_STARTED;
        int32               	aso_updated 			= PIN_ASO_STATUS_UPDATED;

        session_objp  = (poid_t *) PIN_FLIST_FLD_GET(in_flistp,
                				     PIN_FLD_POID, 0, ebufp);

        origin_nw     = PIN_FLIST_FLD_GET(in_flistp, 
						  PIN_FLD_ORIGIN_NETWORK, 0, ebufp);

        vp            = PIN_FLIST_FLD_GET(in_flistp,
                                                   PIN_FLD_START_T, 1, ebufp) ;

        if(vp != NULL){

                start_time = (time_t* )vp;
        }

        /***********************************************************
         * Preparing the Template for Search 
         ***********************************************************/

	fm_tcf_aaa_utils_accounting_on_off_get_type_of_poid(session_objp, type_of_poid);

	fm_tcf_aaa_utils_accounting_on_off_prep_search_template(type_of_poid, template_str, start_time);

	sprintf(msg,"The Template String for Search is --> %s",template_str);

        PIN_ERR_LOG_MSG (PIN_ERR_LEVEL_DEBUG,msg);

        /***********************************************************
         * Set up for doing the session search based on the service
         * object.
         ***********************************************************/

        session_search_flistp = PIN_FLIST_CREATE (ebufp);

        db_id = PIN_POID_GET_DB(session_objp);

        svc_pdp = (poid_t *)PIN_POID_CREATE(db_id, "/search", -1, ebufp);

        PIN_FLIST_FLD_PUT(session_search_flistp, PIN_FLD_POID, (void *)svc_pdp,
                ebufp);

        PIN_FLIST_FLD_SET(session_search_flistp, PIN_FLD_FLAGS, (void *)&s_flags,
                ebufp);

        PIN_FLIST_FLD_SET(session_search_flistp, PIN_FLD_TEMPLATE,
                (void *) template_str, ebufp);


	/* F1 = V1 - ORIGIN_NETWORK match */

	args_flistp = PIN_FLIST_ELEM_ADD(session_search_flistp, PIN_FLD_ARGS, 1, ebufp);
	args_flistp_origin_nw = PIN_FLIST_ELEM_ADD(args_flistp, PIN_FLD_TELCO_INFO, 0, ebufp);

	PIN_FLIST_FLD_SET(args_flistp_origin_nw, PIN_FLD_ORIGIN_NETWORK, origin_nw, ebufp);



	/* F2 = V2 - STATUS = CREATED match */

	args_flistp = PIN_FLIST_ELEM_ADD(session_search_flistp, PIN_FLD_ARGS, 2, ebufp);

	PIN_FLIST_FLD_SET(args_flistp, PIN_FLD_STATUS, (void *)&aso_created, ebufp);



	/* F3 = V3 - STATUS = STARTED match */

	args_flistp = PIN_FLIST_ELEM_ADD(session_search_flistp, PIN_FLD_ARGS, 3, ebufp);

	PIN_FLIST_FLD_SET(args_flistp, PIN_FLD_STATUS, (void *)&aso_started, ebufp);

	/* F4 = V4 - STATUS = UPDATED match */

	args_flistp = PIN_FLIST_ELEM_ADD(session_search_flistp, PIN_FLD_ARGS, 4, ebufp);

	PIN_FLIST_FLD_SET(args_flistp, PIN_FLD_STATUS, (void *)&aso_updated, ebufp);



	if (start_time != NULL) {

		/* F5 = V5 - START_T match */
		args_flistp = PIN_FLIST_ELEM_ADD(session_search_flistp, PIN_FLD_ARGS, 5, ebufp);

		PIN_FLIST_FLD_SET(args_flistp, PIN_FLD_START_T, (void *)start_time, ebufp);
	}



        args_flistp = PIN_FLIST_ELEM_ADD(session_search_flistp, PIN_FLD_RESULTS, 0, ebufp);
        PIN_FLIST_FLD_SET(args_flistp, PIN_FLD_POID, (void *)NULL, ebufp);

        PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
                        "search flist created for GLOBAL_SEARCH", session_search_flistp);

	/*
	 * Create outgoing flist
	 */
	 
	*out_flistpp = session_search_flistp;

	/*
	 * Error?
	 */

	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"fm_gsm_aaa_pol_acc_on_off_search error", ebufp);
	}
	return;
}

/*******************************************************************
 * fm_tcf_aaa_utils_accounting_on_off_get_type_of_poid()
 *
 *  Get the object type from the given poid
 *
 * input:
 *   poid_t*: the poid
 *   char*  : the object type
 *
 *******************************************************************/
void
fm_tcf_aaa_utils_accounting_on_off_get_type_of_poid(
        poid_t      *pp,
        char        *obj_str)
{
        char   *poid_type 	        = NULL;
        char   p_str[PCM_MAX_POID_TYPE] = {""};
        char       *char_p = NULL;

        /* Get the Partial service string */

        poid_type = (char *) PIN_POID_GET_TYPE(pp);
        strcpy(p_str, poid_type);

        /* Get the object type */

        strtok_r(p_str, "/", &char_p);
        strtok_r(NULL, "/", &char_p);
        strcpy(obj_str , (const char *) strtok_r(NULL, "/", &char_p));
}

/*******************************************************************
 * fm_tcf_aaa_utils_accounting_on_off_prep_search_template()
 *
 *  Creates a search template_str for accounting on/off
 *
 * input:
 *   char* : the string that is appended
 *   char* : The String to be updates with the Template
 *   time_t: If present it acts as a criteria to set Template
 *
 *******************************************************************/
void
fm_tcf_aaa_utils_accounting_on_off_prep_search_template(
        char      *type_of_poid,
        char      *template_str,
        time_t    *start_t)
{

        /* Prepare the search string */

        strcpy(template_str, "select X from /active_session/telco/");
        strcat(template_str, type_of_poid);
	/* 
	 * start_t given in ACCOUNTING_ON/OFF is the time at which network element is re-started. This is 
	 * optional field. If start_t is provided then this search has to pick all the ASOs those are
	 * created after the last start of network elem. If start_t is not provided then search should
	 * pick all the ASOs. 
	 */ 
        if(start_t) {

                strcat(template_str, " where F1 = V1 and ( F2 = V2 or F3 = V3 or F4 = V4 ) and F5 >= V5 ") ;
        }
        else {

                strcat(template_str, " where F1 = V1 and ( F2 = V2 or F3 = V3 or F4 = V4 ) ") ;
        }
}
