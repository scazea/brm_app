/*
 * @(#)%Portal Version: fm_gsm_aaa_pol_authorize.c:WirelessVelocityInt:2:2006-Sep-14 11:16:54 %
 *
* Copyright (c) 2004, 2015, Oracle and/or its affiliates. All rights reserved.
 *
 * This material is the confidential property of Oracle Corporation or its 
 * licensors and may be used, reproduced, stored or transmitted only in 
 * accordance with a valid Oracle license or sublicense agreement.
 *
 */

#ifndef lint
static const char Sccs_id[] =
	"@(#) %full_filespec: fm_gsm_aaa_pol_authorize.c~3:csrc:1 %";
#endif
#define FILE_LOGNAME "fm_gsm_aaa_pol_authorize.c ( %version: 3 % )"

/*******************************************************************
 * Contains the functions used by gsm accounting opcodes
 *******************************************************************/

#include <stdio.h>
#include <time.h>
#include "ops/tcf.h"
#include "ops/gsm_aaa.h"
#include <pcm.h>
#include <cm_fm.h>
#include <pin_type.h>
#include <pin_errs.h>
#include <pinlog.h>

/*******************************************************************
 * Global routines contained within, see header (.h)
 *******************************************************************/
/******************************************************************
 * This opcode will create a unique AUTHORIZATION_ID if one is not 
 * provided by the switch for GSM sessions.
 *****************************************************************/
void
op_gsm_aaa_pol_authorize(
        cm_nap_connection_t     *connp,
        int32                   opcode,
        int32                   flags,
        pin_flist_t             *i_flistp,
        pin_flist_t             **o_flistpp,
        pin_errbuf_t            *ebufp)
{

	void		*vp    = NULL;
	char 		*calling_number = NULL;
	char 		*called_number  = NULL;
	char 		*origin_network = NULL;
	time_t 		*start_t      = NULL;
	char 		auth_id [PCM_MAXIMUM_STR];

	/***********************************************************
         * DEBUG: What did we get?
         ***********************************************************/
        PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
                "op_gsm_aaa_pol_authorize input flist",
                i_flistp);

	/* Copy the input flist to output flist */
	*o_flistpp = PIN_FLIST_COPY(i_flistp, ebufp);	

        /***********************************************************
         * Do the real work.
	 * 1. If the authorization Id is not provided in the input
	 *    flist, create the one by using the following parameters
	 *   - CALLING_NUMBER
	 *   - CALLED_NUMBER
	 *   - START_T
	 *   - ORIGIN_NETWORK
	 * These are the mandatory parameters to generate the unique
	 * authorization Id.
         ***********************************************************/
	vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_AUTHORIZATION_ID,
				1, ebufp);
	if (vp == NULL) {
		calling_number = PIN_FLIST_FLD_GET(i_flistp,
					PIN_FLD_CALLING_NUMBER, 0, ebufp);

		called_number  = PIN_FLIST_FLD_GET(i_flistp,
					PIN_FLD_CALLED_NUMBER, 0, ebufp);

		start_t = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_START_T,
					0, ebufp);

		origin_network = PIN_FLIST_FLD_GET(i_flistp,
					PIN_FLD_ORIGIN_NETWORK, 0, ebufp);

		if (calling_number && called_number
			&& start_t && origin_network) {
			pin_snprintf(auth_id, sizeof(auth_id),"%s-%s-%ld-%s", calling_number,
				called_number, *start_t, origin_network);
			PIN_FLIST_FLD_SET(*o_flistpp,
				PIN_FLD_AUTHORIZATION_ID, auth_id, ebufp);
		}
	}

        /***********************************************************
         * Results.
         ***********************************************************/
        if (PIN_ERR_IS_ERR(ebufp)) {
                PIN_FLIST_DESTROY_EX(o_flistpp, (pin_errbuf_t *)NULL);
                *o_flistpp = (pin_flist_t *)NULL;
                PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
                        "op_gsm_aaa_pol_authorize error",
                        ebufp);
        } else {
                PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
                        "op_gsm_aaa_pol_authorize return flist", *o_flistpp);
        }
        return;
}
