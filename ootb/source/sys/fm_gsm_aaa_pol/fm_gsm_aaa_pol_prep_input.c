/*******************************************************************
 *
 * @(#)%Portal Version: fm_gsm_aaa_pol_prep_input.c:PortalBase7.3.1Int:2:2008-Jan-23 03:45:35 %
 *
 * Copyright (c) 2005, 2009, Oracle and/or its affiliates.All rights reserved. 
 *
 * This material is the confidential property of Oracle Corporation or its 
 * licensors and may be used, reproduced, stored or transmitted only in 
 * accordance with a valid Oracle license or sublicense agreement.
 *
 *******************************************************************/

#ifndef lint
static  char Sccs_Id[] = "@(#)%Portal Version: fm_gsm_aaa_pol_prep_input.c:PortalBase7.3.1Int:2:2008-Jan-23 03:45:35 %";
#endif

#include <stdio.h>

#include <pcm.h>
#include <cm_fm.h>
#include <pin_errs.h>
#include <pinlog.h>

#include "ops/gsm_aaa.h"
#include "ops/tcf.h"

#include "pin_tcf.h"
#include "pin_gsm_aaa.h"
#include "pin_asm.h"
#include "pin_act.h"
#include "pin_reserve.h"
#include "pin_rate.h"

#define FILE_SOURCE_ID  "fm_gsm_aaa_pol_prep_input.c(1.6)"

#define CP_MANDATORY_FIELD(from, to, id, vp) {                          \
        (vp) = PIN_FLIST_FLD_GET((from), (id), 0, ebufp);               \
        PIN_FLIST_FLD_SET((to), (id), (void *)(vp), ebufp);             \
}

#define CP_MANDATORY_FIELD(from, to, id, vp) {                          \
        (vp) = PIN_FLIST_FLD_GET((from), (id), 0, ebufp);               \
        PIN_FLIST_FLD_SET((to), (id), (void *)(vp), ebufp);             \
}

#define CP_OPTIONAL_FIELD(from, to, id, vp) {                           \
	if ((from) != NULL) {                                           \
		(vp) = PIN_FLIST_FLD_GET((from), (id), 1, ebufp);       \
		if ((vp) != NULL)                                       \
                    PIN_FLIST_FLD_SET((to), (id), (void *)(vp), ebufp); \
	}                                                               \
}

#define CP_OPTIONAL_FIELD_AS(from, to, idfrom, idto, vp) {              \
        (vp) = PIN_FLIST_FLD_GET((from), (idfrom), 1, ebufp);           \
        if ((vp) != NULL)                                               \
                PIN_FLIST_FLD_SET((to), (idto), (void *)(vp), ebufp);   \
}

#define DROP_OPTIONAL_FIELD(flist, id, vp) {				\
	(vp) = PIN_FLIST_FLD_GET((flist), (id), 1, ebufp);		\
	if ((vp) != NULL)						\
		PIN_FLIST_FLD_DROP((flist), (id), ebufp);		\
}

#define CP_OPTIONAL_AGGREGATE_FIELD(i_flp, session_flp, fld, o_flp, vp) { \
        CP_OPTIONAL_FIELD(i_flp, o_flp, fld, vp);                         \
        if ((vp) == NULL)                                                 \
	    CP_OPTIONAL_FIELD(session_flp, o_flp, fld, vp);               \
}

/*******************************************************************
 * Routines contained herein.
 *******************************************************************/

static void
fm_gsm_aaa_pol_prep_input(
	pcm_context_t		*ctxp,
	pin_flist_t		*in_flistp,
	pin_flist_t		**o_flistpp,
        pin_errbuf_t		*ebufp);

static void
fm_gsm_aaa_pol_aggregate_time( 
	u_int		opcode,
	pin_flist_t     *i_flistp,
	pin_flist_t	*aso_flistp,
        pin_flist_t     **o_flistpp,
        pin_errbuf_t    *ebufp);

static void 
fm_gsm_aaa_pol_aggregate_volume( 
	u_int		opcode,
	pin_flist_t     *i_flistp,
	pin_flist_t	*aso_flistp,
        pin_flist_t     **o_flistpp,
        pin_errbuf_t    *ebufp);

static void 
fm_gsm_aaa_pol_aggregate_amount( 
	u_int		opcode,
	pin_flist_t     *i_flistp,
	pin_flist_t	*aso_flistp,
        pin_flist_t     **o_flistpp,
        pin_errbuf_t    *ebufp);

static void
fm_gsm_aaa_pol_populate_telco_info( 
	pin_flist_t     *i_flistp,
	pin_flist_t	*aso_flistp,
        pin_flist_t     **o_flistpp,
        pin_errbuf_t    *ebufp); 

static void
fm_gsm_aaa_pol_populate_service_codes(
	pin_flist_t     *i_flistp,
	pin_flist_t	*aso_flistp,
        pin_flist_t     **o_flistpp,
        pin_errbuf_t    *ebufp); 

static void
fm_gsm_aaa_pol_populate_gsm_info(
	u_int		opcode,
	pin_flist_t     *i_flistp,
	pin_flist_t	*aso_flistp,
        pin_flist_t     **o_flistpp,
        pin_errbuf_t    *ebufp); 

static void
fm_gsm_aaa_pol_populate_rating_info(
	u_int		opcode,
        pin_flist_t     *i_flistp,
        pin_flist_t     **o_flistpp,
        pin_errbuf_t    *ebufp); 

poid_t*
fm_gsm_aaa_pol_create_session_poid(
        pin_flist_t     *i_flistp,
        pin_errbuf_t    *ebufp);

time_t
fm_gsm_aaa_pol_get_least_start_time(
	pin_flist_t     *i_flistp,
	pin_flist_t	*aso_flistp,
        pin_errbuf_t    *ebufp); 

time_t
fm_gsm_aaa_pol_get_max_end_time(
	pin_flist_t     *i_flistp,
	pin_flist_t	*aso_flistp,
        pin_errbuf_t    *ebufp); 

/*******************************************************************
 * Main routine for the PCM_OP_GSM_AAA_POL_PREP_INPUT opcode
 *******************************************************************/

void
op_gsm_aaa_pol_prep_input(
        cm_nap_connection_t	*connp,
	u_int			opcode,
        int32                   flags,
        pin_flist_t		*in_flistp,
        pin_flist_t		**ret_flistpp,
        pin_errbuf_t		*ebufp)
{
	pcm_context_t		*ctxp 		= connp->dm_ctx;
	pin_flist_t		*r_flistp 	= NULL;

	if (PIN_ERR_IS_ERR(ebufp))
		return;

	PIN_ERR_CLEAR_ERR(ebufp);

	/*
	 * Null out results until we have some.
	 */
	*ret_flistpp = NULL;

	/*
	 * Insanity check.
	 */
	if (opcode != PCM_OP_GSM_AAA_POL_PREP_INPUT) {
		pin_set_err(ebufp, PIN_ERRLOC_FM,
			PIN_ERRCLASS_SYSTEM_DETERMINATE,
			PIN_ERR_BAD_OPCODE, 0, 0, opcode);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"bad opcode in pcm_op_gsm_aaa_pol_prep_input", ebufp);
		return;
	}

	/*
	 * Debug: what did we get?
	 */
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"pcm_op_gsm_aaa_pol_prep_input input flist", in_flistp);

	/*
	 * Call main function to do it
	 */
	fm_gsm_aaa_pol_prep_input(ctxp, in_flistp, &r_flistp, ebufp);

	/*
	 * Results.
	 */
	if (PIN_ERR_IS_ERR(ebufp)) {
		*ret_flistpp = (pin_flist_t *)NULL;
		PIN_FLIST_DESTROY_EX(&r_flistp, NULL);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"pcm_op_gsm_aaa_pol_prep_input error", ebufp);
	} else {
		*ret_flistpp = r_flistp;
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
			"pcm_op_gsm_aaa_pol_prep_input return flist", r_flistp);
	}

	return;
}

/*******************************************************************
 * fm_gsm_aaa_pol_prep_input()
 *
 * DESCRIPTION:
 *
 * Routine to prepare the input for the AAA opcode processing.
 *
 * 1. Aggregates the input and existing session information (from ASO).
 *
 * 2. Amount, time and volume based aggregations are included.
 *
 * 3. Output flist contains all the information to process
 *    the corresponding the AAA opcode.
 *
 * 4. The mode of input (INCREMENTAL or AGGREGATED) is specified 
 *    through the flag PIN_FLD_AGGREGATE_MODE.
 *
 * ASSUMPTIONS:
 *
 * 1. Default policy assumes the input values are in incremental form. 
 *    For example, BYTES_IN and BYTES_OUT provided in the input are
 *    assumed to be incremental, which are cumulated in the output
 *    and is inturn replaced in ASO later. This Policy takes care of 
 *    custom aggregation even if the input is aggregated.
 *
 * ARGUMENTS:
 *
 * ctxp         - Context pointer
 * i_flistp     - Input Flist pointer.
 * o_flistpp    - Output Flist to be returned.
 * ebufp        - Error buffer.
 *
 * INPUT FLIST:
 *
 * PIN_FLD_POID
 * PIN_FLD_ACTIVE_SESSION_ID
 * PIN_FLD_PROGRAM_NAME
 * PIN_FLD_OPCODE
 * PIN_FLD_START_T
 * PIN_FLD_QUANTITY
 * PIN_FLD_AMOUNT
 * PIN_FLD_UNIT
 * PIN_FLD_END_T
 * PIN_FLD_TIMEZONE_ID
 * PIN_FLD_SESSION_ID
 * PIN_FLD_USAGE_TYPE
 * PIN_FLD_RESERVATION_OBJ (Type of Reservation for AUTH/REAUTH)
 * PIN_FLD_EXPIRATION_T (Expiration time of reservation for AUTH/REAUTH)
 * PIN_FLD_TELCO_INFO (??)
 * PIN_FLD_EXTENDED_INFO
 * - PIN_FLD_SERVICE_CODES
 * - PIN_FLD_GSM_INFO
 * PIN_FLD_DELETED_FLAG
 * PIN_FLD_SERVICE_INFO
 * PIN_FLD_CONFIG_INFO
 * PIN_FLD_SESSION_INFO
 * PIN_FLD_RESERVATION_INFO
 * PIN_FLD_AGGREGATE_MODE
 * PIN_FLD_RATING_MODE
 *
 * OUTPUT FLIST:
 *
 * PIN_FLD_POID (M)
 * PIN_FLD_ACTIVE_SESSION_ID (M)
 * PIN_FLD_ACCOUNT_OBJ (M)
 * PIN_FLD_SERVICE_OBJ (M)
 * PIN_FLD_START_T (O)
 * PIN_FLD_END_T (O)
 * PIN_FLD_TIMEZONE_ID (O)
 * PIN_FLD_USAGE_TYPE (O)
 * PIN_FLD_RESERVATION_OBJ (Type of Reservation for AUTH/REAUTH)
 * PIN_FLD_EXPIRATION_T (Expiration time of reservation for AUTH/REAUTH)
 * PIN_FLD_PROGRAM_NAME (M)
 * PIN_FLD_SESSION_ID (O)
 * PIN_FLD_EXTENDED_INFO (M)
 * - PIN_FLD_TELCO_INFO (M)
 * - PIN_FLD_SERVICE_CODES (M)
 * - PIN_FLD_GSM_INFO (M)
 * PIN_FLD_RATING_INFO (O) - For Auth/Reauth only.
 * -PIN_FLD_MIN_QUANTITY
 * -PIN_FLD_BALANCES (For Amount based)
 * -- PIN_FLD_AMOUNT
 * -PIN_FLD_EVENT (For Qty based)
 * -- PIN_FLD_POID
 * -- PIN_FLD_PROGRAM_NAME
 * -- PIN_FLD_ACCOUNT_OBJ
 * -- PIN_FLD_SERVICE_OBJ
 * -- PIN_FLD_START_T
 * -- PIN_FLD_END_T
 * -- PIN_FLD_TIMEZONE_ID
 * -- PIN_FLD_USAGE_TYPE
 * -- PIN_FLD_EXTENDED_INFO
 * ---PIN_FLD_TELCO_INFO
 * ---PIN_FLD_SERVICE_CODES
 * ---PIN_FLD_GSM_INFO
 *
 * RETURN VALUES:
 *
 * NONE
 *******************************************************************/
static void
fm_gsm_aaa_pol_prep_input(
        pcm_context_t           *ctxp,
        pin_flist_t             *i_flistp,
        pin_flist_t             **o_flistpp,
        pin_errbuf_t            *ebufp)
{

	int64			   db			= 0;
        int32                      elem_id              = 0;
        int32                      aaa_opcode           = 0;
        pin_cookie_t               cookie               = NULL;
        pin_flist_t                *aso_flistp          = NULL;
        pin_flist_t                *svc_flistp          = NULL;
        pin_flist_t                *r_flistp            = NULL;
        pin_flist_t                *reserve_flistp      = NULL;
        pin_flist_t                *ext_info_flistp     = NULL;
        pin_flist_t                *in_gsm_info_flistp  = NULL;
        pin_flist_t                *aso_gsm_info_flistp = NULL;
        pin_flist_t                *config_flistp	= NULL;
        pin_flist_t                *telco_info_flistp	= NULL;
        poid_t			   *poidp		= NULL;
        void			   *vp 			= NULL;

	aso_flistp = PIN_FLIST_ELEM_GET_NEXT(i_flistp, PIN_FLD_SESSION_INFO,
				&elem_id, 1, &cookie, ebufp);

	svc_flistp = PIN_FLIST_SUBSTR_GET(i_flistp, PIN_FLD_SERVICE_INFO,
				0, ebufp);

	r_flistp = PIN_FLIST_CREATE(ebufp);

	/* Add ASO base fields. */
	if (aso_flistp != NULL) {
		poidp = PIN_FLIST_FLD_GET(aso_flistp, PIN_FLD_POID, 0, ebufp);
	}
	else {
		poidp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_POID, 0, ebufp);
	}

	PIN_FLIST_FLD_SET(r_flistp, PIN_FLD_POID, poidp, ebufp);

	vp = PIN_FLIST_FLD_GET(svc_flistp, PIN_FLD_POID, 0, ebufp);
	PIN_FLIST_FLD_SET(r_flistp, PIN_FLD_SERVICE_OBJ, vp, ebufp);

	vp = PIN_FLIST_FLD_GET(svc_flistp, PIN_FLD_ACCOUNT_OBJ, 0, ebufp);
	PIN_FLIST_FLD_SET(r_flistp, PIN_FLD_ACCOUNT_OBJ, vp, ebufp);

	vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_PROGRAM_NAME, 0, ebufp);
	PIN_FLIST_FLD_SET(r_flistp, PIN_FLD_PROGRAM_NAME, vp, ebufp);

	vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_ACTIVE_SESSION_ID, 0, ebufp);
	PIN_FLIST_FLD_SET(r_flistp, PIN_FLD_ACTIVE_SESSION_ID, vp, ebufp);

	CP_OPTIONAL_AGGREGATE_FIELD(i_flistp, aso_flistp,
			PIN_FLD_TIMEZONE_ID, r_flistp, vp);
	
	CP_OPTIONAL_AGGREGATE_FIELD(i_flistp, aso_flistp,
			PIN_FLD_USAGE_TYPE, r_flistp, vp);

	/* Copy the SESSION_ID to return flist if passed in input */
	CP_OPTIONAL_FIELD(i_flistp, r_flistp, PIN_FLD_SESSION_ID, vp);


	/* Add RESERVATION_LIST from ASO in the output. */
	if (aso_flistp != NULL) {
		cookie = NULL;
		elem_id = 0;
		while ((reserve_flistp = PIN_FLIST_ELEM_GET_NEXT(aso_flistp,
			PIN_FLD_RESERVATION_LIST, &elem_id,
			1, &cookie, ebufp)) != NULL) {

			PIN_FLIST_ELEM_SET(r_flistp, reserve_flistp,
				PIN_FLD_RESERVATION_LIST, elem_id, ebufp);
		}
	}

	vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_OPCODE, 1, ebufp);
	if (vp != NULL) {
		aaa_opcode = *(int32*) vp;
	}
        
        /* Incase of STOP/CANCEL, pass the DELETED_FLAG from the Config Info. */

        if ((PCM_OP_TELCO_CANCEL_AUTHORIZATION == aaa_opcode) ||
            (PCM_OP_TELCO_STOP_ACCOUNTING == aaa_opcode)) {

                /*
                 * Get the CONFIG_INFO from the Input Flist. 
                 */
                config_flistp = PIN_FLIST_SUBSTR_GET(i_flistp, 
                                    PIN_FLD_CONFIG_INFO, 1, ebufp);

                if (config_flistp != NULL) {

                	/*
                 	 * Get the TELCO_INFO from the CONFIG_INFO 
                 	 */
			telco_info_flistp = PIN_FLIST_SUBSTR_GET(config_flistp,
						PIN_FLD_TELCO_INFO, 0, ebufp);
                	/*
                 	 * Copy the DELETED_FLAG from the TELCO_INFO in Output Flist
                 	 */

                	CP_MANDATORY_FIELD(telco_info_flistp, r_flistp, 
                                          PIN_FLD_DELETED_FLAG, vp);
                }
        }

	/* Nothing to populate if we are cancelling the authorization. */
	if (PCM_OP_TELCO_CANCEL_AUTHORIZATION == aaa_opcode) {
		goto cleanup;
	}

	/* Aggregate the Timestamps present in ASO and Input. */
	fm_gsm_aaa_pol_aggregate_time(aaa_opcode, i_flistp, aso_flistp,
			&r_flistp, ebufp);

	/* Aggregate the Amount for an AMOUNT based request */
	fm_gsm_aaa_pol_aggregate_amount(aaa_opcode, i_flistp, aso_flistp,
			&r_flistp, ebufp);

	/*
	 * Prepare the EXTENDED_INFO for the extensions.
	 * FM_ACT opcodes need INHEIRTED_INFO for START/UPDATE/STOP,
	 * and EXTENDED_INFO for AUTH/REAUTH opcodes.
	 */
	if (PCM_OP_TELCO_AUTHORIZE == aaa_opcode ||
		PCM_OP_TELCO_REAUTHORIZE == aaa_opcode) {
		ext_info_flistp = PIN_FLIST_SUBSTR_ADD(r_flistp,
					PIN_FLD_EXTENDED_INFO, ebufp);
	}
	else {
		ext_info_flistp = PIN_FLIST_SUBSTR_ADD(r_flistp,
					PIN_FLD_INHERITED_INFO, ebufp);
	}

	/* Populate the TELCO_INFO from ASO & Input. */
	fm_gsm_aaa_pol_populate_telco_info(i_flistp, aso_flistp,
			&ext_info_flistp, ebufp);

	/* Populate the SERVICE_CODES from ASO & Input. */
	fm_gsm_aaa_pol_populate_service_codes(i_flistp, aso_flistp,
			&ext_info_flistp, ebufp);

	/* Populate the GSM_INFO substruct from ASO and Input. */
	fm_gsm_aaa_pol_populate_gsm_info(aaa_opcode, i_flistp, aso_flistp,
			&ext_info_flistp, ebufp);

	/* Incase of AUTH/REAUTH, add RATING_INFO substruct. */
	if ((PCM_OP_TELCO_AUTHORIZE == aaa_opcode) ||
	    (PCM_OP_TELCO_REAUTHORIZE == aaa_opcode)) {

		fm_gsm_aaa_pol_populate_rating_info(aaa_opcode, i_flistp,
							&r_flistp, ebufp);

                CP_OPTIONAL_FIELD(i_flistp, r_flistp, PIN_FLD_RESERVATION_OBJ, vp);
                if (NULL == vp) {
                        vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_POID, 0, ebufp);
                        db = PIN_POID_GET_DB(vp);
                        vp = PIN_POID_CREATE(db, PIN_OBJ_TYPE_RESERVATION_ACTIVE, -1, ebufp);
                        PIN_FLIST_FLD_PUT(r_flistp, PIN_FLD_RESERVATION_OBJ, vp, ebufp);
                }

		CP_OPTIONAL_FIELD(i_flistp, r_flistp, PIN_FLD_EXPIRATION_T, vp);
	}

cleanup:

        /*
         * Error?
         */
	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"fm_gsm_aaa_pol_prep_input error", ebufp);
		PIN_FLIST_DESTROY_EX(&r_flistp, NULL);
		*o_flistpp = NULL;
	}
	else {
		*o_flistpp = r_flistp;
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
			"fm_gsm_aaa_pol_prep_input output flist",
			*o_flistpp);
	}

        return;
}

/*******************************************************************
 * fm_gsm_aaa_pol_aggregate_time()
 *
 * DESCRIPTION:
 *
 * 1. Aggregates the Timestamp with the values in the input
 *    and ASO.
 *
 * 2. Following are the Aggregation Criteria w.r.t different AAA
 *    opcodes.
 *
 *    General:
 *         If ASO state is CREATED and START_T is supplied
 *         in the input, then override with the input START_T
 *
 *    Authorize:
 *	   If START_T is specified in the input, this will be used
 *         else current pin_virtual_time is assumed.
 *         This START_T will be assigned to START_T in ASO 
 *
 *    Reauthorize:
 *         Failover Mode 
 *         If ASO is not found and if START_T is provided in the input, 
 *         then set it to ASO START_T else use current pin_virtual_time. 
 *    Reauthorize:
 *	   If START_T is specified in the input and the ASO is present,
 *         take the least of the two.
 *         Failover Mode: 
 *         If ASO is not found and if START_T is provided in the input, 
 *         then set it to ASO START_T else use current pin_virtual_time. 
 *
 *    NOTE:
 *	Here, only used information is aggregated and
 *      Rating information is prepared by considering
 *      the RESERVATION_INFO in the function
 *	fm_gsm_aaa_pol_populate_rating_info().
 *
 *    Start Accounting:
 *      a. Override the START_T from the input.
 *
 *    Update Accounting/Stop Accounting:
 *      a. If START_T is specified, and is smaller than the START_T
 *         present in ASO, override the START_T from the input.
 *
 *      b. If END_T is specified, and is greater than the END_T
 *         present in ASO, override the END_T from input.
 *
 *      c. If END_T is not specified and QUANTITY is specified,
 *         use QUANTITY to calculate the END_T
 *
 *      d. If both END_T and QUANTITY are specified,
 *         END_T takes a precedence over QUANTITY
 *
 *      e. If 'c' is the scenario and the input mode is AGGREGATED,
 *         and start_t != 0, then end_t is calculated as start_t + qty.
 *         if start_t == 0, the end_t is set to pin_virtual_time and
 *         start_t is calculated as end_t - qty.
 *
 *    Cancel Authorize:
 *       None.
 *
 * ARGUMENTS:
 *
 * opcode       - AAA opcode calling this opcode.
 * i_flistp     - Input Flist pointer.
 * aso_flistp   - ASO Flist pointer.
 * o_flistpp    - Output Flist to be returned.
 * ebufp        - Error buffer.
 *
 * NOTE:
 *
 * Output flist memory is allocated by the caller.
 *
 * RETURN VALUES:
 *
 * None
 *
 ******************************************************************/
static void
fm_gsm_aaa_pol_aggregate_time(
        u_int           opcode,
        pin_flist_t     *i_flistp,
        pin_flist_t     *aso_flistp,
        pin_flist_t     **o_flistpp,
        pin_errbuf_t    *ebufp)
{
	pin_flist_t	*cfg_reserve_info_flistp 	= NULL;
	time_t		start_t				= 0;
	time_t		end_t				= 0;
	time_t		old_end_t			= 0;
	time_t		new_end_t			= 0;
	time_t		old_start_t			= 0;
	time_t		new_start_t			= 0;
	time_t		req_start_t			= 0;
	time_t		req_end_t			= 0;
	int32		qty				= 0;
	void		*vp				= NULL;
	int32		*statusp			= NULL;
	int32		agg_mode			= USED_INCR_MODE; 

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_gsm_aaa_pol_aggregate_time input flist",
		i_flistp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_gsm_aaa_pol_aggregate_time ASO flist",
		aso_flistp);

        vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_AGGREGATE_MODE, 1, ebufp);
        if (vp != NULL) {
                agg_mode = *((int32 *)vp);
        }

	/*
	 * General: Always, if START_T is present in the input and ASO status
	 * is CREATED, override the START_T in ASO with the value from input.
	 */
	vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_START_T, 1, ebufp);
	if (vp != NULL) {
		if (aso_flistp != NULL) {
			statusp = PIN_FLIST_FLD_GET(aso_flistp, PIN_FLD_STATUS, 0, ebufp);
			if (PIN_ASO_STATUS_CREATED == *statusp) {
				PIN_FLIST_FLD_SET(aso_flistp, PIN_FLD_START_T, vp, ebufp);
			}
		}
		else {
			PIN_FLIST_FLD_SET(*o_flistpp, PIN_FLD_START_T, vp, ebufp);
		}
	}

	if (PCM_OP_TELCO_AUTHORIZE == opcode) {

		/* Do we have START_T in the input? */
		vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_START_T, 1, ebufp);
		if (NULL == vp) {
			/* Assume pin_virtual_time for authorizing. Later
			 * we will get real START_T.
			 * TODO: How can we take care of Timezone here?.
			 */
			start_t = pin_virtual_time((time_t*)NULL);
		}
		else {
			start_t = *(time_t*)vp;
		}

		PIN_FLIST_FLD_SET(*o_flistpp, PIN_FLD_START_T, &start_t, ebufp);

	}
	else if (PCM_OP_TELCO_REAUTHORIZE == opcode) {

                /* Take the START_T from ASO. */
                if (aso_flistp != NULL) {
                        vp = PIN_FLIST_FLD_GET(aso_flistp, PIN_FLD_START_T, 0, ebufp);
                        start_t = *(time_t*)vp;
                }

                if (0 == start_t) {

                        /*
                         * Failover scenario: If we don't have START_T in ASO
                         * assume current pin_virtual_time().
                         */
                        start_t = pin_virtual_time((time_t*)NULL);
                }

		PIN_FLIST_FLD_SET(*o_flistpp, PIN_FLD_START_T, &start_t, ebufp);

		/*
		 * Populate the END_T from ASO, which is consumed so for.
		 * NOTE: END_T in the input of Reauthorize is the requested END_T
		 *       so we are not considering it here.
		 */
		if (aso_flistp != NULL) {
		        CP_MANDATORY_FIELD(aso_flistp, *o_flistpp, PIN_FLD_END_T, vp);
		}
	}
	else if (PCM_OP_TELCO_START_ACCOUNTING == opcode) {

		/* Assign the START_T from input, else assume current pin_virtual_time. */
		vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_START_T, 1, ebufp);
		if (vp != NULL) {
			PIN_FLIST_FLD_SET(*o_flistpp, PIN_FLD_START_T, vp, ebufp);
		}
		else {
			new_start_t = pin_virtual_time((time_t*)NULL);
			PIN_FLIST_FLD_SET(*o_flistpp, PIN_FLD_START_T, &new_start_t, ebufp);
		}
	}
	else if (PCM_OP_TELCO_STOP_ACCOUNTING == opcode || 
		 PCM_OP_TELCO_UPDATE_ACCOUNTING == opcode) {

                /* Take the least START_T from ASO and input. */
                start_t = fm_gsm_aaa_pol_get_least_start_time(i_flistp,
                                        aso_flistp, ebufp);

		/* Take the maximum END_T from ASO and input. */
		end_t = fm_gsm_aaa_pol_get_max_end_time(i_flistp, aso_flistp, ebufp);

		/* If END_T is not supplied in the input check for the QUANTITY. */
                vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_END_T, 1, ebufp);
                if (NULL == vp) {

                        vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_QUANTITY, 1, ebufp);
                        if (vp != NULL) {

                                qty = (int32) pin_decimal_to_double(vp, ebufp);

                                if (agg_mode & USED_AGGR_MODE) {

					if (start_t != 0) {

						/* QUANTITY is cumulative here.
						 * Add with START_T to get END_T.
						 */
						end_t = start_t + qty;
					}
					else {
						/* Failover scenario: Assume END_T as 
						 * pin_virtual_time and calculate START_T
						 * as END_T - QUANTITY.
                                                 * Used for UPDATE as well sinces we don't 
  						 * have scalar QUANTITY in ASO
						 */
						end_t = pin_virtual_time((time_t*)NULL);
						start_t = end_t - qty;
					}
                                }
                                else if (agg_mode & USED_INCR_MODE) {

			                if (end_t != 0) {

						/* Increment END_T (from ASO) with QUANTITY. */
			                        end_t = end_t + qty;
					}
                                        else {
				                if (start_t != 0) {
				                        end_t = start_t + qty;
						}
				                else { 
							/* Failover scenario: Assume END_T as 
							 * pin_virtual_time and calculate START_T
							 * as END_T - QUANTITY.
							 */
                                                        end_t = pin_virtual_time((time_t*)NULL);
                                                        start_t = end_t - qty;
                                                }
                                        }
                                }
                        }
                }

		/* Stop accounting triggers rating. START_T & END_T should have
		 * a valid value to determine the product validity
		 * (even if we are in Volume based).
		 * Pass the START_T & END_T as pin_virtual_time if not supplied.
		 */
                if (PCM_OP_TELCO_STOP_ACCOUNTING == opcode) {
			if (0 == start_t) {
				start_t = pin_virtual_time((time_t*)NULL);
			}

			if (0 == end_t) {
				end_t = pin_virtual_time((time_t*)NULL);
			}
                }

		PIN_FLIST_FLD_SET(*o_flistpp, PIN_FLD_START_T, &start_t, ebufp);
		PIN_FLIST_FLD_SET(*o_flistpp, PIN_FLD_END_T, &end_t, ebufp);
	}

        /*
         * Error?
         */
        if (PIN_ERR_IS_ERR(ebufp)) {
                PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
                        "fm_gsm_aaa_pol_aggregate_time error", ebufp);
                *o_flistpp = NULL;
        }
	else {
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
			"fm_gsm_aaa_pol_aggregate_time Output flist",
		*o_flistpp);
	}

	return;
}

/*******************************************************************
 * fm_gsm_aaa_pol_aggregate_volume()
 *
 * DESCRIPTION:
 *
 * 1. Aggregates the Volume (BYTES_IN and BYTES_OUT) with the values
 *    in the input and ASO.
 *
 * 2. Following are the Aggregation Criteria w.r.t different AAA
 *    opcodes.
 *
 *    General:
 *      None.
 *
 *    Authorize:
 *      None. 
 *
 *    Reauthorize:
 *      None. 
 *
 *    Start Accounting:
 *      None.
 *
 *    Update Accounting/Stop Accounting:
 *      a. Replace the BYTES_IN and BYTES_OUT field from the input
 *         Here, we assume that input fields are already aggregated
 *         and if it is incremental, this policy should be changed.
 *
 *    Cancel Authorize:
 *       None.
 *
 * ARGUMENTS:
 *
 * opcode       - AAA opcode calling this opcode.
 * i_flistp     - Input Flist pointer.
 * aso_flistp   - ASO Flist pointer.
 * o_flistpp    - Output Flist to be returned.
 * ebufp        - Error buffer.
 *
 * NOTE:
 *
 * Output flist memory is allocated by the caller.
 *
 * RETURN VALUES:
 *
 *   NONE
 *
 ******************************************************************/
static void
fm_gsm_aaa_pol_aggregate_volume(
        u_int           opcode,
        pin_flist_t     *i_flistp,
        pin_flist_t     *aso_flistp,
        pin_flist_t     **o_flistpp,
        pin_errbuf_t    *ebufp)
{
	pin_flist_t	*ext_info_flistp 	= NULL;
	pin_flist_t	*gsm_info_flistp	= NULL;
	pin_flist_t	*aso_gsm_info_flistp	= NULL;
	void		*vp			= NULL;
        int32           bytes_in                = 0;
        int32           bytes_out               = 0;
        int32           aso_bytes_in            = 0;
        int32           aso_bytes_out           = 0;
        int32           agg_mode                = USED_INCR_MODE; 

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_gsm_aaa_pol_aggregate_volume input flist",
		i_flistp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_gsm_aaa_pol_aggregate_volume ASO flist",
		aso_flistp);

	ext_info_flistp = PIN_FLIST_SUBSTR_GET(i_flistp,
					PIN_FLD_EXTENDED_INFO, 0, ebufp);

	gsm_info_flistp = PIN_FLIST_SUBSTR_GET(ext_info_flistp,
					PIN_FLD_GSM_INFO, 0, ebufp);

	if (aso_flistp != NULL) {
		aso_gsm_info_flistp = PIN_FLIST_SUBSTR_GET(aso_flistp,
				PIN_FLD_GSM_INFO, 0, ebufp);
	}
        vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_AGGREGATE_MODE, 1, ebufp);
        if (vp != NULL) {
                agg_mode = *((int32 *)vp);
        }

        if (PCM_OP_TELCO_REAUTHORIZE == opcode) {

                /*
                 * Populate the BYTES_IN and BYTES_OUT from ASO, which is consumed so for.
                 */
                if (aso_gsm_info_flistp != NULL) {

                        CP_MANDATORY_FIELD(aso_gsm_info_flistp, *o_flistpp,
						PIN_FLD_BYTES_IN, vp);
                        CP_MANDATORY_FIELD(aso_gsm_info_flistp, *o_flistpp,
						PIN_FLD_BYTES_OUT, vp);
                }
        }
	if ((PCM_OP_TELCO_STOP_ACCOUNTING == opcode || 
	   PCM_OP_TELCO_UPDATE_ACCOUNTING == opcode)) {

                if (agg_mode & USED_AGGR_MODE) {

		        CP_OPTIONAL_FIELD(gsm_info_flistp, *o_flistpp, PIN_FLD_BYTES_IN, vp);
		        if (vp == NULL) {
			        if (aso_gsm_info_flistp != NULL) {

					/* We dont receive anything in input. Copy the one from ASO.*/
					CP_OPTIONAL_FIELD(aso_gsm_info_flistp, *o_flistpp, PIN_FLD_BYTES_IN, vp);
			        }
			        else {
				        /* Populate it as 0, since RUM would be defined for this event type. */
				        bytes_in = 0;
				        PIN_FLIST_FLD_SET(*o_flistpp, PIN_FLD_BYTES_IN, &bytes_in, ebufp);
			        }
		        }

		        CP_OPTIONAL_FIELD(gsm_info_flistp, *o_flistpp, PIN_FLD_BYTES_OUT, vp);
		        if (NULL == vp) {
			        if (aso_gsm_info_flistp != NULL) {

				        /*  We dont receive anything in input. Copy the one from ASO.*/
				        CP_OPTIONAL_FIELD(aso_gsm_info_flistp, *o_flistpp, PIN_FLD_BYTES_OUT, vp);
			        }
			        else {
				        /* Populate it as 0, since RUM would be defined for this event type. */
				        bytes_out = 0;
				        PIN_FLIST_FLD_SET(*o_flistpp, PIN_FLD_BYTES_OUT, &bytes_out, ebufp);
			        }
		        }
	        }
	        else if (agg_mode & USED_INCR_MODE) {

			/* Read values from input. */
		        vp = PIN_FLIST_FLD_GET(gsm_info_flistp, PIN_FLD_BYTES_IN, 1, ebufp);
		        if (vp != NULL) {
			        bytes_in = *((int *)vp);
		        }

		        vp = PIN_FLIST_FLD_GET(gsm_info_flistp, PIN_FLD_BYTES_OUT, 1, ebufp);
		        if (vp != NULL) {
			        bytes_out = *((int *)vp);
		        }

			/* Read values from ASO. */
			if (aso_gsm_info_flistp != NULL) {

				vp = PIN_FLIST_FLD_GET(aso_gsm_info_flistp, PIN_FLD_BYTES_IN, 1, ebufp);
				if (vp != NULL) {
					aso_bytes_in = *((int *)vp);
				}

				vp = PIN_FLIST_FLD_GET(aso_gsm_info_flistp, PIN_FLD_BYTES_OUT, 1, ebufp);
				if (vp != NULL) {
					aso_bytes_out = *((int *)vp);
				}
			}

			/* Aggregate the values and update the output flist. */
		        bytes_in = bytes_in + aso_bytes_in;
		        bytes_out = bytes_out + aso_bytes_out;

		        PIN_FLIST_FLD_SET(*o_flistpp, PIN_FLD_BYTES_IN, &bytes_in, ebufp);
		        PIN_FLIST_FLD_SET(*o_flistpp, PIN_FLD_BYTES_OUT, &bytes_out, ebufp);
                }
        }

        /*
         * Error?
         */
        if (PIN_ERR_IS_ERR(ebufp)) {
                PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
                        "fm_gsm_aaa_pol_aggregate_volume error", ebufp);
        }
        else {
                PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
                        "fm_gsm_aaa_pol_aggregate_volume Output flist",
                *o_flistpp);
        }

	return;
}

/*******************************************************************
 * fm_gsm_aaa_pol_aggregate_amount()
 *
 * DESCRIPTION:
 *
 * 1. Aggregates the Amount in the input and ASO.
 *
 * 2. Following are the Aggregation Criteria w.r.t different AAA
 *    opcodes.
 *
 *    General:
 *      None.
 *
 *    Authorize:
 *      None. 
 *
 *    Reauthorize:
 *      None. 
 *
 *    Start Accounting:
 *      None.
 *
 *    Update Accounting/Stop Accounting:
 *      a. Replace the BYTES_IN and BYTES_OUT field from the input
 *         Here, we assume that input fields are already aggregated
 *         and if it is incremental, this policy should be changed.
 *
 *    Cancel Authorize:
 *       None.
 *
 * ARGUMENTS:
 *
 * opcode       - AAA opcode calling this opcode.
 * i_flistp     - Input Flist pointer.
 * aso_flistp   - ASO Flist pointer.
 * o_flistpp    - Output Flist to be returned.
 * ebufp        - Error buffer.
 *
 * NOTE:
 *
 * Output flist memory is allocated by the caller.
 *
 * RETURN VALUES:
 *
 * None
 *
 ******************************************************************/
static void
fm_gsm_aaa_pol_aggregate_amount(
        u_int           opcode,
        pin_flist_t     *i_flistp,
        pin_flist_t     *aso_flistp,
        pin_flist_t     **o_flistpp,
        pin_errbuf_t    *ebufp)
{
	void		*vp			= NULL;
        pin_decimal_t   *null_amountp           = NULL;
        pin_decimal_t   *aso_amountp            = NULL;
        pin_decimal_t   *input_amountp          = NULL;
        int32           agg_mode                = USED_INCR_MODE; 
	pin_flist_t	*bal_impacts_flistp	= NULL;
	int32		*elem_id		= 0;
	int32		impact_type		= PIN_IMPACT_TYPE_PRERATED;

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_gsm_aaa_pol_aggregate_amount input flist",
		i_flistp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_gsm_aaa_pol_aggregate_amount ASO flist",
		aso_flistp);

        vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_AGGREGATE_MODE, 1, ebufp);
        if (vp != NULL) {
                agg_mode = *((int32 *)vp);
        }

	/* Non-Amount based scenario is referred by PIN_FLD_AMOUNT as -1. */
	null_amountp = pin_decimal("-1", ebufp);

        if (PCM_OP_TELCO_REAUTHORIZE == opcode) {

                /*
                 * Populate the AMOUNT from ASO, which is consumed so for.
                 */
                if (aso_flistp != NULL) {
                        CP_MANDATORY_FIELD(aso_flistp, *o_flistpp, PIN_FLD_AMOUNT, vp);
                }
        }
	else if (PCM_OP_TELCO_STOP_ACCOUNTING == opcode ||
	    PCM_OP_TELCO_UPDATE_ACCOUNTING == opcode) {

		vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_AMOUNT, 1, ebufp);
		if (vp != NULL) {
			input_amountp = pin_decimal_clone((pin_decimal_t*) vp, ebufp);
		}

		if (aso_flistp != NULL) {
			vp = PIN_FLIST_FLD_GET(aso_flistp, PIN_FLD_AMOUNT, 0, ebufp);
			aso_amountp = pin_decimal_clone((pin_decimal_t*) vp, ebufp);
		}

                if (agg_mode & USED_AGGR_MODE) {

			if (input_amountp != NULL &&
				pin_decimal_compare(input_amountp, null_amountp, ebufp) != 0) {
				PIN_FLIST_FLD_PUT(*o_flistpp, PIN_FLD_AMOUNT, input_amountp, ebufp);
				input_amountp = NULL;

				if (aso_amountp != NULL) {
					pin_decimal_destroy(aso_amountp);
					aso_amountp = NULL;
				}
			}
			else if (aso_amountp != NULL &&
			        pin_decimal_compare(aso_amountp, null_amountp, ebufp) != 0) {
				PIN_FLIST_FLD_PUT(*o_flistpp, PIN_FLD_AMOUNT, aso_amountp, ebufp);
				aso_amountp = NULL;
			}
	        }
	        else if (agg_mode & USED_INCR_MODE) {

			if (input_amountp != NULL) {

				if (aso_amountp != NULL &&
				    pin_decimal_compare(aso_amountp, null_amountp, ebufp) != 0) {

					PIN_FLIST_FLD_PUT(*o_flistpp, PIN_FLD_AMOUNT,
						pin_decimal_add(aso_amountp, input_amountp,ebufp), ebufp);

					pin_decimal_destroy(input_amountp);
					input_amountp = NULL;
					pin_decimal_destroy(aso_amountp);
					aso_amountp = NULL;
				}
				else {
					PIN_FLIST_FLD_PUT(*o_flistpp, PIN_FLD_AMOUNT, input_amountp, ebufp);
					input_amountp = NULL;
				}
			}
                }
	}

	/* To make sure we are destroying the pin_decimal's we created. */
	if (null_amountp != NULL) {
		pin_decimal_destroy(null_amountp);
		null_amountp = NULL;
	}

	if (aso_amountp != NULL) {
		pin_decimal_destroy(aso_amountp);
		aso_amountp = NULL;
	}

	if (input_amountp != NULL) {
		pin_decimal_destroy(input_amountp);
		input_amountp = NULL;
	}

	/* For stop accounting AMOUNT needs to be populated in the bal_impacts array. */
	if (PCM_OP_TELCO_STOP_ACCOUNTING == opcode) {

		/* In case of AMOUNT based request add bal_impacts to output flist. */
        	vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_AMOUNT, 1, ebufp);
        	if (vp != NULL) {

			vp = PIN_FLIST_FLD_GET(*o_flistpp, PIN_FLD_AMOUNT, 1, ebufp);
			if (vp != NULL) {
				/*
				 * Use the UNIT (currency type) for indexing the bal_impacts array.
				 * Mandatory in the input.
				 */
				elem_id = (int32 *) PIN_FLIST_FLD_GET(i_flistp,
						 PIN_FLD_UNIT, 0, ebufp);

				bal_impacts_flistp = PIN_FLIST_ELEM_ADD(*o_flistpp,
						PIN_FLD_BAL_IMPACTS, *elem_id, ebufp);

				PIN_FLIST_FLD_SET(bal_impacts_flistp, PIN_FLD_AMOUNT, vp, ebufp);

				PIN_FLIST_FLD_DROP(*o_flistpp, PIN_FLD_AMOUNT, ebufp);
				PIN_FLIST_FLD_SET(bal_impacts_flistp, PIN_FLD_RESOURCE_ID,
						elem_id, ebufp);

				vp = PIN_FLIST_FLD_GET(*o_flistpp, PIN_FLD_ACCOUNT_OBJ, 0, ebufp);
				PIN_FLIST_FLD_SET(bal_impacts_flistp, PIN_FLD_ACCOUNT_OBJ, vp, ebufp);

				PIN_FLIST_FLD_SET(bal_impacts_flistp, PIN_FLD_IMPACT_TYPE,
						&impact_type, ebufp);
			}
		}
		else {
		 	/* If it is not an AMOUNT based request drop the PIN_FLD_AMOUNT
			   from the output flist as it is not required in the event */
			vp = PIN_FLIST_FLD_GET(*o_flistpp, PIN_FLD_AMOUNT, 1, ebufp);
                     	if (vp != NULL) {
				PIN_FLIST_FLD_DROP(*o_flistpp, PIN_FLD_AMOUNT, ebufp);
			}
		}
	}

        /*
         * Error?
         */
        if (PIN_ERR_IS_ERR(ebufp)) {
                PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
                        "fm_gsm_aaa_pol_aggregate_amount error", ebufp);
        }
        else {
                PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
                        "fm_gsm_aaa_pol_aggregate_amount Output flist",
                *o_flistpp);
        }

	return;
}

/*******************************************************************
 * fm_gsm_aaa_pol_populate_telco_info()
 *
 * DESCRIPTION:
 *
 * 1. Populate the TELCO_INFO with the values from input and ASO.
 *    Following logic is assumed.
 *    If the field is specified in the input, that will override
 *    the value in ASO, else value from ASO is assumed.
 *
 * 2. Following are the TELCO_INFO fields populated.
 *
 *    PIN_FLD_NETWORK_SESSION_ID
 *    PIN_FLD_ORIGIN_NETWORK
 *    PIN_FLD_DESTINATION_NETWORK
 *    PIN_FLD_PRIMARY_MSID
 *    PIN_FLD_SECONDARY_MSID
 *    PIN_FLD_SVC_TYPE
 *    PIN_FLD_SVC_CODE
 *    PIN_FLD_TERMINATE_CAUSE
 *    PIN_FLD_USAGE_CLASS
 *    PIN_FLD_CALLING_FROM
 *    PIN_FLD_CALLED_TO
 *
 * ARGUMENTS:
 *
 * i_flistp     - Input Flist pointer.
 * aso_flistp   - ASO Flist pointer.
 * o_flistpp    - Output Flist to be returned.
 * ebufp        - Error buffer.
 *
 * NOTE:
 *
 * Output flist memory is allocated by the caller.
 *
 * RETURN VALUES:
 *
 * None
 *
 ******************************************************************/
static void
fm_gsm_aaa_pol_populate_telco_info(
        pin_flist_t     *i_flistp,
        pin_flist_t     *aso_flistp,
        pin_flist_t     **o_flistpp,
        pin_errbuf_t    *ebufp)
{

	pin_flist_t	*telco_info_flistp	= 	NULL;
	pin_flist_t	*aso_telco_info_flistp	= 	NULL;
	pin_flist_t	*out_telco_info_flistp	= 	NULL;
	void		*vp			=	NULL;

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_gsm_aaa_pol_populate_telco_info Input flist",
		i_flistp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_gsm_aaa_pol_populate_telco_info ASO flist",
		aso_flistp);

	telco_info_flistp = PIN_FLIST_SUBSTR_GET(i_flistp,
				PIN_FLD_TELCO_INFO, 0, ebufp);

	if (aso_flistp != NULL) {
		aso_telco_info_flistp = PIN_FLIST_SUBSTR_GET(aso_flistp,
					PIN_FLD_TELCO_INFO, 0, ebufp);
	}

	out_telco_info_flistp = PIN_FLIST_SUBSTR_ADD(
				*o_flistpp, PIN_FLD_TELCO_INFO, ebufp);

	CP_OPTIONAL_AGGREGATE_FIELD(telco_info_flistp, aso_telco_info_flistp,
		PIN_FLD_NETWORK_SESSION_ID, out_telco_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(telco_info_flistp, aso_telco_info_flistp,
		PIN_FLD_ORIGIN_NETWORK, out_telco_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(telco_info_flistp, aso_telco_info_flistp,
		PIN_FLD_DESTINATION_NETWORK, out_telco_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(telco_info_flistp, aso_telco_info_flistp,
		PIN_FLD_PRIMARY_MSID, out_telco_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(telco_info_flistp, aso_telco_info_flistp,
		PIN_FLD_SECONDARY_MSID, out_telco_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(telco_info_flistp, aso_telco_info_flistp,
		PIN_FLD_SVC_TYPE, out_telco_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(telco_info_flistp, aso_telco_info_flistp,
		PIN_FLD_SVC_CODE, out_telco_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(telco_info_flistp, aso_telco_info_flistp,
		PIN_FLD_TERMINATE_CAUSE, out_telco_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(telco_info_flistp, aso_telco_info_flistp,
		PIN_FLD_USAGE_CLASS, out_telco_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(telco_info_flistp, aso_telco_info_flistp,
		PIN_FLD_CALLING_FROM, out_telco_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(telco_info_flistp, aso_telco_info_flistp,
		PIN_FLD_CALLED_TO, out_telco_info_flistp, vp);

        /*
         * Error?
         */
        if (PIN_ERR_IS_ERR(ebufp)) {
                PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
                        "fm_gsm_aaa_pol_populate_telco_info error", ebufp);
        }
        else {
                PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
                        "fm_gsm_aaa_pol_populate_telco_info Output flist",
                *o_flistpp);
        }

	return;
}

/*******************************************************************
 * fm_gsm_aaa_pol_populate_service_codes()
 *
 * DESCRIPTION:
 *
 * 1. Populate the SERVICE_CODES array with the values from
 *    input and ASO. Here, Following logic is assumed.
 *
 *    a. If the input flist contains the service codes array, this
 *       will be treated as cumulative, and populated in the output
 *       flist.
 *
 *    b. Else, service codes array present in ASO will be populated
 *       in the output flist, if present in ASO.
 *
 * 2. Following are the SERVICE_CODES array element populated.
 *
 *    PIN_FLD_SS_ACTION_CODE
 *    PIN_FLD_SS_CODE
 *
 * ARGUMENTS:
 *
 * i_flistp     - Input Flist pointer.
 * aso_flistp   - ASO Flist pointer.
 * o_flistpp    - Output Flist to be returned.
 * ebufp        - Error buffer.
 *
 * NOTE:
 *
 * Output flist memory is allocated by the caller.
 *
 * RETURN VALUES:
 *
 *   NONE
 *
 ******************************************************************/
static void
fm_gsm_aaa_pol_populate_service_codes(
        pin_flist_t     *i_flistp,
        pin_flist_t     *aso_flistp,
        pin_flist_t     **o_flistpp,
        pin_errbuf_t    *ebufp)
{
	pin_flist_t  *result_flistp       = NULL;
        pin_cookie_t  cookie              = NULL;
        int32         eid                 = 0;

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_gsm_aaa_pol_populate_service_codes Input flist",
		i_flistp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_gsm_aaa_pol_populate_service_codes ASO flist",
		aso_flistp);

	/* Check whether we have SERVICE_CODES in input flist. */
	while ((result_flistp = PIN_FLIST_ELEM_GET_NEXT(i_flistp,
                PIN_FLD_SERVICE_CODES, &eid, 1, &cookie, ebufp))
                != NULL) {

                PIN_FLIST_ELEM_SET(*o_flistpp, result_flistp,
                        PIN_FLD_SERVICE_CODES, eid, ebufp);
        }

	/* Else, do we have in ASO. (it is ok to check eid) */
	if (eid == 0 && aso_flistp != NULL) {

		cookie = NULL;
		while ((result_flistp = PIN_FLIST_ELEM_GET_NEXT(aso_flistp,
			PIN_FLD_SERVICE_CODES, &eid, 1, &cookie, ebufp))
			!= NULL) {

			PIN_FLIST_ELEM_SET(*o_flistpp, result_flistp,
				PIN_FLD_SERVICE_CODES, eid, ebufp);
		}
	}

        /*
         * Error?
         */
        if (PIN_ERR_IS_ERR(ebufp)) {
                PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
                        "fm_gsm_aaa_pol_populate_service_codes error", ebufp);
        }
        else {
                PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
                        "fm_gsm_aaa_pol_populate_service_codes Output flist",
			*o_flistpp);
        }

	return;
}

/*******************************************************************
 * fm_gsm_aaa_pol_populate_gsm_info()
 *
 * DESCRIPTION:
 *
 * 1. Populate the GSM_INFO with the values from input and ASO.
 *    Following logic is assumed.
 *    If the field is specified in the input, that will override
 *    the value in ASO, else value from ASO is assumed.
 *
 * 2. Following are the GSM_INFO fields populated.
 *
 *    PIN_FLD_SUB_TRANS_ID
 *    PIN_FLD_QOS_REQUESTED
 *    PIN_FLD_QOS_NEGOTIATED
 *    PIN_FLD_ORIGIN_SID
 *    PIN_FLD_NUMBER_OF_UNITS
 *    PIN_FLD_LOC_AREA_CODE
 *    PIN_FLD_IMEI
 *    PIN_FLD_DIRECTION
 *    PIN_FLD_DIALED_NUMBER
 *    PIN_FLD_DESTINATION_SID
 *    PIN_FLD_CELL_ID
 *
 * 3. Following fields in GSM_INFO are aggregated and populated.
 *    (Refer fm_gsm_aaa_pol_aggregate_volume() routine).
 *    BYTES_IN
 *    BYTES_OUT
 *
 * ARGUMENTS:
 *
 * opcode       - AAA Opcode calling this opcode.
 * i_flistp     - Input Flist pointer.
 * aso_flistp   - ASO Flist pointer.
 * o_flistpp    - Output Flist to be returned.
 * ebufp        - Error buffer.
 *
 * NOTE:
 *
 * Output flist memory is allocated by the caller.
 *
 * RETURN VALUES:
 *
 *   NONE
 *
 ******************************************************************/
static void
fm_gsm_aaa_pol_populate_gsm_info(
        u_int           opcode,
        pin_flist_t     *i_flistp,
        pin_flist_t     *aso_flistp,
        pin_flist_t     **o_flistpp,
        pin_errbuf_t    *ebufp)
{
	pin_flist_t	*ext_info_flistp 	= NULL;
	pin_flist_t	*gsm_info_flistp 	= NULL;
	pin_flist_t	*aso_gsm_info_flistp 	= NULL;
	pin_flist_t	*out_gsm_info_flistp 	= NULL;
	void		*vp			= NULL;
	int32 		number_of_units 	= 0;

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_gsm_aaa_pol_populate_gsm_info input flist",
		i_flistp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_gsm_aaa_pol_populate_gsm_info ASO flist",
		aso_flistp);

	ext_info_flistp = PIN_FLIST_SUBSTR_GET(i_flistp,
				PIN_FLD_EXTENDED_INFO, 0, ebufp);

	gsm_info_flistp = PIN_FLIST_SUBSTR_GET(ext_info_flistp,
				PIN_FLD_GSM_INFO, 0, ebufp);

	if (aso_flistp != NULL) {
		aso_gsm_info_flistp = PIN_FLIST_SUBSTR_GET(aso_flistp,
					PIN_FLD_GSM_INFO, 0, ebufp);
	}

	out_gsm_info_flistp = PIN_FLIST_SUBSTR_ADD(
				*o_flistpp, PIN_FLD_GSM_INFO, ebufp);

	CP_OPTIONAL_AGGREGATE_FIELD(gsm_info_flistp, aso_gsm_info_flistp,
		PIN_FLD_SUB_TRANS_ID, out_gsm_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(gsm_info_flistp, aso_gsm_info_flistp,
		PIN_FLD_QOS_REQUESTED, out_gsm_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(gsm_info_flistp, aso_gsm_info_flistp,
		PIN_FLD_QOS_NEGOTIATED, out_gsm_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(gsm_info_flistp, aso_gsm_info_flistp,
		PIN_FLD_ORIGIN_SID, out_gsm_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(gsm_info_flistp, aso_gsm_info_flistp,
		PIN_FLD_NUMBER_OF_UNITS, out_gsm_info_flistp, vp);

	/* We have default RUM available for NUMBER_OF_UNITS for
	 * /event/session/telco/gsm. If this fields is not supplied in the input,
	 * populate it as 0.
	 */
	if (vp == NULL) {
		number_of_units = 0;
		PIN_FLIST_FLD_SET(out_gsm_info_flistp, PIN_FLD_NUMBER_OF_UNITS,
				&number_of_units, ebufp);
	}

	CP_OPTIONAL_AGGREGATE_FIELD(gsm_info_flistp, aso_gsm_info_flistp,
		PIN_FLD_LOC_AREA_CODE, out_gsm_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(gsm_info_flistp, aso_gsm_info_flistp,
		PIN_FLD_IMEI, out_gsm_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(gsm_info_flistp, aso_gsm_info_flistp,
		PIN_FLD_DIRECTION, out_gsm_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(gsm_info_flistp, aso_gsm_info_flistp,
		PIN_FLD_DIALED_NUMBER, out_gsm_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(gsm_info_flistp, aso_gsm_info_flistp,
		PIN_FLD_DESTINATION_SID, out_gsm_info_flistp, vp);

	CP_OPTIONAL_AGGREGATE_FIELD(gsm_info_flistp, aso_gsm_info_flistp,
		PIN_FLD_CELL_ID, out_gsm_info_flistp, vp);

	/* Aggregate the volume BYTES_IN and BYTES_OUT
         * from ASO & input.
	 */
	fm_gsm_aaa_pol_aggregate_volume(opcode, i_flistp, aso_flistp,
			&out_gsm_info_flistp, ebufp);

        /*
         * Error?
         */
        if (PIN_ERR_IS_ERR(ebufp)) {
                PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
                        "fm_gsm_aaa_pol_populate_gsm_info error", ebufp);
                *o_flistpp = NULL;
        }
        else {
                PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
                        "fm_gsm_aaa_pol_populate_gsm_info Output flist",
                        *o_flistpp);
        }

	return;
}

/*******************************************************************
 * fm_gsm_aaa_pol_populate_rating_info()
 *
 * DESCRIPTION:
 *
 * 1. Populates the RATING_INFO substruct, with the aggregated value
 *    present in the output.
 *
 * 2. The aggregation is done based on the request with 
 *    the order of precedence being AMOUNT-VOLUME-DURATION and
 *    each is mutually exclusive of the other two
 *
 * 3. The mode of input (INCREMENTAL/AGGREGATED) taken into consideration 
 *    while aggregating.
 *
 * 4. The rating mode (ADJUST/NO_ADJUST) is also considered along with 
 *    the input mode (INCREMENTAL/AGGREGATED).
 *    while aggregating.
 *
 * 5. AMOUNT BASED REQUEST 
 *    Get the currency type
 *    
 *    AUTHORIZE: 
 *    Amount from the input is set in the rating info
 *    The input mode can be ignored.
 *        
 *    RE-AUTHORIZE: 
 *    If input mode is AGGREGATED
 *    Amount from the input is set in the rating info
 *
 *    If input mode is INCREMENTAL
 *        If rating mode is NO ADJUST
 *            Add the AMOUNT in the input and that in reservation
 *            If reservation is not found, 
 *                Amount from the input is set in the rating info
 *        If rating mode is ADJUST
 *            Add the AMOUNT in the input and that in ASO
 *            If ASO is not found, 
 *                Amount from the input is set in the rating info
 *
 *    If PIN_FLD_AMOUNT is passed, then BALANCES array is created
 *    with the supplied UNIT (Currency type) as index.
 *
 * 6. VOLUME BASED REQUEST 
 *    
 *    AUTHORIZE: 
 *    BYTES_IN and BYTES_OUT are set in the rating info from the input 
 *    The input mode can be ignored.
 *    Perform minimum qty check.
 *    If BYTES_IN and BYTES_OUT are not provided in the input then
 *    minimum qty check not needed. 
 *        
 *    RE-AUTHORIZE: 
 *    If input mode is AGGREGATED
 *    BYTES_IN and BYTES_OUT are set in the rating info from the input 
 *    The input mode can be ignored.
 *
 *    If input mode is INCREMENTAL
 *        If rating mode is NO ADJUST
 *            Add either BYTES_IN or BYTES_OUT in the input and that in 
 *            the QUANTITY in the reservation
 *        If rating mode is ADJUST
 *            Add either BYTES_IN or BYTES_OUT in the input and that in 
 *            QUANTITY in the ASO 
 *
 *
 * 7. DURATION BASED REQUEST 
 *    
 *    AUTHORIZE: 
 *    Set start_t and end_t given in the input to rating info and
 *    calculate the qty.
 *    If end_t is not given and qty is given in the input calculate 
 *    the end_t as end_t = start_t + qty   
 *    If neither end_t nor qty is given in the get the qty from
 *    the config. The input mode can be ignored.
 *    
 *    RE-AUTHORIZE: 
 *    If input mode is AGGREGATED
 *    start_t and end_t from input is set in the rating info
 *    If end_t is not given and qty is given in the input calculate 
 *    the end_t as end_t = start_t + qty   
 *
 *    If input mode is INCREMENTAL
 *        If rating mode is NO ADJUST
 *            Calculate end_t = start_t + qty + qty from reservation 
 *        If rating mode is ADJUST
 *            Calculate end_t = end_t from ASO + qty
 *
 * ARGUMENTS:
 *
 * opcode       - AAA Opcode calling this opcode.
 * i_flistp     - Input Flist pointer.
 * o_flistpp    - Output Flist to be returned.
 * ebufp        - Error buffer.
 *
 * NOTE:
 *
 * Output flist memory is allocated by the caller.
 *
 * RETURN VALUES:
 *
 * None
 * None
 *
 ******************************************************************/
static void
fm_gsm_aaa_pol_populate_rating_info(
        u_int           opcode,
        pin_flist_t     *i_flistp,
        pin_flist_t     **o_flistpp,
        pin_errbuf_t    *ebufp)
{
	pin_flist_t	*rating_info_flistp		= NULL;
	pin_flist_t	*rating_flistp			= NULL;
	pin_flist_t	*bal_flistp			= NULL;
	pin_flist_t	*balances_flistp		= NULL;
	pin_flist_t	*ext_info_flistp		= NULL;
	pin_flist_t	*gsm_info_flistp		= NULL;
	pin_flist_t	*cpy_gsm_info_flistp		= NULL;
	pin_flist_t	*rating_gsm_info_flistp		= NULL;
	pin_flist_t	*reserve_info_flistp		= NULL;
	pin_flist_t	*config_flistp			= NULL;
	pin_flist_t	*cfg_reserve_info_flistp	= NULL;
	poid_t		*event_poidp			= NULL;
	time_t		start_t				= 0;
	time_t		end_t				= 0;
	time_t		old_end_t			= 0;
	int32		req_bytes_in			= 0;
	int32		req_bytes_out			= 0;
	int32		qty				= 0;
	int32		reserved_qty			= 0;
	int32		consumed_qty			= 0;
	int32		total_qty			= 0;
        int32           *elem_id              		= NULL;
	void		*vp				= NULL;
	void		*vp1				= NULL;
	void		*vp_amt				= NULL;
	void		*vp1_amt			= NULL;
	int32		min_qty				= 0;
	int32		min_qty_check_flag		= PIN_BOOLEAN_TRUE;
	int32		min_qty_check_status		= PIN_BOOLEAN_TRUE;
	int32		agg_mode			= USED_INCR_MODE | REQ_INCR_MODE;
	int32		rate_mode			= NO_ADJUST;
	pin_decimal_t   *total_amountp                  = NULL;

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_gsm_aaa_pol_populate_rating_info Input flist",
		i_flistp);

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"fm_gsm_aaa_pol_populate_rating_info Initial Output flist",
		*o_flistpp);

        vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_AGGREGATE_MODE, 1, ebufp);
        if (vp != NULL) {
                agg_mode = *((int32 *)vp);
        }

	/* Get the RESERVATION_INFO. */
	reserve_info_flistp = PIN_FLIST_ELEM_GET(i_flistp, PIN_FLD_RESERVATION_INFO,
						0, 1, ebufp);

	/* Should we adjust the Consumed amount for the reauthorization. */
	vp1 = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_RATING_MODE, 1, ebufp);
	if (vp1 != NULL) {
		rate_mode = *(int32 *)vp1;
	}

	/* Check for AMOUNT based or VOLUME/DURATION based request. */
	/***  AMOUNT based aggregation ***/
	vp_amt = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_AMOUNT, 1, ebufp);
	if (vp_amt != NULL) {

		rating_info_flistp = PIN_FLIST_SUBSTR_ADD(*o_flistpp,
					PIN_FLD_RATING_INFO, ebufp);

                /* 
                 * If the UNIT field is given, then use that
		 * as the currency type
		 */
                elem_id = (int32 *) PIN_FLIST_FLD_GET(i_flistp,
					PIN_FLD_UNIT, 0, ebufp);

                if (elem_id != NULL) {
                        bal_flistp = PIN_FLIST_ELEM_ADD(rating_info_flistp,
					PIN_FLD_BALANCES, *elem_id, ebufp);
                }
                
		if (PCM_OP_TELCO_AUTHORIZE == opcode) {
                        PIN_FLIST_FLD_SET(bal_flistp, PIN_FLD_AMOUNT, vp_amt, ebufp);
                }

		else if (PCM_OP_TELCO_REAUTHORIZE == opcode) {

                        /* If mode is AGGREGATED */
                        if (agg_mode & REQ_AGGR_MODE) {
                                PIN_FLIST_FLD_SET(bal_flistp, PIN_FLD_AMOUNT, vp_amt, ebufp);
                        }

                        /* If mode is INCREMENTAL */
                        else if (agg_mode & REQ_INCR_MODE) {

				if (NO_ADJUST == rate_mode) {

					/* Get the reserved amount from the RESERVATION_INFO.
					 * Here total amount = reserved amount + requested amount
					 */
					if (reserve_info_flistp != NULL) {

                                                if (elem_id != NULL) {
							balances_flistp = PIN_FLIST_ELEM_GET(reserve_info_flistp, 
							    PIN_FLD_BALANCES, *elem_id, 0, ebufp);
              					}

						vp1_amt = PIN_FLIST_FLD_GET(balances_flistp, PIN_FLD_AMOUNT,
									  0, ebufp);

						total_amountp = pin_decimal_add(vp_amt, vp1_amt, ebufp);
		                        }
					else {
						total_amountp = pin_decimal_clone(vp_amt, ebufp);
					}
		                }
				else {
					/* Get the consumed amount from the ASO. We need to adjust
					 * this for the reauthorization request.
					 * Here total amount = consumed amount + requested amount.
					 */
					vp1_amt = PIN_FLIST_FLD_GET(*o_flistpp, PIN_FLD_AMOUNT, 1, ebufp);
					if (vp1_amt != NULL) {
						total_amountp = pin_decimal_add(vp_amt, vp1_amt, ebufp);
					}
					else {
						total_amountp = pin_decimal_clone(vp_amt, ebufp);
					}
				}

                                PIN_FLIST_FLD_PUT(bal_flistp, PIN_FLD_AMOUNT, total_amountp, ebufp);
                                total_amountp = NULL;
                        }
                }

        } /* AMOUNT based aggregation ends */
        /*** VOLUME based and DURATION based aggregation ***/
	else { 

		rating_flistp = PIN_FLIST_COPY(*o_flistpp, ebufp);

		/* Create the /event/session/telco/gsm POID from ASO POID. */
		event_poidp = fm_gsm_aaa_pol_create_session_poid(rating_flistp, ebufp);

		PIN_FLIST_FLD_PUT(rating_flistp, PIN_FLD_POID, event_poidp, ebufp);

		/* Get the reserved_qty from the RESERVATION_INFO. */
		if (reserve_info_flistp != NULL) {
			vp = PIN_FLIST_FLD_GET(reserve_info_flistp, PIN_FLD_QUANTITY,
						0, ebufp);
			reserved_qty = (int32) pin_decimal_to_double(vp, ebufp);
		}

		/*
		 * Get the QUANTITY from the CONFIG_INFO (Used if we dont get the
		 * QUANTITY in the input).
		 */
		config_flistp = PIN_FLIST_SUBSTR_GET(i_flistp, PIN_FLD_CONFIG_INFO, 1, ebufp);

		if (config_flistp != NULL) {
			cfg_reserve_info_flistp = PIN_FLIST_ELEM_GET(config_flistp,
						PIN_FLD_RESERVATION_INFO, 0, 1, ebufp);

			if (cfg_reserve_info_flistp != NULL) {
				CP_MANDATORY_FIELD(cfg_reserve_info_flistp, rating_flistp,
					PIN_FLD_MIN_QUANTITY, vp);

				min_qty = (int32) pin_decimal_to_double(vp, ebufp);
			}

		}

		/*** VOLUME based aggregation ***/
		ext_info_flistp = PIN_FLIST_SUBSTR_GET(i_flistp,
					PIN_FLD_EXTENDED_INFO, 0, ebufp);

		gsm_info_flistp = PIN_FLIST_SUBSTR_GET(ext_info_flistp,
					PIN_FLD_GSM_INFO, 0, ebufp);

		ext_info_flistp = PIN_FLIST_SUBSTR_TAKE(rating_flistp,
					PIN_FLD_EXTENDED_INFO, 0, ebufp);

		rating_gsm_info_flistp = PIN_FLIST_SUBSTR_GET(ext_info_flistp,
						PIN_FLD_GSM_INFO, 0, ebufp);

		if (PCM_OP_TELCO_AUTHORIZE == opcode) {

			/* 
			 * Add the BYTES_IN and BYTES_OUT fields if present in
			 * the input.
			 */
			CP_OPTIONAL_FIELD(gsm_info_flistp, rating_gsm_info_flistp,
					  PIN_FLD_BYTES_IN, vp);
			if (vp == NULL) {

                                /* Populate it as 0, since RUM would be defined for this event type. */
                                req_bytes_in = 0;
                                PIN_FLIST_FLD_SET(rating_gsm_info_flistp, PIN_FLD_BYTES_IN,
						  &req_bytes_in, ebufp);

				min_qty_check_flag = PIN_BOOLEAN_FALSE;
                        }
			else {
				req_bytes_in = *(int32*) vp;
			}

                        CP_OPTIONAL_FIELD(gsm_info_flistp, rating_gsm_info_flistp,
					  PIN_FLD_BYTES_OUT, vp);
                        if (vp == NULL) {

                                /* Populate it as 0, since RUM would be defined for this event type. */
                                req_bytes_out = 0;
                                PIN_FLIST_FLD_SET(rating_gsm_info_flistp, PIN_FLD_BYTES_OUT,
						  &req_bytes_out, ebufp);    
                        }
			else {
				req_bytes_out = *(int32*) vp;
				min_qty_check_flag = PIN_BOOLEAN_TRUE;
			}

			/* 
			 * Minimum quantity check. Requested quantity should be 
			 * more than or equal to minimum quantity configured.
			 */
			if (min_qty_check_flag &&
			   (min_qty > (req_bytes_in + req_bytes_out))) {
				min_qty_check_status = PIN_BOOLEAN_FALSE;
			}
		}
		else if (PCM_OP_TELCO_REAUTHORIZE == opcode) {

                        /* Conver the requested fields to used fields as  
                         * UPDATE_AND_REAUTH opcode sends requested fields as part 
                         * of the input
                         */
                        cpy_gsm_info_flistp = PIN_FLIST_COPY(gsm_info_flistp, ebufp);

                        /* First drop the BYTES_IN and BYTES_OUT fields in the input
                         * which represent the consumed information.
                         * Do it only if you don't see the REQUESTED BYTES fields,
                         * which today signifies an Update and Reauth opcode call.
                         */
                        if (PIN_FLIST_FLD_GET(cpy_gsm_info_flistp, PIN_FLD_REQ_BYTES_IN,
                                              1, ebufp) != NULL) {
                        	DROP_OPTIONAL_FIELD(cpy_gsm_info_flistp, PIN_FLD_BYTES_IN, vp);
			}
                        if (PIN_FLIST_FLD_GET(cpy_gsm_info_flistp, PIN_FLD_REQ_BYTES_OUT,
                                              1, ebufp) != NULL) {
                        	DROP_OPTIONAL_FIELD(cpy_gsm_info_flistp, PIN_FLD_BYTES_OUT, vp);
			}

                        CP_OPTIONAL_FIELD_AS(cpy_gsm_info_flistp, cpy_gsm_info_flistp, PIN_FLD_REQ_BYTES_IN,
                                                       PIN_FLD_BYTES_IN, vp);

                        CP_OPTIONAL_FIELD_AS(cpy_gsm_info_flistp, cpy_gsm_info_flistp, PIN_FLD_REQ_BYTES_OUT,
                                                       PIN_FLD_BYTES_OUT, vp);

                        /* 
                         * Add the BYTES_IN and BYTES_OUT fields if present in
                         * the input.
                         */
                        if (agg_mode & REQ_AGGR_MODE) {
				CP_OPTIONAL_FIELD(cpy_gsm_info_flistp, rating_gsm_info_flistp,
						  PIN_FLD_BYTES_IN, vp);
				if (vp != NULL) {
					req_bytes_in = *(int32*) vp;
				}
				CP_OPTIONAL_FIELD(cpy_gsm_info_flistp, rating_gsm_info_flistp,
						  PIN_FLD_BYTES_OUT, vp);
				if (vp != NULL) {
					req_bytes_out = *(int32*) vp;
				}
                        }
                        else if (agg_mode & REQ_INCR_MODE) {
				vp = PIN_FLIST_FLD_GET(cpy_gsm_info_flistp, PIN_FLD_BYTES_IN, 1, ebufp);
				if (vp != NULL) {

					if (NO_ADJUST == rate_mode) {
						req_bytes_in = *(int32*)vp + reserved_qty;
					}
					else {
						vp1 = PIN_FLIST_FLD_GET(rating_gsm_info_flistp, PIN_FLD_BYTES_IN, 1, ebufp);
						if (vp1 != NULL) {
							consumed_qty = *(int32*)vp1;
						}

						req_bytes_in = *(int32*)vp + consumed_qty;
					}

					PIN_FLIST_FLD_SET(rating_gsm_info_flistp, PIN_FLD_BYTES_IN,
							&req_bytes_in, ebufp);
				}
				else {
					/* Populate it as 0, since RUM would be defined for this event type. */
					req_bytes_in = 0;
					PIN_FLIST_FLD_SET(rating_gsm_info_flistp, PIN_FLD_BYTES_IN,
							  &req_bytes_in, ebufp);

					min_qty_check_flag = PIN_BOOLEAN_FALSE;
				}

				vp = PIN_FLIST_FLD_GET(cpy_gsm_info_flistp, PIN_FLD_BYTES_OUT, 1, ebufp);
				if (vp != NULL) {
					/*
					 * Reserved_qty is cumulative if RUM is BYTES_IN + BYTES_OUT.
					 * If we have already added in req_bytes_in then dont need
					 * to add here again.
					 */
					if (NO_ADJUST == rate_mode) {
						if (req_bytes_in == 0) {
							req_bytes_out = *(int32*)vp + reserved_qty;
						}
						else {
							req_bytes_out = *(int32*)vp;
						}
					}
					else {
						vp1 = PIN_FLIST_FLD_GET(rating_gsm_info_flistp, PIN_FLD_BYTES_OUT, 1, ebufp);
						if (vp1 != NULL) {
							consumed_qty = *(int32*)vp1;
						}

						req_bytes_out = *(int32*)vp + consumed_qty;
					}

					PIN_FLIST_FLD_SET(rating_gsm_info_flistp, PIN_FLD_BYTES_OUT,
							vp, ebufp);

					min_qty_check_flag = PIN_BOOLEAN_TRUE;
				}
				else {
					/* Populate it as 0, since RUM would be defined for this event type. */
					req_bytes_out = 0;
					PIN_FLIST_FLD_SET(rating_gsm_info_flistp, PIN_FLD_BYTES_OUT,
							  &req_bytes_out, ebufp);
				}
			}

			/* 
			 * Minimum quantity check. 
			 * If the rating mode is set to no adjust,
			 * Requested quantity minus reserved quantity should be
			 * more than or equal to minimum quantity configured.
			 * If the rating mode is set to adjust,
			 * Requested quantity minus consumed quantity should be
			 * more than or equal to minimum quantity configured.
			 */
			if (min_qty_check_flag) {
				if ((agg_mode & REQ_AGGR_MODE) && 
						min_qty > req_bytes_in + req_bytes_out) {
					min_qty_check_status = PIN_BOOLEAN_FALSE;
				}
				else if ((!(agg_mode & REQ_AGGR_MODE)) && 
								rate_mode == NO_ADJUST &&
				    (min_qty > (req_bytes_in + req_bytes_out - reserved_qty))) {
					min_qty_check_status = PIN_BOOLEAN_FALSE;
				}
				else if ((!(agg_mode & REQ_AGGR_MODE)) &&
					(min_qty > (req_bytes_in + req_bytes_out - consumed_qty))) {
						min_qty_check_status = PIN_BOOLEAN_FALSE;
				}
			}
                        PIN_FLIST_DESTROY_EX(&cpy_gsm_info_flistp, ebufp);

		} /* VOLUME based aggregation ends */

		/*
		 * DURATION based aggregation. Get the aggregated START_T.
		 */
		vp = PIN_FLIST_FLD_GET(rating_flistp, PIN_FLD_START_T, 0, ebufp);
		if (vp != NULL) {
			start_t = *(time_t*) vp;
		}

		if (PCM_OP_TELCO_AUTHORIZE == opcode) {

			/*
			 * 1. If END_T is supplied in the input, populate the same in rating flist.
			 *
			 * 2. Else if QUANTITY is supplied in the input, calculate the END_T with this.
			 *
			 * 3. Else if we are in Volume based aggregation mode
			 *    i.e req_bytes_in or req_bytes_out has positive value.
			 *    then, some valid END_T should be supplied (rating needs it
			 *    to determine product validity).So, use the START_T as END_T here.
			 *
			 * 4. Else use the QUANTITY from config, and determine END_T based on this.
			 */
			vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_END_T, 1, ebufp);
			if (vp != NULL) {
				end_t = *(time_t*) vp;
				qty = end_t - start_t;
			}
			else if ((vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_QUANTITY, 1, ebufp)) != NULL)
			{
				qty = (int32) pin_decimal_to_double(vp, ebufp);
				end_t = start_t + qty;
			}
			else if (req_bytes_in != 0 || req_bytes_out != 0) {
				end_t = start_t;
				min_qty_check_flag = PIN_BOOLEAN_FALSE; 
			}
			else if (cfg_reserve_info_flistp != NULL) {
				vp = PIN_FLIST_FLD_GET(cfg_reserve_info_flistp,
						PIN_FLD_QUANTITY, 0, ebufp);
				qty = (int32) pin_decimal_to_double(vp, ebufp);
				end_t = start_t + qty;
				min_qty_check_flag = PIN_BOOLEAN_TRUE;
			}
			else {
				/* Nothing passed. Set the END_T = START_T for rating. */ 
				end_t = start_t;
				min_qty_check_flag = PIN_BOOLEAN_FALSE; 
			}

			PIN_FLIST_FLD_SET(rating_flistp, PIN_FLD_END_T, &end_t, ebufp);

			/* Minimum quantity check. */
			if (min_qty_check_flag && min_qty > qty) {
				min_qty_check_status = PIN_BOOLEAN_FALSE;
			}
		}
		else if (PCM_OP_TELCO_REAUTHORIZE == opcode) {

			/*
			 * 1. If already aggregated END_T is present (will be present if END_T is
			 *    supplied in the input of reauthorize, refer aggregate_time() function
			 *    on reauthorize flow), use this for calculating the END_T to rate, else
			 *    assign END_T as START_T (Need valid END_T for Product validity
			 *    in rating).
			 *
			 * 2. If the QUANTITY is supplied in the input, add it to the END_T to
			 *    calculate the END_T to reauthorize.
			 *
			 * 3. If we are not in Volume based aggregation:
			 *    a. If QUANTITY is not supplied in the input, get it from
			 *       the CONFIG_INFO, and calculate the END_T.
			 *    b. If we dont have the aggregated END_T then, calculate the END_T
			 *       END_T = END_T (START_T from step 1) + QUANTITY (input/config)
			 *               + RESERVED_QUANTITY (From RESERVATION_INFO).
			 */

			vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_END_T, 1, ebufp);
			if (vp != NULL) {
				end_t = *(time_t*) vp;
			}
			else if ((vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_QUANTITY,
							 1, ebufp)) != NULL) {

				/* Add the Quantity if supplied in the input.
				 * Rating would based on both Volume and Duration.
				 * Pass both of them to the rating.
				 */
				qty = (int32) pin_decimal_to_double(vp, ebufp);

                                /* If mode is AGGREGATED */
                                if (agg_mode & REQ_AGGR_MODE) {
				        end_t = start_t + qty;
				}
                                else if (agg_mode & REQ_INCR_MODE) {

					if (NO_ADJUST == rate_mode) {
						end_t = start_t + reserved_qty + qty;
					}
					else {
						vp = PIN_FLIST_FLD_GET(rating_flistp, PIN_FLD_END_T, 1, ebufp);
						if (vp != NULL) {
							end_t = *(time_t*)vp + qty;
						}
						else {
							/* We dont have consumed END_T to adjust. */
							end_t = start_t + qty;
						}
					}
				}

				min_qty_check_flag = PIN_BOOLEAN_TRUE;
			}
			else if (req_bytes_in == 0 && req_bytes_out == 0) {

				/* Nothing specified in the input.
				 * Get it from /config/reserve/gsm/telephony.
				 */
				if (cfg_reserve_info_flistp != NULL) {
					vp = PIN_FLIST_FLD_GET(cfg_reserve_info_flistp,
						PIN_FLD_QUANTITY, 0, ebufp);
					qty = (int32) pin_decimal_to_double(vp, ebufp);
				}

				if (end_t == 0) {
					end_t = start_t;
				}

				/* Always works in INCREMENTAL mode */
				end_t = end_t + qty;

				min_qty_check_flag = PIN_BOOLEAN_TRUE;
			}
			else {
                                /* this is a volume based scenario and the END_T must be set
                                 * for rating to validate the product
                                 */
                                if (end_t == 0) {
                                        end_t = start_t;
                                }
			}

			PIN_FLIST_FLD_SET(rating_flistp, PIN_FLD_END_T, &end_t, ebufp);

			/* Minimum quantity check. */
			if (min_qty_check_flag && min_qty > qty) {
				min_qty_check_status = PIN_BOOLEAN_FALSE;
			}
		} /* DURATION based aggregation ends */

		/* Log the status of the Minimum quantity check. */
		if (min_qty_check_status != PIN_BOOLEAN_TRUE) {

                        pin_set_err(ebufp, PIN_ERRLOC_FM,
                                PIN_ERRCLASS_APPLICATION,
                                PIN_ERR_BAD_ARG, PIN_FLD_MIN_QUANTITY, 0, 0);

                        PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
				"fm_gsm_aaa_pol_populate_rating_info error: "
                                "Minimum quantity check failed : "
				"Requested qty is less than Minimum qty configured",
                                ebufp);
		}

		/* Prepare the event substruct */
		rating_info_flistp = PIN_FLIST_SUBSTR_ADD(*o_flistpp,
					PIN_FLD_RATING_INFO, ebufp);

		vp = PIN_FLIST_FLD_TAKE(rating_flistp, PIN_FLD_MIN_QUANTITY, 1, ebufp);
		if (vp != NULL) {
			PIN_FLIST_FLD_PUT(rating_info_flistp, PIN_FLD_MIN_QUANTITY, vp, ebufp);
		}

		PIN_FLIST_CONCAT(rating_flistp, ext_info_flistp, ebufp);
		PIN_FLIST_DESTROY_EX(&ext_info_flistp, NULL);
                PIN_FLIST_SUBSTR_PUT(rating_info_flistp, rating_flistp,
                                        PIN_FLD_EVENT, ebufp);
	}

        /*
         * Error?
         */
        if (PIN_ERR_IS_ERR(ebufp)) {
                PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
                        "fm_gsm_aaa_pol_populate_rating_info error", ebufp);
        }
        else {
                PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
                        "fm_gsm_aaa_pol_populate_rating_info Output flist",
                        *o_flistpp);
        }

	return;
}

/*******************************************************************
 * fm_gsm_aaa_pol_create_session_poid()
 *
 * DESCRIPTION:
 *
 * Creates the /event/session/XXX poid from the /active_session/XXX
 * POID present in the input flist (PIN_FLD_POID).
 *
 * ARGUMENTS:
 *
 * i_flistp     - Input Flist pointer.
 * ebufp        - Error buffer.
 *
 * NOTE:
 *
 * Memory is allocated by this routine for the return POID, caller
 * should release this memory.
 *
 * RETURN VALUES:
 *
 *   poid_t - Event session POID.
 *
 ******************************************************************/
poid_t*
fm_gsm_aaa_pol_create_session_poid(
        pin_flist_t     *i_flistp,
        pin_errbuf_t    *ebufp)
{
        int64           db = 0;
        void            *vp = NULL;
        char            poid_buf[PCM_MAX_POID_TYPE] = PIN_OBJ_TYPE_EVENT_SESSION;
        poid_t          *poidp = NULL;

        poidp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_POID, 0, ebufp);

        vp = (char*)(PIN_POID_GET_TYPE(poidp) +
			strlen(PIN_OBJ_TYPE_ACTIVE_SESSION));

        /* Check if the object type is derived */
        if (strlen((char*)vp)>1) {
                strcat(poid_buf, (char*)vp);
        }

        db = PIN_POID_GET_DB(poidp);

        poidp =  PIN_POID_CREATE(db, poid_buf, -1, ebufp);

        return poidp;
}

/*******************************************************************
 * fm_gsm_aaa_pol_get_least_start_time()
 *
 * DESCRIPTION:
 *
 * Compares the start times given in the input with that present in
 * the ASO, and return the least of the two.
 *
 * ARGUMENTS:
 *
 * i_flistp     - Input Flist pointer.
 * aso_flistp   - ASO Flist pointer.
 * ebufp        - Error buffer.
 *
 * RETURN VALUES:
 *
 *   time_t - Least start time
 *   0 is return when start_t isn't found in both input and ASO
 *
 ******************************************************************/
time_t
fm_gsm_aaa_pol_get_least_start_time(
        pin_flist_t     *i_flistp,
        pin_flist_t     *aso_flistp,
        pin_errbuf_t    *ebufp)
{

        time_t  old_start_t  = 0;
        time_t  new_start_t  = 0;
	void	*vp          = NULL;

	/* Take the START_T from input. */
	vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_START_T, 1, ebufp);
	if (vp != NULL) {
		new_start_t = *(time_t*) vp;
	}

	/* Take the START_T from ASO. */
	if (aso_flistp != NULL) {

		vp = PIN_FLIST_FLD_GET(aso_flistp, PIN_FLD_START_T, 1, ebufp);
		if (vp != NULL) {
			old_start_t = *(time_t*) vp;
		}
	}

	/* Take the least START_T from ASO and input. */
	if (new_start_t != 0 && (old_start_t == 0 || (old_start_t != 0 && new_start_t < old_start_t))) {
		return new_start_t;
	}

	return old_start_t;
}

/*******************************************************************
 * fm_gsm_aaa_pol_get_max_end_time()
 *
 *
 * DESCRIPTION:
 *
 * Compares the end times given in the input with that present in
 * the ASO, and return the maximum of the two.
 *
 * ARGUMENTS:
 *
 * i_flistp     - Input Flist pointer.
 * aso_flistp   - ASO Flist pointer.
 * ebufp        - Error buffer.
 *
 * RETURN VALUES:
 *
 *   time_t - Maximum end time
 *   0 is return when end_t isn't found in both input and ASO
 *
 ******************************************************************/
time_t
fm_gsm_aaa_pol_get_max_end_time(
        pin_flist_t     *i_flistp,
        pin_flist_t     *aso_flistp,
        pin_errbuf_t    *ebufp)
{

        time_t  old_end_t  = 0;
        time_t  new_end_t  = 0;
        void    *vp        = NULL;

        /* Take the END_T from input. */
        vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_END_T, 1, ebufp);
        if (vp != NULL) {
                new_end_t = *(time_t*) vp;
        }

        /* Take the END_T from ASO. */
        if (aso_flistp != NULL) {

                vp = PIN_FLIST_FLD_GET(aso_flistp, PIN_FLD_END_T, 1, ebufp);
                if (vp != NULL) {
                        old_end_t = *(time_t*) vp;
                }
        }

        /* Take the maximum END_T from ASO and input. */
	if (new_end_t != 0 && (old_end_t == 0 || (old_end_t != 0 && new_end_t > old_end_t))) {
        	return new_end_t;
	
	}

        return old_end_t;
}
