/*******************************************************************
 *
 *  @(#)%Portal Version: $Id: fm_gsm_aaa_pol_search_session.c /cgbubrm_7.5.0.portalbase/1 2015/04/07 00:42:05 sumeemis Exp $ %
 *
* Copyright (c) 2004, 2015, Oracle and/or its affiliates. All rights reserved.
 *
 * This material is the confidential property of Oracle Corporation or its 
 * licensors and may be used, reproduced, stored or transmitted only in 
 * accordance with a valid Oracle license or sublicense agreement.
 *
 *******************************************************************/

#ifndef lint
static char Sccs_id[] = "@(#)$Id: fm_gsm_aaa_pol_search_session.c /cgbubrm_7.5.0.portalbase/1 2015/04/07 00:42:05 sumeemis Exp $";
#endif

#include <stdio.h>
#include <pin_errs.h>
#include <pinlog.h>
#include "pcm.h"
#include "ops/gsm_aaa.h"
#include "pin_tcf.h"
#include "pin_gsm_aaa.h"
#include "pin_asm.h"
#include <cm_fm.h>

#define FILE_SOURCE_ID  "fm_gsm_aaa_pol_search_session.c(1.5)"

#define PIN_OBJ_TYPE_EVENT_SESSION "/event/session"

/*******************************************************************
 * Routines contained herein.
 *******************************************************************/
static void
fm_gsm_aaa_pol_search_session(
	pcm_context_t		*ctxp,
	pin_flist_t		*in_flistp,
	pin_flist_t		**out_flistpp,
        pin_errbuf_t		*ebufp);


/*******************************************************************
 *
 * fm_gsm_aaa_pol_validate_auth_id ()
 *
 * Validates the suppied Authorization Id in the input flist.
 * Following are the default validation rules.
 *
 * 1. AUTH_ID is mandatory.
 * 2. AUTH_ID should not be empty string.
 * 3. AUTH_ID should not have any blank spaces or tabs.
 *
 * Input arguments:
 *
 * ctxp - Context pointer.
 * in_flistp - Input flist pointer which contains Authorization Id.
 * ebufp - Error buffer.
 *
 * Return value:
 * None.
 *
 * Error:
 *
 * Returns ebuf with PIN_ERR_BAD_ARG with field
 * PIN_FLD_AUTHORIZATION_ID.
 ******************************************************************/
void
fm_gsm_aaa_pol_validate_auth_id(
        pcm_context_t           *ctxp,
        pin_flist_t             *in_flistp,
        pin_errbuf_t            *ebufp);
 
/*******************************************************************
 * Main routine for the PCM_OP_GSM_AAA_POL_SEARCH_SESSION  command
 *******************************************************************/
void
op_gsm_aaa_pol_search_session(
        cm_nap_connection_t	*connp,
	u_int			opcode,
        int32                   flags,
        pin_flist_t		*in_flistp,
        pin_flist_t		**ret_flistpp,
        pin_errbuf_t		*ebufp)
{

	pcm_context_t		*ctxp = connp->dm_ctx;

	/* To receive the Output from the main Function */

	pin_flist_t		*r_flistp = NULL;

        if (PIN_ERR_IS_ERR(ebufp)){
                return;
               /*******/
        }

	PIN_ERR_CLEAR_ERR(ebufp);

	/*
	 * Null out results until we have some.
	 */
	*ret_flistpp = NULL;

	/*
	 * Insanity check.
	 */
	if (opcode != PCM_OP_GSM_AAA_POL_SEARCH_SESSION) {
		pin_set_err(ebufp, PIN_ERRLOC_FM,
			PIN_ERRCLASS_SYSTEM_DETERMINATE,
			PIN_ERR_BAD_OPCODE, 0, 0, opcode);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"bad opcode in pcm_op_gsm_aaa_pol_search_session", ebufp);
		return;
	}


        /*
         * Debug: what did we get?
         */

        PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
                "op_gsm_aaa_pol_search_session input flist", in_flistp);

	/*
	 * Validate the auth_id before preparing search flist.
	 */
	fm_gsm_aaa_pol_validate_auth_id(ctxp, in_flistp, ebufp);
	if (PIN_ERR_IS_ERR(ebufp)) {
                PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
                        "pcm_op_gsm_aaa_pol_search_session : "
			"Validate auth_id error", ebufp);
		goto cleanup;
		/***********/
        }

	/*
	 * Call main function to do it
	 */
	fm_gsm_aaa_pol_search_session(ctxp, in_flistp, &r_flistp, ebufp);

cleanup:

	/*
	 * Results.
	 */
	if (PIN_ERR_IS_ERR(ebufp)) {
		*ret_flistpp = (pin_flist_t *)NULL;
		PIN_FLIST_DESTROY_EX(&r_flistp, NULL);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"pcm_op_gsm_aaa_pol_search_session error", ebufp);
	} else {
		*ret_flistpp = r_flistp;
		PIN_ERR_CLEAR_ERR(ebufp);
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
			"pcm_op_gsm_aaa_pol_search_session return flist", r_flistp);
	}

	return;
}

/************************************************************************
 * fm_gsm_aaa_pol_search_session()
 * This does the following functions
 * 1. Create a Search Flist with the Search Criteria as 
 *     1.1 ACTIVE_SESSION_ID in case of Active Session.
 *     1.2 TELCO_INFO.NETWORK_SESSION_ID, PRIMARY_MSID and START_T and in 
 *         case of Event Session.
 * 2. As it is a helper Opcode, it Allows the End User to change the 
 *    search criteria.
 *
 ************************************************************************/
static void
fm_gsm_aaa_pol_search_session(
	pcm_context_t		*ctxp,
	pin_flist_t		*in_flistp,
	pin_flist_t		**out_flistpp,
        pin_errbuf_t		*ebufp)
{

	/* To create string for Debugging */

        char             	msg[MAX_STRING_LENGTH] 		= {""};

	/* To Identify whether the Session is Active or Event Type ?? */

	char 			*type_of_poid 			= NULL;
        poid_t                	*session_objp   		= NULL;

	/* The Variables required to Create the Search Flist */

        pin_flist_t      	*session_search_flistp 		= NULL;
        pin_flist_t      	*telco_info_flistp 		= NULL;
        pin_flist_t      	*results_flistp 		= NULL;
        int64            	db_id 				= 0;
        poid_t           	*search_poidp 			= NULL;
       	pin_flist_t      	*args_flistp 			= NULL;
        int32            	s_flags 			= SRCH_DISTINCT;
        char             	query_str[MAX_STRING_LENGTH] 	= "select X from ";
	int32			args_index			= 0;

	/* The variables required to read the criteria of search of Active Session and Event Session */

        void                    *args_fld_value                 = NULL;
        time_t                  *time_in_input_flist 		= NULL;

	/*
	 * The Index name to be hardcoded in case of Active Session.
	 * pin_asm.h should define this. Till then hardcoded here.
	 */

        char                    index_name[MAX_STRING_LENGTH]   = "active_session_active_id_i";

        /***********************************************************
	* Reading the POID Information so that we can identify whether we
	* want to set the search criteria based on the event or activity 
         ***********************************************************/

        session_objp 	= (poid_t *) PIN_FLIST_FLD_GET(in_flistp,
                		PIN_FLD_POID, 0, ebufp);

	type_of_poid = (char *) PIN_POID_GET_TYPE(session_objp);

	if ( ( strstr(type_of_poid, PIN_OBJ_TYPE_ACTIVE_SESSION) == NULL ) &&
	     ( strstr(type_of_poid, PIN_OBJ_TYPE_EVENT_SESSION) == NULL ) ) {

	      PIN_ERR_LOG_MSG (PIN_ERR_LEVEL_DEBUG,"Invalid Session Type ");
	      return;
	}

	strcat(query_str,type_of_poid);

        /***********************************************************
         * Set up for doing the session search based on the Session
         * object.
         ***********************************************************/

        session_search_flistp = PIN_FLIST_CREATE (ebufp);

        db_id = PIN_POID_GET_DB(session_objp);

        search_poidp = (poid_t *)PIN_POID_CREATE(db_id, "/search", -1, ebufp);

        PIN_FLIST_FLD_PUT(session_search_flistp, PIN_FLD_POID, (void *)search_poidp,
                ebufp);

        /***********************************************************
         * Setting the Search Criteria based on the Type of Session
         ***********************************************************/

	if (strstr(type_of_poid, PIN_OBJ_TYPE_ACTIVE_SESSION) != NULL) {

		strcat(query_str," where F1 = V1 ");

        	args_fld_value = PIN_FLIST_FLD_GET(in_flistp,
                		       PIN_FLD_AUTHORIZATION_ID, 0, ebufp);

		/* Setting the AUTH ID in to the FLIST */

        	args_flistp = PIN_FLIST_ELEM_ADD(session_search_flistp, PIN_FLD_ARGS, 1,
                	ebufp);

        	PIN_FLIST_FLD_SET(args_flistp, PIN_FLD_ACTIVE_SESSION_ID, args_fld_value,
                	ebufp);

		/* Note : Adding the index_name at the level of --> session_search_flistp <-- */

        	PIN_FLIST_FLD_SET(session_search_flistp, PIN_FLD_INDEX_NAME, (void *)&index_name,
                	ebufp);

		PIN_FLIST_ELEM_SET(session_search_flistp, NULL, PIN_FLD_RESULTS, 0, ebufp);

	}else if(strstr(type_of_poid, PIN_OBJ_TYPE_EVENT_SESSION) != NULL) {

	 	strcat(query_str," where F1 = V1 "); 

		args_index = 1;
        	args_flistp = PIN_FLIST_ELEM_ADD(session_search_flistp, PIN_FLD_ARGS, args_index,
                	ebufp);

        	args_fld_value = (char *) PIN_FLIST_FLD_GET(in_flistp,
                		       PIN_FLD_AUTHORIZATION_ID, 0, ebufp);

                if(cm_fm_is_timesten()) {
                        PIN_FLIST_FLD_SET(args_flistp, PIN_FLD_EVENT_NO, (char *)args_fld_value, ebufp);
                } else {
			telco_info_flistp = PIN_FLIST_SUBSTR_ADD(args_flistp,PIN_FLD_TELCO_INFO, ebufp);

	       		PIN_FLIST_FLD_SET(telco_info_flistp, PIN_FLD_NETWORK_SESSION_ID, args_fld_value,
       		         		ebufp);

			/* Setting the PRIMARY_MSID in to the FLIST */

			args_fld_value = PIN_FLIST_FLD_GET(in_flistp,
					       PIN_FLD_PRIMARY_MSID, 1, ebufp);

			if(args_fld_value != (void *) NULL ){

				args_index++;
				args_flistp = PIN_FLIST_ELEM_ADD(session_search_flistp, PIN_FLD_ARGS, args_index,
					 ebufp);

				telco_info_flistp = PIN_FLIST_SUBSTR_ADD(args_flistp,PIN_FLD_TELCO_INFO, ebufp);

				PIN_FLIST_FLD_SET(telco_info_flistp, PIN_FLD_PRIMARY_MSID, args_fld_value,
					ebufp);
				pin_snprintf(msg, sizeof(msg),"AND F%d = V%d ",args_index,args_index);
				strcat(query_str,msg);
			}
		}


                /* Setting the START_T in to the FLIST */

                time_in_input_flist = (time_t *) PIN_FLIST_FLD_GET(in_flistp,
                                       PIN_FLD_START_T, 1, ebufp);

                if(time_in_input_flist != (time_t *) NULL ){

			args_index++;

		/* The Value of Tolerance is Yet to be Decided */

                	args_flistp = PIN_FLIST_ELEM_ADD(session_search_flistp, PIN_FLD_ARGS, args_index,
                        	ebufp);

                	PIN_FLIST_FLD_SET(args_flistp, PIN_FLD_START_T, (void *) time_in_input_flist,
                        	ebufp);
			pin_snprintf(msg, sizeof(msg),"AND F%d = V%d ",args_index,args_index);
			strcat(query_str,msg);
		}

		/* Read only POID, which is only required */
		results_flistp = PIN_FLIST_ELEM_ADD(session_search_flistp, PIN_FLD_RESULTS, 0, ebufp);
		PIN_FLIST_FLD_SET(results_flistp, PIN_FLD_POID, (void *)NULL, ebufp);

	}


        /***********************************************************
         * Preparing the Input Flist for Search 
         ***********************************************************/

	pin_snprintf(msg, sizeof(msg), "The Template String for Search is --> %s",query_str);
        PIN_ERR_LOG_MSG (PIN_ERR_LEVEL_DEBUG,msg);

	/***********************************************************************
         * In case of Event session search in TT environment, run Union search
         * to get a complete result set across DB and TT
         ***********************************************************************/

	if(strstr(type_of_poid, PIN_OBJ_TYPE_EVENT_SESSION) != NULL && cm_fm_is_timesten()) {
		s_flags = SRCH_DISTINCT|SRCH_UNION_TT;
	} 
	PIN_FLIST_FLD_SET(session_search_flistp, PIN_FLD_FLAGS, (void *)&s_flags, ebufp);

        PIN_FLIST_FLD_SET(session_search_flistp, PIN_FLD_TEMPLATE,
                (void *) query_str, ebufp);

	/*
	 * Create outgoing flist
	 */
	 
	*out_flistpp = session_search_flistp;

        return;
}

/*******************************************************************
 *
 * fm_gsm_aaa_pol_validate_auth_id ()
 *
 * Validates the suppied Authorization Id in the input flist.
 * Following are the default validation rules.
 *
 * 1. AUTH_ID is mandatory.
 * 2. AUTH_ID should not be empty string.
 * 3. AUTH_ID should not have any blank spaces or tabs.
 *
 * Input arguments:
 *
 * ctxp - Context pointer.
 * in_flistp - Input flist pointer which contains Authorization Id.
 * ebufp - Error buffer.
 *
 * Return value:
 * None.
 *
 * Error:
 *
 * Returns ebuf with PIN_ERR_BAD_ARG with field
 * PIN_FLD_AUTHORIZATION_ID.
 ******************************************************************/
void
fm_gsm_aaa_pol_validate_auth_id(
        pcm_context_t           *ctxp,
        pin_flist_t             *in_flistp,
        pin_errbuf_t            *ebufp)
{

	void 	*vp = NULL;

        /* AUTHORIZATION_ID is the mandatory field for any AAA opcodes */
        vp = PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_AUTHORIZATION_ID, 0, ebufp);

	/*
	 * Default validation rules.
	 *
	 * 1. AUTH_ID is mandatory.
	 * 2. AUTH_ID should not be empty string.
	 * 3. AUTH_ID should not have any blank spaces or tabs.
	 *
	 */
        if (vp == NULL || (strlen((char*)vp) <= 0)
		|| (strchr((const char*)vp, '\t') != NULL)
		|| (strchr((const char*)vp, ' ') != NULL)) {

                PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_ERROR,
                        "fm_gsm_aaa_pol_validate_auth_id(): Authorization Id validation failed ",
                        in_flistp);
                pin_set_err(ebufp, PIN_ERRLOC_FM,
                        PIN_ERRCLASS_APPLICATION, PIN_ERR_BAD_ARG,
                        PIN_FLD_AUTHORIZATION_ID, 0, 0);
        }
	else {
                PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
                        "fm_gsm_aaa_pol_validate_auth_id(): Authorization Id validation passed ",
                        in_flistp);
	}

	return;

}
