/**********************************************************************
 *
 * @(#)% %
 *
 * Copyright (c) 2007 Oracle. All rights reserved.
 *
 * This material is the confidential property of Oracle Corporation or its 
 * licensors and may be used, reproduced, stored or transmitted only in 
 * accordance with a valid Oracle license or sublicense agreement.
 *
 **********************************************************************/

#ifndef lint
static const char Sccs_id[] = "@(#)%Portal Version: fm_gsm_aaa_pol_custom_config.c:WirelessVelocityInt:4:2006-Sep-14 11:19:26 %";
#endif

#include <stdio.h>	/* for FILE * in pcm.h */
#include "ops/gsm_aaa.h"
#include <pcm.h>
#include <cm_fm.h>

/*******************************************************************
 *******************************************************************/

    /*
     * NOTE THAT THE DISPATCH ENTRIES ARE COMMENTED. WHEN YOU OVERRIDE
     * AN IMPLEMENTATION, UNCOMMENT THE LINE BELOW THAT MATCHES THE
     * OPCODE FOR WHICH YOU HAVE PROVIDED AN ALTERNATE IMPLEMENTATION.
     */

struct cm_fm_config fm_gsm_aaa_pol_custom_config[] = {
	/* opcode (as an int), function name (as a string) */
/*	{ PCM_OP_GSM_AAA_POL_AUTHORIZE_PREP_INPUT,\
"op_gsm_aaa_pol_authorize_prep_input"}, */
/*	{ PCM_OP_GSM_AAA_POL_REAUTHORIZE_PREP_INPUT,\
"op_gsm_aaa_pol_reauthorize_prep_input"}, */
/*	{ PCM_OP_GSM_AAA_POL_UPDATE_ACCOUNTING_PREP_INPUT,\
"op_gsm_aaa_pol_update_accounting_prep_input"}, */
/*	{ PCM_OP_GSM_AAA_POL_STOP_ACCOUNTING_PREP_INPUT,\
"op_gsm_aaa_pol_stop_accounting_prep_input"}, */
/*	{ PCM_OP_GSM_AAA_POL_PREP_INPUT,\
"op_gsm_aaa_pol_prep_input"}, */
/*	{ PCM_OP_GSM_AAA_POL_SEARCH_SESSION,\
"op_gsm_aaa_pol_search_session"}, */
/*	{ PCM_OP_GSM_AAA_POL_ACC_ON_OFF_SEARCH,\
"op_gsm_aaa_pol_acc_on_off_search"}, */
/*	{ PCM_OP_GSM_AAA_POL_AUTHORIZE,\
"op_gsm_aaa_pol_authorize"}, */
/*	{ PCM_OP_GSM_AAA_POL_POST_PROCESS,\
"op_gsm_aaa_pol_post_process"}, */
	{ 0,	(char *)0 }
};

PIN_EXPORT
void *
fm_gsm_aaa_pol_custom_config_func() {
	return ((void *) (fm_gsm_aaa_pol_custom_config));
}

