/*
 *
 * (#)$Id: pin_mail.c /cgbubrm_7.5.0.portalbase/1 2015/12/02 21:44:42 akkkumar Exp $ 
* Copyright (c) 1994, 2015, Oracle and/or its affiliates. All rights reserved.
 *
 * This material is the confidential property of Oracle Corporation or its
 * licensors and may be used, reproduced, stored or transmitted only in
 * accordance with a valid Oracle license or sublicense agreement.
 */

#ifndef lint
static  char    Sccs_id[] = "@(#)%Portal Version: pin_mail.c:PortalBase7.3.1Int:1:2007-Sep-28 04:40:54 %";
#endif

#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <sysexits.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include "pcm.h"
#include "pin_act.h"
#include "pin_errs.h"
#include "pinlog.h"
#include "pin_mailer.h"

#define FILE_SOURCE_ID		"pin_mail.c(1.6)"

#define PIN_MAIL_PROG		"pin_mail"

char	default_domain[BUFSIZ];

/*******************************************************************
 * pin_mail_parse_cmdline():
 *******************************************************************/
void
pin_mail_parse_cmdline(
	u_int		argc,
	char		**argv,
	pin_flist_t	*m_flistp,
	pin_errbuf_t	*ebufp)
{
	pin_flist_t	*flistp = NULL;
	int		i=1;
	int		num_recipients=0;

	char		*c_ptr = NULL;
	char		*program = PIN_MAIL_PROG;
	u_int		err;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Parse the args.
	 ***********************************************************/
	while(argv[i] != 0) {

		if(strcmp(argv[i], "-d") == 0) {

			PIN_FLIST_FLD_SET(m_flistp, PIN_FLD_MAILDIR,
				(void *)argv[++i], ebufp);

			pinlog(FILE_SOURCE_ID, __LINE__, LOG_FLAG_DEBUG,
				"Mail directory set to <%s>", argv[i]);

		} else if(strcmp(argv[i], "-f") == 0) {

			PIN_FLIST_FLD_SET(m_flistp, PIN_FLD_SENDER,
				(void *)argv[++i], ebufp);

			pinlog(FILE_SOURCE_ID, __LINE__, LOG_FLAG_DEBUG,
				"Mail sender set to <%s>", argv[i]);

		} else {

			flistp = PIN_FLIST_ELEM_ADD(m_flistp,
				PIN_FLD_RECIPIENTS, num_recipients, ebufp);

			PIN_FLIST_FLD_SET(flistp, PIN_FLD_LOGIN,
				(void *)argv[i], ebufp);

			num_recipients++;

			pinlog(FILE_SOURCE_ID, __LINE__, LOG_FLAG_DEBUG, 
				"Mail recipient set to <%s>", argv[i]);

		}

		/* "Used up" this entry... */
		++i;
	}

	/***********************************************************
	 * Sanity check -- make sure we're sending to somebody.
	 ***********************************************************/
	if(num_recipients == 0) {
		pinlog(FILE_SOURCE_ID, __LINE__, LOG_FLAG_ERROR,
			"No addressees found!");
		ebufp->pin_err = PIN_ERR_MISSING_ARG;
		ebufp->field = PIN_FLD_RECIPIENTS;
	}

	return;
}

/*******************************************************************
 * pin_mail_read_letter():
 *
 *	Read the actual email letter for stdin.
 *
 *******************************************************************/
#ifdef NEEDBLANKLINE
#define RESERVED_SPACE		1
#else
#define RESERVED_SPACE		0
#endif

#define READSIZE		4096
#define ALLOC_BUFSIZE		(4 * 4096)

void
pin_mail_read_letter(
	pin_flist_t	*m_flistp,
	pin_errbuf_t	*ebufp)
{
	int		readcount, bufsize, done = 0, rv;
	char		*buf = (char *)NULL;
	pin_buf_t	pin_buf;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Initialize everything.
	 ***********************************************************/
	readcount = 0;
	bufsize = ALLOC_BUFSIZE;
	buf = (char *)malloc(ALLOC_BUFSIZE);
	if(buf == NULL)
	{
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,"Failed to allocate memory",ebufp);	
	}		
	/***********************************************************
	 * Read the letter.
	 ***********************************************************/
	do {
		/*
		 * Make sure we can hold the next glob of data. Be sure to
		 * reserve some data at the very end so that we can properly
		 * terminate if necessary...
		 */
		if((readcount + READSIZE + RESERVED_SPACE) >= bufsize) {
			bufsize += ALLOC_BUFSIZE;
			buf = (char *)realloc(buf, bufsize);
		}

		/* Do the read! */
		rv = read(0, (buf + readcount), READSIZE);
		switch(rv) {
		case -1:
			/* Error! */
			ebufp->pin_err = PIN_ERR_FILE_IO;
			ebufp->reserved = errno;
			PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
				"Error reading letter from stdin", ebufp);
			goto cleanup;
			/* NOTREACHED */

		case 0:
			/* EOF -- exit */
			done = 1;
			break;
		
		default:
			/* Got some data -- update everything */
			readcount += rv;
			break;
		}
	} while(!done);

#ifdef NEEDBLANKLINE
	/* 
	 * It would seem that we need to add a newline if the last character
	 * is not a newline. Otherwise, there's no space in the users
	 * mailbox. Didn't find this documented anywhere....
	 */
	*(buf + readcount) = '\n';
	readcount ++;
#endif
		
	/***********************************************************
	 * Put the letter on the mail flist.
	 ***********************************************************/
	pin_buf.flag = 0;
	pin_buf.offset = 0;
	pin_buf.xbuf_file = (char *)NULL;

	pin_buf.size = readcount;
	pin_buf.data = buf;
	PIN_FLIST_FLD_SET(m_flistp, PIN_FLD_LETTER, (void *)&pin_buf, ebufp);

	cleanup:
	free(buf);
	buf = (char *)NULL;
	return;
}

/*******************************************************************
 * pin_mail_parse_conf():
 *
 *	Parses the pin.conf file for stuff we might need later.
 *	Also sets up pinlogging and working directory (for cores)
 *	for the mailer as spec in pin.conf.
 *
 *******************************************************************/
void
pin_mail_parse_conf(
	pin_flist_t	*m_flistp,
	pin_errbuf_t	*ebufp)
{
	char		*program = PIN_MAIL_PROG;

	char		*c_ptr = (char *)NULL;
	int		*i_ptr = (int *)0;
	poid_t		*p_ptr = NULL;

	u_int		level;
	char		logfile[256];

	int32		err;


	/***********************************************************
	 * Optional logging params.
	 ***********************************************************/
	/* Defaults. */
	pin_strlcpy(logfile, "default.pinlog",sizeof(logfile));
	level = PIN_ERR_LEVEL_WARNING;

	/* Get the requested program entries. */
	pin_conf(program, "logfile", PIN_FLDT_STR, (caddr_t *)&(c_ptr), &err);
	if (c_ptr != (char *)NULL) {
		pin_strlcpy(logfile, c_ptr,sizeof(logfile));
		free(c_ptr);
		c_ptr = (char *)NULL;
	}

	pin_conf(program, "loglevel", PIN_FLDT_INT, (caddr_t *)&(i_ptr), &err);
	if (i_ptr != (int *)0) {
		level = (u_int)*i_ptr;
		free(i_ptr);
		i_ptr = (int *)0;
	}

	/* Cache the 'default' domain for use in pin_mail_verify_user() */
	memset(default_domain, 0, sizeof(default_domain));	/* cleanse */
	pin_conf(program, "domain", PIN_FLDT_STR, (caddr_t *)&(c_ptr), &err);
	if (c_ptr != (char *)NULL) {
		pin_strlcpy(default_domain, c_ptr,sizeof(default_domain));
		free(c_ptr);
		c_ptr = (char *)NULL;
	}

	/* Ok, setup pinlog with our info. */
	PIN_ERR_SET_PROGRAM(program);
	PIN_ERR_SET_LOGFILE(logfile);
	PIN_ERR_SET_LEVEL(level);

	/***********************************************************
	 * Now the stuff we need later (ie pin_mailer specific).
	 ***********************************************************/
	/***********************************************************
	 * Get the userid poid.
	 ***********************************************************/
	pin_conf("-", "userid", PIN_FLDT_POID, (caddr_t *)&(p_ptr), &err);
	if (err != PIN_ERR_NONE) {

		ebufp->pin_err = PIN_ERR_INVALID_CONF;
		ebufp->reserved = err;
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"missing pin.conf file or userid", ebufp);

		return;
		/*****/

	} else {

		PIN_FLIST_FLD_PUT(m_flistp, PIN_FLD_USERID,
			(void *)p_ptr, ebufp);

		PIN_ERR_LOG_POID(PIN_ERR_LEVEL_DEBUG,
			"Userid poid set to:", p_ptr);

		p_ptr = (poid_t *)NULL;
	}

	/***********************************************************
	 * Get the name of the unix mail user.
	 ***********************************************************/
	pin_conf(program, "mailuser", PIN_FLDT_STR, (caddr_t *)&(c_ptr), &err);
	if (err != PIN_ERR_NONE) {

		ebufp->pin_err = PIN_ERR_INVALID_CONF;
		ebufp->reserved = err;
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"missing pin.conf file or mailuser", ebufp);

		return;
		/*****/

	} else {

		PIN_FLIST_FLD_PUT(m_flistp, PIN_FLD_NAME,
			(void *)c_ptr, ebufp);

		pinlog(FILE_SOURCE_ID, __LINE__, LOG_FLAG_DEBUG,
			"Unix mail user set to <%s>", c_ptr);

		c_ptr = (char *)NULL;
	}

	/***********************************************************
	 * Get the default maildir.
	 ***********************************************************/
	pin_conf(program, "maildir", PIN_FLDT_STR, (caddr_t *)&(c_ptr), &err);
	if (err != PIN_ERR_NONE) {

		ebufp->pin_err = PIN_ERR_INVALID_CONF;
		ebufp->reserved = err;
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"missing pin.conf file or maildir", ebufp);

		return;
		/*****/

	} else {

		PIN_FLIST_FLD_PUT(m_flistp, PIN_FLD_MAILDIR,
			(void *)c_ptr, ebufp);

		pinlog(FILE_SOURCE_ID, __LINE__, LOG_FLAG_DEBUG,
			"Mail directory default to <%s>", c_ptr);

		c_ptr = (char *)NULL;
	}

	/***********************************************************
	 * Finally, is our working dir (for cores etc) different?
	 * Important, change dir only after all other pin.conf.
	 ***********************************************************/
	pin_conf(program, "workdir", PIN_FLDT_STR, (caddr_t *)&(c_ptr), &err);
	if (c_ptr != (char *)NULL) {

		if(access(c_ptr, X_OK) != 0){
			if(mkdir(c_ptr, 0755)== -1)
			{
				pinlog(FILE_SOURCE_ID, __LINE__, LOG_FLAG_DEBUG,
					"Could not able to create directory at %s ",c_ptr);
				free(c_ptr);
				c_ptr= (char *) NULL;
				return;
			}
		}

		if(chdir(c_ptr)< 0 )
		{
			pinlog(FILE_SOURCE_ID, __LINE__, LOG_FLAG_DEBUG,
				"can not able to change directory to %s",c_ptr);
		}
		else
		{
			pinlog(FILE_SOURCE_ID, __LINE__, LOG_FLAG_DEBUG,
				"Working directory set to <%s>", c_ptr);
		}

		free(c_ptr);
		c_ptr = (char *)NULL;
	}

	/***********************************************************
	 * Must be no error.
	 ***********************************************************/
	PIN_ERR_CLEAR_ERR(ebufp);

	return;
}

/*******************************************************************
 * Main
 *
 *	Get everything set up and then call the real deliverer.
 *
 *******************************************************************/
int
main(
	int		argc,
	char		*argv[])
{
	pcm_context_t	*ctxp;
	pin_flist_t	*m_flistp;
	pin_errbuf_t	ebuf;

	u_int		deliver;
	int		exit_code;

	char		*dirs;
	char		path[BUFSIZ];
	char		tmpbuf[BUFSIZ];
	char		*tptr = tmpbuf;

	char		logfile[256];
	pin_strlcpy(logfile, "default.pinlog",sizeof(logfile));

	/***********************************************************
	 * go to apps/pin_mailer directory to find pin.conf file.
	 ***********************************************************/
	/* pin_mailer is invoked from sendmail.cf
	 * as /opt/portal/<ver>/bin/pin_mailer
	 */
	pin_strlcpy(path, argv[0],sizeof(path));

	/* pin_mailer *is* UNIX only, so magic '/' is okay! */
	dirs = strrchr(path, (int)'/');
	if (dirs) {
		path[sizeof(path) - strlen(dirs)-1] = '\0';
		/* move from /opt/portal/<ver>/bin
		 * to /opt/portal/<ver>/apps/pin_mailer
		 */
		pin_strlcat(path, "/../apps/pin_mailer",sizeof(path));
	}

	/* find dir */
	errno = 0;
	if (access(path, X_OK) != 0) {
		pin_snprintf(tptr,BUFSIZ, "Cannot find dir %s: %s.",
				path, strerror(errno));
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_NONE, tptr);
		exit(EX_TEMPFAIL);
	}

	errno = 0;
	(void)chdir(path);
	if (errno != 0) {
		pin_snprintf(tptr,BUFSIZ,"Unable to chdir to %s: %s.",
				path, strerror(errno));
		PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_NONE, tptr);
		exit(EX_TEMPFAIL);
	}
	/* end: chdir ../apps/pin_mailer */

	PIN_ERR_CLEAR_ERR(&ebuf);

	/***********************************************************
	 * Allocate the flist for holding this message.
	 ***********************************************************/
	m_flistp = PIN_FLIST_CREATE(&ebuf);

	/***********************************************************
	 * Setup some the working environment.
	 ***********************************************************/
	pin_mail_parse_conf(m_flistp, &ebuf);
	if (PIN_ERR_IS_ERR(&ebuf)) {
		/* Error already reported. */
		PIN_FLIST_DESTROY(m_flistp, NULL);
		exit(EX_TEMPFAIL);
	}

	/***********************************************************
	 * Parse the command line args.
	 ***********************************************************/
	pin_mail_parse_cmdline(argc, argv, m_flistp, &ebuf);
	if (PIN_ERR_IS_ERR(&ebuf)) {
		/* Error already reported. */
		PIN_FLIST_DESTROY(m_flistp, NULL);
		exit(EX_TEMPFAIL);
	}

	/***********************************************************
	 * Attempt to open up PCM channel.
	 ***********************************************************/
	pin_mail_init_pcm(&ctxp, m_flistp, &ebuf);
	if (PIN_ERR_IS_ERR(&ebuf)) {
		/* Error already reported. */
		PIN_FLIST_DESTROY(m_flistp, NULL);
		exit(EX_OSERR);
	}

	/***********************************************************
	 * Read and buffer stdin.
	 ***********************************************************/
	pin_mail_read_letter(m_flistp, &ebuf);
	if (PIN_ERR_IS_ERR(&ebuf)) {
		/* Error already reported. */
		PIN_FLIST_DESTROY(m_flistp, NULL);
		exit(EX_TEMPFAIL);
	}

	/***********************************************************
	 * Attempt to deliver the mail.
	 ***********************************************************/
	deliver = pin_mail_deliver_mail(ctxp, m_flistp, &ebuf);

	switch(deliver) {
	case PIN_ACT_VERIFY_PASSED:
		exit_code = EX_OK;
		break;
	case PIN_ACT_VERIFY_FAILED:
	default:
		if (PIN_ERR_IS_ERR(&ebuf))
			exit_code = EX_TEMPFAIL;
		else
			exit_code = EX_NOUSER;
		break;
	}

	/***********************************************************
	 * Clean  up.
	 ***********************************************************/
	PIN_FLIST_DESTROY(m_flistp, NULL);

	return (exit_code);
}
