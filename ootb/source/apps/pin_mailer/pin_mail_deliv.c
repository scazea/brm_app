/*
 *
* Copyright (c) 1996, 2015, Oracle and/or its affiliates. All rights reserved.
 *
 *      This material is the confidential property of Oracle Corporation or its
 *      licensors and may be used, reproduced, stored or transmitted only in
 *      accordance with a valid Oracle license or sublicense agreement.
 *
 */

#ifndef lint
static  char    Sccs_id[] = "@(#)%Portal Version: pin_mail_deliv.c:PortalBase7.3.1Int:2:2007-Oct-09 06:36:02 %";
#endif

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/file.h>
#include <sys/wait.h>
#include <sysexits.h>
#include <malloc.h>
#include <errno.h>
#include <pwd.h>

#include "pcm.h"
#include "pin_act.h"
#include "pin_errs.h"
#include "pinlog.h"
#include "pin_mailer.h"

#include "pin_os_unixuid.h"

#if defined(__hpux) || defined(__linux)
typedef struct flock flock_t;
#endif


#define PIN_MAIL_PROG		"pin_mail"


/*******************************************************************
 * pin_mail_mailfile():
 *
 *	Calculates the mailfile in which to store the mail
 *	based on the mail directory and user login.
 *
 *******************************************************************/
char *
pin_mail_mailfile(
	pin_flist_t	*m_flistp,
	pin_flist_t	*l_flistp,
	pin_errbuf_t	*ebufp)
{
	char		*login;
	char		*mdir;

	static char	mailfile[256];

	char		*ptr;

	login = (char *)PIN_FLIST_FLD_GET(l_flistp, PIN_FLD_LOGIN, 0, ebufp);
	mdir = (char *)PIN_FLIST_FLD_GET(m_flistp, PIN_FLD_MAILDIR, 0, ebufp);

	(void)pin_snprintf(mailfile,sizeof(mailfile), "%s/%s", mdir, login);

	ptr = strchr(mailfile, '@');
	if (ptr != NULL)
		*ptr = '\0';

	pinlog(PIN_FILE_SOURCE_ID, __LINE__, LOG_FLAG_DEBUG, 
		"calcing mailfile to <%s>", mailfile);

	return(mailfile);
}

/*******************************************************************
 * pin_mail_store_child():
 *
 *	Open a users mailfile and append the given mailfile to it.
 *	Assumed to be running with the proper uid/gid (of pin user).
 *
 *******************************************************************/
int
pin_mail_store_child(
	char		*mailfile,
	pin_buf_t	*letter,
	pin_errbuf_t	*ebufp)
{
	char		*message = (char *)(letter->data);
	u_int		message_size = letter->size;

#if defined(__hpux) || defined(__linux)
        flock_t         flck = {F_WRLCK, 0, 0, 0, 0 };
#else
        flock_t         flck = {F_WRLCK, 0, 0, 0, 0, 0, 0};
#endif

	int		fd = -1;
	int		tries, orig_size;

	if (PIN_ERR_IS_ERR(ebufp))
		return(EX_CANTCREAT);
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Make sure that the umask points to r/w for self only.
	 ***********************************************************/
	(void)umask(077);

	/***********************************************************
	 * Try to open the mailfile for output.
	 ***********************************************************/
	for(tries=0; tries < 10; ++tries) {
		sleep(10 * tries);
		errno = 0;

		/* Try to open the file. On certain errors, consider fatal */
		fd = open(mailfile, O_WRONLY|O_APPEND|O_CREAT, 0600);
		if(fd >=0)
			break;
		switch(errno) {
		case ENFILE:		/* System table full */
		case EINTR:
			continue;
		default:
			pinlog(PIN_FILE_SOURCE_ID, __LINE__, LOG_FLAG_ERROR,
				"Couldn't open mailbox <%s>", mailfile);
			ebufp->pin_err = PIN_ERR_FILE_IO;
			return(EX_CANTCREAT);
		}
	}

	/***********************************************************
	 * Now, try to acquire the exclusive lock.
	 ***********************************************************/
	if(fcntl(fd, F_SETLK, flck) < 0) {
		pinlog(PIN_FILE_SOURCE_ID, __LINE__, LOG_FLAG_ERROR,
			"Couldn't lock mailbox <%s>", mailfile);
		if(fd >= 0)
			close(fd);
		ebufp->pin_err = PIN_ERR_FILE_IO;
		return(EX_CANTCREAT);
	}

	/***********************************************************
	 * Extra-safe check that we're at the end of the file.
	 ***********************************************************/
	orig_size = lseek(fd, 0, SEEK_END);

	/***********************************************************
	 * Dump the message to the file.
	 ***********************************************************/
	if(write(fd, message, message_size) != message_size) {
		/* 
		 * We didn't get everything written. What should we do?
		 * My best guess is to put the mailfile back to it's
		 * original state. In this manner, when we run out of disk
		 * space, we shouldn't have mailboxes in unknown states...
		 */
		(void)ftruncate(fd, orig_size);
		pinlog(PIN_FILE_SOURCE_ID, __LINE__, LOG_FLAG_ERROR,
			"Error writing file to users mailbox <%s>", mailfile);
		if(fd >= 0)
			close(fd);
		ebufp->pin_err = PIN_ERR_FILE_IO;
		return(EX_CANTCREAT);
	}

	/***********************************************************
	 * Looks good.
	 ***********************************************************/
	pinlog(PIN_FILE_SOURCE_ID, __LINE__, LOG_FLAG_DEBUG,
		"Mail appended to mailbox <%s>", mailfile);

	/***********************************************************
	 * Clean up.
	 ***********************************************************/
	/* NOTE: flock above implicitly released with close... */
	if(fd >= 0)
		close(fd);

	PIN_ERR_CLEAR_ERR(ebufp);

	return(EX_OK);
}
		
/*******************************************************************
 * pin_mail_store_mail():
 *
 *	Attempt to store mail in a users mailbox.
 *	We do this using the uid/gid from pin.conf which
 *	presumably is the id of the shared pin (mail) user.
 *
 *******************************************************************/
void
pin_mail_store_mail(
	pin_flist_t	*flistp,
	pin_flist_t	*m_flistp,
	pin_errbuf_t	*ebufp)
{
	char		*program = PIN_MAIL_PROG;

	char		*mailuser;
	struct passwd	*pwp = NULL ;
	uid_t		uid;
	gid_t		gid;

	char		*mailfile;
	pin_buf_t	*letter;

	pid_t		child_pid;
	int		exit_code, status, i;
	char tmp_buf[255];

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Get the uid/gid for the pin (unix) user.
	 ***********************************************************/
	mailuser = (char *)PIN_FLIST_FLD_GET(m_flistp, PIN_FLD_NAME, 0, ebufp);
	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"missing mailuser in pin_mail_store_mail", ebufp);
		return;
	}

	//if ((pwp = getpwnam(mailuser)) == NULL) {
	if(pin_getpwnam(mailuser, pwp, tmp_buf, sizeof(tmp_buf)) != 0){
		ebufp->pin_err = PIN_ERR_INVALID_CONF;
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"no password file entry for mailuser", ebufp);
		return;
	}
	if(pwp){
	   uid = pwp->pw_uid;
	   gid = pwp->pw_gid;
	}

	/***********************************************************
	 * Build mailfile path, based on configuration.
	 ***********************************************************/
	mailfile = pin_mail_mailfile(m_flistp, flistp, ebufp);
	pinlog(PIN_FILE_SOURCE_ID, __LINE__, LOG_FLAG_DEBUG, 
		"setting mailfile to <%s>", mailfile);

	/***********************************************************
	 * Get the actual letter to save.
	 ***********************************************************/
	letter = (pin_buf_t *)PIN_FLIST_FLD_GET(m_flistp,
		PIN_FLD_LETTER, 0, ebufp);

	/***********************************************************
	 * Fork a child to do the actual file writing.
	 ***********************************************************/
	child_pid = fork();
	switch(child_pid) {
	case -1:
		/* Couldn't fork -- return probemo */
		pinlog(PIN_FILE_SOURCE_ID, __LINE__, LOG_FLAG_ERROR,
			"Couldn't fork child delivery program.");
		ebufp->pin_err = PIN_ERR_BAD_VALUE;
		return;
	
	case 0:
		/* Change the GID to that of the delivery user */
		if (setgid(gid) < 0) {
			pinlog(PIN_FILE_SOURCE_ID, __LINE__, LOG_FLAG_ERROR, 
				"child mailer can't change gid to <%d>", gid);
			exit(EX_OSERR);
		} else {
			pinlog(PIN_FILE_SOURCE_ID, __LINE__, LOG_FLAG_DEBUG, 
				"child mailer changing gid to <%d>", gid);
		}

		/* Change the UID to that of the delivery user */
		if(setuid(uid) < 0) {
			pinlog(PIN_FILE_SOURCE_ID, __LINE__, LOG_FLAG_ERROR,
				"child mailer can't change uid to <%d>", uid);
			exit(EX_OSERR);
		} else {
			pinlog(PIN_FILE_SOURCE_ID, __LINE__, LOG_FLAG_DEBUG, 
				"child mailer changing uid to <%d>", uid);
		}

		/* Attempt to actually perform the mail */
		exit_code = pin_mail_store_child(mailfile, letter, ebufp);

		exit(exit_code);
		/* NOTREACHED */

	default:
		/* Parent -- work will be done further below */
		break;
	}

	/***********************************************************
	 * Hang out until the child dies.
	 ***********************************************************/
	do {
		errno = 0;
		i = wait(&status);
	} while((i >= 0 || errno == EINTR) && i != child_pid);

	if(WIFEXITED(status)) 
		exit_code = WEXITSTATUS(status);
	else
		exit_code = -1;
	switch(exit_code) {
	case EX_OK:
		break;
	default:
		pinlog(PIN_FILE_SOURCE_ID, __LINE__, LOG_FLAG_ERROR, 
			"Child mailer terminated abnormally (%d).", exit_code);
		ebufp->pin_err = PIN_ERR_BAD_VALUE;
		return;
	}

	PIN_ERR_CLEAR_ERR(ebufp);

	return;
}

/*******************************************************************
 * pin_mail_deliver_mail():
 *
 *	Real worker that tries to deliver mail to a given user.
 *
 *******************************************************************/
u_int
pin_mail_deliver_mail(
	pcm_context_t	*ctxp,
	pin_flist_t	*m_flistp,
	pin_errbuf_t	*ebufp)
{
	pin_cookie_t	cookie = NULL;
	pin_flist_t	*flistp = NULL;

	int32		eid;
	u_int		verify;

	/***********************************************************
	 * Debug: the whole message flist.
	 ***********************************************************/
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG, "Mail Msg Flist", m_flistp);

	/***********************************************************
	 * Walk the list of recipients and send them the mail.
	 ***********************************************************/
	flistp = PIN_FLIST_ELEM_GET_NEXT(m_flistp, PIN_FLD_RECIPIENTS,
		&eid, 0, &cookie, ebufp);
	while (flistp != (pin_flist_t *)NULL) {

		/***************************************************
		 * Authenticate this user.
		 ***************************************************/
		verify = pin_mail_verify_user(ctxp, flistp, ebufp);

		switch (verify) {
		case PIN_ACT_VERIFY_PASSED:
			break;
		case PIN_ACT_VERIFY_FAILED:
		default:
			PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
				"Could not verify mail recipient", ebufp);
			PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
				"Bounced mail for recipient", flistp);
			return(PIN_ACT_VERIFY_FAILED);
		}

		/***************************************************
		 * Guess we'll store the mail.
		 ***************************************************/
		pin_mail_store_mail(flistp, m_flistp, ebufp);
		if (PIN_ERR_IS_ERR(ebufp)) {
			PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
				"Could not update user mailbox", ebufp);
			PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
				"Bounced mail for recipient", flistp);
			return(PIN_ACT_VERIFY_FAILED);
		}
			
		/***************************************************
		 * Get the next recipient.
		 ***************************************************/
		flistp = PIN_FLIST_ELEM_GET_NEXT(m_flistp, PIN_FLD_RECIPIENTS,
			&eid, 0, &cookie, ebufp);
	}
	if (ebufp->pin_err == PIN_ERR_NOT_FOUND)
		PIN_ERR_CLEAR_ERR(ebufp);

	return(PIN_ACT_VERIFY_PASSED);
}
