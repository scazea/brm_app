/*******************************************************************
 * 
 * (#)$Id: pop_pcm.c /cgbubrm_7.5.0.portalbase/1 2015/12/02 21:44:42 akkkumar Exp $ 
 * 
* Copyright (c) 1996, 2015, Oracle and/or its affiliates. All rights reserved.
 *
 * This material is the confidential property of Oracle Corporation
 * or its licensors and may be used, reproduced, stored or transmitted 
 * only in accordance with a valid Oracle license or sublicense agreement.
 *
 *******************************************************************/

#ifndef lint
static  char    Sccs_id[] = "@(#)%full_filespec: pop_pcm.c~6:csrc:1 %";
#endif

/*******************************************************************
 *******************************************************************/
	/***********************************************************
	 ***********************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#include <sys/types.h>
#include <pwd.h>

#include "pcm.h"
#include "ops/mail.h"
#include "pin_errs.h"
#include "pinlog.h"
#include "pin_act.h"

#include "popper.h"

#define FILE_SOURCE_ID		"pop_pcm.c(1.7)"

#define PIN_POP_PROG		"popper"

/*******************************************************************
 * Global context.
 *******************************************************************/
pcm_context_t	*ctxp;

/*******************************************************************
 * Routines contained herein.
 *******************************************************************/
struct passwd *pin_pop_getpwnam();

void pin_pop_init_pcm();
void pin_pop_getlogin();
void pin_pop_mailfile();

int pin_pop_auth_user();

/*******************************************************************
 * pin_pop_getpwnam():
 *******************************************************************/
struct passwd *
pin_pop_getpwnam(p, ebufp)
	POP		*p;
	pin_errbuf_t	*ebufp;
{
	char		*user = NULL;
	char		*program = PIN_POP_PROG;

	struct passwd	*pwp = NULL;

	int32		err;

	if (PIN_ERR_IS_ERR(ebufp))
		return(NULL);
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Get the pin user name from the pin.conf file.
	 ***********************************************************/
        pin_conf(program, "mailuser", PIN_FLDT_STR, (caddr_t *)&(user), &err);
        if(err != PIN_ERR_NONE) {
		ebufp->pin_err = PIN_ERR_INVALID_CONF;
		pop_log(p, POP_PRIORITY, "missing pin.conf file or mailuser");
		return(NULL);
        }

	/***********************************************************
	 * Get the password file info for the mail user.
	 ***********************************************************/
	if ((pwp = getpwnam(user)) == NULL) {
		ebufp->pin_err = PIN_ERR_INVALID_CONF;
		pop_log(p, POP_PRIORITY, "no password entry for mailuser");
	}

	/***********************************************************
	 * Clean up.
	 ***********************************************************/
	free(user);

	return(pwp);
}

/*******************************************************************
 * pin_pop_getlogin():
 *
 *	Portal usually stores email logins as user@domain.
 *	Some clients only pass in the user part so we add
 *	a default domain (if conf'ed) when that happens.
 *
 *******************************************************************/
void
pin_pop_getlogin(p, login, ebufp)
	POP		*p;
	char		*login;
	pin_errbuf_t	*ebufp;
{
	char		*domain = NULL;
	char		*program = PIN_POP_PROG;

	char		*ptr = NULL;

	int32		err;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Start with the user name from the client.
	 ***********************************************************/
	(void)strcpy(login, p->user);

	/***********************************************************
	 * If already user@domain, do nothing, otherwise conf.
	 ***********************************************************/
	ptr = strchr(login, '@');
	if (ptr == NULL) {

        	pin_conf(program, "domain", PIN_FLDT_STR,
			(caddr_t *)&(domain), &err);

		if (domain != (char *)NULL) {
			(void)sprintf(login, "%s@%s", p->user, domain);
			free(domain);
		}

	}

	return;
}

/*******************************************************************
 * pin_pop_mailfile():
 *
 *	Routine to determine where a given user's mailfile
 *	actually is. This allows us to someday have our own
 *	policy for how to spread mailfiles across the system.
 *	The ctxp is here in case we need to consult the FM.
 *
 *******************************************************************/
void
pin_pop_mailfile(p, ctxp, ebufp)
	POP		*p;
	pcm_context_t	*ctxp;
	pin_errbuf_t	*ebufp;
{
	char		*maildir = NULL;
	char		*dropdir = NULL;
	char		*program = PIN_POP_PROG;

	char		mailfile[256];

	char		*ptr = NULL;

	int32		err;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Get the base maildir from the pin.conf file.
	 ***********************************************************/
        pin_conf(program, "maildir", PIN_FLDT_STR, (caddr_t *)&(maildir), &err);
        if(err != PIN_ERR_NONE) {
		ebufp->pin_err = PIN_ERR_INVALID_CONF;
		pop_log(p, POP_PRIORITY, "missing pin.conf file or maildir");
		return;
        }

	/***********************************************************
	 * For now, it's just a flat directory.
	 ***********************************************************/
	(void)pin_snprintf(mailfile,sizeof(mailfile), "%s/%s", maildir, p->user);

	ptr = strchr(mailfile, '@');
	if (ptr != NULL)
		*ptr = '\0';

	(void)pin_strlcpy(p->drop_name, mailfile,sizeof(p->drop_name));

	/***********************************************************
	 * Get the base dropdir from the pin.conf file.
	 ***********************************************************/
        pin_conf(program, "dropdir", PIN_FLDT_STR, (caddr_t *)&(dropdir), &err);
	if (dropdir == NULL)
		dropdir = maildir;

	/***********************************************************
	 * For now, also just a flat directory.
	 ***********************************************************/
	(void)pin_snprintf(mailfile,sizeof(mailfile), "%s/%s", dropdir, p->user);

	ptr = strchr(mailfile, '@');
	if (ptr != NULL)
		*ptr = '\0';

	(void)pin_snprintf(p->temp_drop,sizeof(p->temp_drop), "%s.pop", mailfile);

	/***********************************************************
	 * Clean up.
	 ***********************************************************/
	free(maildir);
	free(dropdir);

	return;
}

/*******************************************************************
 * pin_pop_init_pcm():
 *
 * 	Open up a communication channel to the PIN system.
 *	XXX For now we assume a NULL login connection.
 *
 *******************************************************************/
void
pin_pop_init_pcm(p, ctxpp, cfg_uidp, ebufp)
	POP		*p;
	pcm_context_t	**ctxpp;
	poid_t		**cfg_uidp;
	pin_errbuf_t	*ebufp;
{
        int32		err;
        u_int64		database;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Only do this once.
	 ***********************************************************/
	if (ctxp != NULL)
		return;

	/***********************************************************
	 * Get the userid from the pin.conf file.
	 ***********************************************************/
        pin_conf("-", "userid", PIN_FLDT_POID, (caddr_t *)cfg_uidp, &err);
        if(err != PIN_ERR_NONE) {
		ebufp->pin_err = PIN_ERR_INVALID_CONF;
		pop_log(p, POP_PRIORITY, "missing pin.conf file or userid");
        }

	/***********************************************************
	 * Open up a NAP connection
	 ***********************************************************/
	PCM_CONNECT(ctxpp, (int64 *)(&(database)), ebufp);
	if (PIN_ERR_IS_ERR(ebufp)) {
		pop_log(p, POP_PRIORITY, "couldn't open NAP connection");
        }

	/***********************************************************
	 * Error set above
	 ***********************************************************/

	return;
}

/*******************************************************************
 * pin_pop_auth_user():
 *******************************************************************/
int
pin_pop_auth_user(p, pwp)
	POP		*p;
	struct passwd	**pwp;
{
	pin_flist_t	*flistp = NULL;
	pin_flist_t	*r_flistp = NULL;

	poid_t		*userid = NULL;
	struct passwd	*pw_here = NULL;

	u_int		*verify;
	u_int		*reason;

	char		login[256];
	char		*passwd = p->pop_parm[1];

	int		pop_res;
	pin_errbuf_t	ebuf;

	PIN_ERR_CLEAR_ERR(&ebuf);

	/***********************************************************
	 * If needed, open the channel to the PIN system.
	 ***********************************************************/
	pin_pop_init_pcm(p, &ctxp, &userid, &ebuf);
	if (PIN_ERR_IS_ERR(&ebuf))
		return (pop_msg(p,POP_FAILURE, "internal error"));

	/***********************************************************
	 * What login do we actually validate?
	 ***********************************************************/
	pin_pop_getlogin(p, login, &ebuf);
	if (PIN_ERR_IS_ERR(&ebuf))
		return (pop_msg(p,POP_FAILURE, "internal error"));

	/***********************************************************
	 * Create the flist for verifying.
	 ***********************************************************/
	flistp = PIN_FLIST_CREATE(&ebuf);

	/* Add userid poid (since we need one) */
	PIN_FLIST_FLD_SET(flistp, PIN_FLD_POID, (void *)userid, &ebuf);

	/* Add the login and passwd */
	PIN_FLIST_FLD_SET(flistp, PIN_FLD_LOGIN, (void *)login, &ebuf);
	PIN_FLIST_FLD_SET(flistp, PIN_FLD_PASSWD_CLEAR, (void *)passwd, &ebuf);

	/***********************************************************
	 * Call the mail FM to verify delivery.
	 ***********************************************************/
	PCM_OP(ctxp, PCM_OP_MAIL_LOGIN_VERIFY, 0, flistp, &r_flistp, &ebuf);

	/***********************************************************
	 * Get the result to return.
	 ***********************************************************/
	verify = (u_int *)PIN_FLIST_FLD_GET(r_flistp, PIN_FLD_RESULT, 0, &ebuf);
	reason = (u_int *)PIN_FLIST_FLD_GET(r_flistp, PIN_FLD_TYPE, 1, &ebuf);

	if (PIN_ERR_IS_ERR(&ebuf)) {

		pop_res = pop_msg(p,POP_FAILURE, "internal error");

	} else if (*verify == PIN_ACT_VERIFY_PASSED) {

		pop_res = POP_SUCCESS;

	} else if (*reason == (u_int )NULL) {

		pop_res = pop_msg(p,POP_FAILURE, "internal error");

	} else {

		switch (*reason) {
		case PIN_ACT_CHECK_ACCT_STATUS:
		case PIN_ACT_CHECK_SRVC_STATUS:
			pop_res = pop_msg(p,POP_FAILURE,
				"%s: account not active", p->user);
			break;
		case PIN_ACT_CHECK_ACCT_PASSWD:
		case PIN_ACT_CHECK_SRVC_PASSWD:
			pop_res = pop_msg(p,POP_FAILURE,
				"%s: invalid password", p->user);
			break;
		case PIN_ACT_CHECK_CREDIT_AVAIL:
			pop_res = pop_msg(p,POP_FAILURE,
				"%s: account over creditlimit", p->user);
			break;
		default:
			pop_res = pop_msg(p,POP_FAILURE, "internal error");
			break;
		}

	}

	/***********************************************************
	 * Clean up flists.
	 ***********************************************************/
	PIN_FLIST_DESTROY(flistp, NULL);
	PIN_FLIST_DESTROY(r_flistp, NULL);

	/***********************************************************
	 * Ok, set the desired mailfile.
	 ***********************************************************/
	pin_pop_mailfile(p, ctxp, &ebuf);
	if (PIN_ERR_IS_ERR(&ebuf))
		return (pop_msg(p,POP_FAILURE, "internal error"));

	/***********************************************************
	 * Get the passwd entry for mailuser (for uid/gid).
	 ***********************************************************/
	if ((pw_here = pin_pop_getpwnam(p, &ebuf)) == NULL)
		return (pop_msg(p,POP_FAILURE, "internal error"));

	*pwp = pw_here;

	/***********************************************************
	 * Return verify result.
	 ***********************************************************/
	return(pop_res);
}

