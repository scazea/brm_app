setenv PIN_HOME /u02/brm/ebs/brm_oob/brm_app/7.5
setenv PIN_LOG_DIR /u02/brm/ebs/brm_oob/brm_app/7.5/var
setenv PIN_LOG /u02/brm/ebs/brm_oob/brm_app/7.5/var
setenv LIBRARYEXTENSION .so
setenv LIBRARYPREFIX lib

set PERL_VERSION = '5.24.0'

set TPSOURCE=/u02/brm/ebs/brm_oob/brm_app/ThirdParty
if (-f $TPSOURCE/source.me.csh) then
source $TPSOURCE/source.me.csh
endif

if ( ${?PERL5LIB} ) then
setenv PERL5LIB $PIN_HOME/lib:$TPSOURCE/perl/${PERL_VERSION}/lib:$TPSOURCE/perl/${PERL_VERSION}/lib/${PERL_VERSION}:$TPSOURCE/perl/${PERL_VERSION}/lib/site_perl:$PERL5LIB
else
  setenv PERL5LIB $PIN_HOME/lib:$TPSOURCE/perl/${PERL_VERSION}/lib:$TPSOURCE/perl/${PERL_VERSION}/lib/${PERL_VERSION}:$TPSOURCE/perl/${PERL_VERSION}/lib/site_perl
endif
if ( ${?PATH} ) then
  setenv PATH $PIN_HOME/bin:$PIN_HOME/lib:$PATH
else
  setenv PATH $PIN_HOME/bin:$PIN_HOME/lib
endif
if ( ${?LD_LIBRARY_PATH} ) then
  setenv LD_LIBRARY_PATH $PIN_HOME/lib64:$PIN_HOME/lib:$LD_LIBRARY_PATH
else
  setenv LD_LIBRARY_PATH $PIN_HOME/lib64:$PIN_HOME/lib
endif
if ( ${?LD_LIBRARY_PATH_64} ) then
  setenv LD_LIBRARY_PATH_64 $PIN_HOME/lib:$PIN_HOME/lib64:$LD_LIBRARY_PATH_64
else
  setenv LD_LIBRARY_PATH_64 $PIN_HOME/lib:$PIN_HOME/lib64
endif
if ( ${?SHLIB_PATH} ) then
  setenv SHLIB_PATH $PIN_HOME/lib:$PIN_HOME/lib64:$SHLIB_PATH
else
  setenv SHLIB_PATH $PIN_HOME/lib:$PIN_HOME/lib64
endif
if ( ${?LIBPATH} ) then
  setenv LIBPATH $TPSOURCE/perl/${PERL_VERSION}/bin:$PIN_HOME/bin:$PIN_HOME/lib:$PIN_HOME/lib64:$LIBPATH
else
  setenv LIBPATH $TPSOURCE/perl/${PERL_VERSION}/bin:$PIN_HOME/bin:$PIN_HOME/lib:$PIN_HOME/lib64
endif

if ( -f $PIN_HOME/ttsource.me.csh) then
   source $PIN_HOME/ttsource.me.csh
endif

set OSNAME = `uname`
if($OSNAME == 'SunOS') then
  setenv LD_LIBRARY_PATH /usr/sfw/lib:$LD_LIBRARY_PATH
endif
