/*
 * @(#)%Portal Version: pin_gsm_aaa.h:WirelessVelocityInt:4:2006-Sep-14 11:18:59 %
 *
 * Copyright (c) 2000, 2009, Oracle and/or its affiliates.All rights reserved. 
 *
 * This material is the confidential property of Oracle Corporation or its 
 * licensors and may be used, reproduced, stored or transmitted only in 
 * accordance with a valid Oracle license or sublicense agreement.
 *
 */

#ifndef PIN_GSM_AAA_H_
#define PIN_GSM_AAA_H_

#ifdef __cplusplus
extern "C" {
#endif

/* PIN_GSM_CONFIG_TYPE is the type of the config object used in GSM */
#define PIN_GSM_CONFIG_TYPE "/config/aaa/gsm"

/*
 * String constants for caching config.
 */
#define GSM_CONFIG_CACHE_TTL       60*60
#define GSM_CONFIG_CACHE_NAME      "fm_gsm_utils_config_cache"

/*
 * PIN_FLD_CALCMAX_SESSION - Flag to indicate how PCM_OP_CALC_MAX should
 * be called from PCM_OP_GSM_AUTHORIZE.
 */
typedef enum pin_gsm_calcmax_session {
		/* Use the master session for pre-rating in authorize */
        PIN_GSM_CALCMAX_MASTER =	1,
		/* Use the subsession for pre-rating in authorize */
        PIN_GSM_CALCMAX_SUBSESSION =	2
} pin_gsm_calcmax_session;

/*
 * PIN_FLD_SUBSESSION_MODE - The mode controlling the subsession creation
 */
typedef enum pin_gsm_subsession_mode {
		/* not set yet (implies config object not yet read) */
        PIN_GSM_MODE_NOT_SET =	        0,
		/* drop the sub session: data a summurize in the
                   master session only */
        PIN_GSM_MODE_DROP =	        1,
                /* Create and rate sub session immediately + summurize
                   data in the master session */
        PIN_GSM_MODE_RATE_IMMEDIATELY =	2
} pin_gsm_subsession_mode;


/*
 * The change condition gsm subsession field values
 * Values defined by ETSI
 */
typedef enum pin_gsm_change_condition_t {
    /* A change in the QoS */
    PIN_GSM_CC_QOS_CHANGE    =                 0,
    /* On reaching tariff time change */
    PIN_GSM_CC_TARIFF_TIME   =                 1,
    /* Closure of the session */
    PIN_GSM_CC_RECORD_CLOSURE=                 2
} pin_gsm_change_condition_t;



/*
 * PIN_FLD_STATUS - The status of this logout re the PIN system,
 * *not* the NAS status.
 */
typedef enum pin_gsm_status_t {
		/* not set yet (implies session is open) */
	PIN_GSM_STATUS_NOT_SET =	0,
		/* OK - login/logout is OK */
	PIN_GSM_STATUS_OK =		1,
		/* DROP: we drop the event because it's a free APN */
	PIN_GSM_STATUS_DROP =		2,
		/* NOK: error during close and rating of the session */
	PIN_GSM_STATUS_NOK =		3,
		/* one elt of the GSM network was found to have been reset */
	PIN_GSM_STATUS_RESET =		4,
		/* session closed because another session with the same sessionid
		 and GGSN Address came, so it has been considered as orphan (left
		 open because we did not get the close CDR possibly)
		*/
	PIN_GSM_STATUS_ORPHAN =	5
} pin_gsm_status_t;

/*
 * PIN_FLD_CLOSE_REASON - The reason for the release of the CDR.
 * Value defined by ETSI/GSM
 */
typedef enum pin_gsm_close_reason_t {
	/* PDP context release or GSM detach */
	PIN_GSM_CLOSE_NORMAL_RELEASE = 0,
	/* Partial record */
	PIN_GSM_CLOSE_PARTIAL_RECORD = 1,
	/* Partial record call reestablishment */
	PIN_GSM_CLOSE_PARTIAL_RECORD_CALL_REESTABLISHMENT = 2,
	/* unsucessful call attempt */
	PIN_GSM_CLOSE_UNSUCCESSFUL_CALL_ATTEMPT = 3,
	/* abnormal terminaison, PDP or MM context */
	PIN_GSM_CLOSE_ABNORMAL_RELEASE = 4,
	/* Camel (roaming) init call release */
	PIN_GSM_CLOSE_CAMEL_INIT_CALL_RELEASE = 5,
	/* data volume limit */
	PIN_GSM_CLOSE_VOLUME_LIMIT = 16,
	/* time (duration) limit */
	PIN_GSM_CLOSE_TIME_LIMIT = 17,
	/* SGSN Change */
	PIN_GSM_CLOSE_SGSN_CHANGE = 18,
	/* Maximum change condition limit reach */
	PIN_GSM_CLOSE_MAX_CHANGE_COND = 19,
	/* Management intervention */
	PIN_GSM_CLOSE_MNGT_INTERVENTION = 20
} pin_gsm_close_reason_t;


/*******************************************************************
 * PCM_OP_GSM_AUTHORIZE fields
 *******************************************************************/
/*
 * PIN_FLD_RESULT - result of a act_verify operation.
 */
#define	PIN_GSM_AUTHORIZE_PASSED	PIN_BOOLEAN_TRUE
#define	PIN_GSM_AUTHORIZE_FAILED	PIN_BOOLEAN_FALSE

/*
 * PIN_FLD_STATUS - error type of gsm_get/set_call_features opcodes
 *
 * Most are self explanatory, but:
 * DUPLICATE_FILTER_ENTRIES indicates that the same APN
 * was found more than once in a filter list, while
 * DUPLICATE_FILTERS indicates that two or more filters
 * were found with the same mode.
 */

#define PIN_GSM_OK					0
#define PIN_GSM_DUPLICATE_FILTER_ENTRIES		1
#define PIN_GSM_DUPLICATE_FILTERS			2
#define PIN_GSM_BOTH_ALLOW_AND_DENY_LIST_SPECIFIED	3
#define PIN_GSM_INVALID_MODE_SPECIFIED			4

#define PIN_OBJ_TYPE_GSM "gsm"
/*******************************************************************
 * Flag to identify if this opcode call is for a pre or post paid call.
 *******************************************************************/
typedef enum pin_gsm_payment_mode {
	PIN_GSM_PREPAY_MODE =		1,
	PIN_GSM_POSTPAY_MODE =		2
} pin_gsm_payment_mode_t;


/*
 * PIN_FLD_MODE - If current balance is insufficient, should we
 *		  authorize a partial quantity or should we error out?
 */
typedef enum pin_gsm_auth_mode {
	PIN_GSM_ALLOW_PARTIAL =	0,
	PIN_GSM_REQUIRE_ALL =	1
} pin_gsm_auth_mode_t;

/*
 * Values for GSM AAA opcodes
 */
#define PIN_GSM_AAA_NO          PIN_BOOLEAN_FALSE
#define PIN_GSM_AAA_YES         PIN_BOOLEAN_TRUE
#define PIN_GSM_AAA_DONT_KNOW  -1

/************************************************************************
 * Mode of input
 ***********************************************************************/
#define USED_AGGR_MODE          1
#define USED_INCR_MODE          2
#define REQ_AGGR_MODE           4
#define REQ_INCR_MODE           8

/************************************************************************
 * Rating Mode 
 ***********************************************************************/
#define NO_ADJUST        0
#define ADJUST           1

/*******************************************************************
 * fm_gsm_aaa_utils_read_config()
 *
 * Reads the specified config object
 *
 * input:
 *   i_flistp: Input flist containing the information required
 *             to find the config object
 * output:
 *   return_flp contains the config object
 *   Caller is responsible for cleaning up memory!
 *   int32 : indicates if the config object exists
 *
 *******************************************************************/
int32
fm_gsm_aaa_utils_read_config (
        pcm_context_t   *ctxp,
        pin_flist_t     *i_flistp,
        pin_flist_t     **return_flpp,
        int32           read_reserve_obj,
        pin_errbuf_t    *ebufp);

/*******************************************************************
 * fm_gsm_aaa_utils_check_if_read_config()
 *
 * Checks if the specified config type has  been previously read into
 * the cm cache.
 *
 * input:
 *   i_flistp: Flist containing information whether a given config
 *             type was read previously. if so this flist keeps
 *             information as to whether the object was found or not.
 *   config_type: Config type to be looked up
 * output:
 *   ret_val : A substruct that hold the status and the config poid
 *             when the config object has been previously read.
 *   result: PIN_GSM_AAA_DONT_KNOW indicates that a read was never
 *           attempted on a config object of this type.
 *           PIN_GSM_AAA_YES  indicates that a read was attempted on a config
 *           object of this type and an object was found.
 *           PIN_GSM_AAA_NO  indicates that a read was attempted on a config
 *           object of this type and an object was not found.
 *******************************************************************/
void
fm_gsm_aaa_utils_check_if_read_config (
        pcm_context_t   *ctxp,
        pin_flist_t     *i_flistp,
        char            *config_type,
        config_info_struct *ret_val,
        pin_errbuf_t    *ebufp);


/*******************************************************************
 * fm_gsm_aaa_utils_update_if_read_config()
 *
 * Updates the input flist to indicate whether the config object
 * of a given type was found or not.
 *
 * input:
 *   i_flistp: Flist containing information whether a given config
 *             type was read previously. if so this flist keeps
 *             information as to whether the object was found or not.
 *   config_type: Config type to be updated
 *   status: PIN_GSM_AAA_YES or PIN_GSM_AAA_NO
 *******************************************************************/
void
fm_gsm_aaa_utils_update_if_read_config(
        pcm_context_t   *ctxp,
        pin_flist_t     *i_flistp,
        poid_t          *config_type,
        int32           status,
        pin_errbuf_t    *ebufp);

/*******************************************************************
 * fm_gsm_aaa_utils_read_from_cache()
 * Gets the config object from cm cache
 *
 * input:
 * config_poidp: Poid of the config object to be read
 *
 * output:
 * r_flpp : Contains the config object if found.
 *******************************************************************/
void
fm_gsm_aaa_utils_read_from_cache(
        pcm_context_t   *ctxp,
        poid_t          *config_poidp,
        pin_flist_t     **r_flpp,
        pin_errbuf_t    *ebufp);

/*******************************************************************
 * fm_gsm_aaa_utils_get_config()
 *
 *  Searches the database for a config object of the
 *  given type and returns the object if one is found.
 *
 * input:
 *   i_flistp: input flist containing the type only config poid
 *   i_poidp : type only config poid
 *
 * output:
 *   r_flistpp: the flist returned by the search.
 *
 *******************************************************************/
void
fm_gsm_aaa_utils_get_config(
        pcm_context_t   *ctxp,
        poid_t          *i_poidp,
        pin_flist_t     **r_flistpp,
        pin_errbuf_t    *ebufp);


/*******************************************************************
 * Routines defined  in utils to create a GSM_INFO Substruct.
 *******************************************************************/

void
fm_gsm_aaa_utils_create_gsm_info(pin_flist_t **flist_without_gsm_info,
                     pin_errbuf_t *ebufp);


/*******************************************************************
 * Routines defined  to add  PIN_FLD_OBJ_TYPE Field in the FList.
 *******************************************************************/

void 
fm_gsm_aaa_utils_populate_obj_type(pin_flist_t             **input_flistpp,
                        pin_errbuf_t            *ebufp);

/**********************************************************************
 * This Function renames GSM specific Field name to telco specific name
 **********************************************************************/

void
fm_gsm_aaa_utils_rename_field(pin_flist_t            **input_flistp,
                              pin_fld_num_t          existing_name,
                              pin_fld_num_t          telco_specific_name,
                              int64                  priority,
                              pin_errbuf_t           *ebufp);

#ifdef __cplusplus
}
#endif

#endif	/*_PIN_GSM_AAA_H*/
