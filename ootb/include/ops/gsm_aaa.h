/*	
 *	@(#)%Portal Version: gsm_aaa.h:CommonIncludeInt:9:2007-Sep-11 23:04:05 %
 *	
 *	Copyright (c) 1996 - 2007 Oracle. All rights reserved.
 *	
 *	This material is the confidential property of Oracle Corporation or its
 *	licensors and may be used, reproduced, stored or transmitted only in
 *	accordance with a valid Oracle license or sublicense agreement.
 */

#ifndef _PCM_GSM_AAA_OPS_H_
#define _PCM_GSM_AAA_OPS_H_

/*
 * This file contains the opcode definitions for the GENERIC TELCO PCM API.
 */

/* =====================================================================
 *   NAME            : PCM_GSM_AAA_OPS
 *   TOTAL RANGE     : 4034..4100
 *   USED RANGE      : 4034..4062
 *   RESERVED RANGE  : 4063..4100
 *   ASSOCIATED FM   : fm_gsm_aaa, fm_gsm_aaa_pol
 * =====================================================================
 */

#include "ops/base.h"

/* Opcodes for GSM AAA - Will be deprecated in the future
 * because of improper opcode naming convention.
 */
#define PCM_OP_GSM_ACCOUNTING                                   4034
#define PCM_OP_GSM_AUTHENTICATE                                 4035
#define PCM_OP_GSM_AUTHORIZE                                    4036
#define PCM_OP_GSM_REAUTHORIZE                                  4037
#define PCM_OP_GSM_CANCEL_AUTHORIZATION                         4038
#define PCM_OP_GSM_START_ACCOUNTING                             4039
#define PCM_OP_GSM_UPDATE_ACCOUNTING                            4040
#define PCM_OP_GSM_STOP_ACCOUNTING                              4041
#define PCM_OP_GSM_ACCOUNTING_PREP_SESSION_INPUT                4042
#define PCM_OP_GSM_START_ACCOUNTING_PREP_SESSION_INPUT          4043
#define PCM_OP_GSM_UPDATE_ACCOUNTING_PREP_SESSION_INPUT         4044
#define PCM_OP_GSM_STOP_ACCOUNTING_PREP_SESSION_INPUT           4045
#define PCM_OP_GSM_AUTHORIZE_PREP_INPUT                         4046
#define PCM_OP_GSM_HANDLE_SUB_SESSIONS                          4047
#define PCM_OP_GSM_ACCOUNTING_ON                                4048
#define PCM_OP_GSM_ACCOUNTING_OFF                               4049
#define PCM_OP_GSM_UPDATE_AND_REAUTHORIZE                       4050

/* Opcodes for GSM AAA */
#define PCM_OP_GSM_AAA_ACCOUNTING                               4034
#define PCM_OP_GSM_AAA_AUTHENTICATE                             4035
#define PCM_OP_GSM_AAA_AUTHORIZE                                4036
#define PCM_OP_GSM_AAA_REAUTHORIZE                              4037
#define PCM_OP_GSM_AAA_CANCEL_AUTHORIZATION                     4038
#define PCM_OP_GSM_AAA_START_ACCOUNTING                         4039
#define PCM_OP_GSM_AAA_UPDATE_ACCOUNTING                        4040
#define PCM_OP_GSM_AAA_STOP_ACCOUNTING                          4041
#define PCM_OP_GSM_AAA_ACCOUNTING_ON                            4048
#define PCM_OP_GSM_AAA_ACCOUNTING_OFF                           4049
#define PCM_OP_GSM_AAA_UPDATE_AND_REAUTHORIZE                   4050

#define PCM_OP_GSM_AAA_POL_START_ACCOUNTING_PREP_SESSION_INPUT  4051
#define PCM_OP_GSM_AAA_POL_UPDATE_ACCOUNTING_PREP_SESSION_INPUT 4052
#define PCM_OP_GSM_AAA_POL_STOP_ACCOUNTING_PREP_SESSION_INPUT   4053
#define PCM_OP_GSM_AAA_POL_AUTHORIZE   				4054
#define PCM_OP_GSM_AAA_POL_PREP_INPUT				4055
#define PCM_OP_GSM_AAA_POL_SEARCH_SESSION			4056
#define PCM_OP_GSM_AAA_POL_ACC_ON_OFF_SEARCH			4057
#define PCM_OP_GSM_AAA_POL_POST_PROCESS				4058
#define PCM_OP_GSM_AAA_POL_AUTHORIZE_PREP_INPUT			4059
#define PCM_OP_GSM_AAA_POL_REAUTHORIZE_PREP_INPUT		4060
#define PCM_OP_GSM_AAA_POL_UPDATE_ACCOUNTING_PREP_INPUT		4061
#define PCM_OP_GSM_AAA_POL_STOP_ACCOUNTING_PREP_INPUT		4062

#endif /* _PCM_GSM_AAA_OPS_H_ */

