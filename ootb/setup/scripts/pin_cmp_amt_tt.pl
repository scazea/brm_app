#!/usr/bin/env perl
# 
# $Id: pin_cmp_amt_tt.pl /cgbubrm_7.5.0.portalbase/1 2017/02/24 03:22:50 ashutask Exp $
#
# pin_cmp_amt_tt.pl
# 
# Copyright (c) 2011, 2017, Oracle and/or its affiliates. All rights reserved.
#
#    This material is the confidential property of Oracle Corporation
#    or its licensors and may be used, reproduced, stored
#    or transmitted only in accordance with a valid Oracle license or
#    sublicense agreement.

#
#    NAME
#      pin_cmp_amt_tt.pl - Initialize and configures AMT TT
#
#    DESCRIPTION
#      Generates pin.conf wich still requires operator prepration
#
#    NOTES
#      This can go using either pin_setup or alone.
#
#    MODIFIED   (MM/DD/YY)
#    ngougol     03/26/11 - Creation
# 

use Sys::Hostname;
require "pin_oracle_functions.pl";
require "pin_cmp_dm_db.pl";
$host = hostname;

# If running stand alone, without pin_setup
if ( ! ( $RUNNING_IN_PIN_SETUP eq TRUE ) )
{
   require "pin_res.pl";
   require "pin_functions.pl";
   require "../pin_setup.values";

   &ConfigureComponentCalledSeparately ( $0 );
}


######
#
# Configure AMT TT files
#
#####

sub configure_amt_tt_config_files {


  local (%CM) = %MAIN_CM;
  local (%DM) = %MAIN_DM;

  &ReadIn_PinCnf( "pin_cnf_amt_tt.pl" );
  &ReadIn_PinCnf( "pin_cnf_connect.pl" );

  $AMT_TT{'pin_cnf_location'} = "${PIN_HOME}/sys/amt_tt";
  &Configure_PinCnf( $AMT_TT{'pin_cnf_location'}."/$PINCONF",
                     $AMT_TT_PINCONF_HEADER,
                     %AMT_TT_PINCONF_ENTRIES);
  
  &AddPinConfEntries( $AMT_TT{'pin_cnf_location'}."/$PINCONF",
                     %AMT_TT1_PINCONF_ENTRIES);
  
# Appending connection entries since amt might require 
#  As of April 1, 2011, there is no dependency between AMT_TT and CM. However, there is requirement where once pin_amt_tt migrates account, a notification should be sent to Batch Pipeline so that it gets the migrated location of the subscriber. The solution approach is in discussion for this and we may need CM for the same.  
  &AddPinConfEntries( $AMT_TT{'pin_cnf_location'}."/$PINCONF",
                     %CONNECT_PINCONF_ENTRIES);
  
  &AddPinConfEntries( $AMT_TT{'pin_cnf_location'}."/$PINCONF",
                     %AMT_TT_CONNECT_PINCONF_ENTRIES);
  
}
1;
