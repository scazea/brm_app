#!/usr/bin/env perl
#=============================================================
#  @(#) % %
# 
# Copyright (c) 2000, 2017, Oracle and/or its affiliates. All rights reserved.
#       This material is the confidential property of Oracle Corporation 
#       or its licensors and may be used, reproduced, stored
#       or transmitted only in accordance with a valid Oracle license or 
#       sublicense agreement.
#
# Portal installation for the Telco Framework
#
#=============================================================

use Cwd;

# If running stand alone, without pin_setup
if ( ! ( $RUNNING_IN_PIN_SETUP eq TRUE ) )
{
   require "pin_res.pl";
   require "pin_functions.pl";
   require "../pin_setup.values";

   &ConfigureComponentCalledSeparately ( $0 );
}

##########################################
#
# Configure GSM Manager pin.conf files
#
##########################################
sub configure_tcf_aaa_config_files {
  local ( %CM ) = %MAIN_CM;
  local ( %DM ) = %MAIN_DM;
  local ( @FileReadIn );
  local ( $Start );  

  &ReadIn_PinCnf( "pin_cnf_tcf_aaa.pl" );
  &ReadIn_PinCnf( "pin_cnf_connect.pl" );

  #
  # If the sys/cm/pin.conf is there, add the entries to it.
  # If not, add the entries to the temporary pin.conf file.
  #
  if ( -f $CM{'location'}."/".$PINCONF )
  {
	&ReplacePinConfEntries( $CM{'location'}."/".$PINCONF, %TCF_AAA_CM_PINCONF_ENTRIES );
	
  }
  else
  {
    # Create temporary file, if it does not exist.
    $TEMP_PIN_CONF_FILE = $PIN_HOME."/".$IDS_TEMP_PIN_CONF;
    open( PINCONFFILE, ">> $TEMP_PIN_CONF_FILE" );
    close( PINCONFFILE );

    &ReplacePinConfEntries( "$TEMP_PIN_CONF_FILE".$PINCONF, %TCF_AAA_CM_PINCONF_ENTRIES );

    # Display a message saying to append this file to cm/pin.conf file.
    &Output( STDOUT, $IDS_APPEND_PIN_CONF_MESSAGE,
                        $CM{'location'}."/".$PINCONF,
                        $CurrentComponent,
                        $TEMP_PIN_CONF_FILE );
    }

  #
  # If the sys/data/config/pin.conf is there, add the entries to it.
  # otherwise, create it.
  #
  if ( -f $LTAP{'pin_cnf_location'}."/".$PINCONF )
  {
	&ReplacePinConfEntries( $LTAP{'pin_cnf_location'}."/".$PINCONF, %PIN_LOAD_TELCO_AAA_PARAMS_PINCONF_ENTRIES );
	
  }
  else
  {
      # Add CM entries and pin load telco_aaa param entries
      &AddArrays( \%CONNECT_PINCONF_ENTRIES, \%PIN_LOAD_TELCO_AAA_PARAMS_PINCONF_ENTRIES );     
         
    &Configure_PinCnf( $LTAP{'pin_cnf_location'}."/".$PINCONF,
                                    $CONNECT_PINCONF_HEADER,
                                %PIN_LOAD_TELCO_AAA_PARAMS_PINCONF_ENTRIES ); 
  } 

}


###########################################
#
# Configuring database for Telco Framework
#
###########################################
sub configure_tcf_aaa_database {
  
  require "pin_".$MAIN_DM{'db'}->{'vendor'}."_functions.pl";
  require "pin_cmp_dm_db.pl";
  local ( $TMP ) = $SKIP_INIT_OBJECTS;
  local ( %CM ) = %MAIN_CM;
  local ( %DM ) = %MAIN_DM;

  
  &PreModularConfigureDatabase( %CM, %DM );

  ###################################################
  # Creating the tables for the Provisioning feature
  ###################################################
  #
  # Update Portal database ONLY if the provisioning tables are NOT PRESENT.
  #
  $SKIP_INIT_OBJECTS = "NO";
  $USE_SPECIAL_DD_FILE = "YES";

  if ( VerifyPresenceOfTable( "CONFIG_RESERVE_T", %{DM->{'db'}} ) == 0 ){
    $SPECIAL_DD_FILE = "$DD{'location'}/dd_objects_telco_aaa.source";
    $SPECIAL_DD_INIT_FILE = "$DD{'location'}/init_objects_telco_aaa.source";
    $SPECIAL_DD_DROP_FILE = "$DD{'location'}/drop_tables_telco_aaa_".$MAIN_DM{'db'}->{'vendor'}.".source";
  
    if ( $SETUP_DROP_ALL_TABLES =~ m/^YES$/i ) {
       &DropTables( %{MAIN_DM->{"db"}} );
    }
   
    &pin_pre_modular( %{DM->{'db'}} );
    &pin_init( %DM );
    &pin_post_modular( %DM );
  }

 ###################################################
  # Check for presence of table CONFIG_AAA_TELCO_INFO_T, if not present run the 7.2_7.3_dd_objects_telco_aaa.source
  ###################################################

if ( VerifyPresenceOfTable( "CONFIG_AAA_TELCO_INFO_T", %{DM->{'db'}} ) == 0 ) {
  
   	    
    	$SKIP_INIT_OBJECTS = "YES";
    	$USE_SPECIAL_DD_FILE = "YES";
    	$SPECIAL_DD_FILE = "$DD{'location'}/7.2_7.3_dd_objects_telco_aaa.source";
    		 
    	&pin_pre_modular( %{DM->{'db'}} );
    	&pin_init( %DM );
    	&pin_post_modular( %DM );
  
    	$USE_SPECIAL_DD_FILE = "NO";
    	$SKIP_INIT_OBJECTS = $TMP; 

  }  
  
  
  $USE_SPECIAL_DD_FILE = "NO";
  $SKIP_INIT_OBJECTS = $TMP;

  &PostModularConfigureDatabase( %CM, %DM );
        

}

sub configure_tcf_aaa_post_restart {

  local( $TempDir ) = &FixSlashes( "$AAA_LOAD_CONFIG_PATH" );
  local( $CurrentDir ) = cwd();
  local ( %CM ) = %MAIN_CM;
  local( %DM ) = %MAIN_DM;

  &ReadIn_PinCnf( "pin_cnf_connect.pl" );

  #
  # If the sys/data/config/pin.conf is present, then continue
  # If not, add the entries to the pin.conf file.
  #
  if (!( -e $AAA_LOAD_CONFIG_PATH."/".$PINCONF ))
  {
  	  #
	  # Create pin.conf for loading.
	  #
	    &Configure_PinCnf( $AAA_LOAD_CONFIG_PATH."/".$PINCONF,
                       $CONNECT_PINCONF_HEADER,
                       %CONNECT_PINCONF_ENTRIES);
  }
    # Load the pin_telco_aaa_params to create /config/aaa object. 
    
    chdir $TempDir;
    
    &Output( fpLogFile, $IDS_PIN_TELCO_AAA_PARAMS_LOADING );
    &Output( STDOUT, $IDS_PIN_TELCO_AAA_PARAMS_LOADING );

    $Cmd = &FixSlashes( "$PIN_HOME/bin/load_pin_telco_aaa_params \"$ENV{BRM_CONF}/sys/data/config/pin_telco_aaa_params.xml\"");
    $Cmd = &ExecuteShell_Piped( "$PIN_TEMP_DIR/tmp.out", TRUE, $Cmd, "" );

    if( $Cmd != 0 ) {
       &Output( fpLogFile, $IDS_PIN_TELCO_AAA_PARAMS_FAILED );
       &Output( STDOUT, $IDS_PIN_TELCO_AAA_PARAMS_FAILED );
       exit -1;
    } else {
       &Output( fpLogFile, $IDS_PIN_TELCO_AAA_PARAMS_SUCCESS );
       &Output( STDOUT, $IDS_PIN_TELCO_AAA_PARAMS_SUCCESS );
    }  
    
      unlink( "$PIN_TEMP_DIR/tmp.out" );
    
      &configure_reservation_tcf_aaa( *MAIN_CM, *MAIN_DM );      
      chdir $CurrentDir; 
}


  ############################################################
  # Loading pin_config_reservation_aaa_prefs
  ######################################################
    
   sub configure_reservation_tcf_aaa {
    
    $Cmd = &FixSlashes( "$PIN_HOME/bin/load_config_reservation_aaa_prefs -v pin_config_reservation_aaa_prefs" );
    &Output( fpLogFile, $IDS_TCFAAA_RESERVATION_DATA_LOADING );
    &Output( STDOUT, $IDS_TCFAAA_RESERVATION_DATA_LOADING );
    $Cmd = &ExecuteShell_Piped( "$PIN_TEMP_DIR/tmp.out", TRUE, $Cmd, "" );
  
    #
    # If loading of pin_config_reservation_aaa_prefs failed, display the failed message,
    # else display success message.
    #
  
      if( $Cmd != 0 )
      {
           &Output( fpLogFile, $IDS_TCFAAA_RESERVATION_DATA_FAILED );
           &Output( STDOUT, $IDS_TCFAAA_RESERVATION_DATA_FAILED );
           exit -1;
      }
      else
      {
           &Output( fpLogFile, $IDS_TCFAAA_RESERVATION_DATA_SUCCESS );
           &Output( STDOUT, $IDS_TCFAAA_RESERVATION_DATA_SUCCESS );
      }
  
           unlink( "$PIN_TEMP_DIR/tmp.out" );
	
	
   }
1;
