#!/usr/bin/env perl
#=============================================================
# 
# Copyright (c) 2010, 2017, Oracle and/or its affiliates. All rights reserved.
#
#    This material is the confidential property of Oracle Corporation 
#    or its licensors and may be used, reproduced, stored
#    or transmitted only in accordance with a valid Oracle license or
#    sublicense agreement.
#
# Portal installation for the Portal Base Manager Component
#
#=============================================================
use Sys::Hostname;
#use IO::CaptureOutput qw/ capture/ ;
require "pin_oracle_functions.pl";
require "pin_cmp_dm_db.pl";
$host = hostname;

# If running stand alone, without pin_setup
if ( ! ( $RUNNING_IN_PIN_SETUP eq TRUE ) )
{
   require "pin_res.pl";
   require "pin_functions.pl";
   require "../pin_setup.values";

   &ConfigureComponentCalledSeparately ( $0 );
}


#####
#
# Configure Portal Base Manager files
#
#####
sub configure_dm_tt_config_files {
  local( %CM ) = %MAIN_CM;
  local( %DM ) = %MAIN_DM;

  &ReadIn_PinCnf( "pin_cnf_connect.pl" );
  &ReadIn_PinCnf( "pin_cnf_dm_tt.pl" );
  &ReadIn_PinCnf( "pin_cnf_multidb.pl" );

  $TPHome = "/u02/brm/ebs/brm_oob/brm_app/ThirdParty";

  #untar thirdparty file DBI_DBD_files.tar
  `tar -xvf $TPHome/DBI_DBD_files.tar -C $TPHome`;

  #Configure pin_tt_schema values
   &Configure_pintt("pin_tt_schema_gen.values");
   &Configure_pintt("pin_tt_ds_schema_gen.values");

  #Configure pinctl_conf
   &Configure_pinctl_conf();

  &Configure_PinCnf( "${PIN_HOME}/sys/dm_tt/pin.conf", 
                     $DM_TIMESTEN_PINCONF_HEADER, 
                     %DM_TIMESTEN_PINCONF_ENTRIES);

  # If the CM is there, add the entries to it.
  # If not, add the entries to the temporary pin.conf file.
  #
  if ( -f $CM{'location'}."/".$PINCONF )
  {
      open( PINCONFFILE, $CM{'location'}."/".$PINCONF );
      @FileReadIn = <PINCONFFILE>;
      close( PINCONFFILE );

      # Check if entry was created, if not then create it else replace the entry with TimesTen
      $Start = &LocateEntry( "timesten", @FileReadIn );
      if ( $Start == -1 )  # Entry not created before hence create it.
      {
        &AddPinConfEntries( $CM{'location'}."/".$PINCONF, %CM_TIMESTEN_PINCONF_ENTRIES );
      }
      else
      {
        &ReplacePinConfEntries( $CM{'location'}."/".$PINCONF, %CM_TIMESTEN_PINCONF_ENTRIES );
      }
  }
  else
  {
    # Create temporary file, if it does not exist.
    $TEMP_PIN_CONF_FILE = $PIN_HOME."/".$IDS_TEMP_PIN_CONF;
    open( PINCONFFILE, ">> $TEMP_PIN_CONF_FILE" );
    close( PINCONFFILE );
    
        &AddPinConfEntries( $TEMP_PIN_CONF_FILE, %CM_TIMESTEN_PINCONF_ENTRIES );

    # Display a message saying to append this file to cm/pin.conf file.
    &Output( STDOUT, $IDS_APPEND_PIN_CONF_MESSAGE,
                        $CM{'location'}."/".$PINCONF,
                        $CurrentComponent,
                        $TEMP_PIN_CONF_FILE );
   }
  
    &AddArrays( \%CONNECT_PINCONF_ENTRIES, \%MULTIDB_PINCONF_ENTRIES );
  #Generate/ Update $PIN_HOME/apps/multidb/pin.conf
   &config_multidb( $MULTI_DB{'pin_cnf_location'}."/".$PINCONF, 
       "is_timesten",
       \%MULTIDB_TIMESTEN_PINCONF_ENTRIES, 
       \%MULTIDB_PINCONF_ENTRIES );
   
   &ConfigDist();

}

##################
#
# Make/ Fix MultiDB configuration files
#
##################
sub  config_multidb {

# Generate multi db pin.conf
  my ($MULTI_DB_CONF)=  shift(@_);
  local $description = shift(@_);
  local $ENTRIES = shift(@_);
# Check if MultiDB pin.conf already exists
  if ( -f $MULTI_DB_CONF )
  {
    open( CONFFILE,  $MULTI_DB_CONF );
    @FileReadln = <CONFFILE>;
    close( CONFFILE );

#check if entry was created
    $Start = &LocateEntry( $description,  @FileReadln );
    if ( $Start == -1 ) # Add entry since does not exist
    {
      &AddPinConfEntries( $MULTI_DB_CONF, %$ENTRIES);
    }
    else
    {
      &ReplacePinConfEntries( $MULTI_DB_CONF, %$ENTRIES);
    }
  }
  else
  {
    local $MULTIDB_ENTRIES = shift(@_);
#Combining Entries of multidb with corresponding timesten entries 
    &AddArrays( \%$ENTRIES, \%$MULTIDB_ENTRIES );

    &Configure_PinCnf( $MULTI_DB_CONF,
	$MULTIDB_CONF_HEADER,
	 %$MULTIDB_ENTRIES);
  }
}

#######
#
# Configuring database for Infmgr
#
#######
sub configure_dm_tt_database {

  %DM = %DM_ORACLE;

  $DM_ORACLE{'db'}->{'vendor'} = "oracle";

  for $sql ( "$PIN_HOME/sys/dm_tt/data/update_dd_residency.sql",
		#Script to update the residency types of certain objects  
      "$PIN_HOME/sys/dm_tt/data/disable_triggers.sql",
		# Script to disable triggers in Oracle DB
      "$PIN_HOME/sys/dm_tt/data/create_delete_cascade.sql",
		# Sql to create delete cascade on journal_t
      "$PIN_HOME/sys/dm_tt/data/create_global_txn_tbl.source"
		# New table for storing the pending global transaction info
      ) {
    if ( -f $sql)
    {
      &ExecuteSQL_Statement_From_File($sql,
	  TRUE,
	  TRUE,
	  %{DM->{'db'}} );
    }
  }

    @plbs= ("$PIN_HOME/sys/dm_tt/data/create_pkg_pin_sequence_oracle.plb",
	"Procerure create package pin sequence in oracle", 
	"$PIN_HOME/sys/dm_tt/data/tt_misc_procedures.plb" , 
	"Procedure for creating indexes in Oracle.",
	"$PIN_HOME/sys/dm_tt/data/check_latest_poids_procedure.plb",
	"Procedure for verifying whether the events are present in oracle db or not."
	);
  for ($i = 0; $i <= $#plbs; $i+= 2) {
    if ( -f $plbs[$i] )
    {
      &ExecutePLB_file ($plbs[$i],
	  $plbs[$i + 1],
	  %DM_ORACLE );
    }    
  }
}

sub Configure_pintt {

   my $values = shift( @_ );

   open(SWAP_PINTT_SCHEMA,">$PIN_HOME/bin/swap_$values");
   open(PINTT_SCHEMA,"$PIN_HOME/bin/$values");
   while(<PINTT_SCHEMA>) {
     if($_ =~ /\$MAIN_DB\{\'alias\'\}/) {
        $_ = "\$MAIN_DB\{\'alias\'\} = \"$DM_ORACLE{'db'}->{'alias'}\"\;\n";
     }
     if($_ =~ /\$MAIN_DB\{\'user\'\}/) {
        $_ = "\$MAIN_DB\{\'user\'\} = \"$DM_ORACLE{'db'}->{'user'}\"\;\n";
     }
     if($_ =~ /\$MAIN_DB\{\'password\'\}/) {
        $_ = "\$MAIN_DB\{\'password\'\} = \"$DM_ORACLE{'db'}->{'password'}\"\;\n";
     }
     if($_ =~ /\$db_no_for_load_sql/) {
        $_ = "\$db_no_for_load_sql = \"$DM_TT{'db_num'}\"\;\n";
     }
     print SWAP_PINTT_SCHEMA "$_";
   }
   rename("$PIN_HOME/bin/swap_$values","$PIN_HOME/bin/$values");
}

sub Configure_pinctl_conf {

   open(SWAP_PINCTL_SCHEMA,">$PIN_HOME/bin/swap_pin_ctl.conf");
   open(PINCTL_SCHEMA,"$PIN_HOME/bin/pin_ctl.conf");
   while(<PINCTL_SCHEMA>) {
     if($_ =~ /__DM_TT_PORT__/) {
        $_=~s/__DM_TT_PORT__/$DM_TT{'port'}/;
     }
     if($_ =~ /__DM_TT_HOST__/) {
        $_=~s/__DM_TT_HOST__/$host/;
     }
     print SWAP_PINCTL_SCHEMA "$_";
   }
   rename("$PIN_HOME/bin/swap_pin_ctl.conf","$PIN_HOME/bin/pin_ctl.conf");
}
1;
