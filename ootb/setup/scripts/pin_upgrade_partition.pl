#!/usr/bin/env perl
##################################################################################### 
# $Header: bus_platform_vob/upgrade/upgrade_75ps12/oracle/pin_upgrade_partition.pl /cgbubrm_7.5.0.rwsmod/1 2015/04/16 07:03:01 sankumku Exp $
#
# pin_upgrade_partition.pl
# 
# Copyright (c) 2015, Oracle and/or its affiliates. All rights reserved.
#
#    NAME
#      pin_upgrade_partition.pl - Pre Step to Upgrade script to add partition info
#
#    DESCRIPTION
#      Pre Step to Upgrade script to add partition info of table from File
#      merge_pin_setup.data into pin_tables.value.
#
#    NOTES
#      This Script will be called from upgrade script. 
#
#    MODIFIED   (MM/DD/YY)
#    sankumku    04/16/15 - Creation
#######################################################################################
use strict;
use warnings;
use File::Copy;

my $filename1      = 'pin_tables.values';
my $filename       = 'pin_tables_partition';
my $New_pin_tables = 'new_pin_tables.value';
my $File_pin_setup = '../pin_setup.values';
my $check_1 = 0;
my $check_2 = 0;
my $check_3 = 0;
my $row1 = "";
my @grabe_string =('%PIN_CONF_PARTITION_INFO',
                   '%PIN_CONF_SM_INFO',
                   '%PIN_CONF_SM_INFO');
my @insert_string = ('','','','','','');
my $count = 0;
my $object_name = "";

#Taking Backup of Original pin_tables.values
#------------------------------------------

if(!-e "$filename1.upgrade") {
copy($filename1, "$filename1.upgrade");
}

#Open Input File to read Values to Insert
#----------------------------------------
open(my $fh, '<', $filename)
        or die "Could not open file '$filename' $!";

while (my $row = <$fh>) {
        chomp $row;
        my @values = split(' ', $row);
        my $str = $values[0];

        if($str eq "object")
        {
            $object_name = $values[1];
            $count=0;
            my $cmd = "grep -i $object_name pin_tables.values | wc -l";
            $count=`$cmd`;

                        for(my $i=0; $i<6; $i++)
                        {
                                $row = <$fh>;
                                $insert_string[$i] = $row;
                        }
        }
        if ($count==0)
        {
                        open(my $fh1, '<', $filename1)  #Opening pin_tables.value
                                or die "Could not open file '$filename1' $!";

                        open(my $fh2, '>', $New_pin_tables)  #Opening New temp file to merge
                                or die "Could not open file '$New_pin_tables' $!";

                        $check_1 = 0;
                        $check_2 = 0;
                        $check_3 = 0;

                        while ($row1 = <$fh1>)
                        {
                                if ($row1 =~ /^#/)
                                {
                                        print $fh2 "$row1";
                                }
                                elsif( $row1 =~ m/$grabe_string[0]/ && $check_1 !=1)
                                {
                                        print $fh2 "$row1";
                                        print $fh2 "\t$insert_string[0]";
                                        print $fh2 "\t$insert_string[1]";
                                        print $fh2 "\t$insert_string[2]";
                                        $check_1 = 1;
                                }
                                elsif( $row1 =~ m/$grabe_string[1]/ && $check_2 !=1)
                                {
                                        print $fh2 "$row1";
                                        print $fh2 "\t$insert_string[3]";
                                        $check_2 =1;
                                }
                                elsif($row1 =~ m/$grabe_string[2]/ && $check_1 ==1 && $check_3 !=1)
                                {
                                        print $fh2 "$row1";
                                        print $fh2 "\t$insert_string[4]";
                                        $check_3 = 1;
                                }
                                else
                                {
                                        print $fh2 "$row1";
                                }
                        }


                        open(my $fh4, '>>', $File_pin_setup)
                                or die "Could not open file '$File_pin_setup' $!";

                        print $fh4 "\n$insert_string[5]";

			close($File_pin_setup);
                        close($filename1);
                        close($New_pin_tables);

                        move($New_pin_tables, $filename1);

        }
}
close($filename);
 
