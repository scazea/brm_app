#=============================================================
#  @(#) % %
#    
# Copyright (c) 2005, 2017, Oracle and/or its affiliates. All rights reserved.
#       This material is the confidential property of Oracle Corporation 
#       or its licensors and may be used, reproduced, stored
#       or transmitted only in accordance with a valid Oracle license or 
#       sublicense agreement.
#
#==============================================================


$RADIUS_PINCONF_HEADER  = <<END
#************************************************************************
# Configuration File for Portal IPT GAL for Cisco or Dialup Manager
#
#
# Portal IPT uses this file for configuring the GAL for Cisco to 
# communicate with the Connection Manager (CM).
#
# Portal Dialup Manager (formerly called Terminal Server Manager)
# uses this file for configuring the Dialup Manager to communicate with
# the Connection Manager (CM). 
#
# Dialup Manager was formerly known as the Terminal Server Manager or
# TSM, and may also be called RADIUS (Remote Authentication Dial In User
# Service) Manager.
#
# This configuration file is automatically installed and configured with
# default values during Portal installation. You can edit this file to:
#   -- change the default values of the entries.
#   -- disable an entry by inserting a crosshatch (#) at the start of
#        the line.
#   -- enable a commented entry by removing the crosshatch (#).
# 
# Before you make any changes to this file, save a backup copy.
#
# When editing this file, follow the instructions in each section.
# For more information on the general syntax of configuration entries,
# see "Reference Guide to Portal Configuration Files" in the Portal
# online documentation.
#========================================================================
END
;


%RADIUS_PINCONF_ENTRIES = (

	"cm_ptr_0_description", <<END
#========================================================================
# cm_ptr
#
# Specifies a pointer to a CM or CMMP.
#
# Use a separate entry for each CM or CMMP. If pin_radiusd can't
# find the first CM or CMMP, it looks for the next in the list.
#
# Each entry includes three values:
#
#    <protocol> = "ip", for this release
#    <host>     = the name or IP address of the computer running the
#                   CM or CMMP
#    <port>     = the TCP port number of the CM or CMMP on this computer
#
# The port number should match a corresponding cm_ports entry with
# the same port number in the CM or CMMP configuration file. The
# default, 11960, is a commonly specified port for the CM or CMMP.
#=======================================================================
END
	,"cm_ptr_0", "- nap cm_ptr ip $CM{'hostname'} $CM{'port'}"

	,"cm_ptr_userid_description", <<END
#=======================================================================
# cm_ptr_userid
#
# The entry below specifies the database user ID for GAL for Cisco. 
#=======================================================================                                            
END
	,"cm_ptr_userid", "- - userid $DM{'db_num'} /service/pcm_client 1"

	,"cm_ptr_login_description", <<END
#************************************************************************
# The next three entries specify login information for the GAL for Cisco:
#************************************************************************
#
#========================================================================
# login_type
#
# Specifies whether the login name and password are required.
#
# The value for this entry can be:
#
#    0 = Only a user ID is required.
#    1 = (Default) Both a name and a password are required.
#========================================================================
END
	,"cm_ptr_login", "- nap login_type 1"

	,"login_name_description", <<END
#========================================================================
# login_name
#
# Specifies the login name to use when the GAL for Cisco requests a
# connection to Portal Manager. 
#========================================================================
END
	,"login_name", "- nap login_name root.$DM{'db_num'}"

	,"login_pw_description", <<END
#========================================================================
# login_pw
#
# Specifies the password to use when the GAL for Cisco connects to
# Portal Manager.
#========================================================================
END
	,"login_pw", "- nap login_pw   $MAIN_CM{'pcm_admin_passwd'}"

	,"num_connects_description", <<END
#========================================================================
# num_connects
#
# Specifies the number of connections to the CM. 
#
# Usually, this number must be the same as the threads specified in the
# GAL for Cisco configuration file.
#========================================================================
END
	,"num_connects", "- - num_connects $RADIUS{'num_connects'}"

);


%RADIUS_CM_ENTRIES = (

  "Service_description", <<END
#========================================================================
# Service
#
# Identifies the Dialup Manager.
# 
# The ID includes the Portal database number, the object type 
# (/service/ip), and the ID number "0". This entry is reserved for 
# Portal; DO NOT change it.
#========================================================================
END
  ,"Service", "- fm_term service $DM{'db_num'} /service/ip 0"

  ,"trans_id_window_description", <<END
#========================================================================
# trans_id_window
#
# Specifies the time, in seconds, between two dialup sessions that
# identifies the sessions as unique.
#
# If two sessions fall within the specified time, Portal treats 
# them as duplicate sessions. The default is 14400 seconds.
#========================================================================
END
  ,"trans_id_window", "- fm_term trans_id_window $RADIUS{'trans_id_window'}"

  ,"disable_find_and_close_dups_description", <<END
#========================================================================
# disable_find_and_close_dups
#
# Disable the check to see if a new request comes in with
# the same NAS-ID/NAS-PORT combination as an existing open
# session.
#
# If the value is 1, then we allow multiple open sessions with the
# same NAS-ID/NAS-PORT combination. If the value is 0 or if this is
# not present then we go with the default/original scheme where we
# close the older open session as an orphan.
#========================================================================
END
  ,"disable_find_and_close_dups", "- fm_term disable_find_and_close_dups 0"

  ,"stop_only_accounting_description", <<END
#========================================================================
# stop_only_acct
#
# Specifies whether to use stop-only accounting or start-stop accounting.
#
# The value for this entry can be:
#
#    0 = (Default) Use start-stop accounting for logging sessions.
#    1 = Use stop-only accounting for logging sessions.
#
# Note: To use stop-only accounting, you must also configure the RADIUS
#       server to run this type of accounting. See the online
#       documentation for Dialup Manager.
#========================================================================
END
  ,"stop_only_accounting", "- fm_term stop_only_acct $RADIUS{'stop_only_acct'}"

  ,"radius_fm_required_description", <<END
#========================================================================
# radius_fm_required
#
# Required Facilities Modules (FM) configuration entries
#
# The entries below specify the required FMs that are linked to the CM when
# the CM starts. The FMs required by the CM are included in this file when
# you install Portal.  
#
# Caution: DO NOT modify these entries unless you need to change the
#          location of the shared library file. DO NOT change the order of
#          these entries, because certain modules depend on others being
#          loaded before them.
#========================================================================
END
  , "radius_fm_required", <<END
- cm fm_module \$\{PIN_HOME\}/lib/fm_term\$\{LIBRARYEXTENSION\} fm_term_config$FM_FUNCTION_SUFFIX - pin
- cm fm_module \$\{PIN_HOME\}/lib/fm_term_pol\$\{LIBRARYEXTENSION\} fm_term_pol_config$FM_FUNCTION_SUFFIX - pin
END

 );
