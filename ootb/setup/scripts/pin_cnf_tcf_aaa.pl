#=============================================================
#  @(#) % %
#    
# Copyright (c) 2006, 2012, Oracle and/or its affiliates. All rights reserved. 
#       This material is the confidential property of Oracle Corporation 
#       or its licensors and may be used, reproduced, stored
#       or transmitted only in accordance with a valid Oracle license or 
#       sublicense agreement.
#
#==============================================================


$TCF_AAA_PINCONF_HEADER  =  <<END
#************************************************************************
# Configuration File for Portal TCF AAA Manager
#
# This configuration file is automatically installed and configured with
# default values during Portal installation. You can edit this file to:
#   -- change the default values of the entries.
#   -- disable an entry by inserting a crosshatch (#) at the start of
#        the line.
#   -- enable a commented entry by removing the crosshatch (#).
# 
# Before you make any changes to this file, save a backup copy.
#
# When editing this file, follow the instructions in each section.
# For more information on the general syntax of configuration entries,
# see "Reference Guide to Portal Configuration Files" in the Portal
# online documentation.
#************************************************************************
END
;

%TCF_AAA_CM_PINCONF_ENTRIES = (

  "cm_fm_tcf_aaa_config_description", <<END
#========================================================================
# cm_fm_tcf_aaa_config
#
# This FM module provides AAA functionality. 
#
# The entries below specify the required FMs that are linked to the CM when
# the CM starts. The FMs required by the CM are included in this file when
# you install Portal. 
#
# Caution: DO NOT modify these entries unless you need to change the
#          location of the shared library file. DO NOT change the order of
#          these entries, because certain modules depend on others being
#          loaded before them.
#=======================================================================
END
  , "cm_fm_tcf_aaa_config", <<END
- cm fm_module \$\{PIN_HOME\}/lib/fm_tcf_aaa\$\{LIBRARYEXTENSION\} fm_tcf_aaa_config$FM_FUNCTION_SUFFIX fm_tcf_init pin
- cm fm_module \$\{PIN_HOME\}/lib/fm_tcf_aaa_pol\$\{LIBRARYEXTENSION\} fm_tcf_aaa_pol_config$FM_FUNCTION_SUFFIX - pin
END

	, "fm_tcf_utils_config_cache_description", <<END
#========================================================================
# fm_tcf_utils_config_cache
#
# Specifies attributes of the CM cache for the size of tcf aaa
# configurations.
#
# This entry is required because tcf aaa configs are not
# placed in the CM cache when the CM starts.
#
# Important: If this entry is missing, the CM will start
#            with an error logged in the cm.pinlog file.
#
# The default settings are usually adequate, but you can change
# them by editing the values below. For changes to take effect,
# the CM must be restarted.
#
#- cm_cache fm_tcf_utils_config_cache [number of entries]
#                          [cache size]  [hash size]
#
# [number of entries] = the number of entries in the cache
#                       for tcf aaa config. The
#                       default is 100.
#        [cache size] = the memory in bytes of the cache
#                       for tcf aaa config. The
#                       default is 102400.
#         [hash  size] = the number of buckets for tcf aaa
#                       configurations (used in the
#                       hashing algorithm). The default is 13.
#
# Caution: DO NOT modify these entries unless you need to change the
#          location of the shared library file. DO NOT change the order of
#          these entries, because certain modules depend on others being
#          loaded before them.
#========================================================================
END
  	, "fm_tcf_utils_config_cache", "- cm_cache fm_tcf_utils_config_cache 100, 102400, 13"

	, "fm_tcf_aaa_wait_for_all_interim_stop_request_description", <<END
#========================================================================
# fm_tcf_aaa_wait_for_all_interim_stop_request
#
# This configuration entry is used by TCF_AAA to check for all the
# interim stop accounting requests before processing the final stop
# accounting request.
# If any of the interim Stop accounting request is missing
# when processing the final stop accounting request, final stop request
# will be processed as the interim stop request, and
# the interim stop request which comes out of order (as final stop request)
# will close all the active sessions and create events
# based on the configured sub-sesion mode.
#
#  YES - Wait for all the interim stop requests are received (default)
#  NO  - Dont wait for all the interim stop requests.
#
# NOTE: Values "YES" and "NO" are not case sensitive.
#=======================================================================
END
	,"fm_tcf_aaa_wait_for_all_interim_stop_request", "- fm_tcf_aaa wait_for_all_interim_stop_request YES"

	,"fm_tcf_aaa_tcf_config_cache_ttl_description", <<END
#==========================================================
#fm_tcf_aaa_tcf_config_cache_ttl
#
#This configuration entry is used by TCF_AAA to decide to
#refresh the config object from DB periodically.
#
#NOTE : Value should be in seconds.
#==========================================================
END
	,"fm_tcf_aaa_tcf_config_cache_ttl", "#- fm_tcf_aaa tcf_config_cache_ttl  86400"
);

%PIN_LOAD_TELCO_AAA_PARAMS_PINCONF_ENTRIES= (

  "pin_loadTelcoAAAParams_loglevel_description", <<END
#========================================================================
# loadTelcoAAAParams Logging Attributes
#
# Specifies how much information the applications log.
#
# The value for this entry can be:
#
#    0 = no logging
#    1 = (Default) log error messages only
#    2 = log error messages and warnings
#    3 = log error, warning, and debugging messages
#========================================================================
END
	,"pin_loadTelcoAAAParams_loglevel", "- loadTelcoAAAParams loglevel $PIN_LOAD_TELCO_AAA_PARAMS{'log_level'}"


  ,"pin_loadTelcoAAAParams_logfile_description", <<END
#========================================================================
# loadTelcoAAAParams Logfile
#
# Specifies the full path to the log file used by this application.
#
# You can enter any valid path.
#========================================================================
END
	,"pin_loadTelcoAAAParams_logfile", "- loadTelcoAAAParams logfile \$\{\PIN_LOG_DIR\}/loadTelcoAAAParams/loadTelcoAAAParams.pinlog"

 


);
