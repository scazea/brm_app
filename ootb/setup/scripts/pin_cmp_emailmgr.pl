#!/usr/bin/env perl
#=============================================================
#  @(#) % %
#    
# Copyright (c) 1999, 2017, Oracle and/or its affiliates. All rights reserved.
#       This material is the confidential property of Oracle Corporation 
#       or its licensors and may be used, reproduced, stored
#       or transmitted only in accordance with a valid Oracle license or 
#       sublicense agreement.
#
# Portal installation for the Email Manager Component
#
#=============================================================

# If running stand alone, without pin_setup
if ( ! ( $RUNNING_IN_PIN_SETUP eq TRUE ) )
{
   require "pin_res.pl";
   require "pin_functions.pl";
   require "../pin_setup.values";

   &ConfigureComponentCalledSeparately ( $0 );
}


#####
#
# Configure EMAILMGR files
#
#####
sub configure_emailmgr_config_files {
  local ( %CM ) = %MAIN_CM;
  local ( %DM ) = %MAIN_DM;

  if ( $^O !~ /win/i )
  {
    $EMAILMGR{'domain'} = `domainname`;
  }

  &ReadIn_PinCnf( "pin_cnf_emailmgr.pl" );
  &ReadIn_PinCnf( "pin_cnf_connect.pl" );
  &AddArrays( \%CONNECT_PINCONF_ENTRIES, \%PIN_MAILER_PINCONF_ENTRIES );
  &AddArrays( \%CONNECT_PINCONF_ENTRIES, \%POPPER_PINCONF_ENTRIES );

  &Configure_PinCnf( $EMAILMGR{'popper_location'}."/".$PINCONF, $EMAILMGR_PINCONF_HEADER, %POPPER_PINCONF_ENTRIES );
  &Configure_PinCnf( $EMAILMGR{'pin_mailer_location'}."/".$PINCONF, $EMAILMGR_PINCONF_HEADER, %PIN_MAILER_PINCONF_ENTRIES );


  #
  # If the CM is there, add the entries to it.
  # If not, add the entries to the temporary pin.conf file.
  #
  if ( -f $CM{'location'}."/".$PINCONF )
  {
    &ReplacePinConfEntries( $CM{'location'}."/".$PINCONF, %EMAILMGR_CM_PINCONF_ENTRIES );
    
    # Display a message current component entries are appended to cm/pin.conf file.
    &Output( STDOUT, $IDS_CM_PIN_CONF_APPEND_SUCCESS,
    			$CurrentComponent,
    			$CM{'location'}."/".$PINCONF);
    
  }
  else
  {
    # Create temporary file, if it does not exist.
    $TEMP_PIN_CONF_FILE = $PIN_HOME."/".$IDS_TEMP_PIN_CONF;
    open( PINCONFFILE, ">> $TEMP_PIN_CONF_FILE" );
    close( PINCONFFILE );
    &ReplacePinConfEntries( "$TEMP_PIN_CONF_FILE", %EMAILMGR_CM_PINCONF_ENTRIES );

    # Display a message saying to append this file to cm/pin.conf file.
    &Output( STDOUT, $IDS_APPEND_PIN_CONF_MESSAGE,
                        $CM{'location'}."/".$PINCONF,
                        $CurrentComponent,
                        $TEMP_PIN_CONF_FILE );
  };

}
1;
#######
#
# Configuring database for EmailMgr
#
#######
#sub configure_emailmgr_database {
#}
