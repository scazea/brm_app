#!/usr/bin/env perl
#=============================================================
#  @(#) % %
#
# Copyright (c) 2000, 2017, Oracle and/or its affiliates. All rights reserved.
#       This material is the confidential property of Oracle Corporation
#       or its licensors and may be used, reproduced, stored
#       or transmitted only in accordance with a valid Oracle license or
#       sublicense agreement.
#
# Portal installation for the GSM Component
#
#=============================================================

use Cwd;

# If running stand alone, without pin_setup
if ( ! ( $RUNNING_IN_PIN_SETUP eq TRUE ) )
{
   require "pin_res.pl";
   require "pin_functions.pl";
   require "../pin_setup.values";

   &ConfigureComponentCalledSeparately ( $0 );
}

##########################################
#
# Configure GSM Manager pin.conf files
#
##########################################
sub configure_gsm_aaa_config_files {
  local ( %CM ) = %MAIN_CM;
  local ( %DM ) = %MAIN_DM;
  

  &ReadIn_PinCnf( "pin_cnf_gsm_aaa.pl" );

  &AddArrays( \%GSM_AAA_MANAGER_CM_PINCONF_ENTRIES );
  #
  # If the sys/cm/pin.conf is there, add the entries to it.
  # If not, add the entries to the temporary pin.conf file.
  #
  if ( -f $CM{'location'}."/".$PINCONF )
  {
	&ReplacePinConfEntries( $CM{'location'}."/".$PINCONF, %GSM_AAA_MANAGER_CM_PINCONF_ENTRIES );
	
    # Display a message current component entries are appended to cm/pin.conf file.
    &Output( STDOUT, $IDS_CM_PIN_CONF_APPEND_SUCCESS,
    			$CurrentComponent,
   			$CM{'location'}."/".$PINCONF);
	

  }
  else
  {
    # Create temporary file, if it does not exist.
    $TEMP_PIN_CONF_FILE = $PIN_HOME."/".$IDS_TEMP_PIN_CONF;
    open( PINCONFFILE, ">> $TEMP_PIN_CONF_FILE" );
    close( PINCONFFILE );

    &ReplacePinConfEntries( "$TEMP_PIN_CONF_FILE".$PINCONF, %GSM_AAA_MANAGER_CM_PINCONF_ENTRIES );

    # Display a message saying to append this file to cm/pin.conf file.
    &Output( STDOUT, $IDS_APPEND_PIN_CONF_MESSAGE,
                        $CM{'location'}."/".$PINCONF,
                        $CurrentComponent,
                        $TEMP_PIN_CONF_FILE );
    }
}

#####################################
#
# Configuring database for GSM Manager
#
#####################################
sub configure_gsm_aaa_database {

  require "pin_".$MAIN_DM{'db'}->{'vendor'}."_functions.pl";
  require "pin_cmp_dm_db.pl";
  local ( $TMP ) = $SKIP_INIT_OBJECTS;
  local ( %CM ) = %MAIN_CM;
  local ( %DM ) = %MAIN_DM;

  &PreModularConfigureDatabase( %CM, %DM );

  #########################################
  # Creating the tables for the GSM Manager
  #########################################
  $SKIP_INIT_OBJECTS = "NO";
  $USE_SPECIAL_DD_FILE = "YES";

  if ( VerifyPresenceOfTable( "CONFIG_RESERVE_T", %{DM->{'db'}} ) == 0 ){
    $SPECIAL_DD_FILE = "$DD{'location'}/dd_objects_telco_aaa.source";
    $SPECIAL_DD_DROP_FILE = "$DD{'location'}/drop_tables_telco_aaa_".$MAIN_DM{'db'}->{'vendor'}.".source";

    if ( $SETUP_DROP_ALL_TABLES =~ m/^YES$/i ) {
       &DropTables( %{MAIN_DM->{"db"}} );
    }

    &pin_pre_modular( %{DM->{'db'}} );
    &pin_init( %DM );
    &pin_post_modular( %DM );
  }

  ############################################
  # Creating the tables for the GSM AAA Manager
  ############################################
  
  
  if ( VerifyPresenceOfTable( "CONFIG_GSM_INFO_T", %{DM->{'db'}} ) == 0 ){
  
  $SPECIAL_DD_FILE = "$DD{'location'}/dd_objects_gsm_aaa.source";
  $SPECIAL_DD_INIT_FILE = "$DD{'location'}/init_objects_gsm_aaa.source";
  $SPECIAL_DD_DROP_FILE = "$DD{'location'}/drop_tables_gsm_aaa_".$MAIN_DM{'db'}->{'vendor'}.".source";

  if ( $SETUP_DROP_ALL_TABLES =~ m/^YES$/i ) {
    &DropTables( %{MAIN_DM->{"db"}} );
  }

  $SKIP_INIT_OBJECTS = "NO";
  $USE_SPECIAL_DD_FILE = "YES";
       	
  &pin_pre_modular( %{DM->{'db'}} );
  &pin_init( %DM );
  &pin_post_modular( %DM );

  $USE_SPECIAL_DD_FILE = "NO";
  $SKIP_INIT_OBJECTS = $TMP;


}
  #
  # Update Portal database if dd_objects_t does not contain the entry for /config/reserve/gsm.
  #
   if ( VerifyPresenceOfObject( "/config/reserve/gsm", %{DM->{'db'}} ) == 0 ){

       	$SKIP_INIT_OBJECTS = "YES";
       	$USE_SPECIAL_DD_FILE = "YES";
       	$SPECIAL_DD_FILE = "$DD{'location'}/7.0_7.2_dd_objects_gsm_aaa.source";

       	&pin_pre_modular( %{DM->{'db'}} );
       	&pin_init( %DM );
       	&pin_post_modular( %DM );

       	$USE_SPECIAL_DD_FILE = "NO";
       	$SKIP_INIT_OBJECTS = $TMP;
  }

 &PostModularConfigureDatabase( %CM, %DM );

}

#########################################
# Additional configuration for GSM AAA
#########################################
sub configure_gsm_aaa_post_restart {

  local( $TempDir ) = &FixSlashes( "$AAA_LOAD_CONFIG_PATH" );
  local( $CurrentDir ) = cwd();
  local ( %CM ) = %MAIN_CM;
  local( %DM ) = %MAIN_DM;

  &ReadIn_PinCnf( "pin_cnf_connect.pl" );

  #
  # If the sys/data/config/pin.conf is present, then continue
  # If not, add the entries to the pin.conf file.
  #
  if (!( -e $AAA_LOAD_CONFIG_PATH."/".$PINCONF ))
  {
  	  #
	  # Create pin.conf for loading.
	  #
	    &Configure_PinCnf( $AAA_LOAD_CONFIG_PATH."/".$PINCONF,
                       $CONNECT_PINCONF_HEADER,
                       %CONNECT_PINCONF_ENTRIES);
  }

  chdir $TempDir;
      &configure_opcodemap_gsm_aaa( *MAIN_CM, *MAIN_DM ); 
      &configure_reservation_data( *MAIN_CM, *MAIN_DM ); 
      &configure_reservation_tel( *MAIN_CM, *MAIN_DM ); 
      &configure_fax_params( *MAIN_CM, *MAIN_DM ); 
      &configure_sms_params( *MAIN_CM, *MAIN_DM ); 
      &configure_params_gsm_aaa( *MAIN_CM, *MAIN_DM ); 
      &configure_tel_params( *MAIN_CM, *MAIN_DM ); 
      &configure_data_params( *MAIN_CM, *MAIN_DM ); 
  chdir $CurrentDir;    

      }


  ######################################
  # Loading AAA config opcodemap TCF
  #########################################
  
  sub configure_opcodemap_gsm_aaa {
  
  $Cmd = &FixSlashes( "$PIN_HOME/bin/load_aaa_config_opcodemap_tcf -v pin_config_opcodemap_tcf" );
  &Output( fpLogFile, $IDS_GSMAAA_LOADING );
  &Output( STDOUT, $IDS_GSMAAA_LOADING );
  $Cmd = &ExecuteShell_Piped( "$PIN_TEMP_DIR/tmp.out", TRUE, $Cmd, "" );

  #
  # If loading of aaa_config_opcodemap_tcf failed, display the failed message,
  # else display success message.
  #

  if( $Cmd != 0 )
  {
       &Output( fpLogFile, $IDS_GSMAAA_FAILED );
        &Output( STDOUT, $IDS_GSMAAA_FAILED );
        exit -1;
  }
  else
  {
        &Output( fpLogFile, $IDS_GSMAAA_SUCCESS );
        &Output( STDOUT, $IDS_GSMAAA_SUCCESS );
  }
  
  unlink( "$PIN_TEMP_DIR/tmp.out" );
	
  }
	
  ########################################################
  # Loading pin_config_reservation_aaa_prefs_gsm_data 
  #####################################################
    
    sub configure_reservation_data {
    
    $Cmd = &FixSlashes( "$PIN_HOME/bin/load_config_reservation_aaa_prefs pin_config_reservation_aaa_prefs_gsm_data" );
    &Output( fpLogFile, $IDS_GSMAAA_RESERVATION_DATA_LOADING );
    &Output( STDOUT, $IDS_GSMAAA_RESERVATION_DATA_LOADING );
    $Cmd = &ExecuteShell_Piped( "$PIN_TEMP_DIR/tmp.out", TRUE, $Cmd, "" );
  
    #
    # If loading of pin_config_reservation_aaa_prefs_gsm_data failed, display the failed message,
    # else display success message.
    #
  
    if( $Cmd != 0 )
    {
         &Output( fpLogFile, $IDS_GSMAAA_RESERVATION_DATA_FAILED );
          &Output( STDOUT, $IDS_GSMAAA_RESERVATION_DATA_FAILED );
          exit -1;
    }
    else
    {
          &Output( fpLogFile, $IDS_GSMAAA_RESERVATION_DATA_SUCCESS );
          &Output( STDOUT, $IDS_GSMAAA_RESERVATION_DATA_SUCCESS );
   }
  
  unlink( "$PIN_TEMP_DIR/tmp.out" );
   }
	
  
  ############################################################
  # Loading pin_config_reservation_aaa_prefs_gsm_telephony
  ######################################################
    
    sub configure_reservation_tel {
    
    $Cmd = &FixSlashes( "$PIN_HOME/bin/load_config_reservation_aaa_prefs pin_config_reservation_aaa_prefs_gsm_telephony" );
    &Output( fpLogFile, $IDS_GSMAAA_RESERVATION_TEL_LOADING );
    &Output( STDOUT, $IDS_GSMAAA_RESERVATION_TEL_LOADING );
    $Cmd = &ExecuteShell_Piped( "$PIN_TEMP_DIR/tmp.out", TRUE, $Cmd, "" );
  
    #
    # If loading of pin_config_reservation_aaa_prefs_gsm_telephony failed, display the failed message,
    # else display success message.
    #
  
    if( $Cmd != 0 )
    {
         &Output( fpLogFile, $IDS_GSMAAA_RESERVATION_TEL_FAILED );
          &Output( STDOUT, $IDS_GSMAAA_RESERVATION_TEL_FAILED );
          exit -1;
    }
    else
    {
          &Output( fpLogFile, $IDS_GSMAAA_RESERVATION_TEL_SUCCESS );
          &Output( STDOUT, $IDS_GSMAAA_RESERVATION_TEL_SUCCESS );
  }
  
  unlink( "$PIN_TEMP_DIR/tmp.out" );
	
	
  }
	
  #####################################################
  # Loading pin_telco_gsm_fax_aaa_params.xml 
  ######################################################
    
    sub configure_fax_params {
    
    $Cmd = &FixSlashes( "$PIN_HOME/bin/load_pin_telco_aaa_params pin_telco_gsm_fax_aaa_params.xml" );
    &Output( fpLogFile, $IDS_GSMAAA_FAX_PARAMS_LOADING );
    &Output( STDOUT, $IDS_GSMAAA_FAX_PARAMS_LOADING );
    $Cmd = &ExecuteShell_Piped( "$PIN_TEMP_DIR/tmp.out", TRUE, $Cmd, "" );
  
    #
    # If loading of pin_telco_gsm_fax_aaa_params.xml failed, display the failed message,
    # else display success message.
    #
  
    if( $Cmd != 0 )
    {
         &Output( fpLogFile, $IDS_GSMAAA_FAX_PARAMS_FAILED );
          &Output( STDOUT, $IDS_GSMAAA_FAX_PARAMS_FAILED );
          exit -1;
    }
    else
    {
          &Output( fpLogFile, $IDS_GSMAAA_FAX_PARAMS_SUCCESS );
          &Output( STDOUT, $IDS_GSMAAA_FAX_PARAMS_SUCCESS );
  }
  
  unlink( "$PIN_TEMP_DIR/tmp.out" );
	
  }
	
  ############################################################
  # Loading pin_telco_gsm_sms_aaa_params.xml 
  ############################################################
    
    sub configure_sms_params {
    
    $Cmd = &FixSlashes( "$PIN_HOME/bin/load_pin_telco_aaa_params pin_telco_gsm_sms_aaa_params.xml" );
    &Output( fpLogFile, $IDS_GSMAAA_SMS_PARAMS_LOADING );
    &Output( STDOUT, $IDS_GSMAAA_SMS_PARAMS_LOADING );
    $Cmd = &ExecuteShell_Piped( "$PIN_TEMP_DIR/tmp.out", TRUE, $Cmd, "" );
  
    #
    # If loading of pin_telco_gsm_sms_aaa_params.xml failed, display the failed message,
    # else display success message.
    #
  
    if( $Cmd != 0 )
    {
         &Output( fpLogFile, $IDS_GSMAAA_SMS_PARAMS_FAILED );
          &Output( STDOUT, $IDS_GSMAAA_SMS_PARAMS_FAILED );
          exit -1;
    }
    else
    {
          &Output( fpLogFile, $IDS_GSMAAA_SMS_PARAMS_SUCCESS );
          &Output( STDOUT, $IDS_GSMAAA_SMS_PARAMS_SUCCESS );
  }
  
  unlink( "$PIN_TEMP_DIR/tmp.out" );
	
  }
	
  #############################################################
  # Loading pin_telco_gsm_aaa_params.xml 
  ##############################################################
    
    sub configure_params_gsm_aaa {
    
    $Cmd = &FixSlashes( "$PIN_HOME/bin/load_pin_telco_aaa_params pin_telco_gsm_aaa_params.xml" );
    &Output( fpLogFile, $IDS_GSMAAA_PARAMS_LOADING );
    &Output( STDOUT, $IDS_GSMAAA_PARAMS_LOADING );
    $Cmd = &ExecuteShell_Piped( "$PIN_TEMP_DIR/tmp.out", TRUE, $Cmd, "" );
  
    #
    # If loading of pin_telco_gsm_aaa_params.xml failed, display the failed message,
    # else display success message.
    #
  
    if( $Cmd != 0 )
    {
         &Output( fpLogFile, $IDS_GSMAAA_PARAMS_FAILED );
          &Output( STDOUT, $IDS_GSMAAA_PARAMS_FAILED );
          exit -1;
    }
    else
    {
          &Output( fpLogFile, $IDS_GSMAAA_PARAMS_SUCCESS );
          &Output( STDOUT, $IDS_GSMAAA_PARAMS_SUCCESS );
    }
  
  unlink( "$PIN_TEMP_DIR/tmp.out" );
	
   }
	
  #########################################################
  # Loading pin_telco_gsm_telephony_aaa_params.xml
  ########################################################
    
    sub configure_tel_params {
    
    $Cmd = &FixSlashes( "$PIN_HOME/bin/load_pin_telco_aaa_params pin_telco_gsm_telephony_aaa_params.xml" );
    &Output( fpLogFile, $IDS_GSMAAA_TEL_PARAMS_LOADING );
    &Output( STDOUT, $IDS_GSMAAA_TEL_PARAMS_LOADING );
    $Cmd = &ExecuteShell_Piped( "$PIN_TEMP_DIR/tmp.out", TRUE, $Cmd, "" );
  
    #
    # If loading of pin_telco_gsm_telephony_aaa_params.xml failed, display the failed message,
    # else display success message.
    #
  
    if( $Cmd != 0 )
    {
         &Output( fpLogFile, $IDS_GSMAAA_TEL_PARAMS_FAILED );
          &Output( STDOUT, $IDS_GSMAAA_TEL_PARAMS_FAILED );
          exit -1;
    }
    else
    {
          &Output( fpLogFile, $IDS_GSMAAA_TEL_PARAMS_SUCCESS );
          &Output( STDOUT, $IDS_GSMAAA_TEL_PARAMS_SUCCESS );
  }
  unlink( "$PIN_TEMP_DIR/tmp.out" );
	}
	
  #######################################################
  # Loading pin_telco_gsm_data_aaa_params.xml 
  ##################################################
      
      sub configure_data_params {
      
      $Cmd = &FixSlashes( "$PIN_HOME/bin/load_pin_telco_aaa_params pin_telco_gsm_data_aaa_params.xml" );
      &Output( fpLogFile, $IDS_GSMAAA_DATA_PARAMS_LOADING );
      &Output( STDOUT, $IDS_GSMAAA_DATA_PARAMS_LOADING );
      $Cmd = &ExecuteShell_Piped( "$PIN_TEMP_DIR/tmp.out", TRUE, $Cmd, "" );
    
      #
      # If loading of pin_telco_gsm_data_aaa_params.xml failed, display the failed message,
      # else display success message.
      #
    
      if( $Cmd != 0 )
      {
           &Output( fpLogFile, $IDS_GSMAAA_DATA_PARAMS_FAILED );
            &Output( STDOUT, $IDS_GSMAAA_DATA_PARAMS_FAILED );
            exit -1;
      }
      else
      {
            &Output( fpLogFile, $IDS_GSMAAA_DATA_PARAMS_SUCCESS );
            &Output( STDOUT, $IDS_GSMAAA_DATA_PARAMS_SUCCESS );
  }

   unlink( "$PIN_TEMP_DIR/tmp.out" );
	}
1;
