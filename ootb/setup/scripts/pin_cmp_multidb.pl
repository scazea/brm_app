#!/usr/bin/env perl
#=============================================================
#  @(#) % %
# 
# Copyright (c) 2000, 2017, Oracle and/or its affiliates. All rights reserved.
#       This material is the confidential property of Oracle Corporation 
#       or its licensors and may be used, reproduced, stored
#       or transmitted only in accordance with a valid Oracle license or 
#       sublicense agreement.
#
# Portal installation for the MultiDB Manager
#
#=============================================================

use Cwd;

# If running stand alone, without pin_setup
if ( ! ( $RUNNING_IN_PIN_SETUP eq TRUE ) )
{
   require "pin_res.pl";
   require "pin_functions.pl";
   require "../pin_setup.values";
   &ConfigureComponentCalledSeparately ( $0 );
}

##########################################
#
# Configure MultiDB Manager pin.conf files
#
##########################################
sub configure_multidb_config_files {  
  local ( %CM ) = %MAIN_CM;
  local ( %DM ) = %MAIN_DM;
  local ( @FileReadIn );
  local ( $Start );  

  &ReadIn_PinCnf( "pin_cnf_multidb.pl" );
  &ReadIn_PinCnf( "pin_cnf_connect.pl" );
      # Add CM entries and  multi db pin_confirm and pin_config entries
      &AddArrays( \%CONNECT_PINCONF_ENTRIES, \%MULTIDB_PINCONF_ENTRIES );     


           
# Generating multi_db pin.conf
      local ($CONF) = $MULTI_DB{'pin_cnf_location'}."/".$PINCONF;
      if (-f $CONF)
      {
      # if the file already exist, retain the current configuration and make
      # pin.conf.unmodified for references
	$CONF = "$CONF.$CONFIGDISTEXT";
      }
      &Configure_PinCnf($CONF,
	  $MULTIDB_PINCONF_HEADER,
	  %MULTIDB_PINCONF_ENTRIES ); 

      #Generate $ENV{BRM_CONF}/apps/multidb/config_dist.conf
      &ConfigDist();
}
1;
#######
#
# Configuring database for MultiDB Mgr
#
#######
#sub configure_multidb_database {
#}
