DECLARE
   v_ddl_stmt VARCHAR2(100);
   v_count NUMBER;
BEGIN
   SELECT COUNT(table_name) INTO v_count FROM user_tables WHERE table_name = 'INVOICE_CHUNK_IDS';
   IF v_count <>0 THEN
   v_ddl_stmt :=  'drop table INVOICE_CHUNK_IDS';
   EXECUTE IMMEDIATE v_ddl_stmt;
   END IF;
END;
/
SHOW errors;
CREATE  global temporary TABLE INVOICE_CHUNK_IDS
(
  count  NUMBER,
  first  NUMBER,
  last  NUMBER
) on commit delete rows ;
/
SHOW errors;
CREATE OR REPLACE PACKAGE InvTypes wrapped 
a000000
367
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
140 db
kLWH4dunbiaAYp0EZPH1lzziqG4wg/DILp7hf3QCTP6OMefLhY+8CoPJFE0uoV6VPWi1bwtY
LFIEr9iHHA+b5vFO6r81Ds5Q6hV00RPF5a9dzM69iTVQA0bWNybTXn0+bsnYsZ9hMzI0LR5i
vFrJiL8yfJ+aLwI7GkoUHmR1hqxVaCSJwdOU4L5PzOwykjwCICbpBlvCOQtUvAG/HQ/vzNs=


/
SHOW errors;
CREATE OR REPLACE PACKAGE INV wrapped 
a000000
367
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
17a 107
jbVtnIBctuMCCcBAl9I/HUxRJyUwgxDQrcvhf3TpJseenMqo0XxcBnVY50uASmo8XfnEyjVn
r4ubHnOVA8aQLL3TLK8XwcmUGlSJcBQKLo1Gw1LFNLp7MJiPsrDs6w34N9moyOIkTX6HO4Mk
Rlf5W0GEimWamNP98JpnjV7pPn6mCYq/h9jyFqsSVFu6NXL/E4BeGFQhbT9X6tbHBRURoQRZ
xblsVk9O/wcQKHQuuu1u8iuJa9bXBYfNAWoGORHMXgk=

/
SHOW errors;
CREATE OR REPLACE PACKAGE BODY INV wrapped 
a000000
367
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
fa7 54e
CplN88q6mri/08pFsDWUugT7rsEwg83qeSDrfC+Lx6oCOqmDZyoyty/TAbfTxt6MbJ8B2rrG
nE0Fe8MhDU2OFBQ/ehdJCAy3by/T4aJjfjtzbrtDvk5Myai8dEk26Va8NT7t5mt9gSOC/3z0
mdxQRoXTp5MYofSwRO120lAZSZ5sQadmEAyPpqJtembPtOa9iMCTlwsv5oaGqdQvQoLrRQnF
+hqZhTEy9JM1bObYyAdybQzcfIYte1n7NTd7um537CUEH3F8GfF0w256easXR9LcN3MnEn6i
R5yIHe8ltHZxWCc38NmykaoPxyXOJzOzHschg1tDj10thJu4P3APhXFRCsiG3oVyOReMsxYE
/mZlod5kpAjyMgVedp4GfxGf4rBGUYIU7g/VUKRCifakEDVlrGyjOyQgK7/ZwL+bOANFlur+
jKdaTqFX2WIcgbOxZHl+D1gd9CByc1O6iiIQoo88NfeVkhnzUwXRH5hDAE1TV7FmGzqq3JIu
bHGN6cQWXdFLpFX1Rmu7yA7vepJV8xxRBT2GhidzSzqAUc1tq8HfxLtcw93JHmT0PcyWceTg
mHG7lwfGlZiGGbV0Q7C1PrW6Fp3OMpevKBDZewx+OZOP0rA2HOlhHfoEE4GjWPcvEfziFNKj
PPXVBYiFeo1c5ZN9gpeR4TvKn4kwza/5MDfcmLqIDmhPpFc9O6YBVmUF3gb0QSzF4QWOVQto
VOj6zqUm/3dOCyqnCTVT2AZ9LsMO6g778gnRXS8Ba5IKDTZQxwOs3egtMhHtLEU5RIa7/Usf
mswFkyA1QwIDRoI5cwXCaD0G/qwaCZJyGC7ZzEwsn3RZbsxkvJ5P6nhx2cUSol5ug1pXE0d/
WPqJk50yN3znRB9V+ylWtirpPta671fY/vGs4GMdoSvhKrMONUdOWS4D89hE47aTuOVTbWmj
WG4+Rm9/NPwg/DNP/sYqYLhxwhTzoivSfa26OIe8J8otyFXI6zn3s6H0Q2i4fi9SiW3T0kHb
FL1tWaeTVDX9+h7I64ncg/EyJXtqSV1o3b+HylOpcEDXZXUl3oo2EnCOhFmC5knA/Bg5xQvW
Z5kwIYh/R32UFOWRe10c8H83MHctep24FYqdn03dYBvOq/xUjwtqaF+BsjxU4ShdvkU/QHmC
VmKNPqflYX/LYeEF6SzY2NEtfLvF4iqy0CcFnOV1Ad2PcTye/KClUiKGKsKHdG6seRbErZuO
wmZ90dV5LDSx155C4a5YY4l0kpGYVX6zVclzQr5VOzy7UEheXK+52QdDHicUuDKYSUV5r5hZ
/PIDoMTRrNacZ2Ll05/YmbzRKUXkqADUrrz7F9ZRAQ==

/
