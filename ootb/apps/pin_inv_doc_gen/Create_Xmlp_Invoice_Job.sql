Rem
Rem $Header: bus_platform_vob/financial/apps/pin_inv_doc_gen/src/Create_Xmlp_Invoice_Job.sql /cgbubrm_7.3.2.rwsmod/1 2008/12/23 01:06:34 viprasad Exp $
Rem
Rem Create_Xmlp_Invoice_Job.sql
Rem
Rem Copyright (c) 2008, Oracle and/or its affiliates.All rights reserved. 
Rem
Rem    NAME
Rem      Create_Xmlp_Invoice_Job.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      This script creates XMLP_INVOICE_JOB table in schedular database. 
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    viprasad    12/22/08 - Created
Rem

CREATE TABLE XMLP_INVOICE_JOB
(
JOB_ID int ,
INVOICE_OBJ_ID int
)tablespace <tablespacename> storage (initial 100k next 100k pctincrease 0 );
