set serveroutput on size 1000000
-- Copyright (c) 2003 - 2008 Oracle. All rights reserved.This material is the confidential property of Oracle Corporation
-- or its licensors and may be used, reproduced, stored or transmitted only in accordance with a valid Oracle
-- license or sublicense  agreement.
--
CREATE OR REPLACE PROCEDURE receive_event_rc(max_events IN NUMBER,
				             queue_name IN VARCHAR2,
				             event_name_cond IN VARCHAR2)
IS
  begin_time	     NUMBER;
  end_time           NUMBER;
  --
  event_name	     VARCHAR2(32);
  flist_buf	     VARCHAR2(32767); -- max. VARCHAR2 lenght in PL/SQL programs
  large_flist_buf    CLOB;
  mesg_id            VARCHAR2(32);
  enq_time	     DATE;
  --
  start_pos	     NUMBER;
  str_len	     NUMBER;
  --
  no_messages        exception;
  pragma exception_init (no_messages, -25228);
  --
  continue_flag      BOOLEAN;
  --
  rs		     acct_sync.deq_event_cursor;
  rs_rec	     acct_sync.deq_event_rec;
  --
BEGIN
  --
  begin_time := dbms_utility.get_time(); -- 100th of a second
  --
  -- 10 is number of messages in batch returned by acct_sync.deq_event_rc()
  --
  FOR i in 1 .. max_events/10 LOOP 
    --
    continue_flag := true;
    WHILE continue_flag = true LOOP
      BEGIN
        acct_sync.deq_event_rc(queue_name,
                               event_name_cond,
                               10,
                               NULL,
                               rs);
        LOOP
	  FETCH	rs INTO rs_rec;
	  EXIT WHEN rs%NOTFOUND;
            DBMS_OUTPUT.PUT_LINE('mesg_id = '||rs_rec.mesg_id||', enq_time = '||
                                 to_char(rs_rec.enq_time, 'MM/DD/YYYY HH24:MM:ss'));
        END LOOP;
	continue_flag := false;
      EXCEPTION
        WHEN no_messages THEN
	  continue_flag := true;
      END;
    END LOOP;

    --
    COMMIT;
  END LOOP;
  --
  end_time := dbms_utility.get_time();
  dbms_output.put_line('elapsed time = '|| (end_time - begin_time)/100 ||'[sec]');
  -- 
  EXCEPTION
    WHEN no_messages THEN
      DBMS_OUTPUT.PUT_LINE ('exceptions: no_messages');
END;
/
show errors;
