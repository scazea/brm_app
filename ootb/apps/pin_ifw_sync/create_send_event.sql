set serveroutput on size 1000000;
-- Copyright (c) 2003 - 2007 Oracle. All rights reserved.This material is the confidential property of Oracle Corporation
-- or its licensors and may be used, reproduced, stored or transmitted only in accordance with a valid Oracle
-- license or sublicense  agreement.
--
CREATE OR REPLACE PROCEDURE send_event(event_type IN VARCHAR2, -- 'LONG' or 'SHORT'
                                       max_events IN NUMBER,
				       queue_name IN VARCHAR2)
IS
   begin_time	     NUMBER;
   end_time          NUMBER;
   --
   event_name        VARCHAR2(32);
   flist_buf         VARCHAR2(32767); -- max. VARCHAR2 lenght in PL/SQL programs
   -- 
   short_event_name  VARCHAR2(32);
   short_flist_buf   VARCHAR2(32767); -- max. VARCHAR2 lenght in PL/SQL programs
   -- 
   long_event_name   VARCHAR2(32);
   long_flist_buf    VARCHAR2(32767); -- max. VARCHAR2 lenght in PL/SQL programs
   --
   mesg_id	     VARCHAR2(32);
   -- 
BEGIN
  -- approx. 0.35k payload
  short_event_name := 'ShortTestEvent';
  short_flist_buf :=
'0 PIN_FLD_ACCOUNT_OBJ    POID [0] 0.0.0.1 /account 13896 18'||
'0 PIN_FLD_PROGRAM_NAME    STR [0] "short21�21"'||
'0 PIN_FLD_POID           POID [0] 0.0.0.1 /event/notification/service_item/make_bill -1 0'||
'0 PIN_FLD_END_T        TSTAMP [0] (1019818794) Fri Apr 26 11:59:54 2002'||
'0 PIN_FLD_START_T      TSTAMP [0] (1019818794) Fri Apr 26 11:59:54 2002';
  -- approx. 14kbyte payload
  long_event_name := 'LongTestEvent';
  long_flist_buf :=
'0 PIN_FLD_POID           POID [0] 0.0.0.1 /event/notification/account/create -1 0'||
'0 PIN_FLD_NAME            STR [0] "long21�21"'||
'0 PIN_FLD_USERID         POID [0] 0.0.0.1 /service/admin_client 2 90'||
'0 PIN_FLD_SESSION_OBJ    POID [0] 0.0.0.1 /event/session 207622979697632899 0'||
'0 PIN_FLD_ACCOUNT_OBJ    POID [0] 0.0.0.1 /account 1433987 0'||
'0 PIN_FLD_PROGRAM_NAME    STR [0] "cm"'||
'0 PIN_FLD_START_T      TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_END_T        TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_SYS_DESCR       STR [0] "Account created"'||
'0 PIN_FLD_POID           POID [0] 0.0.0.1 /event/customer/billinfo -1 0'||
'0 PIN_FLD_NAME            STR [0] "Customer Mngmt. Event Log"'||
'0 PIN_FLD_USERID         POID [0] 0.0.0.1 /service/admin_client 2 90'||
'0 PIN_FLD_SESSION_OBJ    POID [0] 0.0.0.1 /event/session 207622979697632899 0'||
'0 PIN_FLD_ACCOUNT_OBJ    POID [0] 0.0.0.1 /account 1433987 0'||
'0 PIN_FLD_PROGRAM_NAME    STR [0] "Automatic Account Creation"'||
'0 PIN_FLD_START_T      TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_END_T        TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_SYS_DESCR       STR [0] "Set Billing Info"'||
'0 PIN_FLD_BILLINFO      ARRAY [0] allocated 20, used 13'||
'1     PIN_FLD_MERCHANT        STR [0] ""'||
'1     PIN_FLD_BILL_MODE       STR [0] ""'||
'1     PIN_FLD_BILL_WHEN       INT [0] 0'||
'1     PIN_FLD_NEXT_BILL_T  TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'1     PIN_FLD_BILL_TYPE      ENUM [0] 0'||
'1     PIN_FLD_CURRENCY        INT [0] 0'||
'1     PIN_FLD_CURRENCY_SECONDARY    INT [0] 0'||
'1     PIN_FLD_PARENT         POID [0] 0.0.0.0  0 0'||
'1     PIN_FLD_ACCESS_CODE1    STR [0] ""'||
'1     PIN_FLD_ACCESS_CODE2    STR [0] ""'||
'1     PIN_FLD_BILL_ACTGCYCLES_LEFT    INT [0] 0'||
'1     PIN_FLD_BUSINESS_TYPE   ENUM [0] 0'||
'1     PIN_FLD_ACCOUNT_NO      STR [0] NULL str ptr'||
'0 PIN_FLD_BILLINFO      ARRAY [1] allocated 20, used 13'||
'1     PIN_FLD_MERCHANT        STR [0] "test"'||
'1     PIN_FLD_BILL_MODE       STR [0] ""'||
'1     PIN_FLD_BILL_WHEN       INT [0] 1'||
'1     PIN_FLD_NEXT_BILL_T  TSTAMP [0] (1022281200) Sat May 25 00:00:00 2002'||
'1     PIN_FLD_BILL_TYPE      ENUM [0] 10001'||
'1     PIN_FLD_CURRENCY        INT [0] 826'||
'1     PIN_FLD_CURRENCY_SECONDARY    INT [0] 0'||
'1     PIN_FLD_PARENT         POID [0] 0.0.0.0  0 0'||
'1     PIN_FLD_ACCESS_CODE1    STR [0] ""'||
'1     PIN_FLD_ACCESS_CODE2    STR [0] ""'||
'1     PIN_FLD_BILL_ACTGCYCLES_LEFT    INT [0] 1'||
'1     PIN_FLD_BUSINESS_TYPE   ENUM [0] 1'||
'1     PIN_FLD_ACCOUNT_NO      STR [0] "0.0.0.1-1433987"'||
'0 PIN_FLD_EARNED_START_T TSTAMP [0] (0) <null>'||
'0 PIN_FLD_EARNED_END_T TSTAMP [0] (0) <null>'||
'0 PIN_FLD_EARNED_TYPE     INT [0] 0'||
'0 PIN_FLD_EFFECTIVE_T  TSTAMP [0] (0) <null>'||
'0 PIN_FLD_POID           POID [0] 0.0.0.1 /event/customer/status -1 0'||
'0 PIN_FLD_NAME            STR [0] "Customer Mngmt. Event Log"'||
'0 PIN_FLD_USERID         POID [0] 0.0.0.1 /service/admin_client 2 90'||
'0 PIN_FLD_SESSION_OBJ    POID [0] 0.0.0.1 /event/session 207622979697632899 0'||
'0 PIN_FLD_ACCOUNT_OBJ    POID [0] 0.0.0.1 /account 1433987 0'||
'0 PIN_FLD_PROGRAM_NAME    STR [0] "Automatic Account Creation"'||
'0 PIN_FLD_START_T      TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_END_T        TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_SYS_DESCR       STR [0] "Set Status (acct)"'||
'0 PIN_FLD_STATUSES      ARRAY [0] allocated 20, used 3'||
'1     PIN_FLD_STATUS         ENUM [0] 0'||
'1     PIN_FLD_STATUS_FLAGS    INT [0] 0'||
'1     PIN_FLD_CLOSE_WHEN_T TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_STATUSES      ARRAY [1] allocated 20, used 3'||
'1     PIN_FLD_STATUS         ENUM [0] 10100'||
'1     PIN_FLD_STATUS_FLAGS    INT [0] 0'||
'1     PIN_FLD_CLOSE_WHEN_T TSTAMP [0] (0) <null>'||
'0 PIN_FLD_EARNED_START_T TSTAMP [0] (0) <null>'||
'0 PIN_FLD_EARNED_END_T TSTAMP [0] (0) <null>'||
'0 PIN_FLD_EARNED_TYPE     INT [0] 0'||
'0 PIN_FLD_EFFECTIVE_T  TSTAMP [0] (0) <null>'||
'0 PIN_FLD_POID           POID [0] 0.0.0.1 /event/customer/login -1 0'||
'0 PIN_FLD_NAME            STR [0] "Customer Mngmt. Event Log"'||
'0 PIN_FLD_USERID         POID [0] 0.0.0.1 /service/admin_client 2 90'||
'0 PIN_FLD_SESSION_OBJ    POID [0] 0.0.0.1 /event/session 207622979697632899 0'||
'0 PIN_FLD_ACCOUNT_OBJ    POID [0] 0.0.0.1 /account 1433987 0'||
'0 PIN_FLD_PROGRAM_NAME    STR [0] "Automatic Account Creation"'||
'0 PIN_FLD_START_T      TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_END_T        TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_SERVICE_OBJ    POID [0] 0.0.0.1 /service/gsm/telephony 1436547 0'||
'0 PIN_FLD_SYS_DESCR       STR [0] "Set Login"'||
'0 PIN_FLD_LOGINS        ARRAY [0] allocated 20, used 1'||
'1     PIN_FLD_LOGIN           STR [0] ""'||
'0 PIN_FLD_LOGINS        ARRAY [1] allocated 20, used 1'||
'1     PIN_FLD_LOGIN           STR [0] "416-20020425-143402-0-19701-1-owen"'||
'0 PIN_FLD_EARNED_START_T TSTAMP [0] (0) <null>'||
'0 PIN_FLD_EARNED_END_T TSTAMP [0] (0) <null>'||
'0 PIN_FLD_EARNED_TYPE     INT [0] 0'||
'0 PIN_FLD_EFFECTIVE_T  TSTAMP [0] (0) <null>'||
'0 PIN_FLD_POID           POID [0] 0.0.0.1 /event/customer/status -1 0'||
'0 PIN_FLD_NAME            STR [0] "Customer Mngmt. Event Log"'||
'0 PIN_FLD_USERID         POID [0] 0.0.0.1 /service/admin_client 2 90'||
'0 PIN_FLD_SESSION_OBJ    POID [0] 0.0.0.1 /event/session 207622979697632899 0'||
'0 PIN_FLD_ACCOUNT_OBJ    POID [0] 0.0.0.1 /account 1433987 0'||
'0 PIN_FLD_PROGRAM_NAME    STR [0] "Automatic Account Creation"'||
'0 PIN_FLD_START_T      TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_END_T        TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_SERVICE_OBJ    POID [0] 0.0.0.1 /service/gsm/telephony 1436547 0'||
'0 PIN_FLD_SYS_DESCR       STR [0] "Set Status (srvc)"'||
'0 PIN_FLD_STATUSES      ARRAY [0] allocated 20, used 3'||
'1     PIN_FLD_STATUS         ENUM [0] 0'||
'1     PIN_FLD_STATUS_FLAGS    INT [0] 0'||
'1     PIN_FLD_CLOSE_WHEN_T TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_STATUSES      ARRAY [1] allocated 20, used 3'||
'1     PIN_FLD_STATUS         ENUM [0] 10100'||
'1     PIN_FLD_STATUS_FLAGS    INT [0] 0'||
'1     PIN_FLD_CLOSE_WHEN_T TSTAMP [0] (0) <null>'||
'0 PIN_FLD_EARNED_START_T TSTAMP [0] (0) <null>'||
'0 PIN_FLD_EARNED_END_T TSTAMP [0] (0) <null>'||
'0 PIN_FLD_EARNED_TYPE     INT [0] 0'||
'0 PIN_FLD_EFFECTIVE_T  TSTAMP [0] (0) <null>'||
'0 PIN_FLD_POID           POID [0] 0.0.0.1 /event/customer/login 207622979697635203 0'||
'0 PIN_FLD_NAME            STR [0] "Customer Mngmt. Event Log"'||
'0 PIN_FLD_USERID         POID [0] 0.0.0.1 /service/admin_client 2 90'||
'0 PIN_FLD_SESSION_OBJ    POID [0] 0.0.0.1 /event/session 207622979697632899 0'||
'0 PIN_FLD_ACCOUNT_OBJ    POID [0] 0.0.0.1 /account 1433987 0'||
'0 PIN_FLD_PROGRAM_NAME    STR [0] "fm_utils_wireless.c(1.6)"'||
'0 PIN_FLD_START_T      TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_END_T        TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_SERVICE_OBJ    POID [0] 0.0.0.1 /service/gsm/telephony 1436547 0'||
'0 PIN_FLD_SYS_DESCR       STR [0] "Set Login"'||
'0 PIN_FLD_LOGINS        ARRAY [0] allocated 20, used 1'||
'1     PIN_FLD_LOGIN           STR [0] "416-20020425-143402-0-19701-1-owen"'||
'0 PIN_FLD_LOGINS        ARRAY [1] allocated 20, used 2'||
'1     PIN_FLD_LOGIN           STR [0] "493-20020425-143402-1-19701-1-owen"'||
'1     PIN_FLD_ALIAS_LIST    ARRAY [1] allocated 20, used 1'||
'2         PIN_FLD_NAME            STR [0] "001753100037"'||
'0 PIN_FLD_EARNED_START_T TSTAMP [0] (0) <null>'||
'0 PIN_FLD_EARNED_END_T TSTAMP [0] (0) <null>'||
'0 PIN_FLD_EARNED_TYPE     INT [0] 0'||
'0 PIN_FLD_EFFECTIVE_T  TSTAMP [0] (0) <null>'||
'0 PIN_FLD_POID           POID [0] 0.0.0.1 /event/billing/product/action/purchase 17592187480131 0'||
'0 PIN_FLD_NAME            STR [0] "Billing Event Log"'||
'0 PIN_FLD_USERID         POID [0] 0.0.0.1 /service/admin_client 2 90'||
'0 PIN_FLD_SESSION_OBJ    POID [0] 0.0.0.1 /event/session 207622979697632899 0'||
'0 PIN_FLD_ACCOUNT_OBJ    POID [0] 0.0.0.1 /account 1433987 0'||
'0 PIN_FLD_PROGRAM_NAME    STR [0] "Automatic Account Creation"'||
'0 PIN_FLD_START_T      TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_END_T        TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_SERVICE_OBJ    POID [0] 0.0.0.1 /service/gsm/telephony 1436547 0'||
'0 PIN_FLD_PRODUCT      SUBSTRUCT [0] allocated 20, used 2'||
'1     PIN_FLD_PRODUCT_OBJ    POID [0] 0.0.0.1 /product 98242 3'||
'1     PIN_FLD_NODE_LOCATION    STR [0] "owen#19701/1#20020425-143402.752:owen#19701/1#20020425-143402.752"'||
'0 PIN_FLD_ACTION_INFO   ARRAY [1] allocated 38, used 38'||
'1     PIN_FLD_CYCLE_DISCOUNT DECIMAL [0] 0'||
'1     PIN_FLD_CYCLE_END_CYCLE DECIMAL [0] 0'||
'1     PIN_FLD_CYCLE_END_T  TSTAMP [0] (0) <null>'||
'1     PIN_FLD_CYCLE_START_CYCLE DECIMAL [0] 0'||
'1     PIN_FLD_CYCLE_START_T TSTAMP [0] (1019689200) Thu Apr 25 00:00:00 2002'||
'1     PIN_FLD_PRODUCT_OBJ    POID [0] 0.0.0.1 /product 98242 3'||
'1     PIN_FLD_PURCHASE_DISCOUNT DECIMAL [0] 0'||
'1     PIN_FLD_PURCHASE_END_CYCLE DECIMAL [0] 0'||
'1     PIN_FLD_PURCHASE_END_T TSTAMP [0] (0) <null>'||
'1     PIN_FLD_PURCHASE_START_CYCLE DECIMAL [0] 0'||
'1     PIN_FLD_PURCHASE_START_T TSTAMP [0] (1019689200) Thu Apr 25 00:00:00 2002'||
'1     PIN_FLD_QUANTITY     DECIMAL [0] 1'||
'1     PIN_FLD_STATUS         ENUM [0] 1'||
'1     PIN_FLD_STATUS_FLAGS    INT [0] 0'||
'1     PIN_FLD_USAGE_DISCOUNT DECIMAL [0] 0'||
'1     PIN_FLD_USAGE_END_CYCLE DECIMAL [0] 0'||
'1     PIN_FLD_USAGE_END_T  TSTAMP [0] (0) <null>'||
'1     PIN_FLD_USAGE_START_CYCLE DECIMAL [0] 0'||
'1     PIN_FLD_USAGE_START_T TSTAMP [0] (1019689200) Thu Apr 25 00:00:00 2002'||
'1     PIN_FLD_NODE_LOCATION    STR [0] "owen#19701/1#20020425-143402.752:owen#19701/1#20020425-143402.752"'||
'1     PIN_FLD_DEAL_OBJ       POID [0] 0.0.0.1 /deal 96802 4'||
'1     PIN_FLD_PLAN_OBJ       POID [0] 0.0.0.1 /plan 97826 6'||
'1     PIN_FLD_SPONSOR_OBJ    POID [0] NULL poid pointer'||
'1     PIN_FLD_CREATED_T    TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'1     PIN_FLD_PURCHASE_DISC_AMT DECIMAL [0] 0'||
'1     PIN_FLD_PURCHASE_FEE_AMT DECIMAL [0] 0'||
'1     PIN_FLD_CYCLE_DISC_AMT DECIMAL [0] 0'||
'1     PIN_FLD_CYCLE_FEE_AMT DECIMAL [0] 0'||
'1     PIN_FLD_LAST_MODIFIED_T TSTAMP [0] (0) <null>'||
'1     PIN_FLD_TYPE           ENUM [0] 602'||
'1     PIN_FLD_FLAGS           INT [0] 0'||
'1     PIN_FLD_SERVICE_OBJ    POID [0] 0.0.0.1 /service/gsm/telephony 1436547 0'||
'1     PIN_FLD_MMC_START_T  TSTAMP [0] (0) <null>'||
'1     PIN_FLD_MMC_END_T    TSTAMP [0] (0) <null>'||
'1     PIN_FLD_SMC_START_T  TSTAMP [0] (0) <null>'||
'1     PIN_FLD_SMC_END_T    TSTAMP [0] (0) <null>'||
'1     PIN_FLD_MMC_TYPE        INT [0] 0'||
'1     PIN_FLD_CYCLE_FEE_FLAGS    INT [0] 0'||
'0 PIN_FLD_SYS_DESCR       STR [0] "Purchase Product (srvc): Vodafone 60"'||
'0 PIN_FLD_EARNED_START_T TSTAMP [0] (0) <null>'||
'0 PIN_FLD_EARNED_END_T TSTAMP [0] (0) <null>'||
'0 PIN_FLD_EARNED_TYPE     INT [0] 0'||
'0 PIN_FLD_EFFECTIVE_T  TSTAMP [0] (0) <null>'||
'0 PIN_FLD_ACCOUNT_OBJ    POID [0] 0.0.0.1 /account 1433987 0'||
'0 PIN_FLD_USERID         POID [0] 0.0.0.1 /service/admin_client 2 90'||
'0 PIN_FLD_POID           POID [0] 0.0.0.1 /event/billing/deal/purchase 17592187479107 0'||
'0 PIN_FLD_PROGRAM_NAME    STR [0] "Automatic Account Creation"'||
'0 PIN_FLD_NAME            STR [0] "Billing Event Log"'||
'0 PIN_FLD_SYS_DESCR       STR [0] "Purchase Deal (srvc): Vodafone 60"'||
'0 PIN_FLD_SESSION_OBJ    POID [0] 0.0.0.1 /event/session 207622979697632899 0'||
'0 PIN_FLD_END_T        TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_DEAL_INFO    SUBSTRUCT [0] allocated 20, used 3'||
'1     PIN_FLD_PLAN_OBJ       POID [0] 0.0.0.1 /plan 97826 6'||
'1     PIN_FLD_DEAL_OBJ       POID [0] 0.0.0.1 /deal 96802 4'||
'1     PIN_FLD_NODE_LOCATION    STR [0] "owen#19701/1#20020425-143402.752:owen#19701/1#20020425-143402.752"'||
'0 PIN_FLD_START_T      TSTAMP [0] (1019741642) Thu Apr 25 14:34:02 2002'||
'0 PIN_FLD_SERVICE_OBJ    POID [0] 0.0.0.1 /service/gsm/telephony 1436547 0'||
'0 PIN_FLD_EARNED_START_T TSTAMP [0] (0) <null>'||
'0 PIN_FLD_EARNED_END_T TSTAMP [0] (0) <null>'||
'0 PIN_FLD_EARNED_TYPE     INT [0] 0'||
'0 PIN_FLD_EFFECTIVE_T  TSTAMP [0] (0) <null>'||
'0 PIN_FLD_PRODUCTS      ARRAY [0] allocated 22, used 22'||
'1     PIN_FLD_CYCLE_DISCOUNT DECIMAL [0] 0'||
'1     PIN_FLD_CYCLE_END_CYCLE DECIMAL [0] 0'||
'1     PIN_FLD_CYCLE_END_T  TSTAMP [0] (0) <null>'||
'1     PIN_FLD_CYCLE_START_CYCLE DECIMAL [0] 0'||
'1     PIN_FLD_CYCLE_START_T TSTAMP [0] (0) <null>'||
'1     PIN_FLD_PRODUCT_OBJ    POID [0] 0.0.0.1 /product 98242 3'||
'1     PIN_FLD_PURCHASE_DISCOUNT DECIMAL [0] 0'||
'1     PIN_FLD_PURCHASE_END_CYCLE DECIMAL [0] 0'||
'1     PIN_FLD_PURCHASE_END_T TSTAMP [0] (0) <null>'||
'1     PIN_FLD_PURCHASE_START_CYCLE DECIMAL [0] 0'||
'1     PIN_FLD_PURCHASE_START_T TSTAMP [0] (0) <null>'||
'1     PIN_FLD_QUANTITY     DECIMAL [0] 1'||
'1     PIN_FLD_STATUS         ENUM [0] 1'||
'1     PIN_FLD_STATUS_FLAGS    INT [0] 0'||
'1     PIN_FLD_USAGE_DISCOUNT DECIMAL [0] 0'||
'1     PIN_FLD_USAGE_END_CYCLE DECIMAL [0] 0'||
'1     PIN_FLD_USAGE_END_T  TSTAMP [0] (0) <null>'||
'1     PIN_FLD_USAGE_START_CYCLE DECIMAL [0] 0'||
'1     PIN_FLD_USAGE_START_T TSTAMP [0] (0) <null>'||
'1     PIN_FLD_NODE_LOCATION    STR [0] "owen#19701/1#20020425-143402.752:owen#19701/1#20020425-143402.752"'||
'1     PIN_FLD_DEAL_OBJ       POID [0] 0.0.0.1 /deal 96802 4'||
'1     PIN_FLD_PLAN_OBJ       POID [0] 0.0.0.1 /plan 97826 6'||
'0 PIN_FLD_POID           POID [0] 0.0.0.1 /event/notification/service/post_change -1 0'||
'0 PIN_FLD_NAME            STR [0] "Service Object Update Log"'||
'0 PIN_FLD_USERID         POID [0] 0.0.0.1 /service/admin_client 2 90'||
'0 PIN_FLD_SESSION_OBJ    POID [0] 0.0.0.1 /event/session 207622979697632899 0'||
'0 PIN_FLD_ACCOUNT_OBJ    POID [0] 0.0.0.1 /account 1433987 0'||
'0 PIN_FLD_PROGRAM_NAME    STR [0] "fm_gsm_apply_parameter.c"'||
'0 PIN_FLD_START_T      TSTAMP [0] (1019741643) Thu Apr 25 14:34:03 2002'||
'0 PIN_FLD_END_T        TSTAMP [0] (1019741643) Thu Apr 25 14:34:03 2002'||
'0 PIN_FLD_SYS_DESCR       STR [0] "Service Object Update complete"'||
'0 PIN_FLD_SERVICE_OBJ    POID [0] 0.0.0.1 /service/gsm/telephony 1436547 0'||
'0 PIN_FLD_POID           POID [0] 0.0.0.1 /event/notification/customer/reg_complete -1 0'||
'0 PIN_FLD_NAME            STR [0] "Customer Registration complete"'||
'0 PIN_FLD_USERID         POID [0] 0.0.0.1 /service/admin_client 2 90'||
'0 PIN_FLD_SESSION_OBJ    POID [0] 0.0.0.1 /event/session 207622979697632899 0'||
'0 PIN_FLD_ACCOUNT_OBJ    POID [0] 0.0.0.1 /account 1433987 0'||
'0 PIN_FLD_PROGRAM_NAME    STR [0] "cm"'||
'0 PIN_FLD_START_T      TSTAMP [0] (1019741643) Thu Apr 25 14:34:03 2002'||
'0 PIN_FLD_END_T        TSTAMP [0] (1019741643) Thu Apr 25 14:34:03 2002'||
'0 PIN_FLD_SYS_DESCR       STR [0] "Customer Registration complete"';
  --
  --
  IF (event_type = 'LONG') THEN
    event_name := long_event_name;
    flist_buf := long_flist_buf;
  ELSE 
    event_name := short_event_name;
    flist_buf := short_flist_buf;
  END IF;
  --
  begin_time := dbms_utility.get_time(); -- 100th of a second
  --
  FOR i in 1 .. max_events LOOP
    -- 
    acct_sync.enq_event(event_name, flist_buf, length(flist_buf), queue_name, mesg_id);
    --
    COMMIT;
    -- 
    dbms_output.put_line('mesg_id = '||mesg_id); 
    --
  END LOOP;
  --
  end_time := dbms_utility.get_time();
  dbms_output.put_line('elapsed time = '|| (end_time - begin_time)/100 ||'[sec]');
  dbms_output.put_line('rate = '|| max_events/((end_time - begin_time)/100) ||'[mesg/sec]');
  -- 
END;
/
show errors;









