#!/usr/bin/env perl
#
# @(#)% %
#
# Copyright (c) 2003 - 2006 Oracle. All rights reserved.
#
# This material is the confidential property of Oracle Corporation or its 
# licensors and may be used, reproduced, stored or transmitted only in 
# accordance with a valid Oracle license or sublicense agreement.
#
# Note the pin.conf file must reside either in "." or "/etc"
# The .so and .pm files are (should be) located in this directory.
#
# Usage:
#    perl load_config_gsm.pl [ <sub_session_mode>] 
#    <sub_session_mode>: where  DROP : if you don't want to rate the subsession 
#                               RATE : if you want to rate the subsession 
#                               DELETE_ASO : if you want to Delete the ASO's
#                               DELETE_RESERVATION_OBJ : if you want to Delete the Reservation Object
#                               DELETE_ASO_AND_RESERVATION_OBJ : if you want to Delete ASO & Reservation Object
#                               KEEP_ASO_AND_RESERVATION_OBJ : if you don't want to Delete ASO & Reservation Object
# ...

# pcmif is the perl pcm library
use pcmif;

# Check if arguments given at the commandline are correct
if (! ((($#ARGV == 0) && ($ARGV[0] eq "DROP"))
    || (($#ARGV == 0) && ($ARGV[0] eq "RATE"))
    || (($#ARGV == 0) && ($ARGV[0] eq "DELETE_ASO"))
    || (($#ARGV == 0) && ($ARGV[0] eq "DELETE_ASO_AND_RESERVATION_OBJ"))
    || (($#ARGV == 0) && ($ARGV[0] eq "DELETE_RESERVATION_OBJ"))
    || (($#ARGV == 0) && ($ARGV[0] eq "KEEP_ASO_AND_RESERVATION_OBJ"))
    || (($#ARGV == 1))))  {
    die "Error... $0 requires 1 command line arguments:\n",
    "perl $0 [<sub_session_mode>] \n",
    "<sub_session_mode>: is     DROP : if you don't want to rate the subsession \n",
    "                           RATE : if you want to rate the subsessions \n",
    "                           DELETE_ASO : if you want to Delete the ASO's \n",
    "                           DELETE_RESERVATION_OBJ : if you want to Delete the Reservation Object \n",
    "                           DELETE_ASO_AND_RESERVATION_OBJ : if you want to Delete ASO and Reservation Object \n",
    "                           KEEP_ASO_AND_RESERVATION_OBJ : if you don't want to Delete ASO & Reservation Object \n"; 
}

$sub_only = 0;
$updateDeleteFlag = 0;
$deleteFlag = 0;


# initialize variables based on argument
if (($#ARGV == 0) && ($ARGV[0] eq "DROP")) {

      $sub_session_mode = 1;
      $sub_only = 1;

} elsif (($#ARGV == 0) && ($ARGV[0] eq "RATE")) {

      $sub_session_mode = 2;
      $sub_only = 1;

} elsif (($#ARGV == 0) && ($ARGV[0] eq "DELETE_RESERVATION_OBJ")) {

      $updateDeleteFlag = 1;
      $deleteFlag = 1;

} elsif (($#ARGV == 0) && ($ARGV[0] eq "DELETE_ASO")) {

      $updateDeleteFlag = 1;
      $deleteFlag = 2;

} elsif (($#ARGV == 0) && ($ARGV[0] eq "DELETE_ASO_AND_RESERVATION_OBJ")) {

      $updateDeleteFlag = 1;
      $deleteFlag = 3;

} elsif (($#ARGV == 0) && ($ARGV[0] eq "KEEP_ASO_AND_RESERVATION_OBJ")) {

      $updateDeleteFlag = 1;
      $deleteFlag = 0;

}else {

      $sub_session_mode = 0;

}


## note the "pcmif::" prefix - this is a class prefix, meaning that the
## function "pcm_perl_new_ebuf()" is from the package/class "pcmif".
##
## first thing, get an ebuf for error reporting.

$ebufp = pcmif::pcm_perl_new_ebuf();

## 2nd, do a pcm_connect(), $db_no is a return.

$pcm_ctxp = pcmif::pcm_perl_connect($db_no, $ebufp);

## here is how one converts an ebuf to a printable string.

$ebp1 = pcmif::pcm_perl_ebuf_to_str($ebufp);

## check for errors - best to always do this.

if (pcmif::pcm_perl_is_err($ebufp)) {

  pcmif::pcm_perl_print_ebuf($ebufp);
  exit(1);
} else {

  print "back from pcm_connect()\n";
  print "   DEFAULT db is: $db_no \n";
}

## NOTE: the following convention ($DB_NO) was established for use with
## testnap, to substitute the database number into a printed flist as
## it was parsed into testnap.  We follow the text convention, but
## we let perl do the substitution via this variable (in upper case).
## NOTE: the flist parse should also (since it gets fed $db_no) perform
## this substitution.
# for testnap convention...

$DB_NO = $db_no;

# ALWAYS remember to put $DB_NO in your POID!
# ~~~~~~ 

# search for the gsm/config object
# build search flist

$f1 = <<"XXX"
0 PIN_FLD_POID           POID [0] $DB_NO /search -1 0
0 PIN_FLD_TEMPLATE        STR [0] "select X from /config where F1 = V1 "
0 PIN_FLD_FLAGS           INT [0] 256
0 PIN_FLD_ARGS          ARRAY [1] 
1     PIN_FLD_POID       POID [0] $DB_NO /config/aaa/gsm -1
0 PIN_FLD_RESULTS       ARRAY [0] NULL
XXX
;

$flistp = pcmif::pin_perl_str_to_flist($f1, $db_no, $ebufp);

if (pcmif::pcm_perl_is_err($ebufp)) {

  print "$0 failed while searching the /config/aaa/gsm object::\n$f1\n";
  pcmif::pcm_perl_print_ebuf($ebufp);
  exit(1);
}

#execute opcode
$out_flistp = pcmif::pcm_perl_op($pcm_ctxp, "PCM_OP_SEARCH", 
				 "", $flistp, $ebufp);

# check errors

if (pcmif::pcm_perl_is_err($ebufp)) {

  print "$0 failed while searching the /config/aaa/gsm object\n";
  pcmif::pcm_perl_print_ebuf($ebufp);
  exit(1);
}

#flist to str

$out = pcmif::pin_perl_flist_to_str($out_flistp, $ebufp);

pcmif::pin_flist_destroy($flistp);
pcmif::pin_flist_destroy($out_flistp);

# parse the result to construct following flist

@line = split("\n", $out);

# get the config object poid

@poid = split(" ", $line[3]);

$inputStrForFlist = "";

if ($updateDeleteFlag == 1){

$inputStrForFlist = <<"XXX"
0 PIN_FLD_POID           POID [0] $DB_NO /config/aaa/gsm @poid[6]
0 PIN_FLD_PROGRAM_NAME    STR [0] "$0"
0 PIN_FLD_TELCO_INFO          SUBSTRUCT [0]
1 	PIN_FLD_DELETED_FLAG               INT [0] $deleteFlag
XXX
;

}

# deal with subsession mode only

if ($sub_only == 1) {

$inputStrForFlist = <<"XXX"
0 PIN_FLD_POID           POID [0] $DB_NO /config/aaa/gsm @poid[6]
0 PIN_FLD_PROGRAM_NAME    STR [0] "$0"
0 PIN_FLD_TELCO_INFO          SUBSTRUCT [0]
1       PIN_FLD_SUBSESSION_MODE ENUM [0] $sub_session_mode
XXX
;
}

$flistp00 = pcmif::pin_perl_str_to_flist($inputStrForFlist, $db_no, $ebufp);

if (pcmif::pcm_perl_is_err($ebufp)) {

    print "$0 failed while changing Str to Flist::\n$inputStrForFlist\n";
    pcmif::pcm_perl_print_ebuf($ebufp);
    exit(1);
}
  
$out_flistp00 = pcmif::pcm_perl_op($pcm_ctxp, "PCM_OP_WRITE_FLDS", 
				   "", $flistp00, $ebufp);
  
if (pcmif::pcm_perl_is_err($ebufp)) {

    print "$0 failed while changing /config/aaa/gsm object\n";
    pcmif::pcm_perl_print_ebuf($ebufp);
    exit(1);
}

$out00 = pcmif::pin_perl_flist_to_str($out_flistp00, $ebufp);

pcmif::pin_flist_destroy($flistp00);
pcmif::pin_flist_destroy($out_flistp00);

pcmif::pcm_context_close($pcm_ctxp, 0, $ebufp);

print "\nDone. \n\n";
exit(0);
