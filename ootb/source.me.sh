TPSOURCE=/u02/brm/ebs/brm_oob/brm_app/ThirdParty
PIN_HOME=/u02/brm/ebs/brm_oob/brm_app/7.5
PIN_LOG_DIR=/u02/brm/ebs/brm_oob/brm_app/7.5/var
PIN_LOG=/u02/brm/ebs/brm_oob/brm_app/7.5/var
LIBRARYEXTENSION=.so
LIBRARYPREFIX=lib

PERL_VERSION="5.24.0"

if [ -f $TPSOURCE/source.me.sh ] 
then
. $TPSOURCE/source.me.sh
fi

if [ "$PERL5LIB" = "" ]
then
  PERL5LIB=$PIN_HOME/lib:$TPSOURCE/perl/$PERL_VERSION/lib:$TPSOURCE/perl/$PERL_VERSION/lib/$PERL_VERSION:$TPSOURCE/perl/$PERL_VERSION/lib/site_perl
else
  PERL5LIB=$PIN_HOME/lib:$TPSOURCE/perl/$PERL_VERSION/lib:$TPSOURCE/perl/$PERL_VERSION/lib/$PERL_VERSION:$TPSOURCE/perl/$PERL_VERSION/lib/site_perl:$PERL5LIB
fi
if [ "$PATH" = "" ]
then
  PATH=$PIN_HOME/lib:$PIN_HOME/bin
else
  PATH=$PIN_HOME/bin:$PIN_HOME/lib:$PATH
fi
if [ "$LD_LIBRARY_PATH" = "" ]
then
  LD_LIBRARY_PATH=$PIN_HOME/lib:$PIN_HOME/lib64
else
  LD_LIBRARY_PATH=$PIN_HOME/lib:$PIN_HOME/lib64:$LD_LIBRARY_PATH
fi
if [ "$LD_LIBRARY_PATH_64" = "" ]
then
  LD_LIBRARY_PATH_64=$PIN_HOME/lib:$PIN_HOME/lib64
else
  LD_LIBRARY_PATH_64=$PIN_HOME/lib:$PIN_HOME/lib64:$LD_LIBRARY_PATH_64
fi
if [ "$SHLIB_PATH" = "" ]
then
  SHLIB_PATH=$PIN_HOME/lib:$PIN_HOME/lib64
else
  SHLIB_PATH=$PIN_HOME/lib:$PIN_HOME/lib64:$SHLIB_PATH
fi
if [ "$LIBPATH" = "" ]
then
  LIBPATH=$TPSOURCE/perl/$PERL_VERSION/bin:$PIN_HOME/bin:$PIN_HOME/lib:$PIN_HOME/lib64
else
  LIBPATH=$TPSOURCE/perl/$PERL_VERSION/bin:$PIN_HOME/bin:$PIN_HOME/lib:$PIN_HOME/lib64:$LIBPATH
fi

if [ -f $PIN_HOME/ttsource.me.sh ]
then
. $PIN_HOME/ttsource.me.sh
fi

OS=`uname`
if [ $OS = "SunOS" ]
then
  LD_LIBRARY_PATH=/usr/sfw/lib:$LD_LIBRARY_PATH
fi
export PIN_HOME PIN_LOG_DIR PIN_LOG LIBRARYEXTENSION LIBRARYPREFIX
export PERL5LIB PATH LD_LIBRARY_PATH SHLIB_PATH LIBPATH
