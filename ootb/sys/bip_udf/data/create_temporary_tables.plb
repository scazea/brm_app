SET ECHO ON
SET FEEDBACK 1
SET NUMWIDTH 10
SET LINESIZE 80
SET TRIMSPOOL ON
SET TAB OFF
SET PAGESIZE 100
CREATE OR REPLACE
PROCEDURE create_temp_tables_proc wrapped 
a000000
367
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
7
8d7 3b0
p7GgeUjjKxq6kwN5uP6y7T2Ty0Iwg5Ur1yATfI6araqVmKL163fQaN+JHUQXgTkEN+ufSaKC
IG2i8kRWzGP5CBJhHXVn5X7aR1t8u/hzQarcUXW+27rUNmh1V24abjbMl5D2TxTeRpe9Lhum
xCbZz7RkukmfIG7Ofp9COjwrl1C9Wg+t9g1xxuvfHc3NkO560nnkYA25ZdxUVg/qzmP8Nlrj
rnHLl/bzA6Z6lgGxT8ynGna5SOdh5Ykic+bdRS+6CFSuOz80McqH0xqOa8w9Fjo4ikRsvEle
NE99yUc08hrHEwbHaGN3mEbWKiz0BbTladx98EMi0G51RnqV8R066BxHa05X/cfME4mq8Oz7
/2SgW2ZS8QAjzzDaK5Qku7g8gNtBcbeuIaKGi7JnufSYXTYRLy/QBr02HrqTeq+fYp9LVWMn
It55N24vvr+h2z634ucX51/vTb2PXoCZnLxkdIRyIj4ukABd4fzXnUOI9LzF6SuZzvcfKJPL
DbR6lAErIijT5gjnCuADk7Glwxjyibr7AS0KygoO075UPaXLvJyVOasHVa7MnlPsctixXizK
MHqPZMYqb/dDOjcZZ+nJ4PPhqPobJM/UZtcdFVX9bDmrwDzco9bcQtYOoHVAX5a0CQs5Dhz1
s2gsG6hiQuRgXR8x5YcgsYFO0k3pbOWe+FijzUtFJRczh1CnmvY46LoRSKSYDbR4whctyzJQ
WQXIce3au4Zo5hRiMEKI9GiYtVDTwTLbwlXK4Hl+2Dia7UGOM0pkMqfqmxb959JmdgWf9Smr
qvDfRQmM6hLgRZ92JzwWnOw9uOWycOb4H3b9w0mdO+s6PBPWM81eu3I4XonDUv7HtSSpspdQ
VZCsqbzV1EdZDrKLjyDhTHTcLY4My9+esRe9fwSDvlinO5SHNQk/XZZtAEJO/YEoJII=

/
