######################################################################
#
# File: pin_device_state_num
#
# Copyright (c) 2004, 2013, Oracle and/or its affiliates. All rights reserved. 
#
# This material is the confidential property of Oracle Corporation
# or its licensors and may be used, reproduced, stored or transmitted
# only in accordance with a valid Oracle license or sublicense agreement.
#
#
# Description:
# This file is used to load a configurable state-transition model
# for a particular device type.
#
# Syntax:
# state_id: state_type: string_id: string_ver: opcode_num: flags
#	next_id1: opcode_num1: flags1
#	next_id2: opcode_num2: flags2
#
# Where:
# state_id	Defined in include/pin_<device_subclass>.h file
# state_type	Defined in include/pin_device.h  (i.e. 0=raw)
# string_id	Referenced from /strings
# string_ver	Referenced from /strings
# opcode_num	The opcode to be called when the transition is made
#		into this state (For no opcode, provide a '0')
# flags		The flag value used when calling opcode_num
# next_id1	The state_id of the resulting state in the transition
# opcode_num1	The opcode to be called when the transition is made
#		from stated_id to next_id1
# flags1	The flag value used when calling opcode_num1
#
# Note:
# For this file, the string id and version values are mapped to the 
# sys/msgs/num_device_state.<locale> file(s), and need to be kept
# synchronized.
#
######################################################################

##
# The storable class to be loaded
##
/config/device_state/num

##
# The type of device for this config file
##
/device/num

##
# PIN_NUM_STATE_RAW -> PIN_NUM_STATE_NEW
# PIN_NUM_STATE_RAW -> PIN_NUM_STATE_PORT_IN_NEW
##
0:	0:	0:	1:	0:	0
	1:	0:	0
	5:	0:	0

##
# PIN_NUM_STATE_NEW -> PIN_NUM_STATE_ASSIGNED
##
1:	1:	1:	1:	0:	0
	2:	0:	0

##
# PIN_NUM_STATE_ASSIGNED -> PIN_NUM_STATE_QUARANTINED
# PIN_NUM_STATE_ASSIGNED -> PIN_NUM_STATE_QUARANTINED_PORT_OUT
#
# Note: The opcode PCM_OP_NUM_QUARANTINE is used here (2578)
##
2:	1:	2:	1:	0:	0
	3:	2578:	0
	9:	0:	0

##
# PIN_NUM_STATE_QUARANTINED -> PIN_NUM_STATE_ASSIGNED
# PIN_NUM_STATE_QUARANTINED -> PIN_NUM_STATE_UNASSIGNED
#
# Note: The opcode PCM_OP_NUM_QUARANTINE is used here (2578)
##
3:	1:	3:	1:	0:	0
	2:	2578:	0
	4:	2578:	0
	9:      0:      0

##
# PIN_NUM_STATE_UNASSIGNED -> PIN_NUM_STATE_ASSIGNED
##
4:	1:	4:	1:	0:	0
	2:	0:	0

##
# PIN_NUM_STATE_PORT_IN_NEW -> PIN_NUM_STATE_QUARANTINED_REPATRIATED
##
5:	1:	5:	1:	0:	0
	7:	0:	0
