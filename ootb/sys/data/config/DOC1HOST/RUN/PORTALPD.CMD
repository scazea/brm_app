@echo. 
@echo This command file executes DOC1/EMFE for the sample job for Adobe PDF output
@echo DOC1EMFE will run with the initialization file /u02/brm/brm_oob/brm_app/pin/sys/data/config/DOC1HOST/RESOURCE/PORTALPD.INI
@echo Output will be generated in /u02/brm/brm_oob/brm_app/pin/sys/data/config/DOC1HOST/tmp
@echo. 
doc1emfe ini=/u02/brm/brm_oob/brm_app/pin/sys/data/config/DOC1HOST/RESOURCE/PORTALPD.INI
