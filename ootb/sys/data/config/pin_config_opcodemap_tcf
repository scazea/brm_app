#
# @(#)$Id: pin_config_opcodemap_tcf /cgbubrm_7.5.0.portalbase/4 2012/05/02 05:49:03 sdoddi Exp $
#
# Copyright (c) 2004, 2012, Oracle and/or its affiliates. All rights reserved. 
#
# This material is the confidential property of Oracle Corporation or its 
# licensors and may be used, reproduced, stored or transmitted only in 
# accordance with a valid Oracle license or sublicense agreement.
#
#
# You use this file to create an opcode map for the generic telco 
# framework(TCF) for AAA operations.
#
# The TCF only abstracts the flow for AAA operations. It expects
# opcode hooks for a processing stage to be implemented to 
# compose vertical specfic flists 
# that the framework passes to the  rating/ reservation opcodes.    
#
# The TCF provides the following hooks: 
#
# PCM_OP_TELCO_AUTHORIZE: SEARCH_SESSION, PREP_INPUT, POST_PROCESS,
#                         TAG_SESSION.
#
# PCM_OP_TELCO_REAUTHORIZE: SEARCH_SESSION, PREP_INPUT, POST_PROCESS,
#                           TAG_SESSION.
#
# PCM_OP_TELCO_ACCOUNTING: SEARCH_SESSION, PREP_INPUT, POST_PROCESS,
#                          TAG_SESSION.
#
# PCM_OP_TELCO_START_ACCOUNTING: SEARCH_SESSION, PREP_INPUT, POST_PROCESS,
#                                TAG_SESSION
#
# PCM_OP_TELCO_UPDATE_ACCOUNTING: SEARCH_SESSION, PREP_INPUT, POST_PROCESS,
#                                 TAG_SESSION.
#
# PCM_OP_TELCO_STOP_ACCOUNTING: SEARCH_SESSION, PREP_INPUT, POST_PROCESS,
#                               TAG_SESSION.
#
# Hook given below is specifc to the Dropped Call.
# PCM_OP_TCF_AAA_DETECT_CONTINUATION_CALL: MATCH_CONTINUOUS_CALL.
#
# For each service type handled by the TCF, an opcode map indicating
# the opcode implementing the hook should be provided in this file.
# This will be used to create/update the opcode map of the
# /opcodemap/tcf object.
# 
# The TCF will read the /opcodemap/tcf object and based on    
# the service type it is servicing and attempt to get the opcode 
# that implements the hook for that service type.
# 
# After defining the opcode map, run the 'load_aaa_config_opcodemap_tcf'
# utility to load the opcode map into the Infranet database 
# and update the /opcodemap/tcf object.
#
# Use the following syntax for specifying values for the profiles:
#
#   <tag>:<value>
#   where:
#	<tag> can be one of the following:
#		Framework_Opcode
#		Processing_Stage
#		Opcode_Map
#
#   where:
#   Opcode_Map: <service-type>:<opcode-name>
#
# Use these rules for assigning the processing stage:
#
#	Use Processing Stages that are defined in pin_tcf.h
#
# Use these rules for specifying values:
#      You must specify the Processing_Stage/s  after the
#	Framework_Opcode.
#
# When you load the opcode map using this app, the old opcode map 
# in the /opcodemap/tcf object will be replaced by the current values.
#
#	Sample file:
# 	#comment
# 	#comment
#
#
#	Framework_Opcode: PCM_OP_TELCO_AUTHORIZE
#	Processing_Stage: SEARCH_SESSION
#	Opcode_Map:/service/telco, PCM_OP_TELCO_SEARCH_SESSION
#	Opcode_Map:/service/telco/gsm, PCM_OP_GSM_POL_SEARCH_SESSION
#	Opcode_Map:/service/telco/gsm/fax, PCM_OP_GSM_POL_SEARCH_SESSION
#
# 	# commment
# 	# comment
#
#
#	Framework_Opcode: PCM_OP_TELCO_START_ACCOUNTING
#	Processing_Stage: SEARCH_SESSION
#	Opcode_Map:/service/telco, PCM_OP_TELCO_SEARCH_SESSION
#	Opcode_Map:/service/telco/gsm, PCM_OP_TELCO_SEARCH_SESSION
#	Processing_Stage: PREP_INPUT
#	Opcode_Map:/service/telco, PCM_OP_TELCO_PREP_INPUT
#	Opcode_Map:/service/telco/gsm, PCM_OP_GSM_POL_PREP_INPUT
#	Opcode_Map:/service/telco/gsm/fax, PCM_OP_GSM_POL_PREP_INPUT
#
# Note: TCF AAA does not provide any default implementation of
#       Post process opcode.
#================================================================
# commment
Framework_Opcode: PCM_OP_TELCO_AUTHORIZE
Processing_Stage: PREP_INPUT
Opcode_Map:/service/telco, PCM_OP_TCF_AAA_AUTHORIZE_PREP_INPUT
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_GSM_AAA_POL_AUTHORIZE_PREP_INPUT
Opcode_Map:/service/telco/gsm/sms, PCM_OP_GSM_AAA_POL_AUTHORIZE_PREP_INPUT
Opcode_Map:/service/telco/gsm/data, PCM_OP_GSM_AAA_POL_AUTHORIZE_PREP_INPUT
Opcode_Map:/service/telco/gsm/fax, PCM_OP_GSM_AAA_POL_AUTHORIZE_PREP_INPUT
Opcode_Map:/service/telco/gprs, PCM_OP_GPRS_AAA_POL_AUTHORIZE_PREP_INPUT
Framework_Opcode: PCM_OP_TELCO_REAUTHORIZE
Processing_Stage: PREP_INPUT
Opcode_Map:/service/telco, PCM_OP_TCF_AAA_REAUTHORIZE_PREP_INPUT
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_GSM_AAA_POL_REAUTHORIZE_PREP_INPUT
Opcode_Map:/service/telco/gsm/sms, PCM_OP_GSM_AAA_POL_REAUTHORIZE_PREP_INPUT
Opcode_Map:/service/telco/gsm/data, PCM_OP_GSM_AAA_POL_REAUTHORIZE_PREP_INPUT
Opcode_Map:/service/telco/gsm/fax, PCM_OP_GSM_AAA_POL_REAUTHORIZE_PREP_INPUT
Opcode_Map:/service/telco/gprs, PCM_OP_GPRS_AAA_POL_REAUTHORIZE_PREP_INPUT
Framework_Opcode: PCM_OP_TELCO_CANCEL_AUTHORIZATION
Processing_Stage: PREP_INPUT
Opcode_Map:/service/telco, PCM_OP_TELCO_PREP_INPUT
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_GSM_AAA_POL_PREP_INPUT
Opcode_Map:/service/telco/gsm/sms, PCM_OP_GSM_AAA_POL_PREP_INPUT
Opcode_Map:/service/telco/gsm/data, PCM_OP_GSM_AAA_POL_PREP_INPUT
Opcode_Map:/service/telco/gsm/fax, PCM_OP_GSM_AAA_POL_PREP_INPUT
Framework_Opcode: PCM_OP_TELCO_START_ACCOUNTING
Processing_Stage: PREP_INPUT
Opcode_Map:/service/telco, PCM_OP_TELCO_PREP_INPUT
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_GSM_AAA_POL_PREP_INPUT
Opcode_Map:/service/telco/gsm/sms, PCM_OP_GSM_AAA_POL_PREP_INPUT
Opcode_Map:/service/telco/gsm/data, PCM_OP_GSM_AAA_POL_PREP_INPUT
Opcode_Map:/service/telco/gsm/fax, PCM_OP_GSM_AAA_POL_PREP_INPUT
Framework_Opcode: PCM_OP_TCF_AAA_UPDATE_ACCOUNTING
Processing_Stage: PREP_INPUT
Opcode_Map:/service/telco, PCM_OP_TCF_AAA_UPDATE_ACCOUNTING_PREP_INPUT
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_GSM_AAA_POL_UPDATE_ACCOUNTING_PREP_INPUT
Opcode_Map:/service/telco/gsm/sms, PCM_OP_GSM_AAA_POL_UPDATE_ACCOUNTING_PREP_INPUT
Opcode_Map:/service/telco/gsm/data, PCM_OP_GSM_AAA_POL_UPDATE_ACCOUNTING_PREP_INPUT
Opcode_Map:/service/telco/gsm/fax, PCM_OP_GSM_AAA_POL_UPDATE_ACCOUNTING_PREP_INPUT
Opcode_Map:/service/telco/gprs, PCM_OP_GPRS_AAA_POL_UPDATE_ACCOUNTING_PREP_INPUT
Framework_Opcode: PCM_OP_TELCO_STOP_ACCOUNTING
Processing_Stage: PREP_INPUT
Opcode_Map:/service/telco, PCM_OP_TCF_AAA_STOP_ACCOUNTING_PREP_INPUT
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_GSM_AAA_POL_STOP_ACCOUNTING_PREP_INPUT
Opcode_Map:/service/telco/gsm/sms, PCM_OP_GSM_AAA_POL_STOP_ACCOUNTING_PREP_INPUT
Opcode_Map:/service/telco/gsm/data, PCM_OP_GSM_AAA_POL_STOP_ACCOUNTING_PREP_INPUT
Opcode_Map:/service/telco/gsm/fax, PCM_OP_GSM_AAA_POL_STOP_ACCOUNTING_PREP_INPUT
Opcode_Map:/service/telco/gprs, PCM_OP_GPRS_AAA_POL_STOP_ACCOUNTING_PREP_INPUT
Framework_Opcode: PCM_OP_TELCO_AUTHORIZE
Processing_Stage: SEARCH_SESSION
Opcode_Map:/service/telco, PCM_OP_TELCO_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/sms, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/data, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/fax, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gprs, PCM_OP_GPRS_AAA_POL_SEARCH_SESSION
Framework_Opcode: PCM_OP_TELCO_REAUTHORIZE
Processing_Stage: SEARCH_SESSION
Opcode_Map:/service/telco, PCM_OP_TELCO_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/sms, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/data, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/fax, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gprs, PCM_OP_GPRS_AAA_POL_SEARCH_SESSION
Framework_Opcode: PCM_OP_TELCO_CANCEL_AUTHORIZATION
Processing_Stage: SEARCH_SESSION
Opcode_Map:/service/telco, PCM_OP_TELCO_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/sms, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/data, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/fax, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gprs, PCM_OP_GPRS_AAA_POL_SEARCH_SESSION
Framework_Opcode: PCM_OP_TELCO_START_ACCOUNTING
Processing_Stage: SEARCH_SESSION
Opcode_Map:/service/telco, PCM_OP_TELCO_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/sms, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/data, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/fax, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gprs, PCM_OP_GPRS_AAA_POL_SEARCH_SESSION
Framework_Opcode: PCM_OP_TELCO_UPDATE_ACCOUNTING
Processing_Stage: SEARCH_SESSION
Opcode_Map:/service/telco, PCM_OP_TELCO_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/sms, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/data, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/fax, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gprs, PCM_OP_GPRS_AAA_POL_SEARCH_SESSION
Framework_Opcode: PCM_OP_TELCO_STOP_ACCOUNTING
Processing_Stage: SEARCH_SESSION
Opcode_Map:/service/telco, PCM_OP_TELCO_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/sms, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/data, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gsm/fax, PCM_OP_GSM_AAA_POL_SEARCH_SESSION
Opcode_Map:/service/telco/gprs, PCM_OP_GPRS_AAA_POL_SEARCH_SESSION
Framework_Opcode: PCM_OP_TELCO_ACCOUNTING_ON
Processing_Stage: ACC_ON_OFF_SEARCH
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_GSM_AAA_POL_ACC_ON_OFF_SEARCH
Opcode_Map:/service/telco/gprs, PCM_OP_GPRS_AAA_POL_ACC_ON_OFF_SEARCH
Framework_Opcode: PCM_OP_TELCO_ACCOUNTING_OFF
Processing_Stage: ACC_ON_OFF_SEARCH
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_GSM_AAA_POL_ACC_ON_OFF_SEARCH
Opcode_Map:/service/telco/gprs, PCM_OP_GPRS_AAA_POL_ACC_ON_OFF_SEARCH
Framework_Opcode: PCM_OP_TELCO_REAUTHORIZE
Processing_Stage: POST_PROCESS
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_GSM_AAA_POL_POST_PROCESS
Opcode_Map:/service/telco/gsm/sms, PCM_OP_GSM_AAA_POL_POST_PROCESS
Opcode_Map:/service/telco/gsm/data, PCM_OP_GSM_AAA_POL_POST_PROCESS
Opcode_Map:/service/telco/gsm/fax, PCM_OP_GSM_AAA_POL_POST_PROCESS
Framework_Opcode: PCM_OP_TELCO_UPDATE_AND_REAUTHORIZE
Processing_Stage: POST_PROCESS
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_GSM_AAA_POL_POST_PROCESS
Opcode_Map:/service/telco/gsm/sms, PCM_OP_GSM_AAA_POL_POST_PROCESS
Opcode_Map:/service/telco/gsm/data, PCM_OP_GSM_AAA_POL_POST_PROCESS
Opcode_Map:/service/telco/gsm/fax, PCM_OP_GSM_AAA_POL_POST_PROCESS
#Framework_Opcode: PCM_OP_TELCO_AUTHORIZE
#Processing_Stage: TAG_SESSION
#Opcode_Map:/service/telco/gsm/telephony, PCM_OP_TCF_AAA_DETECT_CONTINUATION_CALL
#Framework_Opcode: PCM_OP_TELCO_STOP_ACCOUNTING
#Processing_Stage: TAG_SESSION
#Opcode_Map:/service/telco/gsm/telephony, PCM_OP_TCF_AAA_DETECT_CONTINUATION_CALL
#Framework_Opcode: PCM_OP_TCF_AAA_DETECT_CONTINUATION_CALL
#Processing_Stage: MATCH_CONTINUOUS_CALL
#Opcode_Map:/service/telco/gsm/telephony, PCM_OP_TCF_AAA_POL_MATCH_CONTINUATION_CALL
Framework_Opcode: PCM_OP_TCF_AAA_ACCOUNTING
Processing_Stage: SEARCH_SESSION
Opcode_Map:/service/telco, PCM_OP_TCF_AAA_SEARCH_SESSION
Framework_Opcode: PCM_OP_TCF_AAA_ACCOUNTING
Processing_Stage: PREP_INPUT
Opcode_Map:/service/telco, PCM_OP_TCF_AAA_STOP_ACCOUNTING_PREP_INPUT
Framework_Opcode: PCM_OP_TCF_AAA_ACCOUNTING
Processing_Stage: VALIDATE_LIFECYCLE
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_TCF_AAA_VALIDATE_LIFECYCLE
Framework_Opcode: PCM_OP_TELCO_AUTHORIZE
Processing_Stage: VALIDATE_LIFECYCLE
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_TCF_AAA_VALIDATE_LIFECYCLE
Framework_Opcode: PCM_OP_TCF_AAA_STOP_ACCOUNTING
Processing_Stage: VALIDATE_LIFECYCLE
Opcode_Map:/service/telco/gsm/telephony, PCM_OP_TCF_AAA_VALIDATE_LIFECYCLE
